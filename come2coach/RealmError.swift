//
//  RealmError.swift
//  come2coach
//
//  Created by Marc Ortlieb on 23.08.16.
//  Copyright © 2016 MaytecNet. All rights reserved.
//

import UIKit
import RealmSwift



enum RealmError: Error {
    case creatingInstance
    case writing(String)
}
