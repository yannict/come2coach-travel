//
// Created by Bernd Freier on 04.02.17.
// Copyright (c) 2017 MaytecNet. All rights reserved.
//

import Foundation

class EnviormentConfiguration {


    var red5Edge1 : String = "35.157.59.253"
    var red5Edge2 : String = "35.157.59.253"
    var amqpEdge1 : String = "35.157.51.44"
    var amqpEdge2 : String = "35.157.51.44"
    var red5BitRate : Int32 = 256
    var red5adaptiveBitRate : Bool = true
    var red5fps : Int32 = 15
    var red5bufferSize : Float = 1
    var red5Port : Int = 8554
    var amqpPort : Int = 80


}