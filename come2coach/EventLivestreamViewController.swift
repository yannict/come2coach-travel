//
//  EventLivestreamViewController.swift
//  come2coach
//
//  Created by Timotheus Laubengaier on 27.04.17.
//  Copyright © 2017 MaytecNet. All rights reserved.
//

import Foundation
import UIKit

class EventLivestreamViewController: UIViewController {

  let placeholderView: UIImageView = {
    let imageView = UIImageView()
    imageView.image = #imageLiteral(resourceName: "00_countdown-coach")
    imageView.contentMode = .scaleAspectFill
    return imageView
  }()
  
  let closeButton: UIButton = {
    let button = UIButton(type: .system)
    button.setImage(#imageLiteral(resourceName: "icon-close"), for: .normal)
    button.tintColor = .white
    return button
  }()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    view.addSubview(placeholderView)
    placeholderView.snp.makeConstraints { (make) in
      make.edges.equalTo(view)
    }
    
    closeButton.addTarget(self, action: #selector(closeButtonPressed), for: .touchUpInside)
    view.addSubview(closeButton)
    closeButton.snp.makeConstraints { (make) in
      make.top.equalTo(view).offset(30)
      make.trailing.equalTo(view).offset(-30)
      
    }
    
  }
  
  func closeButtonPressed() {
    dismiss(animated: true, completion: nil)
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    navigationController?.setNavigationBarHidden(true, animated: true)
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    navigationController?.setNavigationBarHidden(false, animated: true)
  }
  
}
