//
//  segmentedControlTitleType.swift
//  come2coach
//
//  Created by Marc Ortlieb on 25.08.16.
//  Copyright © 2016 MaytecNet. All rights reserved.
//

import Foundation


protocol segmentedControlTitlesType {
    var selectedSegmentControlTitle: segmentedControlTitles? {get}
    associatedtype segmentedControlTitles: RawRepresentable
}

extension segmentedControlTitlesType where segmentedControlTitles.RawValue == String {
    
//    func set(segmentedControl: UISegmentedControl, segments: [segmentedControlTitles]) {
//        segmentedControl.removeAllSegments()
//        for (index, title) in segments.enumerate() {
//            segmentedControl.insertSegmentWithTitle(title.rawValue, atIndex: index, animated: false)
//        }
//    }
    func set(_ segmentedControl: UISegmentedControl, segments: [segmentedControlTitles], selected title: segmentedControlTitles? = nil) {
        segmentedControl.removeAllSegments()
        for (index, title) in segments.enumerated() {
            segmentedControl.insertSegment(withTitle: title.rawValue, at: index, animated: false)
        }
        if let title = title {
           selectSegment(title, For: segmentedControl)
        }
    }
    
    /// if there is a segment with this title in segmentedControl returns true else false
    
    func selectSegment(_ title: segmentedControlTitles, For segmentedControl: UISegmentedControl) -> Bool {
        for index in (0..<segmentedControl.numberOfSegments) {
            if let titleForSegment = segmentedControl.titleForSegment(at: index) {
                if titleForSegment == title.rawValue {
                    segmentedControl.selectedSegmentIndex = index
                    return true
                }
            }
        }
        return false
    }
}
