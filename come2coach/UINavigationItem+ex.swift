//
//  UINavigationItem+ex.swift
//  come2coach
//
//  Created by Marc Ortlieb on 26.08.16.
//  Copyright © 2016 MaytecNet. All rights reserved.
//

import UIKit


extension UINavigationItem {
    
    func hideBackButton(_ isHidden: Bool = true, animated: Bool = false) {
        setHidesBackButton(isHidden, animated: animated)
    }
    
}
