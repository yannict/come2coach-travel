//
//  JSONDecoder.swift
//  come2coach
//
//  Created by Marc Ortlieb on 05.08.16.
//  Copyright © 2016 MaytecNet. All rights reserved.
//

import Foundation
import SwiftyJSON

class JSONDecoder {

    static func calculateDuration(_ duration: [JSON]) -> TimeInterval {
        var hours = 0
        var minutes = 0
        if let hours_ = duration.safeIndex(0)?.intValue {
            hours = hours_
        }
        if let minutes_ = duration.safeIndex(1)?.intValue {
            minutes = minutes_
        }
        let seconds = (hours * 60 * 60) + (minutes * 60)
        //print("calculateDuration - seconds: \(seconds)")
        return Double(seconds)
    }

    static func makeEvent(_ json: JSON, printJSON: Bool = true) -> Event {
        /*
        if printJSON {
            OnDebugging.print("JSONDecoder.makeEvent - json:\n \(json)")
        } */

        let e = Event()

        e.id = json["id"].intValue

        let coachJson = json["coach"]

        e.owner = coachJson["name"].stringValue
        e.ownerImage = coachJson["image"].stringValue
        e.ownerRate = coachJson["rating_points"].doubleValue
        e.coachEMail = coachJson["email"].stringValue
        e.coachFirstName = json["name"].stringValue
        e.coachLastName = json["lname"].stringValue
        e.coachId = coachJson["id"].intValue


        if (e.coachFirstName.isEmpty) {
            e.coachFirstName = coachJson["name"].stringValue
        }

        if (e.coachLastName.isEmpty) {
            e.coachLastName = coachJson["lname"].stringValue
        }

        e.userImage = json["userImage"].stringValue
        e.userAvatar = json["avatar"].stringValue

        e.recordedVideoFile = json["img3"].stringValue

        if (e.userAvatar.isEmpty) {
            e.userAvatar = coachJson["avatar"].stringValue
        }

        e.slug = json["slug"].stringValue
        e.title = json["title"].stringValue

        e.live = json["live"].intValue

        e.img1PathExtension = json["img1"].stringValue
        e.img1RemoteFolder = json["img1"].stringValue

        if (e.img1PathExtension.contains("event-")) {
            e.img1x1PathExtension = e.img1PathExtension + "/img1-1x.jpg"
            e.img1PathExtension = e.img1PathExtension + "/img1-2x.jpg"
        } else {
            e.img1x1PathExtension = e.img1PathExtension
        }

        e.img2PathExtension = json["img2"].stringValue
        e.img2RemoteFolder = json["img2"].stringValue
        e.video = json["video"].stringValue

        if (e.img2PathExtension.contains("event-")) {
            if json["video"].stringValue.isEmpty {
                e.img2PathExtension = e.img2PathExtension + "/img2-2x.jpg"
            } else {
                e.img2PathExtension = e.img2PathExtension + "/img2-video.mov"
            }
        }

        e.img3PathExtension = json["img3"].stringValue

        e.cat1 = json["cat1"].stringValue
        e.cat2 = json["cat2"].stringValue
        e.descriptionText = json["description"].stringValue
        e.text_do = json["text_do"].stringValue
        e.text_learn = json["text_learn"].stringValue
        e.isPublic = json["public"].boolValue
        e.isLiked = json["liked"].boolValue
        e.isWatching = json["isWatching"].boolValue
        e.isWebinar = json["webinar"].boolValue
        e.price = json["price"].doubleValue
        e.minParticipants = json["min_part"].intValue
        e.maxParticipants = json["max_part"].intValue
        //todo: Participants
        e.joinedParticipants = json["seminarParticipants"].intValue

        e.adult = json["adult"].intValue
        e.participants = json["participants"].intValue
        let events: [Event] =
                json["other_events"]
                        .arrayValue
                        .map {
                    JSONDecoder.makeEvent($0)
                }
        e.otherEvents = events
        e.duration = JSONDecoder.calculateDuration(json["duration"].arrayValue)

        var begin = json["begin"]
        e.begin = EventTimeStamp(begin["date"].stringValue,
                begin["timezone"].stringValue,
                begin["timezone_type"].stringValue)

        /*

        #beginchange

        begin = json["begin2"]


        e.begin2 = EventTimeStamp(
                begin["date"].stringValue,
                begin["timezone"].stringValue,
                begin["timezone_type"].stringValue)

        begin = json["begin3"]


        e.begin3 = EventTimeStamp(
                begin["date"].stringValue,
                begin["timezone"].stringValue,
                begin["timezone_type"].stringValue)
        */


        e.handout_url = json["handout_url"].stringValue
        e.handout_desc = json["handout_desc"].stringValue
        e.ext_affiliate_url = json["ext_affiliate_url"].stringValue
        e.ext_affiliate_desc = json["ext_affiliate_desc"].stringValue

        /*
        let adr = Address()
        let addressJSON = json["address"]

        adr.location = addressJSON["location"].stringValue
        adr.street = addressJSON["street"].stringValue
        adr.city = addressJSON["city"].stringValue
        adr.zip = addressJSON["zip"].intValue
        adr.latitude = addressJSON["geo_x"].doubleValue
        adr.longitude = addressJSON["geo_y"].doubleValue

        e.address = adr

        for tag in json["tags"].arrayValue {
            e.tags += tag.stringValue
        }
        */

        return e

    }

}



