//
//  TravelLocationScoutView.swift
//  come2coach
//
//  Created by Timotheus Laubengaier on 27.04.17.
//  Copyright © 2017 MaytecNet. All rights reserved.
//

import Foundation
import UIKit

protocol TravelLocationScoutViewDelegate {
  func overlayPressed()
}

class TravelLocationScoutView: UIView {
  
  var delegate: TravelLocationScoutViewDelegate?
  
  let userImageView: UIImageView = {
    let imageView = UIImageView()
    imageView.backgroundColor = Styleguide.Color.lightGray
    imageView.contentMode = .scaleAspectFill
    imageView.layer.cornerRadius = 40
    imageView.clipsToBounds = true
    imageView.layer.borderWidth = 5
    imageView.layer.borderColor = UIColor.white.withAlphaComponent(0.5).cgColor
    return imageView
  }()
  
  let scoutNameLabel: UILabel = {
    let label = UILabel()
    label.font = Styleguide.Font.light(size: 20)
    label.textColor = Styleguide.Color.text
    let fatAttributes = [NSFontAttributeName: Styleguide.Font.medium(size: 13), NSForegroundColorAttributeName: Styleguide.Color.text]
    let lightAttributes = [NSFontAttributeName: Styleguide.Font.light(size: 13), NSForegroundColorAttributeName: Styleguide.Color.text]
    let completeText = NSMutableAttributedString(string: "Cho Wang\n", attributes: fatAttributes)
    completeText.append(NSAttributedString(string: "Costa Adeje", attributes: lightAttributes))
    label.attributedText = completeText
    label.numberOfLines = 2
    label.textAlignment = .left
    return label
  }()
  
  let locationLabel: UILabel = {
    let label = UILabel()
    label.font = Styleguide.Font.light(size: 20)
    label.textColor = Styleguide.Color.text
    let fatAttributes = [NSFontAttributeName: Styleguide.Font.medium(size: 13), NSForegroundColorAttributeName: Styleguide.Color.text]
    let lightAttributes = [NSFontAttributeName: Styleguide.Font.light(size: 13), NSForegroundColorAttributeName: Styleguide.Color.text]
    let completeText = NSMutableAttributedString(string: "Calabria Food\n", attributes: fatAttributes)
    completeText.append(NSAttributedString(string: "17.06.18 | 16:30", attributes: lightAttributes))
    label.attributedText = completeText
    label.numberOfLines = 2
    label.textAlignment = .left
    return label
  }()
  
  let overlayButton: UIButton = {
    let button = UIButton(type: .custom)
    return button
  }()
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    
    backgroundColor = UIColor.white.withAlphaComponent(0.9)
    
    if let imageUrl = URL(string: "http://cdn.neonsky.com/4db89e34d3312/images/Portraits_14-1.jpg") {
      userImageView.sd_setImage(with: imageUrl)
    }
    
    addSubview(userImageView)
    userImageView.snp.makeConstraints { (make) in
      make.leading.equalTo(self).offset(12)
      make.centerY.equalTo(self)
      make.width.equalTo(80)
      make.height.equalTo(80)
    }
    
    addSubview(scoutNameLabel)
    scoutNameLabel.snp.makeConstraints { (make) in
      make.leading.equalTo(userImageView.snp.trailing).offset(20)
      make.centerY.equalTo(self)
    }
    
    addSubview(locationLabel)
    locationLabel.snp.makeConstraints { (make) in
      make.leading.equalTo(scoutNameLabel.snp.trailing).offset(20)
      make.centerY.equalTo(self)
    }
    
    overlayButton.addTarget(self, action: #selector(overlayPressed), for: .touchUpInside)
    addSubview(overlayButton)
    overlayButton.snp.makeConstraints { (make) in
      make.edges.equalTo(self)
    }
    
  }
  
  func overlayPressed() {
    delegate?.overlayPressed()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
}
