//
//  DefaultEventCell.swift
//  come2coach
//
//  Created by Victor on 31.10.16.
//  Copyright © 2016 MaytecNet. All rights reserved.
//

import Foundation

protocol DefaultEventCellDelegate {
    func defaultEventCell(_ cell: DefaultEventCell, didLikeEvent like: Bool, eventWithId eventId: Int)
}

class DefaultEventCell: UICollectionViewCell {

    @IBOutlet weak var eventImageView: UIImageView!
    @IBOutlet weak var ownerAvatarView: RoundedImageView!
    @IBOutlet weak var eventTitleLabel: UILabel!
    @IBOutlet weak var eventStateLabel: UILabel!
    @IBOutlet weak var likeButton: UIButton!
    var event: Event?
    var delegate: DefaultEventCellDelegate? = nil

    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.cornerRadius = 4.0
        self.layer.masksToBounds = false
        self.layer.shadowOffset = CGSize.init(width: 0, height: 0)
        self.layer.shadowRadius = 3
        self.layer.shadowOpacity = 0.2
        super.prepareForReuse()
    }

    func cleanInfo() {
        eventTitleLabel.text = ""
        eventImageView.image = UIImage.init(named: "placeholder")
        ownerAvatarView.image = UIImage.init(named: "avatar_placeholder")
        eventStateLabel.attributedText = NSAttributedString.init(string: "")
    }

    func fillEventInfoExt(_ event: Event, _ userImage: URL?) {
        self.event = event
        eventTitleLabel.text = event.title
        if event.img1PathExtension != "" {
            print(event.imageURL)
            eventImageView.sd_setImage(with: event.imageURL as URL!, completed: { (image, error, cacheType, url) in
                self.round(self.eventImageView, corners: [UIRectCorner.topLeft, UIRectCorner.topRight], radius: 4.0)
            })
        } else {
            round(eventImageView, corners: [UIRectCorner.topLeft, UIRectCorner.topRight], radius: 4.0)
        }
        ownerAvatarView.sd_setImage(with: userImage as URL!)
        eventStateLabel.attributedText = stateStringForEvent(event)
        var isLiked = self.event?.isLiked
        if self.event?.isWatching == true {
            isLiked = true
        }
        likeButton.setImage(isLiked == true ? UIImage.init(named: "icon_liked") : UIImage.init(named: "icon_like_empty"), for: UIControlState())
    }

    func fillEventInfo(_ event: Event) {
        self.event = event
        eventTitleLabel.text = event.title
        if event.img1PathExtension != "" {
            print(event.imageURL)
            eventImageView.sd_setImage(with: event.imageURL as URL!, completed: { (image, error, cacheType, url) in
                self.round(self.eventImageView, corners: [UIRectCorner.topLeft, UIRectCorner.topRight], radius: 4.0)
            })
        } else {
            round(eventImageView, corners: [UIRectCorner.topLeft, UIRectCorner.topRight], radius: 4.0)
        }
        if event.userImage != "" {
            ownerAvatarView.sd_setImage(with: event.userImageURL as URL!) // NSURL.init(string: event.userImage))
        } else if event.userAvatar != "" {
            ownerAvatarView.sd_setImage(with: URL.init(string: event.userAvatar))
        }
        eventStateLabel.attributedText = stateStringForEvent(event)
        var isLiked = self.event?.isLiked
        if self.event?.isWatching == true {
            isLiked = true
        }
        likeButton.setImage(isLiked == true ? UIImage.init(named: "icon_liked") : UIImage.init(named: "icon_like_empty"), for: UIControlState())
    }

    @IBAction func like() {
        var isLiked = self.event?.isLiked
        if self.event?.isWatching == true {
            isLiked = true
        }
        if let _ = delegate {
            delegate?.defaultEventCell(self, didLikeEvent: !isLiked!, eventWithId: (self.event?.id)!)
        }
    }

    fileprivate func stateStringForEvent(_ event: Event) -> NSAttributedString {

        let currentEvent = LiveStreamingHandler.getEventStatus(event)

        if let time = currentEvent.currentDate {

            let timeAttributes: NSDictionary = [NSForegroundColorAttributeName: UIColor.black]
            let timeString = NSMutableAttributedString(string: time.shortTimeString + " Uhr | ".localized, attributes: timeAttributes as? [String: AnyObject])

            //todo: hier auf live wechseln
            let stateAttributes: NSDictionary = [NSForegroundColorAttributeName: Designs.Colors.coachingTypeBlue]
            //let stateString = NSMutableAttributedString(string: event.isWebinar == true ? "Livestream" : "Videostream")
            let stateString = NSAttributedString(string: event.live == 1 ? "Livestream".localized : "Videostream".localized, attributes: stateAttributes as? [String: AnyObject])

            timeString.append(stateString)

            return timeString

        } else {

            let stateAttributes: NSDictionary = [NSForegroundColorAttributeName: Designs.Colors.coachingTypeBlue]
            return NSMutableAttributedString(string: event.live == 1 ? "Livestream".localized : "Videostream".localized)

        }

        /*
        let stateAttributes: NSDictionary = [NSForegroundColorAttributeName:Designs.Colors.coachingTypeBlue]
        //todo: test
        let stateString = NSMutableAttributedString(string: event.isWebinar == true ? "Online" : event.address.city, attributes: stateAttributes as? [String : AnyObject])
        let timeAttributes: NSDictionary = [NSForegroundColorAttributeName:Designs.Colors.coachingTimeGray]
        let timeString = NSAttributedString(string: ", " + EventDateFormatter.timeLeftToDate(event.begin.date), attributes: timeAttributes as? [String : AnyObject])
        stateString.append(timeString)
        return stateString
        */
    }

    func round(_ view: UIView, corners: UIRectCorner, radius: CGFloat) -> CAShapeLayer {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        view.layer.mask = mask
        return mask
    }

}
