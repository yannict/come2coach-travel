//
//  RoundedView.swift
//
//
//  Created by Igor Prysyazhnyuk on 12/9/16.
//  Copyright © 2017 Steelkiwi. All rights reserved.
//

import UIKit

class RoundedView: UIView {
    @IBInspectable
    var cornerRadius: CGFloat = 8
    
    override func awakeFromNib() {
        super.awakeFromNib()
        roundCorners()
    }
    
    private func roundCorners() {
        layer.cornerRadius = cornerRadius
    }
}
