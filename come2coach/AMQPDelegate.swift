//
// Created by Bernd Freier on 18.01.17.
// Copyright (c) 2017 MaytecNet. All rights reserved.
//

import Foundation

protocol AMQPDelegate {

    func process(_ type : String, _ data: String)

}