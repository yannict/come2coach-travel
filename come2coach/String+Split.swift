//
//  String+Split.swift
//  come2coach
//
//  Created by Marc Ortlieb on 31.07.16.
//  Copyright © 2016 Marc Ortlieb. All rights reserved.
//

import Foundation


// Make String conform to CollectionType:
// "w,x,y,z".split(",") // ["w", "x", "y", "z"]

extension String : Collection {}

