//
//  SubscribeVC.swift
//  come2coach
//
//  Created by Werner Kratochwil Full on 10.08.16.
//  Copyright © 2016 MaytecNet. All rights reserved.
//

import R5Streaming

class ParticipantSubscribeVC: R5VideoViewController, R5StreamDelegate {
    var stream: R5Stream?
    var statusCallback: ((_ statusCode: Int32, _ message: String?) -> Void)?
    var streamName: String?
    var wasStopped = false
    var env : EnviormentConfiguration?

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        view.backgroundColor = UIColor.black

        wasStopped = false

    }


    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        wasStopped = true

    }

    func startStreamWithName(_ myStreamName: String) {

        streamName = myStreamName
        startStream(env!)
    }

    func startStream(_ env : EnviormentConfiguration) {

        guard let currStreamName = streamName else {
            return
        }

        let config = Constants.getLiveStreamingConfig(env)

        // Set up the connection and stream
        if let connection = R5Connection(config: config) {

            self.stream = R5Stream(connection: connection)

            if (self.stream != nil) {

                self.stream!.delegate = self
                self.stream!.client = self

                self.attach(stream)

                //live:
                stream?.play(currStreamName)

            }

            //recorded:  #prerecorded
            //stream?.play("c2c482.flv")

        }

    }

    func stop() {

        wasStopped = true

        self.stream?.stop()

        self.stream?.delegate = nil
        self.stream?.client = nil

    }


    var lastMsg : String = ""
    func statusMessage(_ msg: String) {
         if (!msg.isEmpty) {
             if (msg != lastMsg) {
                 lastMsg = msg
                 DispatchQueue.main.async { [weak self] in
                     guard let self_ = self else {
                         return
                     }
                     self_.statusCallback!(0, msg)
                 }
             }
         }
    }

    func onR5StreamStatus(_ stream: R5Stream!, withStatus statusCode: Int32, withMessage msg: String!) {



        debugPrint("wasStopped = " + String(wasStopped))

        if (!wasStopped) {

            debugPrint("R5 status message " + msg)

            // #lvdebug - correct reconnect coding
            if (statusCode == Int32(r5_status_connection_error.rawValue)) {

                debugPrint("Reconnect to RED5..")

                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(2.0 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: {
                    self.startStream(self.env!)
                })

            }

            if (msg == "NetStream.Play.InSufficientBW.Video") {

                self.statusMessage("Dein Internet ist zu schwach. Du kannst jetzt den Webinar-Raum jetzt nur hören. Schalte Dich auf Video, sobald dein Internet besser ist.".localized)


            } else if (msg == "NetStream.Play.InSufficientBW.Audio") {

                self.statusMessage("Dein Internet ist zu schwach. Schalte Dich auf Audio, sobald dein Internet besser ist.".localized)


            } else {

                if self.statusCallback != nil {
                    self.statusMessage(self.participantMessageForR5Status(r5_status(UInt32(statusCode))))
                }
            }

            /* uncommented, async reconnect can cause crash - the red5 controller now does it automatically
            // #lvdebug - handle bandwidth problems
            if (msg == "NetStream.Play.InSufficientBW.Video" ||
                    msg == "NetStream.Play.InSufficientBW.Audio") {

                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 2) {

                    self.blockStatusMessage = false

                    self.statusMessage("Verbinde Dich erneut...".localized)

                    self.stop()
                    self.startStream()
                }
            }
            */

        }


    }

    func participantMessageForR5Status(_ status: r5_status) -> String {
        switch status {
                //!< A connection with the server has been established.  Streaming has *not* started yet.
        case r5_status_connected:
            return "Verbunden".localized
                //!< The connection with the server has been lost.
        case r5_status_disconnected:
            return "Verbindungsprobleme".localized
                //!< There was an error with the connection.
        case r5_status_connection_error:
            return "Noch keine Verbindung...".localized
                //!< The connection has failed due to timeout.
        case r5_status_connection_timeout:
            return "Timeout".localized
                //!< The connection is fully closed.  Wait for this before releasing assets.
        case r5_status_connection_close:
            return "Warten auf Coach...".localized
                //!< Streaming content has begun as a publisher or subscriber
        case r5_status_start_streaming:
            return "Coaching geht gleich los...".localized
                //!< Streaming content has stopped.
        case r5_status_stop_streaming:
            return "Streaming wurde beendet".localized
                //!< A netstatus event has been received from the server.
        case r5_status_netstatus:
            return "Gleich gehts weiter...".localized
        default:
            return ""
        }

    }


}
