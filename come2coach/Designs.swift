//
//  DesignGuides.swift
//  come2coach
//
//  Created by Marc Ortlieb on 04.08.16.
//  Copyright © 2016 Marc Ortlieb. All rights reserved.
//

import UIKit


struct Designs {
    
    struct Colors {
        
        static var skyBlue: UIColor {
            return UIColor(red: 0.051, green: 0.600, blue: 0.988, alpha: 1.00)
        }
        static var darkBlue: UIColor {
            return UIColor(red: 55.0/255.0, green: 191.0/255.0, blue: 255.0/255.0, alpha: 1.00)
        }
        static var lightBlue: UIColor {
            return UIColor(red: 0.0, green: 210.0/255.0, blue: 255.0/255.0, alpha: 1.00)
        }
        static var coachingTypeBlue: UIColor {
            return UIColor(red: 60.0/255.0, green: 197.0/255.0, blue: 255.0/255.0, alpha: 1.00)
        }
        static var coachingTimeGray: UIColor {
            return UIColor(red: 151.0/255.0, green: 151.0/255.0, blue: 151.0/255.0, alpha: 1.00)
        }
        static var timerGray: UIColor {
            return UIColor(red: 174.0/255.0, green: 172.0/255.0, blue: 160.0/255.0, alpha: 1.00)
        }
        
        static var timerCoachBlue: UIColor {
            return UIColor(red: 71.0/255.0, green: 187.0/255.0, blue: 255.0/255.0, alpha: 1.00)
        }

        static var blueMedium: UIColor {
            return UIColor(red: 43.0/255.0, green: 179.0/255.0, blue: 255.0/255.0, alpha: 1.00)
        }

        
        static var gray: UIColor {
            return UIColor(red: 233.0/255.0, green: 233.0/255.0, blue: 231.0/255.0, alpha: 1.00)
        }
        
        static var grayNumber: UIColor {
            return UIColor(red: 191.0/255.0, green: 201.0/255.0, blue: 204.0/255.0, alpha: 1.00)
        }
        
        static var grayForNextButton: UIColor {
            return UIColor(red: 201.0/255.0, green: 200.0/255.0, blue: 205.0/255.0, alpha: 1.00)
        }
    }
    
    struct FontVars {
        static let navigationBarTitleFont = UIFont.Montserrat.semiBold(12.5)
        static let navigationBarTitleColor = UIColor.white
        static let navigationBarTitleKern = CGFloat(1.2)
        static let coachingStateFont = UIFont.Montserrat.light(12.0)
    }
}
