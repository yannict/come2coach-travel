//
// Created by Bernd Freier on 15.02.17.
// Copyright (c) 2017 MaytecNet. All rights reserved.
//

import Foundation

class CoachingHandoutVC: UIViewController {

    @IBOutlet weak var URLtext: PaddedTextField!


    override func viewDidLoad() {

        URLtext.returnKeyType = UIReturnKeyType.default
        URLtext.text = CreateManager.sharedInstance.handoutURL

    }

    func saveAndClose() {
        if let t = URLtext.text {
            CreateManager.sharedInstance.handoutURL = t
        }

        self.navigationController?.popViewController(animated: false, completion: { [weak self] _ in
        })
    }

    @IBAction func saveButtonPressed(_ sender: Any) {

        saveAndClose()

    }

    @IBAction func backButtonPressed(_ sender: Any) {
        saveAndClose()
    }


}
