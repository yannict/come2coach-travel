//
// Created by Bernd Freier on 17.01.17.
// Copyright (c) 2017 MaytecNet. All rights reserved.
//

import Foundation
import EVReflection

class msgChat: msgBase {

    var msg: String = ""
    var user: String = ""
    var time: Date = Date()

    required override init() {
        super.init()
    }

    required convenience init?(coder: NSCoder) {
        self.init()
    }

}