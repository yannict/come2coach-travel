//
//  Sort+descriptors.swift
//  come2coach
//
//  Created by Marc Ortlieb on 23.08.16.
//  Copyright © 2016 MaytecNet. All rights reserved.
//

import Foundation

/// use as descriptor in func "sort", [3,1,5,2,4] -> [5,4,3,2,1]

func descending<T: Comparable>(_ itemsToCompare: (first:T, second: T)) -> Bool {
    return itemsToCompare.first > itemsToCompare.second
}

/// use as descriptor in func "sort", [3,1,5,2,4] -> [1,2,3,4,5]

func ascending<T: Comparable>(_ itemsToCompare: (first:T, second: T)) -> Bool {
    return itemsToCompare.first > itemsToCompare.second
}
