//
//  CollectionType.swift
//  Interstellar
//
//  Created by Marc Orlieb on 05.06.16.
//  Copyright © 2016 Marc Orlieb. All rights reserved.
//


extension Collection {
        
    /// use: questionImageObjects.find({$0.imageUUID == imageUUID})

    func find(_ predicate: (Self.Iterator.Element) throws -> Bool) rethrows -> Self.Iterator.Element? {
        return try index(where: predicate).map({self[$0]})
    }
}
