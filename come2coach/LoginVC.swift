
//  ViewController.swift
//  come2coach
//
//  Created by Marc Ortlieb on 31.07.16.
//  Copyright © 2016 Marc Ortlieb. All rights reserved.
//

import UIKit
import RealmSwift



class LoginVC: UIViewController {

    @IBOutlet weak var mailTextField:     PaddedTextField!
    @IBOutlet weak var passwordTextField: PaddedTextField!

    @IBOutlet weak var lblPasswortResetBlue: UILabel!
    @IBOutlet weak var inputContainerView: UIView!

    @IBOutlet weak var vResetPWButtonContainer: UIView!

    var mail     = ""
    var password = ""

    var firstLaunch = true

    fileprivate var inputContainerViewCenterY: CGFloat = 0


    @IBAction
    func backToLoginPreviewVCAction(_ sender: AnyObject) {
        User.loginManager.AutoLoginAllowed = false
        //LoginManagerOld.AutoLoginAllowed = false
        slideOutKeyboardIntoBottomThenShowLoginPreviewVC()
    }

    @IBAction func backButtonPressed(_ sender: Any) {
        self.show(Storyboards.ViewControllers.loginPreview)
    }
    

    // MARK: PASSWORD: reset

    @IBAction func didTouchPasswortReset(_ sender: AnyObject) {
        lblPasswortResetBlue.textColor = UIColor.white
    }

    @IBAction func didTapPasswortReset(_ sender: AnyObject) {
        formatPasswortResetLabelBlue()
        //show(Storyboards.ViewControllers.resetPassword, sender: true)
        show(Storyboards.ViewControllers.resetPassword, animated: false)
        //self.navigationController?.pushViewController(Storyboards.ViewControllers.resetPassword, animated: true)
    }


    // MARK: LOGIN
    @IBAction
    func login(_ sender: AnyObject) {

        /*  #inetmanager
        guard InternetConnectionManager.sharedInstance.isConnected == true else
        {
            self.present(self.defaultNoInternetAlertView())
            return
        }
        */

        if let mail = mailTextField.text {
            self.mail = mail
        }
        if let password = passwordTextField.text {
            self.password = password
        }


        User.loginManager.error = { (txt) in


            self.present(UIAlertController("Bitte korrigieren:".localized, txt, actions: UIAlertAction(.OK)), animated: true)

        }

        User.loginManager.showLogin = { () in

            //self.show(Storyboards.ViewControllers.login)

        }

        User.loginManager.completion = { () in


            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let window = appDelegate.window

            window?.rootViewController = UINavigationController(Storyboards.ViewControllers.mainTabBar)

        }


        User.loginManager.login(mail: mail, password: password)

        /*
        LoginManager.sharedInstance
            .loginCome2Coach(mail: mail, password: password) { (errors, updatedUser) in

                if errors.isContaining {
                    var message = ""
                    errors.forEach { message += "\($0).\n" }
                    //message.removeLast() #swift3
                    //message.removeLast() #swift3
                    message += "."

                    self.present(UIAlertController("Bitte korrigieren:".localized, message, actions: UIAlertAction(.OK)), animated: true)
                }
                // onSuccess
                else if errors.isEmpty {
                    updatedUser?.save()
                    EventManager.sharedInstance.processEventsSequence(on: self)

                }
        }
        */
    }


    @IBOutlet weak var btLogin: UIButton!


    override func viewWillAppear(_ animated: Bool) {

        super.viewWillAppear(animated)

        navigationController?.isNavigationBarHidden = true


    }


    override func viewDidLoad() {
        super.viewDidLoad()
        setupKeyboardObservers()
        self.setupTitle(self.navigationItem.title)

        vResetPWButtonContainer.isHidden = false

        /*
        #if DEBUG
            mailTextField.text = "qa14@qa.com"
            passwordTextField.text = "123456"
        #endif
        */

        formatPasswortResetLabelBlue()

        let backgroundTap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view += backgroundTap
    }


    deinit {
        NotificationCenter.default
            .removeObserver(self)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if firstLaunch {
            firstLaunch = false
            slideInKeyboardFromBottom()
        }
    }

//    override func viewWillAppear(animated: Bool) {
//        super.viewWillAppear(animated)
//        hideBackButton()
//    }

    override func willMove(toParentViewController parent: UIViewController?) {
        if let parentVC = parent
        {
            if (!parentVC.isEqual(self.parent)) {
                OnDebugging.print("Back pressed");
                slideOutKeyboardIntoBottomThenShowLoginPreviewVC()
            }
        }
    }



    fileprivate func setupKeyboardObservers() {
        NotificationCenter.default
            .addObserver(self, selector: #selector(keyboardWillShow(_:)), name:NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default
            .addObserver(self, selector: #selector(keyboardWillHide(_:)), name:NSNotification.Name.UIKeyboardWillHide, object: nil)
    }



    @objc
    func dismissKeyboard() {
        mailTextField.resignFirstResponder()
        passwordTextField.resignFirstResponder()
    }

    // MARK: [Keyboard: Animation] move inputContainerView (textFields) on Keyboard show/hide

    @IBOutlet
    fileprivate weak var inputContainerBottomAnchor: NSLayoutConstraint!

    // onViewDidAppaer
    func slideInKeyboardFromBottom() {
        let passwordForgottenButtonHeight: CGFloat = 40
        let deltaHeight: CGFloat = inputContainerView.frame.height + passwordForgottenButtonHeight + 8
        inputContainerBottomAnchor.constant -= deltaHeight
        view.layoutIfNeeded()

        UIView
            .animate(withDuration: 0.5, delay: 0, options: UIViewAnimationOptions(), animations: { [weak self] in
                self?.inputContainerBottomAnchor.constant += deltaHeight
                self?.view.layoutIfNeeded()
                }, completion: nil)
    }
    // on(X)-Button-pressed
    var tempInputContainerBottomAnchorConstant: CGFloat = 2 // handles case where didTap(X)-Button while Keyboard was present
    func slideOutKeyboardIntoBottomThenShowLoginPreviewVC() {
        let passwordForgottenButtonHeight: CGFloat = 40
        let deltaHeight: CGFloat = inputContainerView.frame.height + passwordForgottenButtonHeight + 8
        inputContainerBottomAnchor.constant = tempInputContainerBottomAnchorConstant
        dismissKeyboard()

        view.layoutIfNeeded()

        UIView
            .animate(withDuration: 0.5, delay: 0, options: UIViewAnimationOptions(), animations: { [weak self] in
                guard let self_ = self else {return}
                self_.inputContainerBottomAnchor.constant = 8-deltaHeight
                self_.view.layoutIfNeeded()
                }, completion: { _ in
                    self.show(Storyboards.ViewControllers.loginPreview)
            })
    }



    // SHOW
    func keyboardWillShow(_ notification: Notification) {
        guard let info = notification.userInfo else {
            OnDebugging
                .printError("notification.userInfo == nil",title: "LoginVC.keyboardWillShow()")
            return
        }
        let keyboardFrame: CGRect = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let slideDuration = info[UIKeyboardAnimationDurationUserInfoKey] as! Double
        let curve = info[UIKeyboardAnimationCurveUserInfoKey] as! UInt
        let options = UIViewAnimationOptions(rawValue: curve)

        let keyboardWordsSuggestionsFrameHeight: CGFloat = 48
        inputContainerBottomAnchor.constant = keyboardFrame.height - keyboardWordsSuggestionsFrameHeight
        tempInputContainerBottomAnchorConstant = keyboardFrame.height - keyboardWordsSuggestionsFrameHeight
        view.layoutIfNeeded()


        UIView
            .animate(withDuration: slideDuration, delay: 0, options: options, animations: { [weak self] in
                self?.view.layoutIfNeeded()
                }, completion: nil)
    }
    // HIDE
    func keyboardWillHide(_ notification: Notification) {
        guard let info = notification.userInfo else {
            OnDebugging
                .printError("notification.userInfo == nil",title: "LoginVC.keyboardWillHide()")
            return
        }
        let slideDuration = info[UIKeyboardAnimationDurationUserInfoKey] as! Double
        let curve = info[UIKeyboardAnimationCurveUserInfoKey] as! UInt
        let options = UIViewAnimationOptions(rawValue: curve)

        inputContainerBottomAnchor.constant = 8
        tempInputContainerBottomAnchorConstant = 8
        UIView
            .animate(withDuration: slideDuration, delay: 0, options: options, animations: { [weak self] in
                self?.view.layoutIfNeeded()
                }, completion: nil)
    }


    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == mailTextField
        {
            passwordTextField.becomeFirstResponder()
        } else if textField == passwordTextField
        {
            dismissKeyboard()
            login(passwordTextField)
        }
        return true
    }

    func formatPasswortResetLabelBlue()
    {
        lblPasswortResetBlue?.textColor = Designs.Colors.lightBlue
    }







}

extension LoginVC: UITextFieldDelegate {

    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.layoutIfNeeded()
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return true
    }


}








