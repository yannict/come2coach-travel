//
//  CoachingsTVC.swift
//  come2coach
//
//  Created by Marc Ortlieb on 08.08.16.
//  Copyright © 2016 MaytecNet. All rights reserved.
//

import Foundation

// import Async

import RealmSwift

// Facebook

import FBSDKCoreKit
import FBSDKLoginKit


class CoachingsVC: UIViewController, segmentedControlTitlesType {

    // MARK: TableView

    @IBOutlet
    weak var tableView: UITableView!

    @IBOutlet weak var btTimer: UIButton!

    // MARK: Navigation Buttons

    @IBAction func didTapTimerButton(_ sender: AnyObject) {

        let coachings: [Event]

        switch eventUserType {
        case .coach:
            coachings = ownedCoachings
        case .participant:
            coachings = upcomingBookedCoachings
        }

        if (coachings.count > 0) {
            let event = coachings[0]
            let timerTime = 0.3
            event.testTimer(timerTime)
            EventManager.sharedInstance.checkUpcomingEvents(self)
            show(UIAlertController(
                    "Timer gesetzt".localized,
                    "In \(timerTime) Minuten gesetzt".localized,
                    actions: UIAlertAction(.OK))
            )

        }

    }

    @IBAction
    func showLoginPreviewVCAction(_ sender: AnyObject) {
        //unwind()

        //if logged in with facebook, log out of facebook again
        FBSDKLoginManager().logOut()

        if let user = User.current {
            user.delete()
        }
        show(Storyboards.ViewControllers.loginPreview, sender: false)
    }

    @IBAction
    func reloadNetworkDataAction(_ sender: AnyObject) {
        // unwind() // lead to bug: #49 reload-> Login

        /*  #inetmanager
        guard InternetConnectionManager.sharedInstance.isConnected == true else {
            self.present(self.defaultNoInternetAlertView())
            return
        }
        */

        fetchData()
        EventManager.sharedInstance.checkUpcomingEvents(self)

    }


    // -----------------------------------
    // MARK: segmentedControl
    // -----------------------------------

    enum segmentedControlTitles: String {
        case alle = "ALLE"
        case eigene = "EIGENE"
        case gebucht = "GEBUCHT"
    }

    var selectedSegmentControlTitle: segmentedControlTitles?

    @IBOutlet
    weak var segmentedControl: UISegmentedControl!

    //var selectedControllTitle: segmentedControlTitles?
    @IBAction
    func didTapsegmentedControlAction(_ sender: UISegmentedControl) {
        didTapsegmentedControl(sender)
    }

    func didTapsegmentedControl(_ segmentedControl: UISegmentedControl) {
        if let titleForSegmentAtIndex = segmentedControl.titleForSegment(at: segmentedControl.selectedSegmentIndex), let tappedsegmentedControlTitle = segmentedControlTitles(rawValue: titleForSegmentAtIndex) {

            selectedSegmentControlTitle = tappedsegmentedControlTitle

            switch tappedsegmentedControlTitle {
            case .alle: break    // load all events
            case .eigene:
                eventUserType = .coach
                reloadTableView()
            case .gebucht:
                eventUserType = .participant
                reloadTableView()
            }
        }
    }
    /// set in viewDidLayoutSubviews() to override storyboard
    func setsegmentedControl(_ userType: UserType, selected: segmentedControlTitles? = nil) {
        switch userType {
        case .coach:
            if let selected = selected {
                set(segmentedControl, segments: [.eigene, .gebucht], selected: selected)
            } else {
                set(segmentedControl, segments: [.eigene, .gebucht])
            }
        case .participant:
            if let selected = selected {
                set(segmentedControl, segments: [.gebucht], selected: selected)
            } else {
                set(segmentedControl, segments: [.eigene, .gebucht])
            }
        }
    }


    var eventUserType: UserType = .participant


    // coachings die ich als Coach halten werde

    var ownedCoachings: [Event] = [] {
        //Favorite.makeMOCKFavourites()
        didSet {
            OnDebugging
                    .print("CoachingsVC ownedCoachings.didSet : \(ownedCoachings)")
                    .print("CoachingsVC ownedCoachings.count : \(ownedCoachings.count)")

        }
    }

    // Coachings die ich als Teilnehmer mitmachen werde

    var upcomingBookedCoachings: [Event] = [] {
        //Favorite.makeMOCKFavourites()
        didSet {
            OnDebugging.print("didSet upcomingBookedCoachings: \(upcomingBookedCoachings)\n")
            OnDebugging.print("upcomingBookedCoachings.count: \(upcomingBookedCoachings.count)\n")
        }
    }

    func reloadTableView() {
//        thread?.main { [weak self] in
        DispatchQueue.main.async(execute: { [weak self] in
            self?.tableView.reloadData()
        })
    }


    // MARK: ViewDidLoad

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self

        //avoid lines of empty rows
        tableView.tableFooterView = UIView()

        /*
#if DEBUG
        btTimer.isHidden = false
#endif
*/
        self.setupTitle(self.navigationItem.title)


        segmentedControl.setTitleTextAttributes([NSFontAttributeName: UIFont.Montserrat.regular(11), NSKernAttributeName: 1.1],
                for: UIControlState())

        /* #swift3
        let appearance = SMSegmentAppearance()
        appearance.segmentOnSelectionColour = UIColor(red: 245.0 / 255.0, green: 174.0 / 255.0, blue: 63.0 / 255.0, alpha: 1.0)
        appearance.segmentOffSelectionColour = UIColor.white
        appearance.titleOnSelectionFont = UIFont.systemFont(ofSize: 12.0)
        appearance.titleOffSelectionFont = UIFont.systemFont(ofSize: 12.0)
        appearance.contentVerticalMargin = 10.0
        */

        //        smSegView = SMSegmentView(frame: CGRect(x:0.0,y:0.0,width:151.0,height:20.0), dividerColour: UIColor(white: 0.95, alpha: 0.3), dividerWidth: 1.0, segmentAppearance: appearance)
        //        //smSegView.addSegmentWithTitle("Test")
        //        smSegView.selectedSegmentIndex = 0
        //        self.view.addSubview(smSegView)

        /*
        if let user = User.current {
            if user.isVerified {
                eventUserType = .coach
                selectedSegmentControlTitle = .eigene
            } else {
                eventUserType = .participant
                selectedSegmentControlTitle = .gebucht
            }
        } else {
            OnDebugging
                    .printError("User.current == nil", title: "CoachingsVC.viewDidLoad")
        }
        fetchData()
        */


    }
    // -------------------------------------------------------------------------------------
    // MARK: Handle Internet | add internet observer
    // Ticket: https://www.evernote.com/l/AWoI8QFZsxRHtKzGry5g6opO6fcwcPUR1so
    // -------------------------------------------------------------------------------------

    var InternetConnectionStateDidChangeOnlineObserverToken: String?

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

//        InternetConnectionStateDidChangeOnlineObserverToken =
//            InternetConnectionManager.sharedInstance.addObserver { (internetConnectionStatus) in
//                if internetConnectionStatus == .wifi || internetConnectionStatus == .cellular {
//                    self.fetchData()
//                }
//        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        /*   #inetmanager
        if let token = InternetConnectionStateDidChangeOnlineObserverToken {
            InternetConnectionManager.sharedInstance.removeObserver(with: token)
        }
        */
    }
    // -------------------------------------------------------------------------------------



    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        navigationController?.isNavigationBarHidden = false

        if let indexPath = self.tableView.indexPathForSelectedRow {
            self.tableView.deselectRow(at: indexPath, animated: animated)
        }

        /*   #inetmanager
        if !InternetConnectionManager.sharedInstance.isConnected {
            show(UIAlertController(
                    "Fehler beim aktualisieren der Coachingliste.".localized,
                    "Bitte stelle sicher, dass du eine Internetverbindung hast und aktualisiere dann die Coachings mit dem Reload-Button.".localized,
                    actions: UIAlertAction(.OK))
            )
        }
        */
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        guard let user = User.current else {
            OnDebugging
                    .printError("ERROR REALM - User.current = nil", title: "CoachingsVC.viewDidLayoutSubviews")
            return
        }
        OnDebugging
                .print("REALM: User.current == \(user)", title: "CoachingsVC.viewDidLayoutSubviews()")

        //segmentedControl.frame = CGRect(x:0.0,y:0.0,width:151.0,height:21.0)

        /*
        if user.isVerified {
            setsegmentedControl(.coach, selected: selectedSegmentControlTitle)
        } else {
            setsegmentedControl(.participant, selected: selectedSegmentControlTitle)
        }
        */
    }



    // MARK: Network Requests
    // var thread: Async?
    fileprivate func fetchData() {

        EventManager.sharedInstance.loadAllEvents(self, finishedLoadingHandler: { [weak self] in
            self?.ownedCoachings = EventManager.sharedInstance.coachOnlyEvents
            self?.upcomingBookedCoachings = EventManager.sharedInstance.upcomingBookedEvents
            self?.reloadTableView()
        })

//        thread = Async.userInitiated {
//            switch self.eventUserType {
//            case .coach:
//                HTTPRequest.fetchCoachOnlyEvents { [weak self] events in
//                    self?.ownedCoachings = events.filter {!$0.didEnd}
//                    OnDebugging
//                        .print("ownedCoachings: \(events)", title: "coachingsVC.fetchData() - eventUserType == .coach")
//                }
//                HTTPRequest.fetchUpcomingBookedEvents { [weak self] events in
//                    self?.upcomingBookedCoachings = events.filter {!$0.didEnd}
//                    OnDebugging
//                        .print("upcomingBookedEvents: \(events)", title: "coachingsVC.fetchData() - eventUserType == .coach")
//                }
//            case .participant:
//                HTTPRequest.fetchUpcomingBookedEvents { [weak self] events in
//                    self?.upcomingBookedCoachings = events.filter {!$0.didEnd}
//                    OnDebugging
//                        .print("upcomingBookedEvents: \(events)", title: "coachingsVC.fetchData() - eventUserType == .participant")
//                }
//            }
//        }
    }
}


// MARK: TableView - DataSource

extension CoachingsVC: UITableViewDataSource {

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {

        return nil
        //        switch section {
        //        case 0: return "IN KÜRZE"
        //        case 1: return "TEILGENOMMEN"
        //        default: return nil
        //        }
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
        //        return 40
    }

    func numberOfSections(in tableView: UITableView) -> Int {

        return 1
        /*switch userType {
         case .coach: return 1
         case .participant: return 2
         }*/
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        switch eventUserType {
        case .coach:
            return ownedCoachings.count
        case .participant:
            switch section {
            case 0: return upcomingBookedCoachings.count
                    //case 1: return pastBookedCoachings.count
            default: return 0
            }
        }
    }

    // cell
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: CoachingsTVCell = tableView.dequeuedReusableCell(For: indexPath)
        cell.indexPath = indexPath
        cell.delegate = self

        switch eventUserType {
        case .coach:
            if let ownedCoaching = ownedCoachings.safeIndex(indexPath.row) {
                cell.event = ownedCoaching
                cell.accessoryIcon.setBackgroundImage(UIImage(named: "icon_livestream-coach"), for: UIControlState())
                cell.configureCell()
                //cell.accessoryIcon.setBackgroundImage(UIImage(named: "icon_livestream-coach"), forState: .Highlighted)
            }
        case .participant:
            switch indexPath.section {
            case 0:
                if let upcomingBookedCoaching = upcomingBookedCoachings.safeIndex(indexPath.row) {
                    cell.event = upcomingBookedCoaching
                    cell.accessoryIcon.setBackgroundImage(UIImage(named: "icon_livestream-teilnehmer"), for: UIControlState())
                    cell.configureCell()
                    //cell.accessoryIcon.setBackgroundImage(UIImage(named: "icon_livestream-teilnehmer"), forState: .Highlighted)
                }

                    //            case 1:
                    //                if let pastBookedCoaching = pastBookedCoachings.safeIndex(indexPath.row) {
                    //                    cell.event = pastBookedCoaching
                    //                    cell.accessoryIcon.setBackgroundImage(UIImage(named: "starEmpty"), forState: .Normal)
                    //                    cell.accessoryIcon.setBackgroundImage(UIImage(named: "starEmpty"), forState: .Highlighted)
                    //                }
            default: break
            }
        }

        return cell
    }

}


// MARK: TableView Delegate

extension CoachingsVC: UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //        let row = indexPath.row

        switch eventUserType {
        case .coach:
            redirectDetailsWebview(For: indexPath)
                //            let livestreamingCoachVC = Storyboards.ViewControllers.livestreamingCoach
                //            livestreamingCoachVC.coachingID = ownedCoachings.safeIndex(row)?.id
                //            present(livestreamingCoachVC)

        case .participant:
            switch indexPath.section {
            case 0:
                redirectDetailsWebview(For: indexPath)
                    //                let livestreamingParticipantVC = Storyboards.ViewControllers.livestreamingParticipant
                    //                livestreamingParticipantVC.coachingID = upcomingBookedCoachings.safeIndex(row)?.id
                    //                present(livestreamingParticipantVC)
            case 1: break
            default: break
            }
        }
    }

    fileprivate func redirectDetailsWebview(For indexPath: IndexPath) {

        var coaching: Event?

        switch eventUserType {
        case .coach:
            if let ownedCoaching = ownedCoachings.safeIndex(indexPath.row) {
                coaching = ownedCoaching
            }
        case .participant:
            switch indexPath.section {
            case 0:
                if let upcomingBookedCoaching = upcomingBookedCoachings.safeIndex(indexPath.row) {
                    coaching = upcomingBookedCoaching
                }
            case 1: break
            default: break
            }
        }

        if let event = coaching {
            //let id = event.id
//            let title = event.title
            let base = "https://api.come2coach.de/event/"
            let path = ("\(event.slug)")
            let urlString = base + path
            let url = URL(string: urlString)

            if let url = url {
                let webViewController = Storyboards.ViewControllers.detailsWeb
                webViewController.url = url
                navigate(to: webViewController)
            }
        }
    }


    // MARK: Changing Font Size For UITableView Section Headers
    // http://stackoverflow.com/questions/19802336/changing-font-size-for-uitableview-section-headers

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {

        let sectionTitle: String? = self.tableView(tableView, titleForHeaderInSection: section)
        if sectionTitle == nil || sectionTitle == "" {
            return nil
        }
        let title = UILabel()
        title.text = "      \(sectionTitle)"
        title.textColor = .gray
        //title.backgroundColor = .tableViewHeaderGray
        title.font = UIFont.Montserrat.semiBold(10)

        return title
    }

    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {

        if let view = view as? UITableViewHeaderFooterView {
            view.backgroundView?.backgroundColor = UIColor.clear
            view.textLabel!.backgroundColor = UIColor.clear
            view.textLabel!.textColor = UIColor.white
            view.textLabel!.font = UIFont.boldSystemFont(ofSize: 15)
        }
    }
}


// MARK: CoachingsTVCellDelegate

extension CoachingsVC: CoachingsTVCellDelegate {

    func didPressAccessoryIcon(at indexPath: IndexPath) {

        switch eventUserType {
        case .coach:
            let livestreamingCoachVC = Storyboards.ViewControllers.livestreamingCoach

            if let coaching = ownedCoachings.safeIndex(indexPath.row) {
                livestreamingCoachVC.event = coaching
                present(livestreamingCoachVC, animated: true)
            }

        case .participant:
            switch indexPath.section {
            case 0:
                let livestreamingParticipantVC = Storyboards.ViewControllers.livestreamingParticipant
                if let coaching = upcomingBookedCoachings.safeIndex(indexPath.row) {
                    livestreamingParticipantVC.event = coaching
                    present(livestreamingParticipantVC, animated: true)
                }
                    //                redirectDetailsWebview(For: indexPath)
            case 1:
                break // can one see past events/ Coachings?
            default: break
            }
        }


        OnDebugging.print("indexPath: \(indexPath)")
    }

    // TODO: encode string in UTF8

    fileprivate func preparedPath(_ path: String) -> String {
        let a = path.replacingOccurrences(of: "&", with: "")
        let b = a.replacingOccurrences(of: "ü", with: "ue")
        let result = b.replacingOccurrences(of: " ", with: "-")
        OnDebugging.print("CoachingsVC.preparedPath: \(result)")
        return result
    }
}









