//
//  EventBookingsView.swift
//  come2coach
//
//  Created by Timotheus Laubengaier on 27.04.17.
//  Copyright © 2017 MaytecNet. All rights reserved.
//

import Foundation
import UIKit

class EventBookingsView: UIView {
  
  var events: [String] = ["test", "test2", "test3"]
  
  let titleLabel: UILabel = {
    let label = UILabel()
    label.text = "Buchungsempfehlungen"
    label.font = Styleguide.Font.light(size: 20)
    label.textColor = Styleguide.Color.text
    return label
  }()
  
  let EventCellIdentifier = "EventCell"
  let collectionView: UICollectionView = {
    let flowLayout = UICollectionViewFlowLayout()
    flowLayout.scrollDirection = .horizontal
    flowLayout.itemSize = CGSize(width: 165, height: 185)
    flowLayout.minimumLineSpacing = 15
    flowLayout.minimumInteritemSpacing = 10
    flowLayout.sectionInset = UIEdgeInsetsMake(35, 15, 10, 15)
    let collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: flowLayout)
    collectionView.backgroundColor = .clear
    collectionView.showsHorizontalScrollIndicator = false
    return collectionView
  }()
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    
    addSubview(titleLabel)
    titleLabel.snp.makeConstraints { (make) in
      make.top.equalTo(self).offset(20)
      make.leading.equalTo(self).offset(15)
    }
    
    collectionView.register(EventCell.self, forCellWithReuseIdentifier: EventCellIdentifier)
    collectionView.dataSource = self
    collectionView.delegate = self
    addSubview(collectionView)
    collectionView.snp.makeConstraints { (make) in
      make.edges.equalTo(self)
    }
    
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
}

extension EventBookingsView: UICollectionViewDataSource {
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return events.count
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: EventCellIdentifier, for: indexPath) as! EventCell
    cell.likeButton.isHidden = true
    cell.isSmallCard = true
    if let imageUrl = URL(string: Styleguide.Image.randomImagePath()) {
      cell.imageView.sd_setImage(with: imageUrl)
    }
    return cell
  }
  
}

extension EventBookingsView: UICollectionViewDelegate {
  
}
