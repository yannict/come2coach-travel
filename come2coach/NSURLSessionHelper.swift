//
//  HTTPSHelper.swift
//  DemoRequest
//
//  Created by Marc Orlieb on 29.07.16.
//  Copyright © 2016 Marc Orlieb. All rights reserved.
//
// ISSUES
// NSURLSession/NSURLConnection HTTP load failed (kCFStreamErrorDomainSSL, -9843)
// ERROR Description: http://stackoverflow.com/questions/25335168/synchronous-https-post-request-ios
// see: https://medium.com/swift-programming/learn-nsurlsession-using-swift-ebd80205f87c#.r01fet5rp


import Foundation

class NSURLSessionHelper: NSObject, URLSessionDelegate, URLSessionTaskDelegate {
        
    var callback: (_ result: String, _ error: String?) -> Void = { (resultString, error) in
        if error == nil {
            OnDebugging.print("result: \(resultString)")
        } else {
            OnDebugging.print("error: \(error)")
        }
    }
    
    func httpGet(_ request: URLRequest!, callback: @escaping (String,
        String?) -> Void) {
        let configuration =
            URLSessionConfiguration.default
        let session = Foundation.URLSession(configuration: configuration,
                                   delegate: self,
                                   delegateQueue:OperationQueue.main)
        
        let task = session.dataTask(with: request, completionHandler: { (data, response, error) in
            if error != nil {
                callback("", error!.localizedDescription)
            } else {
                if let result = NSString(data: data!, encoding: String.Encoding.ascii.rawValue) as? String {
                    callback(result, nil)
                } else {
                    callback("", "ERROR: NSURLSessionHelper.httpGet - did fail downcasting to string")
                }
                
            }
        }) 
        task.resume()
    }
    
    
    func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        if let trust = challenge.protectionSpace.serverTrust {
            completionHandler(.useCredential, URLCredential(trust: trust))
        } else {
            completionHandler(.useCredential, nil)
        }
        
    }
    func urlSession(_ session: URLSession, task: URLSessionTask, willPerformHTTPRedirection response: HTTPURLResponse, newRequest request: URLRequest, completionHandler: @escaping (URLRequest?) -> Void) {
        let newRequest: URLRequest? = request  ;OnDebugging.print(newRequest?.description)
        completionHandler(newRequest)
    }
    
}


