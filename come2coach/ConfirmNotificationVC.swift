//
//  ConfirmNotificationVC.swift
//  come2coach
//

import Foundation

class ConfirmNotificationVC: UIViewController {

    func loginHandler(_ errors: [String], user:User?) {

        if errors.isContaining {

            self.present(UIAlertController("Opps!", "Es gab leider ein Problem bei der Anmeldung. Bitte probiere es selbst.".localized, actions: UIAlertAction(.OK)),
                    animated: true)
            self.show(Storyboards.ViewControllers.login)

        } else {

            guard let user = user else {
                self.show(Storyboards.ViewControllers.loginPreview)
                return
            }
            user.save()

            let coachingsVC = Storyboards.ViewControllers.mainTabBar
            self.show(coachingsVC)
        }

    }

    @IBAction func ContinueBtnPressed(_ sender: AnyObject) {


        User.loginManager.AutoLoginAllowed = false


        User.loginManager.completion = { () in


            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let window = appDelegate.window

            window?.rootViewController = UINavigationController(Storyboards.ViewControllers.mainTabBar)

        }

        User.loginManager.error = { (txt) in


            self.present(UIAlertController("Bitte korrigieren:".localized, txt, actions: UIAlertAction(.OK)), animated: true)

        }

        User.loginManager.showLogin = { () in

            self.show(Storyboards.ViewControllers.login)

        }

        User.loginManager.login(mail: RegistrationVC.user!.email, password: RegistrationVC.user!.password)




        /*

        HTTPRequest.getUser { (user, errors) in

            user?.save()

            LoginManagerOld.sharedInstance.loginCome2Coach(User.current!, handler: self.loginHandler)



        }

*/
        /*

        // login user
        LoginManager.sharedInstance
                .loginCome2Coach(mail: RegistrationVC.user!.email, password: RegistrationVC.user!.password) { (errors, updatedUser) in

            if errors.isContaining {

                self.present(UIAlertController("Opps!", "Es gab leider ein Problem bei der Anmeldung. Bitte probiere es selbst.".localized, actions: UIAlertAction(.OK)),
                        animated: true)
                self.show(Storyboards.ViewControllers.login)
            }
            // onSuccess
            else if errors.isEmpty {


                HTTPRequest.getUser { (user, errors) in


                    user?.save()

                    LoginManager.sharedInstance.loginCome2Coach(User.current!, handler: loginHandler)



                }

                updatedUser?.save()
                EventManager.sharedInstance.processEventsSequence(on: self)

            }
        }
        */

    }


}
