//
//  DetailsWebVC.swift
//  come2coach
//
//  Created by Marc Ortlieb on 12.08.16.
//  Copyright © 2016 MaytecNet. All rights reserved.
//

//    let kochen = "https://api.come2coach.de/event/224-kochen-mit-irmgard"
//    let coding = "https://api.come2coach.de/event/225-ios-programming-fuer-einsteiger"
//    let nlp = "https://api.come2coach.de/event/223-nlp-und-coaching"



import UIKit

class DetailsWebVC: UIViewController {

    var url: URL? {
        didSet {
            OnDebugging.print("DetailsWebVC.url - didSet: \(url)")
        }
    }
    
    override func viewDidLoad() { OnDebugging.print("DetailsWebVC.viewDidLoad()")
        super.viewDidLoad()
        loadRequest()
    }
    
    
    @IBOutlet
    weak var webview: UIWebView!
    
    func loadRequest() {
        if let url = url {
            let requestObj = URLRequest(url: url)
            webview.scalesPageToFit = true
            webview.loadRequest(requestObj)
        } else {
            OnDebugging
                .printError("ERROR: Webview - url = nil", title: "DetailsWebVC.loadRequest()")
        }
    }
}


