//
//  AppMode.swift
//  come2coach
//
//  Created by Marc Ortlieb on 17.08.16.
//  Copyright © 2016 MaytecNet. All rights reserved.
//

import Foundation

/**
 handle if let / guard statements according to AppMode
 e.g. only show print statements while debugging for better performance when app is released in Store
*/
private var isDebugginModeEnabled = false


struct OnDebugging {
    
    static var isEnabled:Bool {
        return isDebugginModeEnabled
    }
    
    static func enableDebugginMode() {
        isDebugginModeEnabled = true
    }
    
    
    
    /// only show print statements while debugging for better performance when app is released in Store
    
    static func printError(_ message: String, title: String = "ERROR-DEBUGGING:") {
        if isDebugginModeEnabled {
            printErrorOnDebugging("ERROR: \(title.uppercased()) \(message)\n")
        }
    }
    static func print(_ any: Any, title: String = "DEBUGGING:") -> OnDebugging {
        if isDebugginModeEnabled {
            printOnDebugging("\(title.uppercased()) \(any)\n")
        }
        return OnDebugging()
    }
    /**
     used to concatenate several print() with dot notation:
     
     example:
     
     OnDebugging
     
     .print("CoachingsVC ownedCoachings.didSet : \(ownedCoachings)")
     
     .print("CoachingsVC ownedCoachings.count : \(ownedCoachings.count)")
 */
    func print(_ any: Any, title: String = "DEBUGGING:") -> OnDebugging {
        if isDebugginModeEnabled {
            printOnDebugging("\(title.uppercased()) \(any)\n")
        }
        return self
    }
    func printError(_ any: Any, title: String = "ERROR-DEBUGGING:") {
        if isDebugginModeEnabled {
            printErrorOnDebugging("\(title) \(any)\n")
        }
    }
    
    
    static func fatalError(_ message: String = "") {
        if isDebugginModeEnabled {
            stopSystemOnFatalError(message + "\n")
        }
    }
    
    
    func perform(_ block: ()->Void) {
        if isDebugginModeEnabled {
            block()
        }
    }
    static func perform(_ block: (OnDebugging)->Void) {
        if isDebugginModeEnabled {
            block(OnDebugging())
        }
    }
    
}



private func stopSystemOnFatalError(_ message: String) {
    fatalError(message)
}
private func printOnDebugging(_ message: String) {
    print(message)
}
private func printErrorOnDebugging(_ message: String) {
    print(message)
}
