//
//  Realm+ex.swift
//  come2coach
//
//  Created by Marc Ortlieb on 20.08.16.
//  Copyright © 2016 MaytecNet. All rights reserved.
//

import UIKit
import RealmSwift


extension NSObject: Realmed {}


protocol Realmed {}

extension Realmed {
    
    var defaultRealm: RealmResult<Realm> {
        if let realm = try? Realm() {
            return RealmResult.success(realm)
        } else {
            return RealmResult.Error(RealmError.creatingInstance)
        }
    }
    

    
}
