//
// Created by Bernd Freier on 20.02.17.
// Copyright (c) 2017 MaytecNet. All rights reserved.
//

import Foundation

protocol SharePopupDelegate {
    func shareEventAction()
}

class SharePopup: UIView {

    var delegate : SharePopupDelegate?


    @IBAction func btnShare(_ sender: Any) {

        self.removeFromSuperview()

        delegate?.shareEventAction()
    }

    @IBAction func hidePopup(_ sender: Any) {

        self.removeFromSuperview()

    }

    func showPopover() {

        let path = UIBezierPath.init(rect: self.bounds)
        let fillLayer = CAShapeLayer.init()
        fillLayer.path = path.cgPath
        fillLayer.fillRule = kCAFillRuleEvenOdd
        fillLayer.fillColor = UIColor.black.cgColor
        fillLayer.opacity = 0.5
        self.layer.insertSublayer(fillLayer, at: 0)

        self.layoutSubviews()


    }

}
