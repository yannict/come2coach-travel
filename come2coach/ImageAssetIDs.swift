//
//  UIImage+AssetID.swift
//  come2coach
//
//  Created by Marc Ortlieb on 02.08.16.
//  Copyright © 2016 Marc Ortlieb. All rights reserved.
//

import Foundation

extension UIImage {
    
    /** List of all image asset identifiers
        used to load images programmatically, without typos
        
        Usecase: let image = UIImage(assetIdentifier: .backgroundImage)
    */
    enum AssetID: String {
        case loginBackground // represent name of image asset, of rawValue-type String
    }
    
    convenience init!(assetIdentifier: AssetID) {
        self.init(named: assetIdentifier.rawValue)
    }
}





