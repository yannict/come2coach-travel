//
// Created by Bernd Freier on 26.01.17.
// Copyright (c) 2017 MaytecNet. All rights reserved.
//

import Foundation
import BraintreeDropIn
import Braintree
import Alamofire
import SVProgressHUD

class PaymentHandler  /* BTAppSwitchDelegate, BTViewControllerPresentingDelegate */ {


    let apiKey = "sandbox_7g4njcw3_xr7snwv5pq5cxpz2"

    var owner: UIViewController?
    var participantCount: Int?
    var appointment: String?
    var event: Event?

    func showDropIn(_ ctrl: UIViewController, handler: @escaping (_ success : Bool) -> Void) {


        // Set the theme before initializing Drop-In
        //BTUIKAppearance.darkTheme()

        let request = BTDropInRequest()

        let dropIn = BTDropInController(authorization: apiKey, request: request) { (controller, result, error) in

            if (error != nil) {

                self.owner!.present(UIAlertController("Hast Du Internet?".localized, "Bei der Auswahl der Zahlungsweise ist leider ein Fehler aufgetreten. Versuche es bitte nochmal.".localized,
                        actions: UIAlertAction(.OK)), animated: true)

                handler(false)


            } else if (result?.isCancelled == true) {

                handler(false)


            } else if let result = result {

                if let e = self.event {

                    SVProgressHUD.show()

                    let amount = e.price * Double(self.participantCount!)

                    HTTPRequest.postPaymentNonce(
                            result.paymentMethod?.nonce,
                            e.id,
                            self.appointment,
                            self.participantCount,
                            amount,
                            handler: { (success) in

                                SVProgressHUD.dismiss()

                                handler(success!)

                            }
                    )
                }


                // Use the BTDropInResult properties to update your UI
                // result.paymentOptionType
                // result.paymentMethod
                // result.paymentIcon
                // result.paymentDescription
            }

            controller.dismiss(animated: true, completion: nil)
        }
        ctrl.present(dropIn!, animated: true, completion: nil)
    }


}