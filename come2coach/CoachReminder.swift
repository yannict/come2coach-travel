//
// Created by Bernd Freier on 23.02.17.
// Copyright (c) 2017 MaytecNet. All rights reserved.
//

import Foundation

class CoachReminder   {

    static var active = false
    static var eventId:Int?

    static var remindMe: (() -> Void)!

}

class ProtocolEventRequest {

    static var eventId : Int?
    static var showEvent: ((_ eventId : Int) -> Void)!

}