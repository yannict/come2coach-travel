//
//  ViewControllerManager.swift
//  come2coach
//
//  Created by Marc Ortlieb on 09.08.16.
//  Copyright © 2016 MaytecNet. All rights reserved.
//

import Foundation

// ManagedViewControllers are preferably picked instances of current ViewControllerStack, if not existing in Stack they will be instanciated from Storyboard

struct ManagedViewControllers {
    // Login & Registration
    static var initialStart: InitialStartVC {
        return ViewControllerManager.instanciateVC()
    }
    static var loginPreview: LoginPreviewVC {
        return ViewControllerManager.instanciateVC()
    }
    static var login: LoginVC {
        return ViewControllerManager.instanciateVC()
    }
//    static var registration: RegistrationVC {
//        return ViewControllerManager.instanciateVC()
//    }
//    static var resetPassword: ResetPasswordVC {
//        return ViewControllerManager.instanciateVC()
//    }
    // Coachings
    static var coachings: CoachingsVC {
        return ViewControllerManager.instanciateVC()
    }
    static var detailsWeb: DetailsWebVC {
        return ViewControllerManager.instanciateVC()
    }
    // Livestreaming
    static var countDown: CountDownVC {
        return ViewControllerManager.instanciateVC()
    }
    static var coachPublish: CoachPublishVC {
        return ViewControllerManager.instanciateVC()
    }
    static var participantSubsribe: ParticipantSubscribeVC {
        return ViewControllerManager.instanciateVC()
    }
    
    static var livestreamingCoach: LivestreamingCoachVC {
        return ViewControllerManager.instanciateVC()
    }
    static var livestreamingParticipant: LivestreamingParticipantVC {
        return ViewControllerManager.instanciateVC()
    }
}



class ViewControllerManager: NSObject {
    
    static let sharedInstance = ViewControllerManager()
    fileprivate override init() {}
    
    /// call in AppDelegate 
    func instantiate() {}
    
    lazy var navigationController = {
        UINavigationController(Storyboards.ViewControllers.initialStart)
    }()
    var viewControllers: [UIViewController] {
        return navigationController.viewControllers
    }
    
    
    /// get TopMostViewController
    
    func topViewController(base: UIViewController? = (UIApplication.shared.delegate as! AppDelegate).window?.rootViewController) -> UIViewController {
        
        if let nav = base as? UINavigationController {
            return topViewController(base: nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(base: selected)
            }
        }
        if let presented = base?.presentedViewController {
            return topViewController(base: presented)
        }
        
        if let base = base {
            return base
        } else {
            let rootViewController = UINavigationController(Storyboards.ViewControllers.loginPreview)
            
            if (UIApplication.shared.delegate as! AppDelegate).window != nil {
                (UIApplication.shared.delegate as! AppDelegate).window!.rootViewController = rootViewController
                return rootViewController
            } else {
                (UIApplication.shared.delegate as! AppDelegate).window = UIWindow(frame: UIScreen.main.bounds)
                (UIApplication.shared.delegate as! AppDelegate).window?.rootViewController = rootViewController
                return rootViewController
            }
        }
    }
    
    /* 
     try to get viewController from current VC-Stack, else instanciate new one from Storyboard
     every viewcontroller must be listed in "Storyboards.ViewControllers.all" !
     
     example usage: ManagedViewControllers
     --------------------------------------------
     static var loginPreview: LoginPreviewVC {
     return ViewControllerManager.instanciateVC()
     }
     static var registration: RegistrationVC {
     return ViewControllerManager.instanciateVC()
     }
 
     */
    
    static func instanciateVC<T>() -> T {
        let filtered = ViewControllerManager.sharedInstance.viewControllers.filter { $0 is T }
        if let f = filtered.first {
            return f as! T
        }
        let vc = ViewControllerManager.sharedInstance.storyboardVCs.filter{$0 is T}
        if let vc = vc.first as? T {
            return vc
        } else {
            fatalError("there is no ViewController \(T.self) in .storyboardVCs \n" +
                "-> update .storyboardVCs with instance of \(T.self)")
        }
    }
    lazy fileprivate var storyboardVCs = {
        return Storyboards.ViewControllers.all
    }()
}
