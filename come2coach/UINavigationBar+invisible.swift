//
//  UINavigationBar+invisible.swift
//  come2coach
//
//  Created by Marc Ortlieb on 08.08.16.
//  Copyright © 2016 MaytecNet. All rights reserved.
//
// http://stackoverflow.com/questions/25845855/transparent-navigation-bar-ios

import Foundation


extension UINavigationBar {
    
    static func systemWideSetup() {
        setTransparent()
        setupFont()
        setupBarButtonItems()
    }
    
    /// set in AppDelegate for systemWide use
    
    fileprivate static func setTransparent() {
        
        // Override point for customization after application launch.
        // Sets background to a blank/empty image
        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .default)
        // Sets shadow (line below the bar) to a blank image
        UINavigationBar.appearance().shadowImage = UIImage()
        // Sets the translucent background color
        UINavigationBar.appearance().backgroundColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.0)
        // Set translucent. (Default value is already true, so this can be removed if desired.)
        UINavigationBar.appearance().isTranslucent = true
    }
    
    fileprivate static func setupFont() {
        // print all fonts with:  UIFont.printAllFonts()
        UINavigationBar.appearance().tintColor = .black
        UINavigationBar.appearance().titleTextAttributes = [NSFontAttributeName: UIFont.Montserrat.semiBold(13)]
    }
    
    fileprivate static func setupBarButtonItems() {
        
        UINavigationBar.appearance().tintColor = .white
        UINavigationBar.appearance().titleTextAttributes = [NSFontAttributeName: UIFont.Montserrat.semiBold(13), NSForegroundColorAttributeName: UIColor.white]
        UIBarButtonItem.appearance().setTitleTextAttributes([NSFontAttributeName: UIFont.Montserrat.semiBold(13), NSForegroundColorAttributeName: UIColor.white], for: UIControlState())
    }
}
