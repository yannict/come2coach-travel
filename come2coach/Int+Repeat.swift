//
//  Int+Repeat.swift
//  come2coach
//
//  Created by Marc Ortlieb on 08.08.16.
//  Copyright © 2016 MaytecNet. All rights reserved.
//

import Foundation


extension Int {
    
    func Repeat(_ block: (Int) -> Void) {
        for i in 0..<self {
            block(i)
        }
    }
    func Repeat(_ block: (Void) -> Void) {
        for _ in 0..<self {
            block()
        }
    }
    
    static let formatterWithSeparator: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.groupingSeparator = "."
        formatter.numberStyle = .decimal
        return formatter
    }()
    
    var stringFormattedWithSeparator: String {
        return Int.formatterWithSeparator.string(from: self as NSNumber) ?? ""
    }
}
