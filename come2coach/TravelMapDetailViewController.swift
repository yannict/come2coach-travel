//
//  TravelDetailViewController.swift
//  come2coach
//
//  Created by Timotheus Laubengaier on 25.04.17.
//  Copyright © 2017 MaytecNet. All rights reserved.
//

import Foundation
import UIKit

class TravelMapDetailViewController: UIViewController {
  
  var event: Event? {
    didSet {
      if let event = event {
        scoutView.nameLabel.text = event.tmpUser?.name
      }
    }
  }
  
  let scrollView: UIScrollView = {
    let scrollView = UIScrollView()
    scrollView.contentSize = UIScreen.main.bounds.size
    //    scrollView.delaysContentTouches = false
    //    scrollView.isExclusiveTouch = true
    //    scrollView.canCancelContentTouches = true
    scrollView.showsVerticalScrollIndicator = false
    scrollView.backgroundColor = .clear
    scrollView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
    return scrollView
  }()
  
  let contentView: UIView = {
    let view = UIView()
    view.backgroundColor = .clear
    return view
  }()
  
  let titleLabel: UILabel = {
    let label = UILabel()
    label.font = Styleguide.Font.light(size: 20)
    label.textColor = Styleguide.Color.text
    label.text = "Hidden Beach in Costa Rica"
    label.numberOfLines = 3
    label.textAlignment = .center
    return label
  }()
  
  let locationImageView: UIImageView = {
    let imageView = UIImageView()
    imageView.contentMode = .scaleAspectFill
    imageView.backgroundColor = Styleguide.Color.lightGray
    imageView.clipsToBounds = true
    return imageView
  }()
  
  let scoutView: EventCoachView = {
    let view = EventCoachView()    
    return view
  }()
  
  let travelDetailView: EventIconInfoView = {
    let view = EventIconInfoView()    
    return view
  }()
  
  let participantsView: UIView = {
    let view = UIView()
    view.backgroundColor = Styleguide.Color.lightGray
    view.layer.borderWidth = 1
    return view
  }()
  
  let locationDetailView: EventDescriptionView = {
    let view = EventDescriptionView()    
    return view
  }()
  
  let bookingRecommendationView: EventBookingsView = {
    let view = EventBookingsView()
    view.backgroundColor = Styleguide.Color.tint
    return view
  }()
  
  let coachAdditionalView: EventCoachBookingsView = {
    let view = EventCoachBookingsView()    
    return view
  }()
  
  let moreInfoButton = Styleguide.Button.standard(title: "Mehr Infos", bottomLine: true, topLine: true)
  let bookButton = Styleguide.Button.standard(title: "Scouting buchen")
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    view.backgroundColor = .white
    
    let backButton = UIBarButtonItem(image: #imageLiteral(resourceName: "icon-close"), style: .plain, target: self, action: #selector(dismissPressed))
    backButton.tintColor = Styleguide.Color.text
    navigationItem.setLeftBarButton(backButton, animated: true)
    
    navigationItem.title = "SCOUTING"
    navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: Styleguide.Color.text, NSFontAttributeName: Styleguide.Font.medium(size: 13)!]
    
    
    initUI()
    
    if let event = event {
      titleLabel.text = event.title
      if let testImageUrl = URL(string: event.tmpImageUrl) {
        locationImageView.sd_setImage(with: testImageUrl)
      }
      if let userTestImageUrl = URL(string: event.tmpUserImageUrl) {
        scoutView.coachImageButton.sd_setImage(with: userTestImageUrl, for: .normal)
      }
      if let testUrl = URL(string: "https://www.costarica.com/contentAsset/image/f989844e-960c-4cca-b9c2-87638dc2d18c/fileAsset/filter/Resize,Jpeg/resize_w/1000/Jpeg_q/.8") {
        locationDetailView.locationImageView.sd_setImage(with: testUrl)
      }
    }
    
  }
  
  func initUI() {
    
    // debug
//    scrollView.backgroundColor = .red
//    contentView.backgroundColor = .green
    
    view.addSubview(scrollView)
    scrollView.snp.makeConstraints { (make) in
      make.top.equalTo(view).offset(64)
      make.leading.equalTo(view)
      make.trailing.equalTo(view)
      make.bottom.equalTo(view).offset(-90)
    }
    
    scrollView.addSubview(contentView)
    contentView.snp.makeConstraints { (make) in
      make.top.equalTo(scrollView)
      make.leading.equalTo(scrollView)
      make.trailing.equalTo(scrollView)
      make.bottom.equalTo(scrollView)
      make.width.equalTo(scrollView.snp.width).offset(0) // subtract padding here
    }
    
    contentView.addSubview(titleLabel)
    titleLabel.snp.makeConstraints { (make) in
      make.top.equalTo(contentView).offset(0)
      make.leading.equalTo(contentView).offset(80)
      make.trailing.equalTo(contentView).offset(-80)
    }
    
    contentView.addSubview(locationImageView)
    locationImageView.snp.makeConstraints { (make) in
      make.top.equalTo(titleLabel.snp.bottom).offset(66)
      make.leading.equalTo(contentView)
      make.trailing.equalTo(contentView)
      make.height.equalTo(218)
    }
    
    contentView.addSubview(scoutView)
    // FIXME: replace with protocol
    scoutView.coachImageButton.addTarget(self, action: #selector(userProfilePressed), for: .touchUpInside)
    scoutView.snp.makeConstraints { (make) in
      make.top.equalTo(locationImageView.snp.bottom)
      make.leading.equalTo(contentView)
      make.trailing.equalTo(contentView)
      make.height.equalTo(110)
    }
    
    // remove participant view
    contentView.addSubview(travelDetailView)
    travelDetailView.snp.makeConstraints { (make) in
      make.top.equalTo(scoutView.snp.bottom)
      make.leading.equalTo(contentView)
      make.trailing.equalTo(contentView)
      make.height.equalTo(100)
    }
    
    contentView.addSubview(locationDetailView)
    locationDetailView.snp.makeConstraints { (make) in
      make.top.equalTo(travelDetailView.snp.bottom)
      make.leading.equalTo(contentView)
      make.trailing.equalTo(contentView)
//      make.height.equalTo(300)
    }
    
    contentView.addSubview(bookingRecommendationView)
    bookingRecommendationView.snp.makeConstraints { (make) in
      make.top.equalTo(locationDetailView.snp.bottom)
      make.leading.equalTo(contentView)
      make.trailing.equalTo(contentView)
      make.height.equalTo(282)
    }
    
    contentView.addSubview(coachAdditionalView)
    coachAdditionalView.snp.makeConstraints { (make) in
      make.top.equalTo(bookingRecommendationView.snp.bottom)
      make.leading.equalTo(contentView)
      make.trailing.equalTo(contentView)
      make.height.equalTo(282)
      make.bottom.equalTo(contentView)
    }
    
    // Booking Button
    view.addSubview(bookButton)
    bookButton.addTarget(self, action: #selector(bookButtonPressed), for: .touchUpInside)
    bookButton.snp.makeConstraints { (make) in
      make.bottom.equalTo(view)
      make.leading.equalTo(view)
      make.trailing.equalTo(view)
      make.height.equalTo(45)
    }
    
    view.addSubview(moreInfoButton)
    moreInfoButton.addTarget(self, action: #selector(moreInfoButtonPressed), for: .touchUpInside)
    moreInfoButton.snp.makeConstraints { (make) in
      make.bottom.equalTo(bookButton.snp.top)
      make.leading.equalTo(view)
      make.trailing.equalTo(view)
      make.height.equalTo(45)
    }
    
  }
  
  func userProfilePressed() {
    let vc = ProfileViewController()    
    vc.user = event?.tmpUser
    navigationController?.push(vc)
  }
  
  func bookButtonPressed() {
    let vc = EventLivestreamViewController()
    present(vc, animated: true, completion: nil)
  }
  
  func moreInfoButtonPressed() {
    if let url = URL(string: "https://www.lonelyplanet.com/costa-rica") {
      UIApplication.shared.openURL(url)
    }
  }
  
  func dismissPressed() {
    dismiss(animated: true, completion: nil)
  }
  
}
