//
//  TableViewExtension.swift
//  
//
//  Created by Igor Prysyazhnyuk on 1/17/17.
//  Copyright © 2017 Steelkiwi. All rights reserved.
//

import UIKit

extension UITableView {
    func cell<T: UITableViewCell>(for indexPath: IndexPath) -> T {
        return dequeueReusableCell(withIdentifier: T.name, for: indexPath) as! T
    }
    
    func setAutomaticRowHeight(estimatedRowHeight: CGFloat) {
        rowHeight = UITableViewAutomaticDimension
        self.estimatedRowHeight = estimatedRowHeight
    }
    
    func registerHeader(view: UIView.Type) {
        let viewName = view.name
        register(UINib(nibName: viewName, bundle: nil), forHeaderFooterViewReuseIdentifier: viewName)
    }
    
    func registerCell(cell: UITableViewCell.Type) {
        let cellName = cell.name
        register(UINib(nibName: cellName, bundle: nil), forCellReuseIdentifier: cellName)
    }
}
