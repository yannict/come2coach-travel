//
//  UIFont+Print.swift
//  come2coach
//
//  Created by Marc Ortlieb on 08.08.16.
//  Copyright © 2016 MaytecNet. All rights reserved.
//
// http://codewithchris.com/common-mistakes-with-adding-custom-fonts-to-your-ios-app/


import Foundation


extension UIFont {
    
    static func printAllFonts() {
    
        for family: String in UIFont.familyNames {
            OnDebugging.print("\(family)")
            for names: String in UIFont.fontNames(forFamilyName: family) {
                OnDebugging.print("== \(names)")
            }
        }
    }
}
