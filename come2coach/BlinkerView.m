//
//  BlinkerView.m
//
//  Created by Werner Kratochwil Full on 22.11.09.
//  Copyright 2009 MaytecNet. All rights reserved.
//

#import "BlinkerView.h"


@implementation BlinkerView


- (id)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        // Initialization code
		[self startAnimation];
		[self doAnimate];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)coder
{
    if (self = [super initWithCoder:coder]) {
		[self startAnimation];
		[self doAnimate];
	}
	return self;
}

- (void)drawRect:(CGRect)rect {
    // Drawing code
}



-(void)startAnimation
{
	running = YES;
}

-(void)stopAnimation
{
	running = NO;
}

- (void)doAnimate
{
	self.alpha = MAX_ALPHA;
    
    [UIView animateWithDuration:BLINK_DURATION delay:0.0 options:UIViewAnimationOptionAutoreverse | UIViewAnimationOptionRepeat |UIViewAnimationOptionLayoutSubviews animations:^{
        if (running)
            self.alpha = MIN_ALPHA;
    } completion:nil];
    
    /*
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:BLINK_DURATION];
	[UIView setAnimationRepeatAutoreverses:YES];
	[UIView setAnimationRepeatCount:FLT_MAX];
	[UIView setAnimationCurve: UIViewAnimationCurveLinear];
	if (running)
		self.alpha = MIN_ALPHA;
	[UIView commitAnimations];	*/
}

@end
