//
//  S3Manager.swift
//  come2coach
//
//  Created by Alex Kupchak on 11/21/16.
//  Copyright © 2016 MaytecNet. All rights reserved.
//

import Foundation
import AWSS3
import Photos
import MRProgress

class S3Manager {
    class var sharedInstance: S3Manager {

        struct Static {
            static let instance = S3Manager()
        }

        return Static.instance
    }

    static func uploadFileToS3(_ remoteFileName: String?, generatedImageUrl: URL, contentType: String, progress: @escaping (_ totalBytesSent: Int64, _ totalBytesExpectedToSend: Int64) -> Void, completion: @escaping (_ success: Bool, _ s3URL: String?) -> Void) {

        if let remoteName = remoteFileName {

            if let uploadRequest = AWSS3TransferManagerUploadRequest() {
                uploadRequest.body = generatedImageUrl
                uploadRequest.key = remoteName
                uploadRequest.bucket = Constants.S3BucketName
                uploadRequest.contentType = contentType

                uploadRequest.uploadProgress = { (bytesSent: Int64, totalBytesSent: Int64, totalBytesExpectedToSend: Int64) in
                    DispatchQueue.main.sync(execute: { () -> Void in
                        // Prepare for progress view
                        //
                        progress(totalBytesSent, totalBytesExpectedToSend)
                    })
                }

                let transferManager = AWSS3TransferManager.default()

                transferManager?.upload(uploadRequest).continue(
                                with: AWSExecutor.mainThread(),
                                withSuccessBlock: { (task: AWSTask) -> Any? in

                                    if let error = task.error {
                                        OnDebugging.print("Upload failed with error: (\(error.localizedDescription))")
                                    }

                                    if let exception = task.exception {
                                        OnDebugging.print("Upload failed with exception (\(exception))")
                                    }

                                    if task.result != nil {
                                        let s3URL = "https://c2c-app.s3.amazonaws.com/\(uploadRequest.key!)"
                                        OnDebugging.print("Uploaded to:\n\(s3URL)")
                                        completion(true, s3URL)
                                        return nil
                                    } else {
                                        OnDebugging.print("Unexpected empty result.")
                                    }

                                    completion(false, nil)

                                    return nil
                                })
            }
        }

        // Perform file upload
        //
        /* #swift3
        transferManager.upload(uploadRequest).continue { (task) -> AnyObject! in
            if let error = task.error {
                OnDebugging.print("Upload failed with error: (\(error.localizedDescription))")
            }
            
            if let exception = task.exception {
                OnDebugging.print("Upload failed with exception (\(exception))")
            }
            
            if task.result != nil {
                let s3URL = "https://c2c-app.s3.amazonaws.com/\(uploadRequest.key!)"
                OnDebugging.print("Uploaded to:\n\(s3URL)")
                completion(success: true, s3URL: s3URL)
                return nil
            }
            else {
                OnDebugging.print("Unexpected empty result.")
            }
            
            completion(success: false, s3URL: nil)
            return nil
        } */
    }

    static func downloadFileFromS3(_ localFileName: String?, progress: @escaping (_ totalBytesSent: Int64, _ totalBytesExpectedToSend: Int64) -> Void, completion: @escaping (_ success: Bool, _ contentType: String?, _ body: URL?) -> Void) {
        if localFileName == nil {
            return
        }

        let remoteName = localFileName!

        // Construct the NSURL for the download location.
        //
        let downloadingFilePath = "file://" + NSTemporaryDirectory() + "downloaded-\(remoteName)"
        let downloadingFileURL = URL(string: downloadingFilePath)

        //        let documents = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0]
        //        let writePath = "file://" + documents + "downloaded-\(remoteName)"
        //        let downloadingFileURL = NSURL(string: writePath)

        // Construct the download request.
        //
        let downloadRequest = AWSS3TransferManagerDownloadRequest()

        downloadRequest?.bucket = Constants.S3BucketName
        downloadRequest?.key = remoteName
        downloadRequest?.downloadingFileURL = downloadingFileURL

        downloadRequest?.downloadProgress = { (bytesSent: Int64, totalBytesSent: Int64, totalBytesExpectedToSend: Int64) in
            DispatchQueue.main.sync(execute: { () -> Void in
                // Prepare for progress view
                //
                progress(totalBytesSent, totalBytesExpectedToSend)
            })
        }

        let transferManager = AWSS3TransferManager.default()


        transferManager?.download(downloadRequest).continue(
                        with: AWSExecutor.mainThread(),
                        withSuccessBlock: { (task: AWSTask) -> Any? in

                            if let error = task.error {
                                OnDebugging.print("Upload failed with error: (\(error.localizedDescription))")
                            }

                            if let exception = task.exception {
                                OnDebugging.print("Upload failed with exception (\(exception))")
                            }

                            if task.result != nil {
                                let downloadOutput = task.result as! AWSS3TransferManagerDownloadOutput
                                OnDebugging.print(downloadOutput)
                                completion(true, downloadOutput.contentType, downloadOutput.body as? URL)
                                return nil
                            } else {
                                OnDebugging.print("Unexpected empty result.")
                            }

                            completion(false, nil, nil)
                            return nil
                        })

        /* #swift3
        transferManager.download(downloadRequest).continue { (task) -> AnyObject? in
            if let error = task.error {
                OnDebugging.print("Upload failed with error: (\(error.localizedDescription))")
            }
            
            if let exception = task.exception {
                OnDebugging.print("Upload failed with exception (\(exception))")
            }
            
            if task.result != nil {
                let downloadOutput = task.result as! AWSS3TransferManagerDownloadOutput
                OnDebugging.print(downloadOutput)
                completion(success: true, contentType: downloadOutput.contentType, body: downloadOutput.body as? URL)
                return nil
            }
            else {
                OnDebugging.print("Unexpected empty result.")
            }
            
            completion(success: false, contentType: nil, body: nil)
            return nil
        }
        */
    }
}
