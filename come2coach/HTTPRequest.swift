//
//  HTTPRequest.swift
//  come2coach
//
//  Created by Marc Ortlieb on 10.08.16.
//  Copyright © 2016 MaytecNet. All rights reserved.
//


import Foundation
import Alamofire
import SwiftyJSON


/*

Testdata for paypal

User: sandbox_payment@come2coach.de
Pwd: c2cSandbox

*/

extension Request {

    public func debugRequest(_ title: String) -> Self {



        let f = DateFormatter()
        f.timeStyle = .medium
        f.dateStyle = .medium


        debugPrint("------------------------------------- REQUEST (" + title + ") ---------------------------------")
        debugPrint(" Execution time " + f.string(from: NSDate() as Date) + "")
        debugPrint("-------------------------------------------------------------------------------------------")

        debugPrint(self)

        debugPrint("------------------------------------- END (" + title + ") ---------------------------------")

        // let s =  " REQUEST (" + title + ") : "  + self.debugDescription

        // HTTPRequest.logDebug(msg: s)




        return self
    }
}

class HTTPRequest {

    // Production
    //
    // static let rootUrl = "https://api.come2coach.de/v1/"

    // Staging
         static let rootUrl = "https://staging-api.come2coach.de/v1/"

    // Mock server
    //
    //    static let rootUrl = "https://private-anon-bf9860fc5f-come2coachapi.apiary-mock.com/v1/"

    static let coachingsPerPage = 12


    static func logDebug(msg: String) {

        let url = "http://logs-01.loggly.com/inputs/8c65fa25-9314-4e17-9627-b9e8bae73b4f/tag/http/"


        let parameters: [String: AnyObject] = [
                "message": msg as AnyObject
        ]

        Alamofire
                .request(url,
                        method: .post,
                        parameters: parameters,
                        encoding: JSONEncoding.default)
                .validate()
                .responseJSON { response in

                    debugPrint("some result here")
                }

    }

    static func makeRequest(methodPath: String, method: HTTPMethod, params: [String: Any?]? = nil, onSuccess: @escaping (JSON) -> (), onError: @escaping (String) -> ()) {
        let url = rootUrl + methodPath
        
        let headers = [
            "Token": Constants.API.accessToken,
            "Content-Type": "application/json",
            "Authorization": "Bearer \(JSONWebToken.sharedInstance.value)"
        ]
        
        Alamofire.request(url, method: method, parameters: params, encoding: JSONEncoding.default, headers: headers)
            .debugRequest(url)
            .responseJSON { dataResponse in
                
                if let error = dataResponse.result.error {
                    onError(error.localizedDescription)
                    return
                }
                
                guard let value = dataResponse.result.value, let response = dataResponse.response else {
                    onError("No response value")
                    OnDebugging.printError("response.result.value == nil", title: "HTTPRequest")
                    return
                }
                
                let json = JSON(value)
                if response.statusCode < 300 { onSuccess(json) }
                else { onError(json["message"].string ?? "Unknown error") }
        }
    }
    
// MARK: Event
    
    static func fetchEvent(with eventID: Int, handler: @escaping (_ event: Event) -> Void) {

        let url = rootUrl + "event/\(eventID)"
        let parameters: [String: AnyObject]? = nil

        let headers = [
                "Token": Constants.API.accessToken,
                "Content-Type": "application/json",
                "Authorization": "Bearer \(JSONWebToken.sharedInstance.value)"
        ]

        Alamofire.request(url, method: .get, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
                .debugRequest(url)
                .responseJSON { response in

                    HTTPResponse
                            .validateStatusCode(response.response?.statusCode)

                    // parse json
                    guard let value = response.result.value else {
                        OnDebugging.printError("response.result.value == nil", title: "HTTPRequest.fetchEventWithID()")
                        return
                    }

                    //debugPrint(value)

                    let json = JSON(value)
                    let event = JSONDecoder.makeEvent(json)

                    handler(event)
                }
    }

    static func joinEvent(eventId: Int, participantsEmails: [String], onSuccess: @escaping () -> (), onError: @escaping (String) -> ()) {
        let params = ["participantMails": participantsEmails]
        makeRequest(methodPath: "event/\(eventId)/join", method: .post, params: params, onSuccess: { (json) in
            onSuccess()
        }, onError: onError)
    }
    
    static func unlikeEvent(with eventId: Int, handler: @escaping (_ success: Bool) -> Void) {

        let url = rootUrl + "event/\(eventId)/like"

        let headers = [
                "Token": Constants.API.accessToken,
                "Content-Type": "application/json",
                "Authorization": "Bearer \(JSONWebToken.sharedInstance.value)"
        ]

        Alamofire
                .request(url,
                        method: .delete,
                        parameters: nil,
                        encoding: JSONEncoding.default,
                        headers: headers)
                .validate()
                .debugRequest(url)
                .responseJSON { response in
                    HTTPResponse
                            .validateStatusCode(response.response?.statusCode)
                    switch response.result {
                    case .success(let responseJson):
                        OnDebugging.print("likeEvent successful: \(responseJson)")
                        guard let statusCode = response.response?.statusCode else {
                            OnDebugging.print("ERROR: updateEvent() - statusCode == nil")
                            return
                        }
                        switch statusCode {
                                // https://httpstatuses.com
                        case 204: handler(true)
                        default: OnDebugging.print("response status code: \(statusCode)", title: "HTTPRequest.likeEvent()")
                        }

                    case .failure(let error):
                        OnDebugging
                                .printError("HTTPRequest.updateEvent response.result.Failure: \(error)")

                    }
                }
    }

    static func likeEvent(with eventId: Int, handler: @escaping (_ success: Bool) -> Void) {

        let url = rootUrl + "event/\(eventId)/like"

        let headers = [
                "Token": Constants.API.accessToken,
                "Content-Type": "application/json",
                "Authorization": "Bearer \(JSONWebToken.sharedInstance.value)"
        ]

        Alamofire
                .request(url,
                        method: .post,
                        parameters: nil,
                        encoding: JSONEncoding.default,
                        headers: headers)
                .validate()
                .debugRequest(url)
                .responseJSON { response in
                    HTTPResponse
                            .validateStatusCode(response.response?.statusCode)
                    switch response.result {
                    case .success(let responseJson):
                        OnDebugging.print("likeEvent successful: \(responseJson)")
                        guard let statusCode = response.response?.statusCode else {
                            OnDebugging.print("ERROR: updateEvent() - statusCode == nil")
                            return
                        }
                        switch statusCode {

                        case 204: handler(true)
                        default: OnDebugging.print("response status code: \(statusCode)", title: "HTTPRequest.likeEvent()")
                        }

                    case .failure(let error):
                        OnDebugging
                                .printError("HTTPRequest.updateEvent response.result.Failure: \(error)")

                    }
                }
    }


    static func rateEvent(with eventId: Int, rate rateValue: Int, handler: @escaping (_ success: Bool) -> Void) {


        let url = rootUrl + "event/\(eventId)/rate"

        let headers = [
                "Token": Constants.API.accessToken,
                "Content-Type": "application/json",
                "Authorization": "Bearer \(JSONWebToken.sharedInstance.value)"
        ]

        let rs = String(rateValue) + "," + String(rateValue) + "," + String(rateValue)

        let parameters = [
                "rating": rs
        ]

        Alamofire
                .request(url,
                        method: .post,
                        parameters: parameters,
                        encoding: JSONEncoding.default,
                        headers: headers)
                .validate()
                .debugRequest(url)
                .responseJSON { response in

                    HTTPResponse
                            .validateStatusCode(response.response?.statusCode)

                    handler(true)
                }


    }


    static func mailEvent(with event: Event, text: String, handler: @escaping (_ success: Bool) -> Void) {

        let url = rootUrl + "event/\(event.id)/mail"

        let headers = [
                "Token": Constants.API.accessToken,
                "Content-Type": "application/json",
                "Authorization": "Bearer \(JSONWebToken.sharedInstance.value)"
        ]

        let parameters: [String: AnyObject] = [
                "message": text as AnyObject
        ]

        Alamofire
                .request(url,
                        method: .post,
                        parameters: parameters,
                        encoding: JSONEncoding.default,
                        headers: headers)
                .validate()
                .debugRequest(url)
                .responseJSON { response in

                    HTTPResponse
                            .validateStatusCode(response.response?.statusCode)
                    switch response.result {
                    case .success(let responseJson):
                        OnDebugging.print("mailEvent successful: \(responseJson)")
                        guard let statusCode = response.response?.statusCode else {
                            OnDebugging.print("ERROR: updateEvent() - statusCode == nil")
                            return
                        }
                        switch statusCode {
                                // https://httpstatuses.com
                        case 204: handler(true)
                        default: OnDebugging.print("response status code: \(statusCode)", title: "HTTPRequest.likeEvent()")
                        }

                    case .failure(let error):
                        OnDebugging
                                .printError("HTTPRequest.updateEvent response.result.Failure: \(error)")

                    }
                }
    }
    
    static func handlePayment(eventId: Int, transactionId: String, coinsCount: Int, onSuccess: @escaping (Int) -> (), onError: @escaping (String) -> ()) {
        let params: [String: Any] = ["transactionId": transactionId,
                                     "credits": coinsCount]
        makeRequest(methodPath: "event/\(eventId)/payment/inapp", method: .post, params: params, onSuccess: { (json) in
            onSuccess(json["user_total"].intValue)
        }, onError: onError)
    }

// MARK: EIGENE
/// Ich als Coach sehe meine events die ich halte

    static func fetchCoachEvents(_ handler: @escaping (_ events: [Event]) -> Void) {

        let url = rootUrl + "events/user"

        let parameters: [String: Any] = [
                "pagination": ["page": "0", "per_page": "\(coachingsPerPage)"],
                "type": "public", // "public,joined" // public
                "webinar": "true"
        ]

        let headers = [
                "Token": Constants.API.accessToken,
                "Content-Type": "application/json",
                "Authorization": "Bearer \(JSONWebToken.sharedInstance.value)"
        ]
        Alamofire
                .request(url,
                        method: .post,
                        parameters: parameters,
                        encoding: JSONEncoding.default,
                        headers: headers)
                .validate()
                .debugRequest(url)
                .responseJSON { response in

                    HTTPResponse
                            .validateStatusCode(response.response?.statusCode)
                    // parse json
                    guard let value = response.result.value else {
                        OnDebugging.printError("response.result.value == nil", title: "HTTPRequest.fetchCoachOnlyEvents()")
                        return
                    }


                    func makeEventUserTypeCoach(_ event: Event) -> Event {
                        var copy = event
                        copy.userType = .coach
                        return copy
                    }

                    let json = JSON(value)["public"]["events"]
                    let events: [Event] =
                            json.arrayValue
                                    .map {
                                        JSONDecoder.makeEvent($0)
                                    }
                                    .map(makeEventUserTypeCoach)

                    handler(events)
                }
    }

    static func fetchCoachOnlyEvents(_ page: Int, perPage: Int, handler: @escaping (_ events: [Event], _ page: Int, _ count: Int) -> Void) {

        let url = rootUrl + "events/user"

        let parameters: [String: Any] = [
                "pagination": ["page": "\(page)", "per_page": "\(perPage)"],
                "type": "public", // "public,joined" // public
                "live": "all"
                //"webinar": "true"
        ]

        let headers = [
                "Token": Constants.API.accessToken,
                "Content-Type": "application/json",
                "Authorization": "Bearer \(JSONWebToken.sharedInstance.value)"
        ]
        Alamofire
                .request(url,
                        method: .post,
                        parameters: parameters,
                        encoding: JSONEncoding.default,
                        headers: headers)
                .validate()
                .debugRequest(url)
                .responseJSON { response in

                    HTTPResponse
                            .validateStatusCode(response.response?.statusCode)
                    // parse json
                    guard let value = response.result.value else {
                        OnDebugging.printError("response.result.value == nil", title: "HTTPRequest.fetchCoachOnlyEvents()")
                        return
                    }


                    func makeEventUserTypeCoach(_ event: Event) -> Event {
                        var copy = event
                        copy.userType = .coach
                        return copy
                    }

                    let json = JSON(value)["public"]["events"]
                    let events: [Event] =
                            json.arrayValue
                                    .map {
                                        JSONDecoder.makeEvent($0)
                                    }
                                    .map(makeEventUserTypeCoach)

                    handler(events, Int(JSON(value)["public"]["pagination"]["page"].rawString()!)!, Int(JSON(value)["public"]["total"].rawString()!)!)
                }
    }

    static func fetchArchiveEvents(_ page: Int, perPage: Int, handler: @escaping (_ events: [Event], _ page: Int, _ count: Int) -> Void) {

        let url = rootUrl + "events/user"

        let parameters: [String: Any] = [
                "pagination": ["page": "\(page)", "per_page": "\(perPage)"],
                "type": "archiv", // "public,joined" // public
                "live": "all"
                //"webinar": "true"
        ]

        let headers = [
                "Token": Constants.API.accessToken,
                "Content-Type": "application/json",
                "Authorization": "Bearer \(JSONWebToken.sharedInstance.value)"
        ]
        Alamofire
                .request(url,
                        method: .post,
                        parameters: parameters,
                        encoding: JSONEncoding.default,
                        headers: headers)
                .validate()
                .debugRequest(url)
                .responseJSON { response in

                    HTTPResponse
                            .validateStatusCode(response.response?.statusCode)
                    // parse json
                    guard let value = response.result.value else {
                        OnDebugging.printError("response.result.value == nil", title: "HTTPRequest.fetchCoachOnlyEvents()")
                        return
                    }


                    func makeEventUserTypeCoach(_ event: Event) -> Event {
                        var copy = event
                        copy.userType = .coach
                        return copy
                    }

                    let json = JSON(value)["archiv"]["events"]
                    let events: [Event] =
                            json.arrayValue
                                    .map {
                                        JSONDecoder.makeEvent($0)
                                    }
                                    .map(makeEventUserTypeCoach)

                    handler(events, Int(JSON(value)["archiv"]["pagination"]["page"].rawString()!)!, Int(JSON(value)["archiv"]["total"].rawString()!)!)
                }
    }

// MARK: GEBUCHT
/// ich als Teilnehmer sehe welche events ich gebucht habe

    static func fetchUpcomingBookedEvents(_ handler: @escaping (_ events: [Event]) -> Void) {

        let url = rootUrl + "events/user"

        let parameters: [String: Any] = [
                "pagination": ["page": "0", "per_page": "\(coachingsPerPage)"],
                "type": "joined",
                "webinar": "true"
        ]

        let headers = [
                "Token": Constants.API.accessToken,
                "Content-Type": "application/json",
                "Authorization": "Bearer \(JSONWebToken.sharedInstance.value)"
        ]

        Alamofire
                .request(url,
                        method: .post,
                        parameters: parameters,
                        encoding: JSONEncoding.default,
                        headers: headers)
                .validate()
                .debugRequest(url)
                .responseJSON { response in

                    HTTPResponse
                            .validateStatusCode(response.response?.statusCode)
                    // parse json
                    switch response.result {
                    case .failure(let error):
                        OnDebugging.printError("Fehler beim Laden: \(error)", title: "HTTPRequest.fetchUpcomingBookedEvents()()")

                    case .success:
                        OnDebugging.print(response)

                        guard let value = response.result.value else {
                            OnDebugging.printError("response.result.value == nil", title: "HTTPRequest.fetchUpcomingBookedEvents()")
                            return
                        }


                        func makeEventUserTypeParticipant(_ event: Event) -> Event {
                            var copy = event
                            copy.userType = .participant
                            return copy
                        }

                        let json = JSON(value)
                        let events: [Event] =
                                json
                                        .arrayValue
                                        .map {
                                            JSONDecoder.makeEvent($0)
                                        }
                                        .map(makeEventUserTypeParticipant)

                        handler(events)
                    }
                }
    }

    static func requestResetPassword(For mail: String, success: @escaping ((_ didSendMail: Bool) -> Void)) {

        let parameters: [String: AnyObject] = [
                "email": mail as AnyObject
        ]
        let headers = [
                "Token": Constants.API.accessToken,
                "Content-Type": "application/json",
                "Authorization": "Bearer \(JSONWebToken.sharedInstance.value)"
        ]
        Alamofire
                .request(rootUrl + "auth/reset",
                        method: .post,
                        parameters: parameters,
                        encoding: JSONEncoding.default,
                        headers: headers)

                .responseJSON { response in
                    HTTPResponse
                            .validateStatusCode(response.response?.statusCode)

                    guard let value = response.result.value else {
                        OnDebugging
                                .printError("response.result.value == nil", title: "HTTPRequest.requestResetPassword()")
                        return
                    }
                    let json = JSON(value)
                    OnDebugging.print(json)
                    //                JSON
                    //                {
                    //                    "success" : true
                    //                }

                    success(json["success"].boolValue)
                }
    }


    static func updateEvent(with event: Event, errors: @escaping ([String]) -> Void) {

        let url = rootUrl + "event/\(event.id)"

        let headers = [
                "Token": Constants.API.accessToken,
                "Content-Type": "application/json",
                "Authorization": "Bearer \(JSONWebToken.sharedInstance.value)"
        ]
        let parameters: [String: Any] = ["title": event.title as AnyObject,
                                         "cat1": event.cat1 as AnyObject,
                                         //TODO: find solution for cat2
                                         //"cat2": event.cat2,
                                         "description": event.descriptionText as AnyObject,
                                         "text_do": event.text_do as AnyObject,
                                         "text_learn": event.text_learn as AnyObject,
                                         //TODO: find solution for tags
                                         //"tags": event.tags,
                                         "webinar": event.isWebinar as AnyObject,
                                         "price": event.price as AnyObject,
                                         "min_part": event.minParticipants as AnyObject,
                                         "max_part": event.maxParticipants as AnyObject,
                                         "begin": event.begin.date.makeDateString(),
                                         "duration": event.durationArrString,
                                         "adult": event.adult,
                                         //TODO: find solution for address
                                         //                "address": event.address,
                                         "public": event.isPublic]

        OnDebugging.print("Parameters: \(parameters)")

        Alamofire
                .request(url,
                        method: .post,
                        parameters: parameters,
                        encoding: JSONEncoding.default,
                        headers: headers)
                .validate()
                .debugRequest(url)
                .responseJSON { response in
                    switch response.result {
                    case .success(let response):
                        OnDebugging.print("updateEvent successful: \(response)")
                    case .failure(let error):
                        OnDebugging
                                .printError("HTTPRequest.updateEvent response.result.Failure: \(error)")
                        guard let statusCode = response.response?.statusCode else {
                            OnDebugging.print("ERROR: updateEvent() - statusCode == nil")
                            return
                        }
                        switch statusCode {
                                // https://httpstatuses.com
                        case 403: errors(["User is not the coach of the event."])
                        default: OnDebugging.print("response status code: \(statusCode)", title: "HTTPRequest.updateEvent()")
                        }
                    }
                }
    }


// MARK: events

    static func fetchTodayEvents(_ page: Int, perPage: Int, live: Int, handler: @escaping (_ events: [Event], _ page: Int, _ count: Int) -> Void) {

        var timeInterval = DateComponents()
        timeInterval.hour = 5
        let toDate = Calendar.current.date(byAdding: timeInterval, to: Date())!


        let url = rootUrl + "events"
        let parameters: [String: Any] = [
                "pagination": ["page": "\(page)", "per_page": "\(perPage)"],
                "live": live,
                "date": ["from": Date().makeDateStringWith(), // Date().makeFromDateString(),
                         "to": toDate.makeDateStringWith()] //  Date().makeToDateString()]

        ]
        //print(parameters)
        let headers = [
                "Token": Constants.API.accessToken,
                "Content-Type": "application/json",
                "Authorization": "Bearer \(JSONWebToken.sharedInstance.value)"
        ]

        Alamofire
                .request(url,
                        method: .post,
                        parameters: parameters,
                        encoding: JSONEncoding.default,
                        headers: headers)
                .validate()
                .debugRequest(url)
                .responseJSON { response in
                    HTTPResponse
                            .validateStatusCode(response.response?.statusCode)

                    // parse json
                    guard let value = response.result.value else {
                        OnDebugging.print("ERROR: ALAMOFIRE - response.result.value == nil")
                        return
                    }
                    //OnDebugging.print("response.result.value: \(value)")

                    let events: [Event] =
                            JSON(value)["events"]
                                    .arrayValue
                                    .map {
                                JSONDecoder.makeEvent($0, printJSON: true)
                            }

                    handler(events, Int(JSON(value)["pagination"]["page"].rawString()!)!, Int(JSON(value)["total"].rawString()!)!)
                }
    }

    static func fetchUpcomingEvents(_ page: Int, perPage: Int, live: Int, handler: @escaping (_ events: [Event], _ page: Int, _ count: Int) -> Void) {

        let url = rootUrl + "events"
        let parameters: [String: Any] = [
                "pagination": ["page": "\(page)", "per_page": "\(perPage)"],
                "live": live
        ]

        let headers = [
                "Token": Constants.API.accessToken,
                "Content-Type": "application/json",
                "Authorization": "Bearer \(JSONWebToken.sharedInstance.value)"
        ]

        Alamofire
                .request(url,
                        method: .post,
                        parameters: parameters,
                        encoding: JSONEncoding.default,
                        headers: headers)
                .validate()
                .debugRequest(url)
                .responseJSON { response in
                    HTTPResponse
                            .validateStatusCode(response.response?.statusCode)

                    // parse json
                    guard let value = response.result.value else {
                        OnDebugging.print("ERROR: ALAMOFIRE - response.result.value == nil")
                        return
                    }
                    OnDebugging.print("response.result.value: \(value)")

                    let events: [Event] =
                            JSON(value)["events"]
                                    .arrayValue
                                    .map {
                                JSONDecoder.makeEvent($0, printJSON: true)
                            }

                    handler(events,
                            Int(JSON(value)["pagination"]["page"].rawString()!)!,
                            Int(JSON(value)["total"].rawString()!)!)
                }
    }

    static func fetchFavoriteEvents(_ handler: @escaping (_ events: [Event]) -> Void) {

        let url = rootUrl + "events/favorites"
        let parameters: [String: Any] = [
                "pagination": ["page": "0", "per_page": "\(coachingsPerPage)"],
                "type": "public",
                "webinar": "true"
        ]

        let headers = [
                "Token": Constants.API.accessToken,
                "Content-Type": "application/json",
                "Authorization": "Bearer \(JSONWebToken.sharedInstance.value)"
        ]

        Alamofire
                .request(url,
                        method: .post,
                        parameters: parameters,
                        encoding: JSONEncoding.default,
                        headers: headers)
                .validate()
                .debugRequest(url)
                .responseJSON { response in
                    HTTPResponse
                            .validateStatusCode(response.response?.statusCode)

                    // parse json
                    guard let value = response.result.value else {
                        OnDebugging.print("ERROR: ALAMOFIRE - response.result.value == nil")
                        return
                    }
                    //OnDebugging.print("response.result.value: \(value)")

                    let events: [Event] =
                            JSON(value)
                                    .arrayValue
                                    .map {
                                JSONDecoder.makeEvent($0, printJSON: true)
                            }

                    handler(events)
                }
    }

    static func fetchLikedEvents(_ page: Int, perPage: Int, handler: @escaping (_ events: [Event], _ page: Int, _ count: Int) -> Void) {

        let url = rootUrl + "events/user"

        let parameters: [String: Any] = [
                "pagination": ["page": "\(page)", "per_page": "\(perPage)"],
                "type": "liked"
                //"webinar": "true"
        ]

        let headers = [
                "Token": Constants.API.accessToken,
                "Content-Type": "application/json",
                "Authorization": "Bearer \(JSONWebToken.sharedInstance.value)"
        ]
        Alamofire
                .request(url,
                        method: .post,
                        parameters: parameters,
                        encoding: JSONEncoding.default,
                        headers: headers)
                .validate()
                .debugRequest(url)
                .responseJSON { response in
                    HTTPResponse
                            .validateStatusCode(response.response?.statusCode)
                    // parse json
                    guard let value = response.result.value else {
                        OnDebugging.printError("response.result.value == nil", title: "HTTPRequest.fetchCoachOnlyEvents()")
                        return
                    }


                    func makeEventUserTypeCoach(_ event: Event) -> Event {
                        var copy = event
                        copy.userType = .coach
                        return copy
                    }

                    debugPrint(value)

                    let json = JSON(value)["liked"]["events"]
                    let events: [Event] =
                            json.arrayValue
                                    .map {
                                        JSONDecoder.makeEvent($0)
                                    }
                                    .map(makeEventUserTypeCoach)

                    handler(events, Int(JSON(value)["liked"]["pagination"]["page"].rawString()!)!, Int(JSON(value)["liked"]["total"].rawString()!)!)
                }
    }
// MARK: Upload image for event

    static func uploadImage(_ eventId: Int, imageType: String, filePath: String, handler: @escaping (_ error: [String]?, _ success: Bool) -> Void) {
        // Production server
        //
        let url = URL(string: HTTPRequest.rootUrl + "event/\(eventId)/\(imageType)")!

        let headers = [
                "Token": Constants.API.accessToken,
                "Content-Type": "application/json",
                "Authorization": "Bearer \(JSONWebToken.sharedInstance.value)"
        ]

        let parameters = ["file": filePath]

        Alamofire
                .request(url, method: .post,
                        parameters: parameters,
                        encoding: JSONEncoding.default,
                        headers: headers)
                .validate()
                .responseJSON { response in
                    HTTPResponse
                            .validateStatusCode(response.response?.statusCode)

                    switch response.result {
                    case .failure(let error):
                        OnDebugging.print(error.localizedDescription)
                        handler([error.localizedDescription], false)
                    case .success:
                        guard response.result.value != nil else {
                            OnDebugging.print("response.result.value == nil")
                            return
                        }

                        handler(nil, true)
                    }
                }
    }

    static func fetchJoinedEvents(_ page: Int, perPage: Int, handler: @escaping (_ events: [Event], _ page: Int, _ count: Int) -> Void) {

        let url = rootUrl + "events/user"

        let parameters: [String: Any] = [
                "pagination": ["page": "\(page)", "per_page": "\(perPage)"],
                "type": "joined",
                "webinar": "true"
        ]

        let headers = [
                "Token": Constants.API.accessToken,
                "Content-Type": "application/json",
                "Authorization": "Bearer \(JSONWebToken.sharedInstance.value)"
        ]
        Alamofire
                .request(url,
                        method: .post,
                        parameters: parameters,
                        encoding: JSONEncoding.default,
                        headers: headers)
                .validate()
                .debugRequest(url)
                .responseJSON { response in
                    HTTPResponse
                            .validateStatusCode(response.response?.statusCode)
                    // parse json
                    guard let value = response.result.value else {
                        OnDebugging.printError("response.result.value == nil", title: "HTTPRequest.fetchCoachOnlyEvents()")
                        return
                    }
                    let events: [Event] =
                            JSON(value)["joined"]["events"]
                                    .arrayValue
                                    .map {
                                JSONDecoder.makeEvent($0, printJSON: true)
                            }

                    handler(events, Int(JSON(value)["joined"]["pagination"]["page"].rawString()!)!, Int(JSON(value)["joined"]["total"].rawString()!)!)

                }
    }
//=================================================================================
// MARK: User
//=================================================================================


    static func getUser(_ handler: @escaping (_ user: User?, _ error: [String]?) -> Void) {
        let user = User.current

        // Production
        //
        let url = URL(string: rootUrl + "user")!

        let headers = [
                "Token": Constants.API.accessToken,
                "Content-Type": "application/json",
                "Authorization": "Bearer \(JSONWebToken.sharedInstance.value)"
        ]

        Alamofire
                .request(url,
                        method: .get,
                        encoding: JSONEncoding.default,
                        headers: headers)
                .validate()
                .debugRequest(url.absoluteString)
                .responseJSON { response in
                    HTTPResponse
                            .validateStatusCode(response.response?.statusCode)


                    switch response.result {
                    case .failure(let error):
                        OnDebugging.print(error.localizedDescription)
                        handler(nil, [error.localizedDescription])
                    case .success:
                        // parse json
                        guard let value = response.result.value else {
                            OnDebugging.print("ERROR: ALAMOFIRE - response.result.value == nil")
                            return
                        }

                        let json = JSON(rawValue: value)

                        user!.write(
                                json!["user"]["name"].stringValue,
                                lname: json!["user"]["lname"].stringValue,
                                password: json!["user"]["password"].stringValue,
                                email: json!["user"]["email"].stringValue,
                                mobile: json!["user"]["mobile"].stringValue,
                                ppEmail: json!["user"]["pp_email"].stringValue,
                                imageUrl: !json!["user"]["image"].stringValue.isEmpty ? json!["user"]["image"].stringValue : json!["user"]["avatar"].stringValue,
                                about: json!["user"]["about"].stringValue,
                                pushToken: json!["user"]["push_token"].stringValue,
                                wishListReminder: json!["user"]["notifications"].intValue,
                                verified: json!["user"]["verified"].intValue,
                                id: json!["user"]["id"].intValue,
                                coins: json!["totals"]["coins"].intValue,
                                earnings: json!["totals"]["earnings"].doubleValue
                        )


                        debugPrint(response.result.value)


                        handler(user!, nil)
                    }
                }
    }

    static func getSpecificUser(_ userId: Int, handler: @escaping (_ success: Bool, _ json: JSON?) -> Void) {

        // Production
        //
        let url = URL(string: rootUrl + "user/" + String(userId))!

        let headers = [
                "Token": Constants.API.accessToken,
                "Content-Type": "application/json",
                "Authorization": "Bearer \(JSONWebToken.sharedInstance.value)"
        ]

        Alamofire
                .request(url,
                        method: .get,
                        encoding: JSONEncoding.default,
                        headers: headers)
                .validate()
                .debugRequest(url.absoluteString)
                .responseJSON { response in

                    HTTPResponse
                            .validateStatusCode(response.response?.statusCode)


                    switch response.result {
                    case .failure(let error):
                        OnDebugging.print(error.localizedDescription)
                        handler(false, nil)
                    case .success:
                        // parse json
                        guard let value = response.result.value else {
                            OnDebugging.print("ERROR: ALAMOFIRE - response.result.value == nil")
                            return
                        }

                        let json = JSON(rawValue: value)

                        debugPrint(response.result.value)


                        handler(true, json)
                    }
                }

    }

    static func notificationState(_ enable: Int, handler: @escaping (_ success: Bool) -> Void) {

        // Production
        //
        let url = URL(string: rootUrl + "user/pushnotification")!

        let headers = [
                "Token": Constants.API.accessToken,
                "Content-Type": "application/json",
                "Authorization": "Bearer \(JSONWebToken.sharedInstance.value)"
        ]

        let parameters = ["enable": enable]

        Alamofire
                .request(url, method: .post,
                        parameters: parameters,
                        encoding: JSONEncoding.default,
                        headers: headers)
                .validate()
                .debugRequest(url.absoluteString)
                .responseJSON { response in

                    HTTPResponse
                            .validateStatusCode(response.response?.statusCode)

                    // parse json
                    guard let value = response.result.value else {
                        OnDebugging.print("ERROR: ALAMOFIRE - response.result.value == nil")
                        return
                    }

                    let json = JSON(rawValue: value)

                    debugPrint(json)

                    handler(true)

                }

    }

    static func deleteUser(_ handler: @escaping (_ result: Bool, _ error: [String]?) -> Void) {

        // Production
        //
        let url = URL(string: rootUrl + "user")!

        let headers = [
                "Token": Constants.API.accessToken,
                "Content-Type": "application/json",
                "Authorization": "Bearer \(JSONWebToken.sharedInstance.value)"
        ]

        Alamofire
                .request(url,
                        method: .delete,
                        encoding: JSONEncoding.default,
                        headers: headers)
                .validate()
                .debugRequest(url.absoluteString)
                .responseJSON { response in
                    HTTPResponse
                            .validateStatusCode(response.response?.statusCode)

                    guard let value = response.result.value else {
                        OnDebugging.print("response.result.value == nil")
                        return
                    }

                    debugPrint(value)

                    handler(true, nil)
                    /*
                    switch response.result {
                    case .Failure(let error):
                        OnDebugging.print(error.localizedDescription)
                        handler(result: false, error: [error.localizedDescription])
                    case .Success:
                        handler(result: true, error: nil)
                    } */
                }

    }

    static func updatePushToken(_ token: String) {
        // Production
        //
        let url = URL(string: rootUrl + "user")!

        let parameters: [String: Any] = ["push_token": token]

        let headers = [
                "Token": Constants.API.accessToken,
                "Content-Type": "application/json",
                "Authorization": "Bearer \(JSONWebToken.sharedInstance.value)"
        ]

        Alamofire
                .request(url,
                        method: .post,
                        parameters: parameters,
                        encoding: JSONEncoding.default,
                        headers: headers)
                .validate()
                .debugRequest(url.absoluteString)
                .responseJSON { response in

                    HTTPResponse
                            .validateStatusCode(response.response?.statusCode)
                }
    }


    static func updateUser(_ user: User?, password: String?, paypalEmail: String?, handler: @escaping (_ result: Bool, _ error: [String]?) -> Void) {
        // Production
        //
        let url = URL(string: rootUrl + "user")!

        let parameters: [String: Any] = ["name": user!.name,
                                         "lname": user!.lname,
                                         "email": user!.email,
                                         "password": password == nil ? user!.password : password!,
                                         "pp_email": paypalEmail == nil ? user!.paypalEmail : paypalEmail!,
                                         "mobile": user!.mobile,
                                         "about": user!.about,
                                         "push_token": user!.push_token]


        let headers = [
                "Token": Constants.API.accessToken,
                "Content-Type": "application/json",
                "Authorization": "Bearer \(JSONWebToken.sharedInstance.value)"
        ]

        Alamofire
                .request(url,
                        method: .post,
                        parameters: parameters,
                        encoding: JSONEncoding.default,
                        headers: headers)
                .validate()
                .debugRequest(url.absoluteString)
                .responseJSON { response in
                    HTTPResponse
                            .validateStatusCode(response.response?.statusCode)

                    switch response.result {
                    case .failure(let error):
                        OnDebugging.print(error.localizedDescription)
                        handler(false, [error.localizedDescription])
                    case .success:
                        guard let value = response.result.value else {
                            OnDebugging.print("response.result.value == nil")
                            return
                        }

                        let json = JSON(value)
                        let responseDictionary = json.dictionaryValue

                        // posible error
                        if let errorString = responseDictionary["message"]?.stringValue {
                            OnDebugging.print("Error - \(errorString)")
                            handler(false, ["Error - \(errorString)"])
                            return
                        }

                        if let responseErrors = responseDictionary["errors"]?.dictionaryValue {
                            // posible error
                            OnDebugging.print("Error - \(responseErrors)")
                            handler(false, ["Error - \(responseErrors)"])
                        } else {
                            handler(true, nil)
                        }
                    }
                }
    }

    static func uploadUserImage(_ filePath: String, handler: @escaping (_ error: [String]?, _ success: Bool) -> Void) {
        // Production
        //
        let url = URL(string: rootUrl + "user/image")!

        let headers = [
                "Token": Constants.API.accessToken,
                "Content-Type": "application/json",
                "Authorization": "Bearer \(JSONWebToken.sharedInstance.value)"
        ]

        let parameters = ["file": filePath]

        Alamofire
                .request(url,
                        method: .post,
                        parameters: parameters,
                        encoding: JSONEncoding.default,
                        headers: headers)
                .validate()
                .responseJSON { response in
                    HTTPResponse
                            .validateStatusCode(response.response?.statusCode)

                    switch response.result {
                    case .failure(let error):
                        OnDebugging.print(error.localizedDescription)
                        handler([error.localizedDescription], false)
                    case .success:
                        guard let value = response.result.value else {
                            OnDebugging.print("response.result.value == nil")
                            return
                        }

                        let json = JSON(value)
                        let responseDictionary = json.dictionaryValue

                        // posible error
                        if let errorString = responseDictionary["message"]?.stringValue {
                            OnDebugging.print("Error - \(errorString)")
                            handler(["Error - \(errorString)"], false)
                            return
                        }

                        if let responseErrors = responseDictionary["errors"]?.dictionaryValue {
                            // posible error
                            OnDebugging.print("Error - \(responseErrors)")
                            handler(["Error - \(responseErrors)"], false)
                        } else {
                            handler(nil, true)
                        }
                    }
                }
    }

    static func isBooked(_ eventId: Int, handler: @escaping (_ success: Bool, _ json: JSON) -> Void) {

        let url = URL(string: rootUrl + "event/" + String(eventId) + "/booked")!

        let headers = [
                "Token": Constants.API.accessToken,
                "Content-Type": "application/json",
                "Authorization": "Bearer \(JSONWebToken.sharedInstance.value)"
        ]


        Alamofire
                .request(url,
                        method: .post,
                        encoding: JSONEncoding.default,
                        headers: headers)
                .validate()
                .responseJSON { response in

                    HTTPResponse
                            .validateStatusCode(response.response?.statusCode)

                    switch response.result {
                    case .failure(let error):
                        OnDebugging.print(error.localizedDescription)
                        handler(false, nil)
                    case .success:
                        guard let value = response.result.value else {
                            OnDebugging.print("response.result.value == nil")
                            return
                        }

                        let json = JSON(value)

                        handler(true, json)
                    }
                }

    }

    static func shareLink(_ eventId: Int, handler: @escaping (_ success: Bool, _ json: JSON) -> Void) {

        let url = URL(string: rootUrl + "event/" + String(eventId) + "/affiliate")!

        let headers = [
                "Token": Constants.API.accessToken,
                "Content-Type": "application/json",
                "Authorization": "Bearer \(JSONWebToken.sharedInstance.value)"
        ]

        Alamofire
                .request(url,
                        method: .get,
                        encoding: JSONEncoding.default,
                        headers: headers)
                .validate()
                .responseJSON { response in

                    HTTPResponse
                            .validateStatusCode(response.response?.statusCode)

                    switch response.result {
                    case .failure(let error):
                        OnDebugging.print(error.localizedDescription)
                        handler(false, nil)
                    case .success:
                        guard let value = response.result.value else {
                            OnDebugging.print("response.result.value == nil")
                            return
                        }

                        let json = JSON(value)
                        handler(true, json)
                    }
                }

    }

    static func postShareLink(_ affiliateCode: String, handler: @escaping (_ success: Bool, _ eventId: Int) -> Void) {

        let url = URL(string: rootUrl + "affiliate/" + affiliateCode)!

        let headers = [
                "Token": Constants.API.accessToken,
                "Content-Type": "application/json",
                "Authorization": "Bearer \(JSONWebToken.sharedInstance.value)"
        ]

        Alamofire
                .request(url,
                        method: .get,
                        encoding: JSONEncoding.default,
                        headers: headers)
                .validate()
                .responseJSON { response in

                    HTTPResponse
                            .validateStatusCode(response.response?.statusCode)

                    switch response.result {
                    case .failure(let error):
                        OnDebugging.print(error.localizedDescription)
                        handler(false, -1)
                    case .success:
                        guard let value = response.result.value else {
                            OnDebugging.print("response.result.value == nil")
                            return
                        }

                        let json = JSON(value)
                        handler(true, json["event_id"].intValue)
                    }
                }


    }


    static func rejectEvent(_ eventId: Int, handler: @escaping (_ success: Bool, _ json: JSON) -> Void) {

        let url = URL(string: rootUrl + "event/" + String(eventId) + "/cancel")!

        let headers = [
                "Token": Constants.API.accessToken,
                "Content-Type": "application/json",
                "Authorization": "Bearer \(JSONWebToken.sharedInstance.value)"
        ]

        let parameters = ["date": "begin"]

        Alamofire
                .request(url,
                        method: .post,
                        parameters: parameters,
                        encoding: JSONEncoding.default,
                        headers: headers)
                .validate()
                .debugRequest(url.absoluteString)
                .responseJSON { response in
                    HTTPResponse
                            .validateStatusCode(response.response?.statusCode)

                    switch response.result {
                    case .failure(let error):
                        handler(false, nil)
                    case .success:

                        guard let value = response.result.value else {
                            OnDebugging.print("response.result.value == nil")
                            return
                        }

                        let json = JSON(value)

                        handler(true, json)

                    }
                }

    }
    
    static func getUserCoinsCount(onSuccess: @escaping (Int) -> (), onError: @escaping (String) -> ()) {
        makeRequest(methodPath: "user/credits", method: .post, onSuccess: { (json) in
            onSuccess(json["total"].intValue)
        }, onError: onError)
    }

//=================================================================================
// MARK: User verification
//=================================================================================


    static func verificationStep1(_ phone: String, handler: @escaping (_ result: AnyObject?, _ error: [String]?) -> Void) {

        // Production
        //
        let url = URL(string: rootUrl + "user/verify")!

        let parameters = [
                "mobile": phone
        ]

        let headers = [
                "Token": Constants.API.accessToken,
                "Content-Type": "application/json",
                "Authorization": "Bearer \(JSONWebToken.sharedInstance.value)"
        ]

        Alamofire
                .request(url,
                        method: .post,
                        parameters: parameters,
                        encoding: JSONEncoding.default,
                        headers: headers)
                .validate()
                .debugRequest(url.absoluteString)
                .responseJSON { response in
                    HTTPResponse
                            .validateStatusCode(response.response?.statusCode)

                    switch response.result {
                    case .failure(let error):
                        OnDebugging.print(error.localizedDescription)
                        handler(nil, [error.localizedDescription])
                    case .success:
                        guard let value = response.result.value else {
                            OnDebugging.print("response.result.value == nil")
                            return
                        }

                        debugPrint(value)

                        let json = JSON(value)
                        let responseDictionary = json.dictionaryValue

                        // posible error
                        if let errorString = responseDictionary["message"]?.stringValue {
                            OnDebugging.print("Error - \(errorString)")
                            handler(nil, ["Error - \(errorString)"])
                            return
                        }

                        if let responseErrors = responseDictionary["errors"]?.dictionaryValue {
                            // posible error
                            OnDebugging.print("Error - \(responseErrors)")
                            handler(nil, ["Error - \(responseErrors)"])
                        } else {
                            handler(nil, nil)
                        }
                    }
                }
    }

    static func verificationStep2(_ code: String, handler: @escaping (_ result: Bool?, _ error: [String]?) -> Void) {

        // Production
        //
        let url = URL(string: rootUrl + "user/verify")!

        let parameters = [
                "code": code
        ]

        let headers = [
                "Token": Constants.API.accessToken,
                "Content-Type": "application/json",
                "Authorization": "Bearer \(JSONWebToken.sharedInstance.value)"
        ]

        Alamofire
                .request(url,
                        method: .put,
                        parameters: parameters,
                        encoding: JSONEncoding.default,
                        headers: headers)
                .validate()
                .debugRequest(url.absoluteString)
                .responseJSON { response in

                    HTTPResponse
                            .validateStatusCode(response.response?.statusCode)

                    switch response.result {
                    case .failure(let error):
                        OnDebugging.print(error.localizedDescription)
                        handler(nil, [error.localizedDescription])
                    case .success:
                        handler(true, nil)
                    }
                }

    }

    static func fetchClientToken(_ eventId: Int!, handler: @escaping (_ success: Bool, _ token: String?) -> Void) {

        //let url = "http://org.maytec.net/customers/c2c/token.php"

        let url = rootUrl + "event/" + String(eventId) + "/payment/token"

        let parameters = ["userId": String(User.current!.id)]

        let headers = [
                "Token": Constants.API.accessToken,
                "Content-Type": "application/json",
                "Authorization": "Bearer \(JSONWebToken.sharedInstance.value)"
        ]

        Alamofire
                .request(url,
                        method: .post,
                        parameters: parameters,
                        encoding: JSONEncoding.default,
                        headers: headers)
                .validate()
                .debugRequest(url)
                .responseJSON { response in

                    switch response.result {

                    case .failure:
                        handler(false, nil)
                        break

                    case .success:

                        // parse json
                        guard let value = response.result.value else {
                            OnDebugging.printError("response.result.value == nil")
                            return
                        }

                        let json = JSON(value)

                        debugPrint(json["clientToken"])

                        handler(true, json["clientToken"].stringValue)

                        break
                    }
                }


    }


    static func postPaymentNonce(_ nonce: String?,
                                 _ eventId: Int!,
                                 _ appointment: String?,
                                 _ participants: Int?,
                                 _ amount: Double?,
                                 handler: @escaping (_ success: Bool?) -> Void) {


        let url = rootUrl + "event/" + String(eventId) + "/payment/verify"

        let parameters: [String: Any] = [
                "nonce": nonce,
                "eventId": eventId,
                "appointment": appointment,
                "participants": participants,
                "amount": amount,
                "userId": String(User.current!.id)
        ]

        let headers = [
                "Token": Constants.API.accessToken,
                "Content-Type": "application/json",
                "Authorization": "Bearer \(JSONWebToken.sharedInstance.value)"
        ]

        Alamofire
                .request(url,
                        method: .post,
                        parameters: parameters,
                        encoding: JSONEncoding.default,
                        headers: headers)
                .validate()
                .debugRequest(url)
                .responseJSON { response in

                    switch response.result {

                    case .failure:
                        handler(false)
                        break

                    case .success:

                        guard let value = response.result.value else {
                            OnDebugging.printError("response.result.value == nil")
                            return
                        }

                        let json = JSON(value)

                        handler(json["success"].boolValue)

                        break
                    }
                }
    }

    static func testNotification() {


        let url = rootUrl + "pushtest/" + String(User.current!.id)

        let headers = [
                "Token": Constants.API.accessToken,
                "Content-Type": "application/json",
                "Authorization": "Bearer \(JSONWebToken.sharedInstance.value)"
        ]

        Alamofire
                .request(url,
                        method: .post,
                        encoding: JSONEncoding.default,
                        headers: headers)
                .validate()
                .debugRequest(url)
                .responseJSON { response in

                    switch response.result {

                    case .failure:

                        break

                    case .success:

                        guard let value = response.result.value else {
                            OnDebugging.printError("response.result.value == nil")
                            return
                        }

                        let json = JSON(value)

                        debugPrint(json)

                        break
                    }
                }


    }

    static func getConfiguration(handler: @escaping (_ env: EnviormentConfiguration?) -> Void) {

        let url = "http://org.maytec.net/customers/c2c/env.php"

        let headers = [
                "Token": Constants.API.accessToken,
                "Content-Type": "application/json",
                "Authorization": "Bearer \(JSONWebToken.sharedInstance.value)"
        ]

        Alamofire
                .request(url,
                        method: .get,
                        headers: headers)
                .validate()
                .debugRequest(url)
                .responseJSON { response in

                    switch response.result {

                    case .failure:

                        handler(EnviormentConfiguration())

                        return

                    case .success:

                        guard let value = response.result.value else {

                            handler(EnviormentConfiguration())

                            return
                        }

                        let json = JSON(value)


                        let config = EnviormentConfiguration()
                        config.amqpEdge1 = json["amqp-edge-1"].stringValue
                        config.amqpEdge2 = json["amqp-edge-2"].stringValue
                        config.amqpPort = json["amqp-port"].intValue
                        config.red5Edge1 = json["red5-edge-1"].stringValue
                        config.red5Edge2 = json["red5-edge-2"].stringValue
                        config.red5BitRate = json["red5-bitrate"].int32Value
                        config.red5Port = json["red5-port"].intValue
                        config.red5fps = json["red5-fps"].int32Value
                        config.red5adaptiveBitRate = json["red5-adaptive"].boolValue
                        config.red5bufferSize = json["red5-buffertime"].floatValue

                        debugPrint(json)


                        handler(config)
                        break
                    }
                }


    }


}
