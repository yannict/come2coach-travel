//
//  PaddedTextField.swift
//  come2coach
//
//  Created by Werner Kratochwil Full on 04.08.16.
//  Copyright © 2016 MaytecNet. All rights reserved.
//

import UIKit

@IBDesignable
open class PaddedTextField: UITextField {
    @IBInspectable open var insetX: CGFloat = 0
    @IBInspectable open var insetY: CGFloat = 0
    
    // placeholder position
    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: insetX , dy: insetY)
    }
    
    // text position
    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: insetX , dy: insetY)
    }
}

