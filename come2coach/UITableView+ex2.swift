//
//  UITableView+ex.swift
//  come2coach
//
//  Created by Marc Ortlieb on 05.08.16.
//  Copyright © 2016 MaytecNet. All rights reserved.
//

import Foundation


protocol Reusable: class {
    static var reuseIdentifier: String { get }
    static var nib: UINib? { get }
}

extension Reusable {
    static var reuseIdentifier: String { return String(describing: self) }
    static var nib: UINib? { return nil }
}



extension UITableView {
    
    
    func dequeuedReusableCell<T: UITableViewCell>(For indexPath: IndexPath) -> T where T: Reusable {
        return self.dequeueReusableCell(withIdentifier: T.reuseIdentifier, for: indexPath) as! T
    }
    func register<T: UITableViewCell>(_: T.Type) where T: Reusable {
        if let nib = T.nib {
            self.register(nib, forCellReuseIdentifier: T.reuseIdentifier)
        } else {
            self.register(T.self, forCellReuseIdentifier: T.reuseIdentifier)
        }
    }
    
    
    
    func dequeuedReusableHeaderFooterView<T: UITableViewHeaderFooterView>() -> T? where T: Reusable {
        return self.dequeueReusableHeaderFooterView(withIdentifier: T.reuseIdentifier) as! T?
    }
    func registerReusableHeaderFooterView<T: UITableViewHeaderFooterView>(_: T.Type) where T: Reusable {
        if let nib = T.nib {
            self.register(nib, forHeaderFooterViewReuseIdentifier: T.reuseIdentifier)
        } else {
            self.register(T.self, forHeaderFooterViewReuseIdentifier: T.reuseIdentifier)
        }
    }
    
    /// let the cells automatically define their height
    
    func selfSizingCells(estimatedRowHeight: CGFloat = 40) {
        self.estimatedRowHeight = estimatedRowHeight
        rowHeight = UITableViewAutomaticDimension
    }
    
}
