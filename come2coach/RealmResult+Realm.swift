//
//  Result+Realm.swift
//  come2coach
//
//  Created by Marc Ortlieb on 23.08.16.
//  Copyright © 2016 MaytecNet. All rights reserved.
//

import Foundation
import RealmSwift


extension RealmResult where T: Realm {
    
    func write(_ f: (Realm)-> Void) -> RealmResult<T> {
        switch self {
        case .Error: return self
        case .success(let realm):
            do {
                try realm.write {
                    f(realm)
                }
            } catch let error as NSError {
                return RealmResult.Error(RealmError.writing(error.localizedDescription))
            }
            return self
        }
    }
    func add(_ persons: [Object], update: Bool = false)  -> RealmResult<T> {
        switch self {
        case .Error: return self
        case .success(let realm):
            do {
                try realm.write {
                    persons.forEach {
                        realm.add($0, update: update)
                    }
                }
            } catch let error as NSError {
                return RealmResult.Error(RealmError.writing(error.localizedDescription))
            }
            return self
        }
    }
    func add(_ person: Object, update: Bool = false)  -> RealmResult<T> {
        switch self {
        case .Error: return self
        case .success(let realm):
            do {
                try realm.write {
                    realm.add(person, update: update)
                }
            } catch let error as NSError {
                return RealmResult.Error(RealmError.writing(error.localizedDescription))
            }
            return self
        }
    }
    func delete(_ person: Object)  -> RealmResult<T> {
        switch self {
        case .Error: return self
        case .success(let realm):
            do {
                try realm.write {
                    realm.delete(person)
                }
            } catch let error as NSError {
                return RealmResult.Error(RealmError.writing(error.localizedDescription))
            }
            return self
        }
    }
    
    func deleteAll() -> RealmResult<T> {
        switch self {
        case .Error: return self
        case .success(let realm):
            do {
                try realm.write {
                    realm.deleteAll()
                    
                }
            } catch let error as NSError {
                return RealmResult.Error(RealmError.writing("Couldn't write realm \(error.localizedDescription)"))
            }
            return self
        }
    }
   
    
    func printErrors() -> RealmResult<T> {
        switch self {
        case .success(_): break
        case .Error(let error):
        
        switch error {
        case RealmError.writing(let errorDescription): OnDebugging.printError("\(RealmError.writing(errorDescription))", title: "ERROR: Realm.printErrors()")
        case RealmError.creatingInstance: OnDebugging.printError("Could not create Realm instance", title: "ERROR: Realm.printErrors()")
        default: OnDebugging.printError("ERROR: Realm: [\(error)]", title: "ERROR: Realm.printErrors()")
            }
        }
        return self
    }
}







