//
//  UIView+Tools.m
//  MaytecNet
//
//  Created by Werner Kratochwil on 16.06.15.
//  Copyright (c) 2015 MaytecNet All rights reserved.
//

#import "UIView+Tools.h"

@implementation UIView (Tools)

- (void)pin2SuperViewWithAttribute:(NSLayoutAttribute)attribute
{
    [self pin2SuperViewWithAttribute:attribute distance:0.0f];
}

- (void)pin2SuperViewWithAttribute:(NSLayoutAttribute)attribute distance:(CGFloat)distance
{
    [self pin2View:self.superview withAttribute:attribute distance:distance];
}

- (void)pin2View:(UIView *)otherView withAttribute:(NSLayoutAttribute)attribute
{
    [self pin2View:otherView withAttribute:attribute distance:0.0f];
}

- (void)pin2View:(UIView *)otherView withAttribute:(NSLayoutAttribute)attribute
  otherAttribute:(NSLayoutAttribute)otherAttribute
{
    [self pin2View:otherView withAttribute:attribute otherAttribute:otherAttribute distance:0.0f];
}

- (void)pin2View:(UIView *)otherView withAttribute:(NSLayoutAttribute)attribute
  otherAttribute:(NSLayoutAttribute)otherAttribute distance:(CGFloat)distance
{
    [self.superview addConstraint:[NSLayoutConstraint constraintWithItem:self
                                                               attribute:attribute
                                                               relatedBy:NSLayoutRelationEqual
                                                                  toItem:otherView
                                                               attribute:otherAttribute
                                                              multiplier:1.0f
                                                                constant:distance]];
    
}

- (void)pin2View:(UIView *)otherView withAttribute:(NSLayoutAttribute)attribute distance:(CGFloat)distance
{
    [self.superview addConstraint:[NSLayoutConstraint constraintWithItem:self
                                                               attribute:attribute
                                                               relatedBy:NSLayoutRelationEqual
                                                                  toItem:otherView
                                                               attribute:attribute
                                                              multiplier:1.0f
                                                                constant:distance]];
}


- (void)pinAllEdges2View:(UIView *)otherView
{
    [self pin2View:otherView withAttribute:NSLayoutAttributeBottom];
    [self pin2View:otherView withAttribute:NSLayoutAttributeTop];
    [self pin2View:otherView withAttribute:NSLayoutAttributeLeading];
    [self pin2View:otherView withAttribute:NSLayoutAttributeTrailing];
}

@end
