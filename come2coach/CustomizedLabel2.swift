//
//  CustomizedLabel2.swift
//  come2coach
//
//  Created by Bernd Freier on 12.12.16.
//  Copyright © 2016 MaytecNet. All rights reserved.
//

import Foundation

open class C2CLabel2: UILabel {

    func letterSpace(_ text: String?) {
        if let t = text {
            let strA = NSMutableAttributedString(string: t)
            try? {
                strA.addAttribute(NSKernAttributeName, value: CGFloat(1.1), range: NSRange(location: 0, length: t.characters.count))
                self.attributedText = strA;
            }
        }

    }

    override open func awakeFromNib() {
        letterSpace(self.text)

    }

    override init(frame: CGRect) {
        super.init(frame: frame)
    }

    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }

    override open var text: String? {
        didSet {
            letterSpace(text);
        }
    }
}
