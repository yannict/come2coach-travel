//
//  CoachingEventUpcomingCell.swift
//  come2coach
//
//  Created by Timotheus Laubengaier on 24.04.17.
//  Copyright © 2017 MaytecNet. All rights reserved.
//

import Foundation
import UIKit

protocol EventCellDelegate {
  func likeButtonPressed(cell: EventCell)
}

class EventCell: UICollectionViewCell {
  
  var delegate: EventCellDelegate?
  
  var event: Event? {
    didSet {
      titleLabel.text = event?.title
      if let imageUrl = event?.imageURL {
        imageView.sd_setImage(with: imageUrl)
      }
      if let testImageUrl = URL(string: event!.tmpImageUrl) {
        imageView.sd_setImage(with: testImageUrl)
      }
      if let userImageUrl = event?.userImageURL {
        coachImageView.sd_setImage(with: userImageUrl)
      }
      if let testUserImageUrl = URL(string: event!.tmpUserImageUrl) {
        coachImageView.sd_setImage(with: testUserImageUrl)
      }
      print("Event: \(event!.id) is liked = \(event!.isLiked)")
      likeButton.isSelected = event!.isLiked
    }
  }
  
  var isSmallCard: Bool = false {
    didSet {
      titleLabel.font = UIFont.systemFont(ofSize: 11)
      subTitleLabel.font = UIFont.systemFont(ofSize: 11)
    }
  }
  
  let imageView: UIImageView = {
    let view = UIImageView()
    view.contentMode = .scaleAspectFill
    view.clipsToBounds = true
    view.backgroundColor = Styleguide.Color.lightGray
    return view
  }()
  
  let coachImageView: UIImageView = {
    let view = UIImageView()
    view.contentMode = .scaleAspectFill
    view.backgroundColor = Styleguide.Color.tint
    view.clipsToBounds = true
    view.layer.cornerRadius = 25
    return view
  }()
  
  let titleLabel: UILabel = {
    let label = UILabel()
    label.textColor = Styleguide.Color.text
    label.font = Styleguide.Font.regular(size: 16)
    label.text = "Hidden Beach in Malaysia"
    return label
  }()
  
  let subTitleLabel: UILabel = {
    let label = UILabel()
    label.textColor = Styleguide.Color.text
    label.font = Styleguide.Font.light(size: 13)
    label.text = "01.01.18 | Livestream"
    return label
  }()
  
  let likeButton: UIButton = {
    let button = UIButton(type: .system)
    button.tintColor = Styleguide.Color.secondary
    button.setImage(#imageLiteral(resourceName: "icon_like_empty"), for: .normal)
    button.setImage(#imageLiteral(resourceName: "icon_liked"), for: .selected)
    button.imageEdgeInsets = UIEdgeInsetsMake(2, 2, 2, 2)
    button.imageView?.contentMode = .scaleAspectFit
    button.imageView?.clipsToBounds = true
    return button
  }()
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    
    backgroundColor = .white
    layer.cornerRadius = Styleguide.Roundness.main
    clipsToBounds = true
    
    addSubview(imageView)
    imageView.snp.makeConstraints { (make) in
      make.top.equalTo(self)
      make.leading.equalTo(self)
      make.trailing.equalTo(self)
      make.height.equalTo(self).multipliedBy(0.7)
    }
    
    addSubview(coachImageView)
    coachImageView.snp.makeConstraints { (make) in
      make.bottom.equalTo(imageView.snp.bottom).offset(15)
      make.trailing.equalTo(self).offset(-10)
      make.width.equalTo(50)
      make.height.equalTo(50)
    }
    
    likeButton.addTarget(self, action: #selector(likeButtonPressed), for: .touchUpInside)
    addSubview(likeButton)
    likeButton.snp.makeConstraints { (make) in
      make.trailing.equalTo(coachImageView.snp.trailing)
      make.top.equalTo(coachImageView.snp.bottom).offset(0)
      make.width.equalTo(44)
      make.height.equalTo(44)
    }
    
    addSubview(titleLabel)
    titleLabel.snp.makeConstraints { (make) in
      make.top.equalTo(imageView.snp.bottom).offset(20)
      make.leading.equalTo(self).offset(15)
      make.trailing.equalTo(likeButton.snp.leading).offset(5)
    }
    
    addSubview(subTitleLabel)
    subTitleLabel.snp.makeConstraints { (make) in
      make.top.equalTo(titleLabel.snp.bottom).offset(0)
      make.leading.equalTo(titleLabel.snp.leading)
      make.trailing.equalTo(likeButton.snp.leading).offset(5)
    }
    
  }
  
  func likeButtonPressed() {
    delegate?.likeButtonPressed(cell: self)
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
}
