//
//  LivestreamingParticipantVC.swift
//  come2coach
//
//  Copyright © 2017 Maytec.net. All rights reserved.
//

import UIKit

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func <<T:Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l < r
    case (nil, _?):
        return true
    default:
        return false
    }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func ><T:Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l > r
    default:
        return rhs < lhs
    }
}


class LivestreamingParticipantVC: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var tfMessage: UITextField!
    @IBOutlet weak var labLastMessage: UILabel!
    @IBOutlet weak var constraintKeyboard: NSLayoutConstraint!
    @IBOutlet weak var vVisibleScreen: UIView!
    @IBOutlet weak var btSend: UIButton!
    @IBOutlet weak var lblStatus: UILabel!

    @IBOutlet weak var headerTitleText: C2CLabel!
    @IBOutlet weak var timerLabel: UILabel!
    @IBOutlet weak var circleView: CircleLoaderView!
    
    @IBOutlet weak var countDownView: UIView!
    
    
    // live stream view
    var subscriber: ParticipantSubscribeVC

    // live stream name
    var streamName: String?

    // event data
    var event: Event?
    var env: EnviormentConfiguration?
    var eventStatus: EventStatus?

    // amqp Client - handles messaging
    var amqpClient: AMQPStreamingHandler?

    // handle chat bubble in view
    var bubbleCtrl: bubbleViewController?

    var playRecorded: Bool = false

    required init?(coder aDecoder: NSCoder) {

        subscriber = Storyboards.ViewControllers.participantSubsribe
        super.init(coder: aDecoder)

        subscriber.statusCallback = { [weak self] (statusCode: Int32, message: String?) -> Void in

            if let m = message {
                self?.displayStatusMessage(m)
            }

        }
    }


    override func viewDidLoad() {

        super.viewDidLoad()

        // setup UX

        bubbleCtrl = bubbleViewController(self)

        btSend.isEnabled = false

        self.tfMessage.delegate = self

        // setup gestures

        let backgroundTap = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        vVisibleScreen += backgroundTap


    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        AMQPstart()

        if (!playRecorded) {

            eventStatus = LiveStreamingHandler.getEventStatus(event!)

            // check view mode (countdown or stream)
            if let status = eventStatus {

                if (!status.pending && status.running) {
                    headerTitleText.text = ""
                    switchCountDown(true)

                    LSstart()

                } else if (status.pending) {
                    startCountDown()
                } else {

                    self.present(UIAlertController("Schon vorbei.".localized, "Das Coaching ist leider schon vorbei.".localized,
                            actions: UIAlertAction(.OK)), animated: true)
                }
            }
        } else {

            // video stream (VOD)

            headerTitleText.text = "VIDEOSTREAM"
            switchCountDown(true)

            LSstart()

        }

    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        // setup live stream controller
        subscriber.env = env

        startObservingKeyboard()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        // stop countdown timer, if needed
        if (isRunning) {
            countDownTimer.invalidate()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    func switchCountDown(_ hidden: Bool) {
        countDownView.isHidden = hidden
    }

    // MARK: Count Down

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        circleView.setCircleRadius(circleView.bounds.width / 2)
    }

    fileprivate let totalSeconds = CGFloat(60 * 60)
    fileprivate var secRemaining = 0
    fileprivate var countDownTimer = Timer()
    fileprivate var isRunning = false

    fileprivate var progress: CGFloat {
        let elapsedSec = totalSeconds - CGFloat(secRemaining)
        return
        elapsedSec / totalSeconds
    }
    fileprivate var timerLabelText: String {
        let min = secRemaining / 60
        let sec = secRemaining >= 60 ? (secRemaining % 60) : secRemaining
        return
        String(format: "%02d:%02d", min, sec)
    }
    var isValidEvent: Bool {
        if let e = eventStatus {

            guard let secStrt = e.currentDate!.secTillEventStart else {
                return false
            }

            return secStrt > 0 ? true : false

        } else {
            return false
        }
    }

    func startCountDown() {

        if isValidEvent {

            // show timer
            switchCountDown(false)

            guard let e = eventStatus else { return }

            if let secTillEventStart = e.currentDate!.secTillEventStart {

                secRemaining = secTillEventStart > 0 ? secTillEventStart : 0

                isRunning = true

                headerTitleText.text = "COACHING COUNTDOWN".localized

                countDownTimer =
                        Timer.scheduledTimer(timeInterval: 1,
                                target: self,
                                selector: #selector(updateProgressView),
                                userInfo: nil,
                                repeats: true)
            }
        }

    }
    @objc
    fileprivate func updateProgressView() {

        debugPrint("hit timer")

        if let e = eventStatus {

            if let secTillEventStart = e.currentDate!.secTillEventStart {
                secRemaining = secTillEventStart > 0 ? secTillEventStart : 0
                circleView.progress = progress
                timerLabel.text = timerLabelText

                if secRemaining <= 0 && isRunning {

                    isRunning = false

                    // stop timer
                    countDownTimer.invalidate()

                    // show timer
                    switchCountDown(true)

                    /*

                      SESSION STARTS

                     */

                    headerTitleText.text = ""

                    LSstart()


                }
            }

        }


    }



    // MARK: Live Streaming

    func AMQPstart() {

        // start amqp connection
        if let u = User.current {
            if let e = self.event {

                amqpClient = AMQPStreamingHandler()
                amqpClient?.userId = String(u.id) // "user-" + String(u.id)
                amqpClient?.eventId = String(e.id)
                amqpClient?.connect(env!, delegate: self) // .user("user-" + String(u.id), String(e.id))

                //amqpClient?.sendEnteredRoom("user-" + String(u.id), "coach-" + String(e.id), u.name, u.lname)

                amqpClient?.sendEnteredRoom("user-" + String(u.id), AMQPStreamingHandler.getCoachRoutingKey(event!) , u.name, u.lname)

                btSend.isEnabled = true
            }

        }

    }

    // Subscribe LiveStream
    func LSstart() {

        // specify stream to display



        if (playRecorded) {

            if (!event!.recordedVideoFile.isEmpty)
            {
                streamName = event!.recordedVideoFile // (event == nil) ? "c2c" : "c2c\(event!.formerId).flv"
            } else {
                streamName = (event == nil) ? "c2c" : "c2c\(event!.id).flv"
            }

        } else {
            streamName = (event == nil) ? "c2c" : "c2c\(event!.id)"
        }

        debugPrint("Play stream " + streamName!)

        if let _streamName = streamName {

            self.subscriber.startStreamWithName(_streamName)

            self.view.addSubview(subscriber.view)
            self.view.sendSubview(toBack: subscriber.view)

        }
    }

    // MARK: View actions

    @IBAction func didPressLeave(_ sender: AnyObject) {

        let alertTitle = event!.didEnd ? "Willst du das Webinar jetzt wirklich verlassen?".localized : "Willst du das Webinar jetzt wirklich verlassen?".localized

        let alertViewController =
                UIAlertController(title: "Verlassen", message: alertTitle, preferredStyle: .actionSheet)
        alertViewController.addAction(UIAlertAction(title: "Ja".localized, style: .default, handler: { (action: UIAlertAction!) in
            self.leaveWebinar()
        }))

        alertViewController.addAction(UIAlertAction(title: "Oops, doch nicht".localized, style: .default, handler: { (action: UIAlertAction!) in
        }))

        self.present(alertViewController, animated: true)

    }


    @IBAction func didPressSendMessage(_ sender: AnyObject) {

        //close keyboard after writing question
        hideKeyboard()

        sendMessage()

    }


    func sendMessage() {
        if let msg = self.tfMessage.text {

            if (!msg.isEmpty) {
                amqpClient?.sendMessage(self.tfMessage.text)

                //   bubbleCtrl?.addChatBubble(data: bubbleData(text: self.tfMessage.text, image: nil, date: NSDate(), type: .mine))

                self.tfMessage.text = ""
            }

        }
    }

    func leaveWebinar() {

        // release observers
        stopObservingKeyboard()

        // send exit room notification to coach
        if let u = User.current {
            if let e = self.event {
                amqpClient?.sendExitRoom("user-" + String(u.id), AMQPStreamingHandler.getCoachRoutingKey(event!) , u.name, u.lname)
            }
        }

        // stop streaming
        subscriber.stop()

        // remove live stream from background
        subscriber.view.removeFromSuperview()

        // close amqp connection
        amqpClient?.close()

        self.dismiss(animated: true, completion: nil)

    }

    // invoked, when app enters background
    func didEnterBackground() {


        // release observers
        // stopObservingKeyboard()

        // send exit room notification to coach
        if let u = User.current {
            if let e = self.event {
                amqpClient?.sendExitRoom("user-" + String(u.id), AMQPStreamingHandler.getCoachRoutingKey(event!) , u.name, u.lname)
            }
        }

        // stop streaming
        subscriber.stop()

        // remove live stream from background
        subscriber.view.removeFromSuperview()

        // close amqp connection
        amqpClient?.close()


        //stopWebinar()
        //hideSubscriberView()
    }


    // invoked, when app enters foreground
    func willEnterForeground() {

        LSstart()

        //if let currStreamName = streamName {
        //    showSubscriberView()
        //}
    }

    // MARK: Keyboard functions

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        sendMessage()
        return false
    }

    func startObservingKeyboard() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)

    }

    func stopObservingKeyboard() {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }

    func keyboardWillChange(_ notification: Notification) {

        let info = notification.userInfo
        let kbFrame = info?[UIKeyboardFrameEndUserInfoKey]
        let keyboardFrame = (kbFrame as AnyObject).cgRectValue

        var height: CGFloat = 0.0
        if (keyboardFrame != nil) {
            height = (keyboardFrame!.size.height) + 5.0
        }

        updateAndApplyKeyboardConstraintTo(height, withNotification: notification)

    }

    func keyboardWillShow(_ notification: Notification) {

        bubbleCtrl?.hideBubbles()
    }

    func keyboardWillHide(_ notification: Notification) {
        updateAndApplyKeyboardConstraintTo(0, withNotification: notification)

        bubbleCtrl?.showBubbles()
    }

    func hideKeyboard() {
        self.tfMessage.resignFirstResponder()
    }

    func updateAndApplyKeyboardConstraintTo(_ height: CGFloat, withNotification notification: Notification) {
        let info = notification.userInfo
        var animationDuration = (info?[UIKeyboardAnimationDurationUserInfoKey] as AnyObject).doubleValue

        if (animationDuration == nil || animationDuration == 0) {
            animationDuration = 0.5
        }

        constraintKeyboard?.constant = height
        //self.vVisibleScreen.addConstraint(constraintKeyboard)
        if (isViewLoaded) {
            self.vVisibleScreen.setNeedsUpdateConstraints()
            UIView.animate(withDuration: animationDuration!, animations: {
                self.vVisibleScreen.layoutIfNeeded()
            })
        }
    }

    func displayStatusMessage(_ message: String) {
        DispatchQueue.main.async(execute: { [weak self] in
            self?.lblStatus.text = message
            UIView.animate(withDuration: 0.5, animations: { [weak self] in
                self?.lblStatus.alpha = 1.0
            }, completion: { (_) in
                UIView.animate(withDuration: 0.5, delay: 5.0, options: UIViewAnimationOptions.curveEaseIn, animations: { [weak self] in
                    self?.lblStatus.alpha = 0.0
                }, completion: nil)
            })
        })

    }


}


extension LivestreamingParticipantVC: AMQPDelegate {

    func process(_ type: String, _ data: String) {

        switch type {

        case "chat":

            let msg = msgChat(json: data)

            DispatchQueue.main.async {
                self.bubbleCtrl?.addChatBubble(data: bubbleData(text: msg.msg, headerText: msg.user, image: nil, date: NSDate(), type: .mine))

            }
            break

        default: return
        }

    }

}
