//
// Created by Bernd Freier on 17.01.17.
// Copyright (c) 2017 MaytecNet. All rights reserved.
//

import Foundation

enum BubbleDataType: Int{
    case mine = 0
    case opponent
}

class bubbleData {

    var text: String?
    var headerText: String?
    var image: UIImage?
    var date: NSDate?
    var type: BubbleDataType

    init(text: String?, headerText: String?, image: UIImage?,date: NSDate? , type:BubbleDataType = .mine) {
        // Default type is Mine
        self.text = text
        self.image = image
        self.date = date
        self.type = type
        self.headerText = headerText
    }
}

