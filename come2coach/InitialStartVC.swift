//
//  InitialStartVC.swift
//  come2coach
//
//  Created by Marc Ortlieb on 23.08.16.
//  Copyright © 2016 MaytecNet. All rights reserved.
//

import UIKit


class InitialStartVC: UIViewController {
    
    // -------------------------------------------------------------------------------------
    // MARK: Handle Internet | add internet observer
    // Ticket: https://www.evernote.com/l/AWoI8QFZsxRHtKzGry5g6opO6fcwcPUR1so
    // -------------------------------------------------------------------------------------
    
    var InternetConnectionStateDidChangeOnlineObserverToken: String?
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.hideBackButton()

        /*  #inetmanager
        guard InternetConnectionManager.sharedInstance.isConnected == true else
        {
            // wenn keine Internet-Verbindung zu den Coachings weiterleiten
            self.show(Storyboards.ViewControllers.coachings)
            return
        }
        */


    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        /*  #inetmanager
        if let token = InternetConnectionStateDidChangeOnlineObserverToken {
            InternetConnectionManager.sharedInstance.removeObserver(with: token)
        }
        */
    }
    // -------------------------------------------------------------------------------------
    
    
//    override func viewDidAppear(animated: Bool) {
//        super.viewDidAppear(animated)
//        EventManager.sharedInstance.processEventsSequence(on: self)
//        
//       
//    }
    
    
    
}







