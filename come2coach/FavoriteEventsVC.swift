//
//  FavoriteEventsVC.swift
//  come2coach
//
//  Created by Victor on 01.11.16.
//  Copyright © 2016 MaytecNet. All rights reserved.
//

import Foundation
import MessageUI
import SVProgressHUD

class FavoriteEventsVC: UIViewController {

    @IBOutlet weak var eventsTableView: UITableView!
    @IBOutlet weak var tableTitleLabel: UILabel!

    @IBOutlet weak var favoriteHeader: UIView!
    
    
    // static var showLikedEventsOnStart = true
    var isLiked = true
    var likedEvents: [Event]! = [Event]()
    var joinedEvents: [Event]! = [Event]()

    var likedPage = 0
    var likedCount = 0
    var joinedPage = 0
    var joinedCount = 0
    @IBOutlet weak var toggleButton: UISegmentedControl!

    //MARK: Life cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        // isLiked = FavoriteEventsVC.showLikedEventsOnStart

        let tFont = NSDictionary(object: Styleguide.Color.text, forKey: NSForegroundColorAttributeName as NSCopying)
        let tFont2 = NSDictionary(object: UIColor.white, forKey: NSForegroundColorAttributeName as NSCopying)
        toggleButton.setTitleTextAttributes(tFont2 as! [AnyHashable: Any], for: .selected)
        toggleButton.setTitleTextAttributes(tFont as! [AnyHashable: Any], for: UIControlState())

        toggleButton.layer.cornerRadius = 10.0;
        toggleButton.layer.borderColor = Styleguide.Color.text.cgColor
        toggleButton.layer.borderWidth = 1.0;
        toggleButton.layer.masksToBounds = true;

        eventsTableView.tableFooterView = UIView.init(frame: CGRect.zero)
        eventsTableView.reloadData()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableTitleLabel.text = isLiked ? "LIEBLINGS - COACHINGS".localized : "GEBUCHTE COACHINGS".localized
        isLiked ? getAllLikedEvents() : getAllJoinedEvents()

        // Protocol actions
        ProtocolEventRequest.showEvent = { (eventId) in

            HTTPRequest.fetchEvent(with: eventId) { (event) in

                ProtocolEventRequest.eventId = nil

                let detailsVC = Storyboards.ViewControllers.eventDetails
                detailsVC.mode = "normal"
                detailsVC.event = event

                self.navigationController?.push(detailsVC, animated: true, completion: nil)

            }

        }

    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //FavoriteEventsVC.showLikedEventsOnStart = isLiked
    }

    //MARK: Actions

    @IBAction func switchDisplayMode() {
        isLiked = !isLiked
        tableTitleLabel.text = isLiked ? "LIEBLINGS - COACHINGS".localized : "GEBUCHTE COACHINGS".localized
        isLiked ? getAllLikedEvents() : getAllJoinedEvents()
        eventsTableView.reloadData()
    }

    func getLikedEvents(_ page: Int, perPage: Int) {
        HTTPRequest.fetchLikedEvents(page, perPage: perPage) { (events, page, count) in
            self.likedCount = count
            self.likedPage = page
            self.likedEvents.append(contentsOf: events)
            self.eventsTableView.reloadData()
        }
    }

    func getAllLikedEvents() {
        HTTPRequest.fetchLikedEvents(0, perPage: (likedPage + 1) * HTTPRequest.coachingsPerPage) { (events, page, count) in
            self.favoriteHeader.isHidden = count == 0
            self.likedCount = count
            self.likedEvents = events
            self.eventsTableView.reloadData()
        }
    }

    func getJoinedEvents(_ page: Int, perPage: Int) {

        HTTPRequest.fetchJoinedEvents(page, perPage: perPage) { (events, page, count) in
            self.joinedCount = count
            self.joinedPage = page
            self.joinedEvents.append(contentsOf: events)
            self.eventsTableView.reloadData()
        }

    }

    func getAllJoinedEvents() {

        HTTPRequest.fetchJoinedEvents(0, perPage: (joinedPage + 1) * HTTPRequest.coachingsPerPage) { (events, page, count) in
            self.favoriteHeader.isHidden = count == 0
            self.joinedCount = count
            self.joinedEvents = events
            self.eventsTableView.reloadData()
        }


    }

}

extension FavoriteEventsVC : MFMailComposeViewControllerDelegate {

    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }

}

//MARK: EventTableCellDelegate

extension FavoriteEventsVC: EventTableCellDelegate {

    func eventTableCell(_ cell: EventTableCell, didLikeEvent like: Bool, eventWithId eventId: Int) {
        if like == true {
            HTTPRequest.likeEvent(with: eventId) { (success) in
                if success == true {
                    for event in self.likedEvents {
                        if event.id == eventId {
                            event.isLiked = like
                            event.isWatching = like
                        }
                    }
                    self.eventsTableView.reloadData()
                }
            }
        } else {
            HTTPRequest.unlikeEvent(with: eventId) { (success) in
                if success == true {
                    for event in self.likedEvents {
                        if event.id == eventId {
                            event.isLiked = like
                            event.isWatching = like
                        }
                    }

                    self.eventsTableView.reloadData()
                }
            }
        }
    }

    func sendMail(_ event: Event) {


        SVProgressHUD.show()

        // get up-to-date event data
        HTTPRequest.fetchEvent(with: event.id) { (e) in

            SVProgressHUD.dismiss()

            let mailComposerVC = MFMailComposeViewController()
            mailComposerVC.mailComposeDelegate = self

            mailComposerVC.setToRecipients([e.coachEMail])
            mailComposerVC.setSubject("Hi " + e.coachFirstName)
            mailComposerVC.navigationBar.tintColor = UIColor.black

            if MFMailComposeViewController.canSendMail() {
                self.present(mailComposerVC, animated: true, completion: nil)
            } else {
                self.present(UIAlertController("Upps".localized, "Die Mail konnte nicht gesendet werden.".localized,
                        actions: UIAlertAction(.OK)), animated: true)
            }

        }

    }


    func liveHandler(_ event: Event) {

        let ls = LiveStreamingHandler()
        ls.launch(event.id, controller: self)

    }
}

//MARK: UITableViewDataSource

extension FavoriteEventsVC: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isLiked == true {
            return likedEvents.count
        } else {
            return joinedEvents.count
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.ViewControllers.eventTableCell, for: indexPath) as! EventTableCell
        cell.cleanInfo()
        cell.delegate = self
        let event = isLiked == true ? likedEvents[indexPath.row] : joinedEvents[indexPath.row]
        cell.event = event
        if isLiked == true {
            cell.fillEventInfo(likedEvents[indexPath.row], isLiked: isLiked)
            if indexPath.row == (likedPage + 1) * HTTPRequest.coachingsPerPage - 1 {
                getLikedEvents(likedPage + 1, perPage: HTTPRequest.coachingsPerPage)
            }
        } else {
            cell.fillEventInfo(joinedEvents[indexPath.row], isLiked: isLiked)
            if indexPath.row == (joinedPage + 1) * HTTPRequest.coachingsPerPage - 1 {
                getJoinedEvents(joinedPage + 1, perPage: HTTPRequest.coachingsPerPage)
            }
        }

        return cell
    }

}

//MARK: UITableViewDelegate

extension FavoriteEventsVC: UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let detailsVC = Storyboards.ViewControllers.eventDetails
        detailsVC.mode = isLiked ? "normal" : "joined"
        detailsVC.event = isLiked ? likedEvents[indexPath.row] : joinedEvents[indexPath.row]
        self.navigationController?.push(detailsVC, animated: true, completion: nil)
    }

}
