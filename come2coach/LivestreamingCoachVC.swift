//
//  LivestreamingCoachVC.swift
//  come2coach
//
//  Copyright © 2017 Maytec.net. All rights reserved.
//

import UIKit
import Foundation
import RealmSwift
import Unbox

class LivestreamingCoachVC: UIViewController, UITextFieldDelegate {

    let screenSize: CGRect = UIScreen.main.bounds
    let slideDuration = 0.7

    @IBOutlet weak var constraintKeyboard: NSLayoutConstraint!
    var publisher: CoachPublishVC
    var isPublishing = false
    var questions: Results<Question>?

    @IBOutlet weak var vVisibleScreen: UIView!
    var event: Event?
    var streamName: String?
    var env: EnviormentConfiguration?
    var eventStatus: EventStatus?
    var bubbleCtrl: bubbleViewController?


    var amqpClient: AMQPStreamingHandler?
    var users: [String: msgRoom] = [:]

    fileprivate let lockQuestions = NSLock()
    @IBOutlet weak var vParticipants: UIView!
    @IBOutlet weak var vQuestions: UIView!
    @IBOutlet weak var btShowQuestions: UIButton!
    @IBOutlet weak var btShowParticipants: UIButton!

    @IBOutlet weak var btLeaveLiveStreaming: UIButton!

    @IBOutlet weak var questionsTableView: UITableView!
    @IBOutlet weak var participantsTableView: UITableView!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblTitleParticipants: UILabel!
    @IBOutlet weak var lblTitleQuestions: UILabel!

    @IBOutlet weak var constrBottomParticipantView: NSLayoutConstraint!
    @IBOutlet weak var constrBottomQuestionView: NSLayoutConstraint!

    @IBOutlet weak var headerTitleText: C2CLabel!

    @IBOutlet weak var timerLabel: UILabel!

    @IBOutlet weak var tfMessage: PaddedTextField!
    @IBOutlet weak var btnSend: UIButton!
    @IBOutlet weak var circleView: CircleLoaderView!

    /*
    // MARK: - Navigation

    */
    @IBOutlet weak var btnChatButton: UIButton!

    @IBOutlet weak var btnParticipants: UIButton!

    @IBOutlet weak var btnCamera: UIButton!

    @IBAction func didPressCameraChange(_ sender: Any) {

        changeCamera()
    }

    @IBAction func didPressLeave(_ sender: AnyObject) {

        let alertTitle = (event!.didEnd) ? "Willst du das Webinar jetzt wirklich verlassen?".localized : "Willst du das Webinar jetzt wirklich verlassen?".localized
        let alertViewController =
                UIAlertController(title: "Beenden".localized, message: alertTitle, preferredStyle: .actionSheet)
        alertViewController.addAction(UIAlertAction(title: "Ja".localized, style: .default, handler: { _ in
            OnDebugging.print("Handle Ok logic here")
            self.leaveWebinar()
        }))

        alertViewController.addAction(UIAlertAction(title: "Oops, doch nicht".localized, style: .default, handler: { (action: UIAlertAction!) in
            OnDebugging.print("Handle Cancel logic here")
        }))

        self.present(alertViewController, animated: true)
        OnDebugging.print("Leave")
    }

    @IBAction func didPressShowUserMessages(_ sender: AnyObject) {

        // set chat icon
        if let image = UIImage(named: "icon-chat") {
            btnChatButton.setImage(image, for:UIControlState.normal)
        }

        vParticipants.isHidden = true

        slideInView(vQuestions, withBottomConstraint: constrBottomQuestionView)


    }

    @IBAction func didPressHideUserMessages(_ sender: AnyObject) {


        slideOutView(vQuestions, withBottomConstraint: constrBottomQuestionView)
    }

    @IBAction func didPressShowParticipants(_ sender: AnyObject) {

        // set chat icon
        if let image = UIImage(named: "icon-user") {
            btnParticipants.setImage(image, for:UIControlState.normal)
        }

        vQuestions.isHidden = true

        slideInView(vParticipants, withBottomConstraint: constrBottomParticipantView)
    }

    @IBAction func didPressHideParticipants(_ sender: AnyObject) {

        slideOutView(vParticipants, withBottomConstraint: constrBottomParticipantView)
    }

    func changeCamera() {

        DispatchQueue.main.async(execute: {
            self.publisher.changeVideoStreamSource()
        })

    }

    override func viewDidLoad() {
        super.viewDidLoad()

        //let frameSize = self.view.bounds
        //publisher.view.layer.frame = frameSize

        view.backgroundColor = UIColor.black

        bubbleCtrl = bubbleViewController(self)

        btnSend.isEnabled = false

        self.tfMessage.delegate = self

        //LSinit()
        publisher.view.pinAllEdges2View(self.view)

        if let _event = event, let realm = try? Realm() {
            self.lockQuestions.lock()
            questions = realm.objects(Question.self)
                    .filter("eventID = \(_event.id)")
                    .sorted(byKeyPath: "timestamp", ascending: false)
            self.lockQuestions.unlock()
            self.updateQuestionCount()
        } else {
            //btShowQuestions.setTitle("", for: UIControlState())
        }

        // MARK: QuestionsTableview - Setup

        questionsTableView.dataSource = self
        questionsTableView.selfSizingCells()
        questionsTableView.cellLayoutMarginsFollowReadableWidth = false
        //avoid lines of empty rows
        questionsTableView.tableFooterView = UIView()

        // blur effect for QuestionsTableView
        //vQuestions.backgroundColor = UIColor.clear

        /*
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.light)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        //always fill the view
        blurEffectView.frame = self.view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        vQuestions.addSubview(blurEffectView)
        vQuestions.sendSubview(toBack: blurEffectView)
        */

        // MARK: ParticipantTableView

        participantsTableView.dataSource = self
        participantsTableView.selfSizingCells()
        participantsTableView.cellLayoutMarginsFollowReadableWidth = false
        //avoid lines of empty rows
        participantsTableView.tableFooterView = UIView()


        let backgroundTap = UITapGestureRecognizer(target: self, action: #selector(hideAll))
        publisher.view += backgroundTap
        //vVisibleScreen += backgroundTap


        let CameraTap = UITapGestureRecognizer(target: self, action: #selector(LivestreamingCoachVC.changeCamera));
        CameraTap.numberOfTapsRequired = 2
        publisher.view += CameraTap
        //vVisibleScreen += CameraTap


    }


    required init?(coder aDecoder: NSCoder) {

        publisher = Storyboards.ViewControllers.coachPublish

        super.init(coder: aDecoder)

        publisher.statusCallback = { [weak self] (statusCode: Int32, message: String?) -> Void in

            OnDebugging.print("Status in Livestreaming Coach: \(message)")
            if let m = message {
                self?.displayStatusMessage(m)
            }
        }


    }

    override func viewDidAppear(_ animated: Bool) {

        super.viewDidAppear(animated)

        /*

        moved to lifetime

        // start live streaming
        self.view.addSubview(publisher.view)
        self.view.sendSubview(toBack: publisher.view)

        */


        /*
        // start amqp connection
        if let e = self.event {

            amqpClient = AMQPStreamingHandler()
            amqpClient?.connect(delegate: self).user("coach-" + String(e.id), String(e.id))

        }
        */

        eventStatus = LiveStreamingHandler.getEventStatus(event!)

        // start stream
        AMQPstart()

        //LSstart()

        // check view mode (countdown or stream)
        if let status = eventStatus {

            if (!status.pending && status.running) {

                LSstart("live")

                headerTitleText.text = ""
                switchTopButtons(false)
                switchCountDown(true)
                displayStatusMessage("Dein Coaching ist live...")

            } else if (status.pending) {

                LSstart("countdown")

                startCountDown()

            } else {

                LSstart("live")

                self.present(UIAlertController("Schon vorbei.".localized, "Das Coaching ist leider schon vorbei.".localized,
                        actions: UIAlertAction(.OK)), animated: true)
            }
        }

    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        startObservingKeyboard()
    }


    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        // stop countdown timer, if needed
        if (isRunning) {
            countDownTimer.invalidate()
        }


    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        isPublishing = false
    }

    func switchTopButtons(_ hidden: Bool) {

        btnChatButton.isHidden = hidden
        btnParticipants.isHidden = hidden
        btnCamera.isHidden = hidden

    }

    func switchCountDown(_ hidden: Bool) {
        circleView.isHidden = hidden
    }

    // MARK: Count Down

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        circleView.setCircleRadius(circleView.bounds.width / 2)
    }

    fileprivate let totalSeconds = CGFloat(60 * 60)
    fileprivate var secRemaining = 0
    fileprivate var countDownTimer = Timer()
    fileprivate var isRunning = false

    fileprivate var progress: CGFloat {
        let elapsedSec = totalSeconds - CGFloat(secRemaining)
        return
        elapsedSec / totalSeconds
    }
    fileprivate var timerLabelText: String {
        let min = secRemaining / 60
        let sec = secRemaining >= 60 ? (secRemaining % 60) : secRemaining
        return
        String(format: "%02d:%02d", min, sec)
    }
    var isValidEvent: Bool {
        if let e = eventStatus {

            guard let secStrt = e.currentDate!.secTillEventStart else {
                return false
            }

            return secStrt > 0 ? true : false

        } else {
            return false
        }
    }

    func startCountDown() {

        if isValidEvent {

            // disable top buttons
            switchTopButtons(true)

            // show timer
            switchCountDown(false)

            guard let e = eventStatus else { return }

            if let secTillEventStart = e.currentDate!.secTillEventStart {

                secRemaining = secTillEventStart > 0 ? secTillEventStart : 0

                isRunning = true

                headerTitleText.text = "COACHING COUNTDOWN".localized

                countDownTimer =
                        Timer.scheduledTimer(timeInterval: 1,
                                target: self,
                                selector: #selector(updateProgressView),
                                userInfo: nil,
                                repeats: true)
            }
        }

    }

    @objc
    fileprivate func updateProgressView() {

        if let e = eventStatus {

            if let secTillEventStart = e.currentDate!.secTillEventStart {
                secRemaining = secTillEventStart > 0 ? secTillEventStart : 0
                circleView.progress = progress
                timerLabel.text = timerLabelText

                if secRemaining <= 0 && isRunning {

                    isRunning = false

                    // stop timer
                    countDownTimer.invalidate()

                    // show timer
                    switchCountDown(true)

                    /*

                      SESSION STARTS

                     */

                    headerTitleText.text = ""


                    // remove live stream from background
                    publisher.stop();
                    publisher.view.removeFromSuperview()

                    LSstart("live")

                    // enable top buttons
                    switchTopButtons(false)

                    displayStatusMessage("Dein Coaching ist live...")




                }
            }

        }



    }

    // MARK: Connections

    /*
    // LifeTime Management of Live Streaming Component
    func LSinit() {

        publisher.view.pinAllEdges2View(self.view)
        publisher.env = env

        streamName = (event == nil) ? "c2c" : "c2c\(event!.id)"

        publisher.streamName = streamName

    }
    */


    func AMQPstart() {

        // start amqp connection
        if let e = self.event {

            amqpClient = AMQPStreamingHandler()
            amqpClient?.userId = String(e.coachId) // "coach-" + String(e.id)
            amqpClient?.eventId = String(e.id)
            amqpClient?.connect(env!, delegate: self) //.user("coach-" + String(e.id), String(e.id))

            btnSend.isEnabled = true
        }

    }


    func LSstart(_ mode : String) {

        streamName = (event == nil) ? "c2c" : "c2c\(event!.id)"
        publisher.env = env

        if (mode == "countdown") {

            streamName = (event == nil) ? "c2c" : "c2c\(event!.id)-absent"
            publisher.recordType = R5RecordTypeLive

        } else if (mode == "live") {

            publisher.recordType = R5RecordTypeAppend

        } else if (mode == "continue") {

            publisher.recordType = R5RecordTypeAppend
        }

        publisher.streamName = streamName

        // start live streaming
        self.view.addSubview(publisher.view)
        self.view.sendSubview(toBack: publisher.view)
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func hideAll() {
        slideOutView(vParticipants, withBottomConstraint: constrBottomParticipantView)
        slideOutView(vQuestions, withBottomConstraint: constrBottomQuestionView)
        hideKeyboard()
    }

    func hideTableViews() {
        slideOutView(vParticipants, withBottomConstraint: constrBottomParticipantView)
        slideOutView(vQuestions, withBottomConstraint: constrBottomQuestionView)
    }

    func slideInView(_ view: UIView, withBottomConstraint bottomConstraint: NSLayoutConstraint) {
        self.view.bringSubview(toFront: view)
        view.isHidden = false
        bottomConstraint.constant = -view.frame.height
        self.view.layoutIfNeeded()
        //hide leave streaming button when subview is visible
        //btLeaveLiveStreaming.isHidden = true
        bottomConstraint.constant = 39 // 0 + Chat Field Height
        UIView.animate(withDuration: slideDuration, animations: {
            self.view.layoutIfNeeded()
        }, completion: nil)
    }

    func slideOutView(_ view: UIView, withBottomConstraint bottomConstraint: NSLayoutConstraint) {

        //show leave streaming button again when subview is hidden
        //btLeaveLiveStreaming.isHidden = false
        bottomConstraint.constant = -view.frame.height
        UIView.animate(withDuration: slideDuration, animations: {
            self.view.layoutIfNeeded()
        }, completion: nil)

    }

    func loadQuestionsFromDB() {

    }


    func leaveWebinar() {

        // release observers
        stopObservingKeyboard()

        publisher.stop();

        // remove live stream from background
        publisher.view.removeFromSuperview()

        amqpClient?.close()

        self.dismiss(animated: true, completion: nil)

    }

    func didEnterBackground() {

        publisher.stop();

        // remove live stream from background
        publisher.view.removeFromSuperview()


        if (!isRunning) {
            amqpClient?.sendMessage("Der Coach hat den Webinar-Raum kurz verlassen...".localized)
        }

        amqpClient?.close()

    }


    func willEnterForeground() {

        AMQPstart()

        LSstart("continue")

        if (!isRunning) {
            amqpClient?.sendMessage("Der Coach ist wieder da.".localized)
        }

    }


    func updateQuestionCount() {
        self.lockQuestions.lock()
        let fragenAnz = self.questions?.count
        self.lockQuestions.unlock()
        if let _fragenAnz = fragenAnz {
            //self.btShowQuestions.setTitle("\(_fragenAnz)", for: UIControlState.normal)
            self.lblTitleQuestions.text = "\(_fragenAnz) FRAGEN AN DICH".localized
        }
    }

    func displayStatusMessage(_ message: String) {
        DispatchQueue.main.async(execute: { [weak self] in
            self?.lblStatus.text = message
            UIView.animate(withDuration: 0.5, animations: { [weak self] in
                self?.lblStatus.alpha = 1.0
            }, completion: { (_) in
                UIView.animate(withDuration: 0.5, delay: 5.0, options: UIViewAnimationOptions.curveEaseIn, animations: { [weak self] in
                    self?.lblStatus.alpha = 0.0
                }, completion: nil)
            })
        })

    }


    func sendMessage() {
        if let msg = self.tfMessage.text {

            if (!msg.isEmpty) {
                amqpClient?.sendMessage(self.tfMessage.text)

                self.tfMessage.text = ""
            }

        }
    }


    @IBAction func didPressSendMessage(_ sender: Any) {

        //close keyboard after writing question
        hideKeyboard()

        sendMessage()
    }


    func startObservingKeyboard() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)

    }

    func stopObservingKeyboard() {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }

    func keyboardWillChange(_ notification: Notification) {

        let info = notification.userInfo
        let kbFrame = info?[UIKeyboardFrameEndUserInfoKey]
        let keyboardFrame = (kbFrame as AnyObject).cgRectValue

        var height: CGFloat = 0.0
        if (keyboardFrame != nil) {
            height = (keyboardFrame!.size.height) + 5.0
        }

        updateAndApplyKeyboardConstraintTo(height, withNotification: notification)

    }

    func keyboardWillShow(_ notification: Notification) {

        bubbleCtrl?.hideBubbles()

        hideTableViews()
    }

    func keyboardWillHide(_ notification: Notification) {
        updateAndApplyKeyboardConstraintTo(0, withNotification: notification)

        bubbleCtrl?.showBubbles()
    }

    func hideKeyboard() {
        self.tfMessage.resignFirstResponder()
    }

    func updateAndApplyKeyboardConstraintTo(_ height: CGFloat, withNotification notification: Notification) {
        let info = notification.userInfo
        var animationDuration = (info?[UIKeyboardAnimationDurationUserInfoKey] as AnyObject).doubleValue

        if (animationDuration == nil || animationDuration == 0) {
            animationDuration = 0.5
        }

        constraintKeyboard?.constant = height
        //self.vVisibleScreen.addConstraint(constraintKeyboard)
        if (isViewLoaded) {
            self.vVisibleScreen.setNeedsUpdateConstraints()
            UIView.animate(withDuration: animationDuration!, animations: {
                self.vVisibleScreen.layoutIfNeeded()
            })
        }
    }

}


// MARK: questions view


extension LivestreamingCoachVC: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        if tableView == self.questionsTableView {
            var questCount = 0
            self.lockQuestions.lock()
            if let _questions = questions {
                questCount = _questions.count
            }
            self.lockQuestions.unlock()
            return questCount
        } else {
            return users.count
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if (tableView == questionsTableView) {

            let cell: QuestionsTVCell = tableView.dequeuedReusableCell(For: indexPath)
            self.lockQuestions.lock()
            if let question = questions?.resultsSafeIndex(indexPath.row) {
                cell.question = question
            }
            self.lockQuestions.unlock()
            cell.indexPath = indexPath
            cell.delegate = self
            return cell
        } else {
            let cell: ParticipantsTVCell = tableView.dequeuedReusableCell(For: indexPath)

            let user = Array(users.keys)[indexPath.row]

            let statusMsg = users[user]!.action == "entered" ? "anwesend".localized : "verlassen"

            cell.nameLabel.text = users[user]!.firstName + " " + users[user]!.lastName + " (" + statusMsg + ")"
            cell.indexPath = indexPath
            //cell.delegate = self
            return cell

        }
    }
}

extension LivestreamingCoachVC: QuestionsTVCellDelegate {

    func didTapCheckmark(at indexPath: IndexPath) {

        questionsTableView.beginUpdates()
        self.lockQuestions.lock()
        if let question = questions?.resultsSafeIndex(indexPath.row), let _event = self.event, let realm = try? Realm() {
            question.delete()
            questions = realm.objects(Question.self)
                    .filter("eventID = \(_event.id)")
                    .sorted(byKeyPath: "timestamp", ascending: false)
        }
        self.lockQuestions.unlock()
        questionsTableView.deleteRows(at: [indexPath], with: .fade)
        questionsTableView.endUpdates()
        decCellIndexPathReferences(startingWith: indexPath)
        updateQuestionCount()
    }

    func decCellIndexPathReferences(startingWith indexPath: IndexPath) {
        for tvCell in questionsTableView.visibleCells {
            if let cell = tvCell as? QuestionsTVCell, let cellIndexPath = cell.indexPath {
                if cellIndexPath.row > indexPath.row {
                    cell.indexPath = IndexPath(row: cellIndexPath.row - 1, section: cellIndexPath.section)
                }
            }
        }
    }
}


extension LivestreamingCoachVC: AMQPDelegate {

    func process(_ type: String, _ data: String) {

        switch type {

        case "chat":

            let msg = msgChat(json: data)

            DispatchQueue.main.async {

                let question = Question()
                question.displayName = msg.user
                question.text = msg.msg
                question.eventID = self.event!.id
                question.add()


                if let realm = try? Realm() {
                    self.lockQuestions.lock()
                    self.questions = realm.objects(Question.self)
                            .filter("eventID = \(self.event!.id)")
                            .sorted(byKeyPath: "timestamp", ascending: false)
                    self.lockQuestions.unlock()
                    self.questionsTableView.reloadData()
                    self.updateQuestionCount()
                }

                // show bubble
                self.bubbleCtrl?.addChatBubble(data: bubbleData(text: msg.msg, headerText: msg.user, image: nil, date: NSDate(), type: .mine))

                // set chat icon
                if let image = UIImage(named: "icon-chat-filled") {
                    self.btnChatButton.setImage(image, for:UIControlState.normal)
                }

            }

            break

        case "room":
            let room = msgRoom(json: data)

            users[room.userID] = room

            DispatchQueue.main.async {

                self.participantsTableView.dataSource = self
                self.participantsTableView.reloadData()
                let usersCount = self.users.count
                self.lblTitleParticipants.text = "\(usersCount) TEILNEHMER".localized

                // set chat icon
                if let image = UIImage(named: "icon-user-filled") {
                    self.btnParticipants.setImage(image, for:UIControlState.normal)
                }

            }

            break
        default: return
        }


    }

}


