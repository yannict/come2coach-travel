//
// Created by Bernd Freier on 18.01.17.
// Copyright (c) 2017 MaytecNet. All rights reserved.
//

import Foundation


class bubbleViewController {

    var firstChatBubbleY: CGFloat = 10.0
    var internalPadding: CGFloat = 8.0
    var lastMessageType: BubbleDataType?
    var view: UIViewController?

    var bubbles = [bubbleView]()

    init(_ view: UIViewController) {

        self.view = view
        lastMessageType = .mine
        firstChatBubbleY = ScreenSize.SCREEN_HEIGHT - 40.0
    }

    func hideBubbles() {

        DispatchQueue.main.async { [weak self] in

            if let self_ = self {

                for index in stride(from: 0, to: self_.bubbles.count, by: 1) {

                    self_.bubbles[index].isHidden = true

                }
            }
        }
    }

    func showBubbles() {

        DispatchQueue.main.async { [weak self] in

            if let self_ = self {

                for index in stride(from: 0, to: self_.bubbles.count, by: 1) {

                    self_.bubbles[index].isHidden = false

                }
            }

        }
    }

    func recalcBubblesY() {

        var start = firstChatBubbleY

        for index in stride(from: bubbles.count, through: 1, by: -1) {

            start = start - (bubbles[index - 1].frame.size.height + internalPadding)

            bubbles[index - 1].frame.origin.y = start

        }

        if (bubbles.count > 5) {

            // #229 animate all bubbles greater than 5


            for index in stride(from: 5, to: bubbles.count, by: 1) {

                UIView.animate(withDuration: 1.5, delay: 1.0, options: .curveEaseOut, animations: {
                    self.bubbles[index - 5].alpha = 0.0
                }, completion: { (finished) in
                    if (finished) {

                        self.bubbles[index - 5].removeFromSuperview()
                        //self.bubbles.remove(at: index - 5)

                    }
                })

            }


            /*

            UIView.animate(withDuration: 1.5, delay: 1.0, options: .curveEaseOut, animations: {
                self.bubbles[0].alpha = 0.0
            }, completion: { (finished) in
                if (finished) {

                    self.bubbles.remove(at: 0)
                    //self.bubbles[0].removeFromSuperview()

                    //debugPrint(self.bubbles.count)
                }
            })
            */


        }

    }

    func addChatBubble(data: bubbleData) {

        // var padding: CGFloat = lastMessageType == data.type ? internalPadding / 3.0 : internalPadding
        var chatBubble = bubbleView(data: data, startY: firstChatBubbleY)
        view?.view.addSubview(chatBubble)

        bubbles.append(chatBubble)

        recalcBubblesY()


    }

}