//
//  UIView+AddItems.swift
//  come2coach
//
//  Created by Marc Ortlieb on 06.08.16.
//  Copyright © 2016 MaytecNet. All rights reserved.
//

import Foundation

/// shorthand for adding gestureRecognizer to view

func +=(left: UIView, right: UIGestureRecognizer) {
    return left.addGestureRecognizer(right)
}

/// add bunch of gestureGecognizers to view

func +=(left: UIView, right: [UIGestureRecognizer]) {
    return right.forEach { left += $0 }
}