//
//  Results+safeIndex.swift
//  come2coach
//
//  Created by Werner Kratochwil Full on 21.09.16.
//  Copyright © 2016 MaytecNet. All rights reserved.
//

import Foundation
import RealmSwift

extension Results {
    
    // See Swiftz: https://github.com/typelift/Swiftz/blob/master/Swiftz/ArrayExt.swift#L214
    /// Safely indexes into an array by converting out of bounds errors to nils.
    
    public func resultsSafeIndex(_ i : Int) -> Element? {
        if i < self.count && i >= 0 {
            return self[i]
        } else {
            return nil
        }
    }
}
