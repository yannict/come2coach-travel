//
//  Notifications.swift
//  SmartCar
//
//  Created by Marc Ortlieb on 24.06.16.
//  Copyright © 2016 Marc Ortlieb. All rights reserved.
//

import UIKit


class LocalNotificationManager {

    static let sharedInstance = LocalNotificationManager()
    fileprivate init() {
    }

    enum Actions: String {
        case Background, Foreground
    }

    func manageNotifications(For events: [Event]) {
        //cancelAllSheduledNotifications()
        resetIconBadge()
        if let scheduledLocalNotifications = UIApplication.shared.scheduledLocalNotifications {

            func cancelAllEvents(_ notification: UILocalNotification) {
                if notification.category == Constants.LocalNotifications.Category.coachingBegins {
                    UIApplication.shared.cancelLocalNotification(notification)
                    OnDebugging
                            .print("removed local notification: \(notification)", title: "LocalNotificationManager.manageNotifications(events)")
                }
            }

            scheduledLocalNotifications
                    .forEach(cancelAllEvents)

            /*
            OnDebugging
                    .print("localNotifications.count: \(scheduledLocalNotifications.filter {
                $0.category == Constants.LocalNotifications.Category.coachingBegins
            }.count),
                    title: "LocalNotificationManager.manageNotifications(events)")

            */

        }
        // filteredEventsForNotifications
        events
                .filter {
                    !$0.didEnd
                }
                .filter {
                    !$0.isHappeningNow
                }
                .filter {
                    !$0.isInCountDownPhaseNow
                }
                .forEach(scheduleNewNotification)
    }
    // new
    func scheduleNewNotification(For event: Event) {

        /*
        if event.begin.date == Date.errorDate {
            OnDebugging
                .printError("event.begin.date == NSDate.errorDate", title: "LocalNotificationManager.scheduleNewNotification()")
            return
        }
        */

        let notification = UILocalNotification()
        notification.alertBody = "Dein Coaching beginnt gleich!".localized
        notification.alertAction = "Streiche, um zu deinem Coaching zu kommen.".localized
        notification.soundName = UILocalNotificationDefaultSoundName
        notification.fireDate = Calendar.current.date(byAdding: .minute, value: -10, to: now)
        // notification.fireDate = event.begin.date - (10 * 60)
        notification.category = Constants.LocalNotifications.Category.coachingBegins
        notification.userInfo = [Constants.LocalNotifications.UserInfoKeys.coachingID: event.id]

        UIApplication.shared.scheduleLocalNotification(notification)

        OnDebugging
                .print("scheduled NEW local notification \(notification)", title: "LocalNotificationManager.scheduleNewNotification()")
    }

    func cancelAllSheduledNotifications() {
        UIApplication.shared.cancelAllLocalNotifications()
        resetIconBadge()
    }

    func resetIconBadge(to nr: Int = 0) {
        UIApplication.shared.applicationIconBadgeNumber = 0
    }


    /// Check Registration State

    func isRegistered() -> Bool {
        guard let settings = UIApplication.shared.currentUserNotificationSettings
                else {
            return false
        }
        return
        settings.types == UIUserNotificationType() ? false : true
    }

    /// MARK: register for local notifications reminding user for upcoming coaching events

    func registerCoachingBeginsWithOptionalUserActions() {

//        let ActionOne =
//            UIMutableUserNotificationAction().initWith {
//            $0.identifier = Actions.Background.rawValue
//            $0.title = "Background"
//            $0.activationMode = .Background
//            $0.authenticationRequired = true
//            $0.destructive = false
//        }
//        let ActionTwo =
//            UIMutableUserNotificationAction().initWith {
//            $0.identifier = Actions.Foreground.rawValue
//            $0.title = "Foreground"
//            $0.activationMode = .Foreground
//            $0.authenticationRequired = true
//            $0.destructive = false
//        }
        let counterCategory =
                UIMutableUserNotificationCategory()
        counterCategory.identifier = Constants.LocalNotifications.Category.coachingBegins


        let settings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound],
                        categories: NSSet(object: counterCategory) as?
                        Set<UIUserNotificationCategory>)

        UIApplication.shared
                .registerUserNotificationSettings(settings)
    }


}

