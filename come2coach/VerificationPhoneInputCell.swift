//
// Created by Bernd Freier on 27.02.17.
// Copyright (c) 2017 MaytecNet. All rights reserved.
//

import Foundation
import PhoneNumberKit


class VerificationPhoneInputCell: UITableViewCell {


    @IBOutlet weak var textField: PhoneNumberTextField!

    weak var delegate: VerificationInputCellDelegate?
    var contentType: String!


    override func awakeFromNib() {
        super.awakeFromNib()
        //valueTextField.delegate = self

        textField.addTarget(self, action: #selector(VerificationPhoneInputCell.textFieldDidChange(_:)), for: UIControlEvents.editingDidEnd)
    }

    func textFieldDidChange(_ tf: UITextField) {


        /*
        let number = tf.text!.components(separatedBy: CharacterSet.decimalDigits.inverted)
                .joined()

        delegate?.verifyNumber(self, textField.currentRegion, String( number ))
        */

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    func setData(_ value: String, contentType: String, placeholder: String) {
        self.contentType = contentType
        textField.text = value
        textField.placeholder = placeholder

    }

}
