//
// Created by Bernd Freier on 17.01.17.
// Copyright (c) 2017 MaytecNet. All rights reserved.
//

import Foundation
import UIKit

struct ScreenSize
{
    static let SCREEN_WIDTH = UIScreen.main.bounds.size.width
    static let SCREEN_HEIGHT = UIScreen.main.bounds.size.height
    static let SCREEN_MAX_LENGTH = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    static let SCREEN_MIN_LENGTH = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
}



class bubbleView : UIView {

    var imageViewChat: UIImageView?
    var imageViewBG: UIImageView?
    var text: String?
    var labelChatText: UILabel?
    var labelUserName: UILabel?

    init(data: bubbleData, startY: CGFloat){

        super.init(frame: bubbleView.framePrimary(data.type, startY:startY))

        // Making Background color as gray color
        self.backgroundColor = UIColor.lightGray
        self.layer.cornerRadius = 5
        self.alpha = 0.85

        let padding: CGFloat = 8.0

        // 2. Drawing image if any
        if let chatImage = data.image {

            let width: CGFloat = min(chatImage.size.width, self.frame.size.width - 2 * padding)
            let height: CGFloat = chatImage.size.height * (width / chatImage.size.width)
            imageViewChat = UIImageView(frame: CGRect( x:padding, y:padding, width:width, height:height))
            imageViewChat?.image = chatImage
            imageViewChat?.layer.cornerRadius = 5.0
            imageViewChat?.layer.masksToBounds = true
            self.addSubview(imageViewChat!)
        }

        // 3. Going to add Text if any
        if let chatText = data.text {
            // frame calculation
            var startX = padding
            var startY:CGFloat = 5.0
            if let imageView = imageViewChat {
                startY += imageViewChat!.frame.maxY
            }

            // user name label
            labelUserName = UILabel(frame: CGRect( x:startX, y:startY, width: self.frame.width - 2 * startX , height: 5))
            labelUserName?.textAlignment = .right // data.type == .mine ? .right : .left
            //labelUserName?.font = UIFont.systemFont(ofSize: 10)
            labelUserName?.font = UIFont.boldSystemFont(ofSize: 10.0)
            labelUserName?.numberOfLines = 0 // Making it multiline
            labelUserName?.text = data.headerText
            labelUserName?.sizeToFit() // Getting fullsize of it
            self.addSubview(labelUserName!)


            startY = startY + labelUserName!.frame.size.height + 3.0

            // text label
            labelChatText = UILabel(frame: CGRect( x:startX, y:startY, width: self.frame.width - 2 * startX , height: 5))
            labelChatText?.textAlignment = .left // data.type == .mine ? .right : .left
            labelChatText?.font = UIFont.systemFont(ofSize: 14)
            labelChatText?.numberOfLines = 0 // Making it multiline
            labelChatText?.text = data.text
            labelChatText?.sizeToFit() // Getting fullsize of it
            self.addSubview(labelChatText!)
        }
        // 4. Calculation of new width and height of the chat bubble view
        var viewHeight: CGFloat = 0.0
        var viewWidth: CGFloat = 0.0
        if let imageView = imageViewChat {
            // Height calculation of the parent view depending upon the image view and text label
            viewWidth = max(imageViewChat!.frame.maxX, labelChatText!.frame.maxX ) + padding
            viewHeight = max(imageViewChat!.frame.maxY, labelChatText!.frame.maxY) + padding

        } else {
            viewHeight = labelChatText!.frame.maxY + padding/2
            viewWidth = max(labelChatText!.frame.width, labelUserName!.frame.width) + labelChatText!.frame.minX + padding
        }

        // 5. Adding new width and height of the chat bubble frame
        self.frame = CGRect( x:self.frame.minX, y:self.frame.minY, width:viewWidth, height:viewHeight)
    }

    // 6. View persistance support
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    //MARK: - FRAME CALCULATION
    class func framePrimary(_ type:BubbleDataType, startY: CGFloat) -> CGRect{
        let paddingFactor: CGFloat = 0.02
        let sidePadding = ScreenSize.SCREEN_WIDTH * paddingFactor
        let maxWidth = ScreenSize.SCREEN_WIDTH * 0.65 // We are cosidering 65% of the screen width as the Maximum with of a single bubble
        let startX: CGFloat = type == .opponent ? ScreenSize.SCREEN_WIDTH * (CGFloat(1.0) - paddingFactor) - maxWidth : sidePadding
        return CGRect(x:startX, y:startY, width:maxWidth, height:5) // 5 is the primary height before drawing starts
    }
}