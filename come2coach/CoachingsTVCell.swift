//
//  CoachingsVCell.swift
//  come2coach
//
//  Created by Marc Ortlieb on 08.08.16.
//  Copyright © 2016 MaytecNet. All rights reserved.
//

import UIKit
// import Async



protocol CoachingsTVCellDelegate: class {
    func didPressAccessoryIcon(at indexPath: IndexPath)
}

class CoachingsTVCell: UITableViewCell, Reusable {
    
    @IBOutlet weak var previewImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var accessoryIcon: UIButton!
    
    
    weak var delegate: CoachingsTVCellDelegate?
    
    var event: Event?
    var indexPath: IndexPath?
    
    @IBAction
    func accessoryIconAction(_ sender: AnyObject) {
        if let indexPath = indexPath {
            OnDebugging.print("FavouritesTVCell.accessoryIconAction - indexPath: (\(indexPath))")
            delegate?.didPressAccessoryIcon(at: indexPath)
        }
    }
    override func prepareForReuse() {
        previewImageView?.image = nil
        titleLabel?.text = ""
        subtitleLabel?.text = ""
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        // selectionStyle = .None
        prepareForReuse()
    }
    
    // MARK: Initializers
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    
    // MARK: make AttributedString for SubTitleLabelText
    // http://stackoverflow.com/questions/24666515/how-do-i-make-an-attributed-string-using-swift

    /* #swift3
    fileprivate func makeAttributedSubtitleText(_ location: String, _ secTillEventStart: Int64) -> NSAttributedString {
        
        var font = UIFont.Montserrat.regular(12)
        
        
        // ajust content according to secTillEventStart
        var time = ""
        //let min: Int64 = 60 * 60
        let hour:Int64 = 3600
        let day:Int64  = 86400
        
        if secTillEventStart > 2 * day {
            time = "in \(secTillEventStart / day) Tagen"
        } else if secTillEventStart >= day {
            time = "in 1 Tag".localized
        } else if secTillEventStart > 2 * hour {
            time = "in \(secTillEventStart / hour) Stunden"
        } else if secTillEventStart >= hour {
            time = "in 1 Stunde".localized
            
        } else if let event = event, (secTillEventStart < hour) && (!event.didEnd) && (!event.isHappeningNow) {
            time = "in weniger als einer Stunde".localized
            font = UIFont.Montserrat.regular(11)
        } else if let event = event, event.isHappeningNow {
            time = "LÄUFT GERADE".localized
        }
    
    
        //        } else if secTillEventStart > 2 * min {
        //            time = "in \(secTillEventStart / min) Minuten"
        //        } else if secTillEventStart >= min {
        //            time = "in 1 Minute"
        //        } else if secTillEventStart < min {
        //            time = "in \(secTillEventStart) Sekunden"
        //        }
        
        
        // "online" / "Würzburg"

        let locationAttributes = [
            NSForegroundColorAttributeName: Designs.Colors.coachingTypeBlue,
            NSFontAttributeName: font
        ]
        let locationAttributedString = NSAttributedString(string: location, attributes: locationAttributes)

        
        //
        let timeAttributes = [
            NSForegroundColorAttributeName: Designs.Colors.coachingTimeGray, 
            NSFontAttributeName: font
        ]
        let timeAttributedString = NSAttributedString(string: ", \(time)", attributes: timeAttributes)
        
        return
            locationAttributedString + timeAttributedString



        return nil
    }
    */
    
    func configureCell()
    {
        /*
        if let _event = event
        {
            if (_event.image != nil)
            {
                previewImageView.image = _event.image
            }
            // title
            titleLabel.text = _event.title
            //self_.titleLabel.font = UIFont.Montserrat.bold(13)
            
            // subTitle
            let location = _event.isWebinar ? "Online" : _event.address.city
            
            if let secTillEventStart = now.seconds(to: _event.begin.dateString) {
                let subTitleLabelText = makeAttributedSubtitleText(location, Int64(secTillEventStart))
                subtitleLabel.attributedText = subTitleLabelText
            }
            // [accessoryIcon]: hide/ show
            #if DEBUG
                accessoryIcon.hidden = false
            #else
                accessoryIcon.isHidden = _event.isHappeningNow ? false : true
            #endif
            Async
                .userInitiated {[weak self] in
                    guard let self_ = self else {return}
                    if (_event.image == nil)
                    {
                        _event.image = _event.imageURL
                            .flatMap{NSData(contentsOfURL: $0)}
                            .flatMap{UIImage(data: $0)}
                        Async.main(block:
                            {
                                self_.previewImageView.image = _event.image
                        })
                    }
            }
            
        }
        */
    }

}





