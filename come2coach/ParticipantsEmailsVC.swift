//
//  ParticipantsEmailsVC.swift
//  come2coach
//
//  Created by Igor Prysyazhnyuk on 3/23/17.
//  Copyright © 2017 MaytecNet. All rights reserved.
//

import UIKit

protocol ParticipantsEmailsVCDelegate: class {
    func participantsEmailsFilled(emails: [String])
}

class ParticipantsEmailsVC: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomConstraintForKeyboard: NSLayoutConstraint!
    @IBOutlet weak var blurView: UIVisualEffectView!
    @IBOutlet weak var bookButton: CoinsCountButton!
    
    fileprivate var participantsCount = 1
    fileprivate var emails = [Int: String]()
    fileprivate let rowHeight: CGFloat = 54
    fileprivate var coinsCount = 0
    
    private weak var delegate: ParticipantsEmailsVCDelegate?
    private let animationDuration = 0.3
    
    static func show(participantsCount: Int, coinsCount: Int, fromVC: UIViewController, delegate: ParticipantsEmailsVCDelegate?) {
        let vc: ParticipantsEmailsVC = UIStoryboard(name: Constants.Storyboards.coachings).instanciateVCByName()
        vc.participantsCount = participantsCount
        vc.coinsCount = coinsCount
        vc.delegate = delegate
        vc.modalPresentationStyle = .overCurrentContext
        vc.view.isHidden = true
        fromVC.present(vc, animated: false) {
            vc.fadeIn()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateUI()
        updateTableViewHeight()
        initEmptyEmails()
        initLocalNotifications()
    }
    
    private func updateUI() {
        bookButton.coins = coinsCount
    }
    
    private func fadeIn() {
        view.isHidden = false
        view.alpha = 0
        UIView.animate(withDuration: animationDuration) { 
            self.view.alpha = 1
        }
    }
    
    private func initEmptyEmails() {
        for i in 0 ..< participantsCount { emails[i] = "" }
    }
    
    private func initLocalNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name:NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    private func updateTableViewHeight() {
        tableViewHeightConstraint.constant = CGFloat(participantsCount) * rowHeight
    }
    
    @IBAction private func dismiss() {
        UIView.animate(withDuration: animationDuration, animations: { 
            self.view.alpha = 0
        }) { (completed) in
            self.dismiss(animated: false)
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func keyboardWillShow(notification: NSNotification) {
        let keyboardFrame = getKeyboardFrame(notification: notification)
        
        UIView.animate(withDuration: animationDuration) {
            self.bottomConstraintForKeyboard.constant = keyboardFrame.height
            self.view.layoutIfNeeded()
        }
    }
    
    func getKeyboardFrame(notification: NSNotification) -> CGRect {
        var userInfo = notification.userInfo!
        let keyboardFrame = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        return view.convert(keyboardFrame, from: nil)
    }
    
    func keyboardWillHide(notification: NSNotification) {
        UIView.animate(withDuration: animationDuration) {
            self.bottomConstraintForKeyboard.constant = 0
            self.view.layoutIfNeeded()
        }
    }
    
    private func validate() -> Bool {
        var isValid = true
        var i = 0
        
        func showError(error: String) {
            isValid = false
            tableView.scrollToRow(at: IndexPath(row: i, section: 0), at: UITableViewScrollPosition.bottom, animated: true)
            UIAlertController.show(fromVC: self, error: error)
        }
        
        for email in emails.values {
            if !email.validateEmail() {
                showError(error: "Bitte gib eine korrekte E-Mail-Adresse ein.".localized)
                break
            }
            if Array(emails.values).filter({ $0 == email }).count > 1 {
                showError(error: "Eine der hinterlegten E-Mail Adresse ist mehrfach eingegeben. Jeder Teilnehmer benötigt eine individuelle E-Mail Adresse".localized)
                break
            }
            i += 1
        }
        
        return isValid
    }
    
    @IBAction func bookTapped(_ sender: Any) {
        if (validate()) {
            let emailsArray = Array(emails.values)
            delegate?.participantsEmailsFilled(emails: emailsArray)
            dismiss()
        }
    }
}

extension ParticipantsEmailsVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return participantsCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: ParticipantEmailCell = tableView.cell(for: indexPath)
        let index = indexPath.row
        cell.setData(email: emails[index], index: index, delegate: self)
        return cell
    }
}

extension ParticipantsEmailsVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return rowHeight
    }
}

extension ParticipantsEmailsVC: ParticipantEmailCellDelegate {
    func emailChanged(email: String, index: Int) {
        emails[index] = email
    }
}
