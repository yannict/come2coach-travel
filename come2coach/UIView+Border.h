//
//  UIView+Border.h
//  MaytecNet
//
//  Created by Werner Kratochwil on 16.06.15.
//  Copyright (c) 2015 MaytecNet All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Border)

@property (nonatomic) IBInspectable UIColor *borderColor;
@property (nonatomic) IBInspectable NSInteger borderWidth;
@property (nonatomic) IBInspectable NSInteger cornerRadius;

@end
