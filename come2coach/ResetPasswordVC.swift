//
//  ResetPasswordVC.swift
//  come2coach
//
//  Created by Alex Kupchak on 11/7/16.
//  Copyright © 2016 MaytecNet. All rights reserved.
//

import Foundation
import SVProgressHUD
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class ResetPasswordVC: UIViewController {
    @IBOutlet weak var bottomViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomWaitingViewConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var emailTextField: PaddedTextField!
    @IBOutlet weak var waitingView: UIView!
    @IBOutlet weak var sendMailView: UIView!
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var popUpBackgroundView: UIView!
    
    @IBOutlet weak var titleLabel: UILabel!
    // MARK: Life cycle
    
    override func viewDidLoad() {
        setupKeyboardObservers()
        //waitingView.hidden = true
        popUpBackgroundView.alpha = 0
        
        /*
        let t1str = "ANMELDUNG".localized
        let t1strA = NSMutableAttributedString(string: t1str)
        t1strA.addAttribute(NSKernAttributeName, value: CGFloat(1.4), range: NSRange(location:0, length: t1str.characters.count ))
        titleLabel.attributedText = t1strA
         */
        
        
        let deltaHeight: CGFloat = 253
        bottomViewConstraint.constant -= deltaHeight
        bottomWaitingViewConstraint.constant -= deltaHeight + 4
        
        let backgroundTap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view += backgroundTap
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        slideContentFromBottom()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

    }
    
    deinit {
        NotificationCenter.default
            .removeObserver(self)
    }

    // MARK: Actions
    
    @IBAction func clickSendMail(_ sender: AnyObject) {
        if emailTextField.text?.characters.count > 0 {
            SVProgressHUD.show()
            LoginController.resetPassword(emailTextField.text!, handler: { (errors) in
                SVProgressHUD.dismiss()
                if errors == nil {
                    OnDebugging.print("Send email: success")
                    self.emailTextField.resignFirstResponder()
                    self.sendMailView.isHidden = true
                    self.backgroundView.isHidden = true
                    self.slideWaitingViewFromBottom()
                }
            })
        }
    }
    
    @IBAction func clickCancel(_ sender: AnyObject) {
        //self.navigationController?.popViewController(animated: true)
        //self.show(Storyboards.ViewControllers.loginPreview, animated: true)
        slideOutContentIntoBottom()
    }
    
    // MARK: NSNotificationCenter
    
    // SHOW Keyboard
    func keyboardWillShow(_ notification: Notification) {
        guard let info = notification.userInfo else {
            return
        }
        
        let keyboardFrame: CGRect = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let slideDuration = info[UIKeyboardAnimationDurationUserInfoKey] as! Double
        let curve = info[UIKeyboardAnimationCurveUserInfoKey] as! UInt
        let options = UIViewAnimationOptions(rawValue: curve)
        
        bottomViewConstraint.constant = keyboardFrame.height + 4
        view.layoutIfNeeded()
        
        UIView
            .animate(withDuration: slideDuration, delay: 0, options: options, animations: { [weak self] in
                self?.view.layoutIfNeeded()
                }, completion: nil)
    }
    
    // HIDE Keyboard
    func keyboardWillHide(_ notification: Notification) {
        guard let info = notification.userInfo else {
            OnDebugging
                .printError("notification.userInfo == nil",title: "LoginVC.keyboardWillHide()")
            return
        }
        let slideDuration = info[UIKeyboardAnimationDurationUserInfoKey] as! Double
        let curve = info[UIKeyboardAnimationCurveUserInfoKey] as! UInt
        let options = UIViewAnimationOptions(rawValue: curve)
        
        bottomViewConstraint.constant = 4
        UIView
            .animate(withDuration: slideDuration, delay: 0, options: options, animations: { [weak self] in
                self?.view.layoutIfNeeded()
                }, completion: nil)
    }
    
    // MARK: Others
    
    func dismissKeyboard() {
        emailTextField.resignFirstResponder()
    }
    
    func slideContentFromBottom() {
        let sendMailViewHeight: CGFloat = 253
        let deltaHeight: CGFloat = sendMailViewHeight
//        bottomViewConstraint.constant -= deltaHeight
//        
//        // hide waiting view
//        //
//        bottomWaitingViewConstraint.constant -= deltaHeight + 4
        view.layoutIfNeeded()
        
        UIView
            .animate(withDuration: 0.5, delay: 0, options: UIViewAnimationOptions(), animations: { [weak self] in
                self?.popUpBackgroundView.alpha = 0.5
                self?.bottomViewConstraint.constant += deltaHeight
                self?.view.layoutIfNeeded()
                }, completion: nil)
    }
    
    func slideOutContentIntoBottom() {
        let deltaHeight: CGFloat = 253
        dismissKeyboard()
        
        view.layoutIfNeeded()
        
        UIView
            .animate(withDuration: 0.5, delay: 0, options: UIViewAnimationOptions(), animations: { [weak self] in
                guard let self_ = self else {return}
                self_.bottomViewConstraint.constant -= deltaHeight
                self_.bottomWaitingViewConstraint.constant -= deltaHeight
                self_.view.layoutIfNeeded()
                }, completion: { _ in
                    self.show(Storyboards.ViewControllers.login)
            })
    }
    
    func slideWaitingViewFromBottom() {
        let sendMailViewHeight: CGFloat = 253
        let deltaHeight: CGFloat = sendMailViewHeight
        view.layoutIfNeeded()
        
        UIView
            .animate(withDuration: 0.5, delay: 0, options: UIViewAnimationOptions(), animations: { [weak self] in
                self?.bottomWaitingViewConstraint.constant += deltaHeight + 4
                self?.view.layoutIfNeeded()
                }, completion: nil)
    }
    
    //    func slideOutWaitingViewIntoBottom() {
    //        let deltaHeight: CGFloat = passwordForgottenButtonHeight + 8 + subscribeForgottenButtonHeight
    //        bottomViewConstraint.constant = 0
    //        dismissKeyboard()
    //
    //        view.layoutIfNeeded()
    //
    //        UIView
    //            .animateWithDuration(0.5, delay: 0, options: .CurveEaseInOut, animations: { [weak self] in
    //                guard let self_ = self else {return}
    //                self_.bottomViewConstraint.constant = 8 - deltaHeight
    //                self_.view.layoutIfNeeded()
    //                }, completion: { _ in
    //                    self.show(Storyboards.ViewControllers.loginPreview)
    //            })
    //    }
    
    fileprivate func setupKeyboardObservers() {
        NotificationCenter.default
            .addObserver(self, selector: #selector(keyboardWillShow(_:)), name:NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default
            .addObserver(self, selector: #selector(keyboardWillHide(_:)), name:NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
}
