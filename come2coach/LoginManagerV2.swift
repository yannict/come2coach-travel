//
// Created by Bernd Freier on 25.02.17.
// Copyright (c) 2017 MaytecNet. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import SVProgressHUD
import FacebookCore
import FacebookLogin

class LoginController {

    var isAuthenticated = false
    var AutoLoginAllowed = true

    var completion: (() -> Void)!
    var error: ((_ txt: String) -> Void)!
    var showLogin: (() -> Void)!


    func loginHandler(_ errors: [String], user: User?) {


        SVProgressHUD.dismiss()

        if errors.isContaining {
            var message = ""
            errors.forEach {
                message += "\($0).\n"
            }
            message += "."

            error(message)

        }
        // onSuccess
        else if errors.isEmpty {
            user?.save()

            isAuthenticated = true

            AutoLoginAllowed = true

            completion()
        }

    }

    func autoLogin(_ user: User?) {

        isAuthenticated = false

        if let u = User.current {

            if (AutoLoginAllowed) {

                login(mail: u.email, password: u.password)

            }
        }

    }

    func login(mail: String, password: String) {

        isAuthenticated = false

        if let u = User.current {

            if (AutoLoginAllowed && (u.lastLoginType == "come2coach" || u.lastLoginType == "facebook")) {

                switch u.lastLoginType {
                case "come2coach":
                    SVProgressHUD.show()

                    loginCome2Coach(mail: mail, password: password, handler: loginHandler)

                    break

                case "facebook":
                    loginFB(u.facebookToken, handler: loginHandler)
                    //LoginManagerOld.sharedInstance.loginFB(u.facebookToken, handler: loginHandler)

                    break

                default:
                    AutoLoginAllowed = true
                    showLogin()
                    //self.viewController?.show(Storyboards.ViewControllers.login)
                    break

                }
            } else {
                loginCome2Coach(mail: mail, password: password, handler: loginHandler)
            }

        } else {
            loginCome2Coach(mail: mail, password: password, handler: loginHandler)
        }

    }



    @objc func loginFacebook(_ vc : UIViewController) {

        isAuthenticated = false

        let loginManager = LoginManager()

        loginManager.logIn([.publicProfile, .email], viewController: vc) { loginResult in
            switch loginResult {
            case .failed(let e):
                self.error("Der Facebook Login hat leider nicht geklappt.")
            case .cancelled:
                //print("User cancelled login.")
                    break
            case .success(let grantedPermissions, let declinedPermissions, let accessToken):
                self.loginFB(accessToken.authenticationToken) { (errors, user) in

                    if errors.isContaining {

                        self.error("Der Facebook Login hat leider nicht geklappt.")

                    }
                    // onSuccess
                    else if errors.isEmpty {
                        user?.save()

                        self.isAuthenticated = true

                        self.AutoLoginAllowed = true

                        self.completion()

                    }
                }

                break
            }
        }

    }

    func loginCome2Coach(mail: String, password: String, handler: @escaping (_ errors: [String], _ user: User?) -> Void) {

        let headers = [
                "Token": Constants.API.accessToken,
                "Content-Type": "application/json"
        ]
        var parameters = [
                "email": mail,
                "password": password
        ]

        Alamofire
                .request(HTTPRequest.rootUrl + "auth/login",
                        method: .post,
                        parameters: parameters,
                        encoding: JSONEncoding.default,
                        headers: headers)
                .validate()
                .responseJSON { response in

                    OnDebugging.print("HTTPRequest.login() - statusCode \(response.response?.statusCode)")

                    if let statusCode = response.response?.statusCode {
                        switch statusCode {
                        case 401: handler(["Mail und/oder Passwort falsch".localized], nil)
                        default: break
                        }
                    }

                    var alertErrors = [String]()
                    var user = User()

                    switch response.result {
                    case .failure(let error):
                        OnDebugging
                                .printError("HTTPRequest.login() response.result.Failure: \(error)")
                        handler([error.localizedDescription], nil)
                    case .success:
                        guard let value = response.result.value else {
                            OnDebugging
                                    .printError("HTTPRequest.login() response.result.value == nil")
                            return
                        }
                        let json = JSON(value)

                        OnDebugging
                                .print("HTTPRequest.login() - JSON(value)\(json)")


                        let success = json["success"].boolValue

                        if !success {
                            // possible errors
                            let responseErrors = json["errors"]
                            let email = responseErrors["email"].arrayValue
                            let password = responseErrors["password"].arrayValue

                            // handle errors
                            if email.contains("min") {
                                alertErrors += "Deine E-Mail ist zu kurz".localized
                            }
                            if email.contains("required") {
                                alertErrors += "Gib deine E-Mail ein".localized
                            }
                            if password.contains("required") {
                                alertErrors += "Gib dein Passwort ein".localized
                            }
                            if password.contains("min") {
                                alertErrors += "Dein Passwort ist zu kurz".localized
                            }
                            if alertErrors.isEmpty {
                                alertErrors += "Mail und/oder Passwort falsch".localized
                            }
                            handler(alertErrors, nil)
                        } else {

                            user = User()
                            user.JSONWebToken = json["token"].stringValue
                            let userJSON = json["user"]
                            user.email = userJSON["email"].stringValue
                            user.password = password
                            user.isVerified = userJSON["verified"].intValue
                            user.lname = userJSON["lname"].stringValue
                            user.name = userJSON["name"].stringValue
                            user.provider = userJSON["provider"].stringValue
                            user.provider_id = userJSON["provider_id"].stringValue
                            user.lastLoginType = User.LoginType.come2coach.rawValue
                            user.isVerified = userJSON["verified"].intValue
                            user.id = userJSON["id"].intValue
                            user.push_token = json["push_token"].stringValue

                            handler([], user)
                        }

                    }

                }
    }

    func loginFB(_ FBToken: String, handler: @escaping (_ errors: [String], _ user: User?) -> Void) {

        let headers = [
                "Token": Constants.API.accessToken,
                "Content-Type": "application/json"
        ]
        OnDebugging.print("FBToken: \(FBToken)")

        var parameters = [
                "token": FBToken,
        ]

        /*
        if let pushToken = EventManager.sharedInstance.pushNotificationDeviceToken {
            parameters["APNSPushToken"] = pushToken
        }
        */

        Alamofire
                .request(
                        HTTPRequest.rootUrl + "auth/login/facebook",
                        method: .post,
                        parameters: parameters,
                        encoding: JSONEncoding.default,
                        headers: headers)
                .validate()
                .responseJSON { response in


                    if let statusCode = response.response?.statusCode {
                        switch statusCode {
                        case 401: handler(["Mail und/oder Passwort falsch"], nil)
                        default: break
                        }
                    }



                    var errors = [String]()


                    switch response.result {
                    case .failure(let error):
                        OnDebugging
                                .printError("HTTPRequest: response.result == failure error:\(error.localizedDescription)")

                        handler([error.localizedDescription], nil)

                    case .success:
                        OnDebugging.print("HTTPRequest: Success")
                        guard let value = response.result.value else {
                            OnDebugging
                                    .printError("value = response.result.value",
                                    title: "HTTPRequest.loginFB()")
                            return
                        }
                        let json = JSON(value); OnDebugging.print(json)


                        let success = json["success"].boolValue

                        if !success {
                            // possible errors
                            let responseErrors = json["errors"]
                            let email = responseErrors["email"].arrayValue
                            let password = responseErrors["password"].arrayValue

                            // handle errors
                            if email.contains("min") {
                                errors += ["Dein Passwort ist zu kurz".localized]
                            }
                            if email.contains("required") {
                                errors += ["Gib deine Mail ein".localized]
                            }
                            if password.contains("required") {
                                errors += ["Gib dein Passwort ein".localized]
                            }

                            if let accessDenied = json["error"].string {
                                errors += accessDenied

                                OnDebugging
                                        .print("API: Access denied - error: \(accessDenied)")
                            }
                            handler(errors, nil)
                        } else {

                            let user = User()
                            user.JSONWebToken = json["token"].stringValue
                            user.facebookToken = FBToken

                            let userJSON = json["user"]
                            user.email = userJSON["email"].stringValue
                            user.isVerified = userJSON["verified"].intValue
                            user.lname = userJSON["lname"].stringValue
                            user.name = userJSON["name"].stringValue
                            user.provider = userJSON["provider"].stringValue
                            user.provider_id = userJSON["provider_id"].stringValue
                            user.lastLoginType = User.LoginType.facebook.rawValue
                            user.isVerified = userJSON["verified"].intValue
                            user.id = userJSON["id"].intValue
                            user.push_token = json["push_token"].stringValue

                            handler(errors, user)
                        }
                    }


                }
    }


    static func register(_ user: User, handler: @escaping (_ errors: [String]) -> Void) {

        let headers = [
                "Token": Constants.API.accessToken,
                "Content-Type": "application/json"
        ]

        let parameters: [String: AnyObject] = [
                "name": user.name as AnyObject,
                "lname": user.lname as AnyObject,
                "email": user.email as AnyObject,
                "provider": user.provider as AnyObject,
                "provider_id": user.provider_id as AnyObject,
                "provider_token": user.provider_token as AnyObject,
                "password": user.password as AnyObject,
                "newsletter": user.subscribeNewsletter as AnyObject,
                "image": user.imageStringUrl! as AnyObject
        ]

        Alamofire
                .request(
                        HTTPRequest.rootUrl + "auth/register",
                        method: .post,
                        parameters: parameters,
                        encoding: JSONEncoding.default,
                        headers: headers)
                .validate()
                .responseJSON { response in

                    OnDebugging.print("serverResponseStatusCode: \(response.response?.statusCode)")


                    switch response.result {
                    case .failure(let error):
                        handler([error.localizedDescription])
                    case .success:
                        guard let value = response.result.value else {
                            OnDebugging.print("response.result.value == nil")
                            return
                        }
                        let json = JSON(value); OnDebugging.print(json)

                        // Wired API Response
                        // If registration is successfull server response is null!
                        // if not sucessfull -> only Errors (& success value not delivered)
                        let success = json["sucess"].boolValue // API typo ?

                        if !success {

                            // possible errors
                            let responseErrors = json["errors"]
                            let name = responseErrors["name"].arrayValue
                            let email = responseErrors["email"].arrayValue
                            let password = responseErrors["password"].arrayValue

                            var errors = [String]()

                            // handle errors
                            if email.contains("unique") {
                                errors += ["Diese E-Mail ist bereits registriert"]
                            }
                            if email.contains("email") {
                                errors += ["Diese E-Mail ist unzulässig"]
                            }
                            if name.contains("min") {
                                errors += ["Wähle einen längeren Namen"]
                            }
                            if name.contains("alpha") {
                                errors += ["Nur Buchstaben sind im Namen erlaubt"]
                            }
                            // check emtyness

                            if name.contains("required") {
                                errors += ["Gib deinen Namen ein"]
                            }
                            if email.contains("required") {
                                errors += ["Gib deine Mail an"]
                            }
                            if password.contains("required") {
                                errors += ["Setze ein Passwort"]
                            }

                            if password.contains("min") {
                                errors += ["Benutze ein längeres Passwort"]
                            }
                            handler(errors)
                        }
                    }
                }
    }


    static func newPassword(_ token: String, _ password: String, handler: @escaping (_ success : Bool) -> Void) {

        let url = URL(string: HTTPRequest.rootUrl + "auth/password/" + token)!

        let headers = [
                "Token": Constants.API.accessToken,
                "Content-Type": "application/json"
        ]

        let parameters = [
                "password": password
        ]

        Alamofire
                .request(url,
                        method: .post,
                        parameters: parameters,
                        encoding: JSONEncoding.default,
                        headers: headers)
                .validate()
                .debugRequest(url.absoluteString)
                .responseJSON { response in


                    switch response.result {
                    case .failure(let error):
                        // OnDebugging.print("Error - \(error.localizedDescription)")
                        handler(false)
                    case .success:

                        guard let value = response.result.value else {
                            OnDebugging.print("response.result.value == nil")
                            return
                        }

                        handler(true)

                        return

                    }
                }


    }


    static func resetPassword(_ email: String, handler: @escaping (_ errors: [String]?) -> Void) {
        // Production
        //
        let url = URL(string: HTTPRequest.rootUrl + "auth/reset")!
        let headers = [
                "Token": Constants.API.accessToken,
                "Content-Type": "application/json"
        ]
        let parameters = [
                "email": email
        ]

        Alamofire
                .request(url,
                        method: .post,
                        parameters: parameters,
                        encoding: JSONEncoding.default,
                        headers: headers)
                .validate()
                .responseJSON { response in
                    switch response.result {
                    case .failure(let error):
                        OnDebugging.print("Error - \(error.localizedDescription)")
                        handler([error.localizedDescription])
                    case .success:
                        guard let value = response.result.value else {
                            OnDebugging.print("response.result.value == nil")
                            return
                        }

                        let json = JSON(value)
                        let responseDictionary = json.dictionaryValue

                        // posible error
                        if let errorString = responseDictionary["message"]?.stringValue {
                            OnDebugging.print("Error - \(errorString)")
                            handler(["Error - \(errorString)"])
                            return
                        }

                        if let responseErrors = responseDictionary["errors"]?.dictionaryValue {
                            // posible error
                            OnDebugging.print("Error - \(responseErrors)")
                            handler(["Error - \(responseErrors)"])
                        } else {
                            handler(nil)
                        }
                    }
                }
    }


}
