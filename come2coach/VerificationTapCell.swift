//
// Created by Bernd Freier on 21.02.17.
// Copyright (c) 2017 MaytecNet. All rights reserved.
//

import Foundation

protocol VerificationDescrCellDelegate: class {
    func descrStep(_ sender: AnyObject, value: String)
}

class VerificationTapCell: UITableViewCell {

    weak var delegate: VerificationDescrCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    @IBAction func buttonPressed(_ sender: Any) {

        delegate?.descrStep(self, value: "")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
