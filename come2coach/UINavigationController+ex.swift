//
//  UINavigationController+ex.swift
//  come2coach
//
//  Created by Marc Ortlieb on 01.08.16.
//  Copyright © 2016 Marc Ortlieb. All rights reserved.
//

import UIKit


extension UINavigationController {


    func push(_ viewController: UIViewController, animated: Bool = true, completion: (() -> Void)? = nil) {
        pushViewController(viewController, animated: animated)
        
        guard let _completion = completion else {return}
        
        guard animated, let coordinator = transitionCoordinator else {
            _completion()
            return
        }
        
        coordinator.animate(
            // pass nil here or do something animated if you'd like, e.g.:
            alongsideTransition: { context in
                viewController.setNeedsStatusBarAppearanceUpdate()
            },
            completion: { context in
                _completion()
            }
        )
    }
    
    func callCompletion(_ viewController: UIViewController? = nil, animated: Bool = true, completion: (() -> Void)? = nil)
    {
        guard let _completion = completion else {return}
        
        guard animated, let coordinator = transitionCoordinator else {
            _completion()
            return
        }
        
        coordinator.animate(
            // pass nil here or do something animated if you'd like, e.g.:
            alongsideTransition: { context in
                viewController?.setNeedsStatusBarAppearanceUpdate()
            },
            completion: { context in
                _completion()
            }
        )
    }

    func pop(to viewController: UIViewController, animated: Bool = true, completion: ((() -> Void)?) = nil) {
        let viewControllerList = popToViewController(viewController, animated:animated)
        let poppedVC = viewControllerList?.safeIndex(0)
        callCompletion(poppedVC, animated:animated, completion: completion)
    }

    func popViewController(animated: Bool = true, completion: (() -> Void)? = nil) {
        let poppedVC = self.popViewController(animated: animated)
        callCompletion(poppedVC, animated:animated, completion: completion)
    }

    func popToRoot(_ animated: Bool = false, completion: (() -> Void)? = nil) {
        let viewControllerList = popToRootViewController(animated: animated)
        let poppedVC = viewControllerList?.safeIndex(0)
        callCompletion(poppedVC, animated:animated, completion: completion)
    }
    
    convenience init(_ wrappedRootViewController: UIViewController) {
        self.init(rootViewController: wrappedRootViewController)
    }
}

