//
//  CoachPublishVC.swift
//  come2coach
//
//  Copyright © 2016 MaytecNet. All rights reserved.
//

import R5Streaming

class CoachPublishVC: R5VideoViewController, R5StreamDelegate {
    var stream: R5Stream?
    var statusCallback: ((_ statusCode: Int32, _ message: String?) -> Void)?
    var streamName: String?
    var wasStopped = false
    var env : EnviormentConfiguration?
    var recordType : R5RecordType = R5RecordTypeRecord


    override func viewWillAppear(_ animated: Bool) {

        super.viewWillAppear(animated)

        wasStopped = false

        view.backgroundColor = UIColor.black

        if (setupStream(env!)) {

            if let pubStreamName = streamName {
                startStreamWithName(pubStreamName)
            }

        } else {
            self.stream = nil
        }


    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        wasStopped = true

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    func statusMessage(_ msg: String) {
             DispatchQueue.main.async {
                self.statusCallback!(0, msg)
            }
    }


    /*
        Change Video Source front/back camera
    */
    func changeVideoStreamSource() {

        var frontCamera: AVCaptureDevice?
        var backCamera: AVCaptureDevice?

        for device in AVCaptureDevice.devices() {
            let device = device as! AVCaptureDevice
            if frontCamera == nil && device.position == AVCaptureDevicePosition.front {
                frontCamera = device
                continue;
            } else if backCamera == nil && device.position == AVCaptureDevicePosition.back {
                backCamera = device
            }

        }

        let camera = stream?.getVideoSource() as! R5Camera
        if (camera.device === frontCamera) {
            camera.device = backCamera;
        } else {
            camera.device = frontCamera;
        }

    }

    func setupStream(_ env : EnviormentConfiguration) -> Bool {

        let config = Constants.getLiveStreamingConfig(env)

        let vStatus = AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo)

        // before attaching devices, check permission
        switch vStatus {

        case .authorized:
            // go ahead
            break
        case .denied, .restricted:
            // the user explicitly denied camera usage or is not allowed to access the camera devices
            statusMessage("Die App hat keine Berechtigung deine Video Kamera zu nutzen. Prüfe deine Berechtigungen in den Einstellungen.".localized)
            return false
        default:
            break
        }

        // Set up the connection and stream
        if let connection = R5Connection(config: config) {

            if let stream = R5Stream(connection: connection) {

                self.stream = stream

                self.stream?.delegate = self

                // Attach the video from camera to stream
                let videoDevice = AVCaptureDevice.devices(withMediaType: AVMediaTypeVideo).last as? AVCaptureDevice
                if videoDevice != nil {

                    if let camera = R5Camera(device: videoDevice, andBitRate: env.red5BitRate) {
                        camera.width = Constants.API.liveRecordingWidth
                        camera.height = Constants.API.liveRecordingHeight
                        camera.orientation = 90
                        camera.adaptiveBitRate = env.red5adaptiveBitRate
                        camera.fps = env.red5fps

                        self.stream?.attachVideo(camera)
                    }
                }

                // Attach the audio from microphone to stream
                let audioDevice = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeAudio)
                if (audioDevice != nil) {
                    if let microphone = R5Microphone(device: audioDevice) {
                        microphone.bitrate = 32
                        microphone.device = audioDevice;

                        self.stream?.attachAudio(microphone)
                    }
                }

                if (audioDevice != nil && videoDevice == nil) {
                    statusMessage("Dein Video ist nicht verfügbar, die Teilnehmer können Dich nicht sehen. Prüfe deine Einstellungen.".localized)
                }

                if (audioDevice == nil && videoDevice != nil) {
                    statusMessage("Dein Mikrofon ist nicht verfügbar, du kannst nicht sprechen. Prüfe deine Einstellungen.".localized)
                }

                if (audioDevice == nil && videoDevice == nil) {

                    statusMessage("Deine Video Kamera und dein Mikrofon ist nicht verfügbar. Prüfe deine Einstellungen.".localized)

                    return false
                }

                self.attach(self.stream)

                return true

            } else {
                return false
            }
        } else {
            return false
        }

    }

    // deprecated
    func attach2Stream() {
        if let pubStream = self.stream {
            self.attach(pubStream)
        }
    }


    func startStreamWithName(_ myStreamName: String) {

        streamName = myStreamName
        startStream()
    }


    func startStream() {

        guard let currStreamName = streamName else {
            return
        }

        if let myStream = self.stream {

            // add adaptive bit rate controller
            if let adaptive = env?.red5adaptiveBitRate {
                if (adaptive) {
                    let controller = R5AdaptiveBitrateController()
                    controller.attach(to: myStream)
                    controller.requiresVideo = true
                }
                else {
                    self.attach(myStream)
                }
            } else  {
                self.attach(myStream)
            }

            self.stream?.delegate = self

            // #prerecording, each stream gets now recorded by default
            // myStream.publish(currStreamName, type: R5RecordTypeLive)

            myStream.publish(currStreamName, type: recordType)

        } else {
            debugPrint("RED5: I have no stream - can not start stream !!")
        }

        OnDebugging.print("Starting Streamname: \(streamName)")

    }

    func continueStream() {

        wasStopped = false
        stream?.delegate = self

        startStream()

    }

    func stop() {

        wasStopped = true

        if let s = stream {


            s.stop()


            s.delegate = nil
        }

    }


    //R5StreamDelegate protocol functions

    func onR5StreamStatus(_ stream: R5Stream!, withStatus statusCode: Int32, withMessage msg: String!) {


        if (!wasStopped) {

            debugPrint("R5 status message " + msg)

            // #lvdebug - correct reconnect coding
            if (statusCode == Int32(r5_status_connection_error.rawValue)) {

                debugPrint("Reconnect to RED5..")

                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(2.0 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: {
                    self.startStream()
                })

            }

            if (msg == "NetStream.Play.InSufficientBW.Video") {

                self.statusMessage("Dein Internet ist zu schwach. Du kannst jetzt den Webinar-Raum jetzt nur hören. Schalte Dich auf Video, sobald dein Internet besser ist.".localized)

            } else if (msg == "NetStream.Play.InSufficientBW.Audio") {

                self.statusMessage("Dein Internet ist zu schwach. Schalte Dich auf Audio, sobald dein Internet besser ist.".localized)

            } /* else {

                if self.statusCallback != nil {
                    statusMessage(self.coachMessageForR5Status(r5_status(UInt32(statusCode))))
                }
            } */

            // #lvdebug - handle bandwidth problems
            if (msg == "NetStream.Play.InSufficientBW.Video" ||
                    msg == "NetStream.Play.InSufficientBW.Audio") {

                debugPrint("Reconnnect cause video lost..")
                /*

                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 7) {

                    self.blockStatusMessage = false

                    self.statusMessage("Verbinde Dich erneut...".localized)

                    self.stop()
                    self.startStream()
                }
                */

            }

        }

    }

    func coachMessageForR5Status(_ status: r5_status) -> String {
        switch status {
                //!< A connection with the server has been established.  Streaming has *not* started yet.
        case r5_status_connected:
            return "Verbunden".localized
                //!< The connection with the server has been lost.
        case r5_status_disconnected:
            return "Verbindungsprobleme".localized
                //!< There was an error with the connection.
        case r5_status_connection_error:
            return "Fehler beim Verbinden".localized
                //!< The connection has failed due to timeout.
        case r5_status_connection_timeout:
            return "Timeout".localized
                //!< The connection is fully closed.  Wait for this before releasing assets.
        case r5_status_connection_close:
            return "Verbindung geschlossen".localized
                //!< Streaming content has begun as a publisher or subscriber
        case r5_status_start_streaming:
            return "Und Aktion...".localized
                //!< Streaming content has stopped.
        case r5_status_stop_streaming:
            return "Streaming wurde beendet".localized
                //!< A netstatus event has been received from the server.
        case r5_status_netstatus:
            return "Netzwerkprobleme".localized
        default:
            return "unbekannter Fehler"
        }

    }


}
