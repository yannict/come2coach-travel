//
// Created by Bernd Freier on 21.02.17.
// Copyright (c) 2017 MaytecNet. All rights reserved.
//

import Foundation

class VerificationVC1 : UIViewController {

    @IBAction func btnRegisterPressed(_ sender: Any) {

        // show(Storyboards.ViewControllers.verifyCoach, animated: true, modally: true)

        show(Storyboards.ViewControllers.verifyCoach, animated: true)

    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    
}
