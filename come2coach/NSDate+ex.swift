//
//  NSDate+ex.swift
//  come2coach
//
//  Created by Marc Ortlieb on 04.08.16.
//  Copyright © 2016 Marc Ortlieb. All rights reserved.
//
// RESSOURCES:
// ----------------------------------------------------
// http://stackoverflow.com/questions/27182023/getting-the-difference-between-two-nsdates-in-months-days-hours-minutes-seconds
// NSDate Tutorial: // https://www.appcoda.com/nsdate/
// ----------------------------------------------------

import UIKit


var now: Date {
    return Date()
}


extension Date {

    /// format "yyyy-MM-dd HH:mm:ss.SSSSSS", eg: "2016-08-31 08:00:00.000000"

    func makeDateString() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"

        return dateFormatter.string(from: self)
    }

    func makeDateStringWith(_ format: String = "yyyy-MM-dd HH:mm:ss") -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format

        return dateFormatter.string(from: self)
    }

    func makeFromDateString() -> String {

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter.string(from: self) + " 00:00:00"
    }

    func makeToDateString() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"

        return dateFormatter.string(from: self) + " 23:59:00"
    }

    func seconds(to dateString: String) -> Int? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss.SSSSSS"

        guard let startDate = dateFormatter.date(from: dateString) else {
            OnDebugging
                    .printError("dateString: \(dateString) is not a valid input type!",
                    title: "extension NSDate.seconds(to dateString: String)")
            return nil
        }
        return startDate.secondsFrom(self)
    }

    /// default-dateFormat == "yyyy-MM-dd HH:mm:ss.SSSSSS", eg: "2016-08-31 08:00:00.000000"

    static func makeDate(_ string: String, dateFormat: String = "yyyy-MM-dd HH:mm:ss.SSSSSS") -> Date? {
        //
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat
        return dateFormatter.date(from: string)
    }

    static func makeDateString(_ date: Date, dateFormat: String = "dd.MM.yy") -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat
        return dateFormatter.string(from: date)
    }

    static func makeTimeString(_ date: Date, dateFormat: String = "HH:mm") -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat
        return dateFormatter.string(from: date)
    }


    func yearsFrom(_ date: Date) -> Int {
        return (Calendar.current as NSCalendar).components(.year, from: date, to: self, options: []).year!
    }

    func monthsFrom(_ date: Date) -> Int {
        return (Calendar.current as NSCalendar).components(.month, from: date, to: self, options: []).month!
    }

    func weeksFrom(_ date: Date) -> Int {
        return (Calendar.current as NSCalendar).components(.weekOfYear, from: date, to: self, options: []).weekOfYear!
    }

    func daysFrom(_ date: Date) -> Int {
        return (Calendar.current as NSCalendar).components(.day, from: date, to: self, options: []).day!
    }

    func hoursFrom(_ date: Date) -> Int {
        return (Calendar.current as NSCalendar).components(.hour, from: date, to: self, options: []).hour!
    }

    func minutesFrom(_ date: Date) -> Int {
        return (Calendar.current as NSCalendar).components(.minute, from: date, to: self, options: []).minute!
    }

    func secondsFrom(_ date: Date) -> Int {
        return (Calendar.current as NSCalendar).components(.second, from: date, to: self, options: []).second!
    }

    func offsetFrom(_ date: Date) -> String {
        if yearsFrom(date) > 0 {
            return "\(yearsFrom(date))y"
        }
        if monthsFrom(date) > 0 {
            return "\(monthsFrom(date))M"
        }
        if weeksFrom(date) > 0 {
            return "\(weeksFrom(date))w"
        }
        if daysFrom(date) > 0 {
            return "\(daysFrom(date))d"
        }
        if hoursFrom(date) > 0 {
            return "\(hoursFrom(date))h"
        }
        if minutesFrom(date) > 0 {
            return "\(minutesFrom(date))m"
        }
        if secondsFrom(date) > 0 {
            return "\(secondsFrom(date))s"
        }
        return ""
    }
}


// MARK: Compare two NSDate Objects
// Source: http://stackoverflow.com/questions/26198526/nsdate-comparison-using-swift


public func <(lhs: Date, rhs: Date) -> Bool {
    return lhs.compare(rhs) == .orderedAscending
}

// extension Date: Comparable {}  #swift3


/// add seconds to a date to get new Date

public func +(lhs: Date, rhs: TimeInterval) -> Date {
    return lhs.addingTimeInterval(rhs)
}

/// subtract seconds from Date to get new Date
public func -(lhs: Date, rhs: TimeInterval) -> Date {
    return lhs.addingTimeInterval(-rhs)
}


