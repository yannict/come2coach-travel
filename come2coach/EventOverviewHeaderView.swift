//
//  HeaderView.swift
//  come2coach
//
//  Created by Timotheus Laubengaier on 24.04.17.
//  Copyright © 2017 MaytecNet. All rights reserved.
//

import Foundation
import UIKit

protocol EventOverviewHeaderViewDelegate {
  func segmentViewPressed()
}

class EventOverviewHeaderView: UIView {
  
  var delegate: EventOverviewHeaderViewDelegate?
  
  var options: [String] = []
//  {
//    didSet {
//      segmentedView.removeAllSegments()
//      for item in options {
//        segmentedView.insertSegment(withTitle: item, at: 0, animated: false)
//      }
//      segmentedView.selectedSegmentIndex = 1
//    }
//  }
  
  let titleLabel: UILabel = {
    let label = UILabel()
    label.textColor = Styleguide.Color.text
    label.font = Styleguide.Font.medium(size: 13)
    label.text = "Hidden Beach in Malaysia"
    label.textAlignment = .center
    return label
  }()
  
  let segmentedView: UISegmentedControl = {
    let view = UISegmentedControl(items: ["Map", "Livestream", "Videostream"])
    view.tintColor = Styleguide.Color.text    
    return view
  }()
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    
    addSubview(titleLabel)
    titleLabel.snp.makeConstraints { (make) in
      make.top.equalTo(self)
      make.leading.equalTo(self)
      make.trailing.equalTo(self)
    }
    
    segmentedView.addTarget(self, action: #selector(segmentViewPressed), for: .valueChanged)
    addSubview(segmentedView)
    segmentedView.snp.makeConstraints { (make) in
      //make.top.equalTo(titleLabel.snp.bottom).offset(20)
      make.top.equalTo(self).offset(30)
      make.leading.equalTo(self).offset(20)
      make.trailing.equalTo(self).offset(-20)
      make.height.equalTo(30)
    }
    
  }
  
  func segmentViewPressed() {
    delegate?.segmentViewPressed()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
}
