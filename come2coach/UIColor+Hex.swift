//
//  UIColor+Hex.swift
//  come2coach
//
//  Created by Timotheus Laubengaier on 24.04.17.
//  Copyright © 2017 MaytecNet. All rights reserved.
//

import Foundation

extension UIColor {
  
  convenience init(hex: Int) {
    let components = (
      R: CGFloat((hex >> 16) & 0xff) / 255,
      G: CGFloat((hex >> 08) & 0xff) / 255,
      B: CGFloat((hex >> 00) & 0xff) / 255
    )
    self.init(red: components.R, green: components.G, blue: components.B, alpha: 1)
  }
  
}
