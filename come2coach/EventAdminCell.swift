//
//  EventAdminCell.swift
//  come2coach
//
//  Created by Victor on 07.11.16.
//  Copyright © 2016 MaytecNet. All rights reserved.
//

import Foundation

protocol EventAdminCellDelegate {
    func changeParticipantsForEvent(_ event: Event)

    func shareEvent(_ event: Event)

    func republishEvent(_ event: Event)

    func sendMail(_ event: Event)
}

class EventAdminCell: UICollectionViewCell {

    @IBOutlet weak var eventImageView: UIImageView!
    @IBOutlet weak var eventTitleLabel: UILabel!
    @IBOutlet weak var eventStateLabel: UILabel!
    @IBOutlet weak var changeSubscriberButton: UIButton?
    @IBOutlet weak var mailButton: UIButton!
    @IBOutlet weak var shareButton: UIButton?
    @IBOutlet weak var republishButton: UIButton?

    var event: Event?
    var delegate: EventAdminCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.cornerRadius = 4.0
        if Constants.Sizes.screenWidth < 375 {
            for tag in 100 ..< 103 {
                if let buttonLabel = viewWithTag(tag) as? UILabel {
                    buttonLabel.font = UIFont.init(name: "Montserrat-Light", size: 8.0)
                }
            }
        }
    }

    func cleanInfo() {
        eventTitleLabel.text = ""
        eventImageView.image = UIImage.init(named: "placeholder")
        eventStateLabel.attributedText = NSAttributedString.init(string: "")
    }

    func fillEventInfo(_ event: Event) {
        self.event = event
        eventTitleLabel.text = event.title
        if event.img1x1PathExtension != "" {
            eventImageView.sd_setImage(with: event.image1xURL as URL!)
        }
        eventStateLabel.attributedText = stateStringForEvent(event)
    }

    @IBAction func changeParticipants() {
        if let _ = delegate {
            delegate!.changeParticipantsForEvent(event!)
        }
    }

    @IBAction func share() {
        if let _ = delegate {
            delegate!.shareEvent(event!)
        }
    }

    @IBAction func republish() {
        if let _ = delegate {
            delegate!.republishEvent(event!)
        }
    }

    @IBAction func sendMail() {
        if let _ = delegate {
            delegate!.sendMail(event!)
        }
    }

    fileprivate func stateStringForEvent(_ event: Event) -> NSAttributedString {

        let currentEvent = LiveStreamingHandler.getEventStatus(event)

        if let time = currentEvent.currentDate {

            let timeAttributes: NSDictionary = [NSForegroundColorAttributeName: UIColor.black]
            let timeString = NSMutableAttributedString(string: time.shortTimeString + " Uhr | ".localized, attributes: timeAttributes as? [String: AnyObject])

            //todo: hier auf live wechseln
            let stateAttributes: NSDictionary = [NSForegroundColorAttributeName: Designs.Colors.coachingTypeBlue]
            //let stateString = NSMutableAttributedString(string: event.isWebinar == true ? "Livestream" : "Videostream")
            let stateString = NSAttributedString(string: event.live == 1 ? "Livestream".localized : "Videostream".localized, attributes: stateAttributes as? [String: AnyObject])

            timeString.append(stateString)

            return timeString

        } else {

            let stateAttributes: NSDictionary = [NSForegroundColorAttributeName: Designs.Colors.coachingTypeBlue]
            return NSMutableAttributedString(string: event.live == 1 ? "Livestream".localized : "Videostream".localized)

        }



        /*

        let timeString = NSAttributedString(string: ", " + EventDateFormatter.timeLeftToDate(event.begin.date), attributes: timeAttributes as? [String : AnyObject])
        stateString.append(timeString)
        return stateString
        */
    }

}
