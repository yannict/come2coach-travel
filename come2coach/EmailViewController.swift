//
//  EmailViewController.swift
//  come2coach
//
//  Created by Victor on 16.11.16.
//  Copyright © 2016 MaytecNet. All rights reserved.
//

import Foundation

class EmailViewController: UIViewController {
    
    @IBOutlet weak var emailField : UITextView!
    var event : Event!
    
    @IBAction func back() {
        self.dismissViewController(true, completion: nil)
    }
    
    @IBAction func sendMail() {
        HTTPRequest.mailEvent(with: event, text: emailField.text) { (success) in
            self.back()
        }
    }
    
}
