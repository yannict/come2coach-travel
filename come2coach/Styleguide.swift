//
//  Styleguide.swift
//  come2coach
//
//  Created by Timotheus Laubengaier on 24.04.17.
//  Copyright © 2017 MaytecNet. All rights reserved.
//

import Foundation
import UIKit

struct Styleguide {
  
  struct Roundness {
    static let main: CGFloat = 5
  }
  
  struct Color {
    
    static let primary = UIColor(hex: 0x00DEBB)
    static let secondary = UIColor(hex: 0x02145A)
    
    static let tint = UIColor(hex: 0x00DEBB)
    static let tintAlt = UIColor(hex: 0x00DED5)
    
    static let lightGray = UIColor(hex: 0xEEEEEE)
    
    static let text = UIColor(hex: 0x02145A)
  }
  
  struct Font {
    
    static func light(size: CGFloat) -> UIFont? {
      return UIFont(name: "Montserrat-Light", size: size)
    }
    
    static func regular(size: CGFloat) -> UIFont? {
      return UIFont(name: "Montserrat-Regular", size: size)
    }
    
    static func medium(size: CGFloat) -> UIFont? {
      return UIFont(name: "Montserrat-SemiBold", size: size)
    }
    
    static func bold(size: CGFloat) -> UIFont? {
      return UIFont(name: "Montserrat-Bold", size: size)
    }
    
  }
  
  struct Gradient {
    
    // https://developer.apple.com/reference/quartzcore/cagradientlayer
    static let main = [Color.tint.cgColor, Color.tintAlt.cgColor]
    
  }
  
  struct Button {
    
    static func standard(title: String, bottomLine: Bool = false, topLine: Bool = false) -> UIButton {
      let button = UIButton(type: .system)
      button.setTitle(title, for: .normal)
      button.titleLabel?.font = Font.medium(size: 13)
      button.backgroundColor = Color.tint
      button.tintColor = .white
      
      // for stacked buttons
      if bottomLine {
        let bottomLine = UIView()
        bottomLine.backgroundColor = UIColor.white.withAlphaComponent(0.55)
        button.addSubview(bottomLine)
        bottomLine.snp.makeConstraints { (make) in
          make.bottom.equalTo(button)
          make.leading.equalTo(button)
          make.trailing.equalTo(button)
          make.height.equalTo(1)
        }
      }
      if topLine {
        let topLine = UIView()
        topLine.backgroundColor = UIColor.white.withAlphaComponent(0.55)
        button.addSubview(topLine)
        topLine.snp.makeConstraints { (make) in
          make.top.equalTo(button)
          make.leading.equalTo(button)
          make.trailing.equalTo(button)
          make.height.equalTo(1)
        }
      }
      
      return button
    }
    
    static func rating(title: String) -> UIButton {
      let button = UIButton(type: .system)
      button.tintColor = Styleguide.Color.text
      button.setTitle(title, for: .normal)
      button.titleLabel?.textColor = Styleguide.Color.text
      button.titleLabel?.font = Font.medium(size: 13)
      button.backgroundColor = Color.tint
      button.layer.cornerRadius = 16
      return button
    }
    
  }
  
  struct Image {
    
    static let vacationImages = [
      "https://instagram.ftxl1-1.fna.fbcdn.net/t51.2885-15/s750x750/sh0.08/e35/18161375_1885360708371093_5494288710990036992_n.jpg",
      "https://instagram.ftxl1-1.fna.fbcdn.net/t51.2885-15/e35/18013769_1936189006615710_7303618292306935808_n.jpg",
      "https://instagram.ftxl1-1.fna.fbcdn.net/t51.2885-15/e35/18096173_262620014147052_3251570253464862720_n.jpg",
      "https://instagram.ftxl1-1.fna.fbcdn.net/t51.2885-15/e35/18013709_123404048209431_4912933787618246656_n.jpg",
      "https://instagram.ftxl1-1.fna.fbcdn.net/t51.2885-15/s640x640/sh0.08/e35/18160957_169128530278096_467860476999499776_n.jpg",
      "https://instagram.ftxl1-1.fna.fbcdn.net/t51.2885-15/e35/18160564_286941005088723_4744409239542300672_n.jpg",
      "https://instagram.ftxl1-1.fna.fbcdn.net/t51.2885-15/e35/18160719_360274197708191_6302504232138309632_n.jpg",
      "https://instagram.ftxl1-1.fna.fbcdn.net/t51.2885-15/e35/18094973_1944117045811635_1976040553682305024_n.jpg",
      "https://instagram.ftxl1-1.fna.fbcdn.net/t51.2885-15/e35/18094539_1411482475541121_9070744915087982592_n.jpg"
    ]
    
    static func randomImagePath() -> String {
      let count = Image.vacationImages.count
      let rnd = Int(arc4random_uniform(UInt32(count)))
      return Image.vacationImages[rnd]
    }
    
  }
  
}
