//
//  UIView.swift
//  Dreamcatcher
//  UIView+AddSubViews.swift
//  come2coach
//
//  Created by Marc Orlieb on 17.06.16.
//  Copyright © 2016 Marc Ortlieb. All rights reserved.
//  Created by Marc Ortlieb on 06.08.16.
//  Copyright © 2016 MaytecNet. All rights reserved.
//

import UIKit

/// shorthand for add subview to view

func +=(left: UIView, right: UIView) {
    return left.addSubview(right)
}

/// add bunch of subviews to a view

func +=(left: UIView, right: [UIView]) {
    return left.addSubviews(right)
}


extension UIView {
    
    /// add a bunch of subviews
    
    func addSubviews(_ subviews: [UIView]) {
        subviews.forEach {addSubview($0)}
    }
    
    func setSinglePixelBorder(_ color:UIColor)
    {
        let layer = self.layer
        layer.borderColor = color.cgColor
        let borderWidth = CGFloat(1.0 / UIScreen.main.scale)
        layer.borderWidth = borderWidth
        layer.allowsEdgeAntialiasing = true
        layer.edgeAntialiasingMask = [.layerBottomEdge, .layerTopEdge, .layerLeftEdge, .layerRightEdge]
        layer.needsDisplayOnBoundsChange = false
        layer.rasterizationScale = UIScreen.main.scale
        //layer.contentsScale = UIScreen.mainScreen().scale
    }

}



