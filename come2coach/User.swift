//
//  User.swift
//  come2coach
//
//  Created by Marc Ortlieb on 31.07.16.
//  Copyright © 2016 Marc Ortlieb. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON


class User: Object {

    static var newDeviceToken : String?

    static var loginManager = LoginController()

    static func checkConnection(_ view : UIViewController) {

        view.present(UIAlertController("Internet".localized, "Keine Netzwerkverbindung.".localized,
                actions: UIAlertAction(.OK)), animated: true)
    }

    enum LoginType: String {
        case come2coach, facebook
    }

    dynamic var id = 0

    dynamic var lastLoginType = ""
    // User.LoginType.rowValue
    dynamic var JSONWebToken = ""

    dynamic var name = ""
    dynamic var lname = ""

    dynamic var email = ""
    dynamic var password = ""
    dynamic var facebookToken = ""
    dynamic var about = ""
    dynamic var wishlistReminder = 0
    dynamic var push_token = ""

    dynamic var mobile = ""
    dynamic var paypalEmail = ""

    dynamic var provider = "internal"
    dynamic var provider_id = "1234567890"
    dynamic var provider_token = "1234567890"

    // added property by Maytec Team -- above previous team

    dynamic var subscribeNewsletter = true

    /// User did phoneNr verification to be allowed to make new events as COACH
    dynamic var isVerified = 0

    dynamic var image: Data?
    dynamic var imageStringUrl: String?
    dynamic var imageName: String?

    dynamic var earnings  = 0.0
    dynamic var coins = 0


    dynamic var realmID = Constants.RealmID.user
//    /** REALM: primaryKey - https://realm.io/docs/swift/latest/#primary-keys
//     primaryKey needs to be a class function which returns the name of the property which is the primary key, not an instance method which returns the value of the primary key.
//     additionally dynamic var realmID = Constants.Realm.PrimaryKeys.user
//     */
    override class func primaryKey() -> String? {
        return "realmID"
    }




    /**
     load user() from Realm, using PrimaryKey: Constants.RealmID.user
     */

    static var current: User? {
        if let user = try? Realm().objects(User.self).resultsSafeIndex(0) {
            return user
        }
        return nil
    }

    /**
        Password Reset Token (set by AppDelegate / c2c Protocol )
    **/
    static var token: String?


    /**
     REALM: Objects: Update or create & add to Realm https://realm.io/docs/swift/latest/#creating-and-updating-objects-with-primary-keys
     if object is brand new to REALM adds object,
     if object already exists, updates values (for existing User-Object with same primaryKey)
     */
    func save() {
        defaultRealm
                .add(self, update: true)
                .printErrors()
    }

    func firstSaveFoRProfile(_ name: String = "", lname: String = "", password: String = "", email: String = "", mobile: String = "", ppEmail: String = "", imageUrl: String = "") {
        defaultRealm.write { (realm) in
            if name.characters.count > 0 {
                self.name = name
            }
            if lname.characters.count > 0 {
                self.lname = lname
            }
            if password.characters.count > 0 {
                self.password = password
            }
            if email.characters.count > 0 {
                self.email = email
            }
            if mobile.characters.count > 0 {
                self.mobile = mobile
            }
            if ppEmail.characters.count > 0 {
                self.paypalEmail = ppEmail
            }
            if imageUrl.characters.count > 0 {
                self.imageStringUrl = imageUrl
            }

            let realmNew = realm as Realm
            realmNew.add(self, update: true)
        }
    }

    // REALM: Update Object
    func write(_ name: String = "", lname: String = "", password: String = "", email: String = "",
               mobile: String = "",
               ppEmail: String = "",
               imageUrl: String = "",
               about: String = "",
               pushToken: String = "",
               wishListReminder: Int? = nil,
               verified: Int? = nil,
               id: Int = 0,
               coins: Int? = nil,
               earnings : Double? = nil,
               completion: (() -> Void)? = nil) {
        defaultRealm.write { _ in
            if name.characters.count > 0 {
                self.name = name
            }
            if lname.characters.count > 0 {
                self.lname = lname
            }
            if password.characters.count > 0 {
                self.password = password
            }
            if email.characters.count > 0 {
                self.email = email
            }
            if mobile.characters.count > 0 {
                self.mobile = mobile
            }
            if let cns = coins {
                self.coins = cns
            }

            if let earn = earnings {
                self.earnings = earn
            }
            if (!about.isEmpty) {
                self.about = about
            }
            if id > 0 {
                self.id = id
            }
            if ppEmail.characters.count > 0 {
                self.paypalEmail = ppEmail
                //completion!()
            }
            if imageUrl.characters.count > 0 {
                self.imageStringUrl = imageUrl

                let imageName = imageUrl.replacingOccurrences(of: "https://c2c-app.s3.amazonaws.com/c2c-app/", with: "")
                // #swift3
                // let imageName = imageUrl.stringByReplacingOccurrencesOfString("https://c2c-app.s3.amazonaws.com/c2c-app/", withString: "")
                self.imageName = imageName
            }
            if let v = verified {
                self.isVerified = v;
            }
            if (!pushToken.isEmpty) {
                self.push_token = pushToken
            }

            if let wl = wishListReminder {
                self.wishlistReminder = wl
            }

        }
    }

    // REALM: Delete Object
    func delete() {
        defaultRealm
                .delete(self)
                .printErrors()
    }

    func logout() {

        User.loginManager.isAuthenticated = false

        defaultRealm.write { _ in
            self.lastLoginType = ""
        }
    }

    func saveImage(_ image: UIImage) {
        defaultRealm.write { _ in
            self.image = UIImagePNGRepresentation(image)
        }
    }

    func userImage() -> UIImage {
        return UIImage(data: self.image!, scale: 1.0)!
    }


}


