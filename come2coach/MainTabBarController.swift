//
//  MainTabBarController.swift
//  come2coach
//
//  Created by Victor on 01.11.16.
//  Copyright © 2016 MaytecNet. All rights reserved.
//

import Foundation

class MainTabBarController: UITabBarController {

    //MARK: Life cycle
    // static var showFavorites = false

    static var sharedInstance : MainTabBarController?

    override func viewDidLoad() {
        super.viewDidLoad()
      
      tabBar.tintColor = Styleguide.Color.tint
      
        navigationController?.navigationBar.isHidden = true
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        let eventOverview = UINavigationController(EventOverviewViewController())
        let eventOverviewTabBarItem = UITabBarItem.init(title: nil, image: #imageLiteral(resourceName: "menueIcon").withRenderingMode(.alwaysOriginal), selectedImage: #imageLiteral(resourceName: "menueIconFilled").withRenderingMode(.alwaysOriginal))
        eventOverviewTabBarItem.imageInsets = UIEdgeInsetsMake(3, 0, -3, 0)
        eventOverview.tabBarItem = eventOverviewTabBarItem
        let eventAdmin = UINavigationController(Storyboards.ViewControllers.eventAdmin)

        let eventAdminTabBarItem = UITabBarItem.init(title: nil, image: UIImage.init(named: "icon_plus")?.withRenderingMode(.alwaysTemplate), selectedImage: UIImage.init(named: "icon_plus_filled")?.withRenderingMode(.alwaysTemplate))
        eventAdminTabBarItem.imageInsets = UIEdgeInsetsMake(3, 0, -3, 0)
        eventAdmin.tabBarItem = eventAdminTabBarItem

        let favoriteEvents = Storyboards.ViewControllers.favoriteEvents
        let favoriteEventsTabBarItem = UITabBarItem.init(title: nil, image: UIImage.init(named: "icon_favorite")?.withRenderingMode(.alwaysTemplate), selectedImage: UIImage.init(named: "icon_favorite_selected")?.withRenderingMode(.alwaysTemplate))

        favoriteEventsTabBarItem.imageInsets = UIEdgeInsetsMake(3, 0, -3, 0)
        favoriteEvents.tabBarItem = favoriteEventsTabBarItem
        let profile = Storyboards.ViewControllers.profile
        let profileTabBarItem = UITabBarItem.init(title: nil, image: UIImage.init(named: "icon_profile")?.withRenderingMode(.alwaysTemplate), selectedImage: UIImage.init(named: "icon_profile_selected")?.withRenderingMode(.alwaysTemplate))
        profileTabBarItem.imageInsets = UIEdgeInsetsMake(3, 0, -3, 0)
        profile.tabBarItem = profileTabBarItem
        self.viewControllers = [eventOverview, eventAdmin, favoriteEvents, profile]

        MainTabBarController.sharedInstance = self

    }

}
