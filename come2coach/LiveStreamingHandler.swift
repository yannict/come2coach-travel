//
// Created by Bernd Freier on 03.01.17.
// Copyright (c) 2017 MaytecNet. All rights reserved.
//

import Foundation
import SVProgressHUD

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func <<T:Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l < r
    case (nil, _?):
        return true
    default:
        return false
    }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func ><T:Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l > r
    default:
        return rhs < lhs
    }
}

class EventStatus {

    var pending: Bool = false
    var running: Bool = false
    var currentDate: EventTimeStamp?
    var index : String = ""

}

class LiveStreamingHandler {

    static func getEventStatus(_ event: Event) -> EventStatus {

        let result = EventStatus()

        // check if begins already started
        result.currentDate = event.begin
        result.index = "begin1"
        var pending = event.begin.date.timeIntervalSinceNow > 0
        var running = !pending && event.begin.date.addingTimeInterval(event.duration).timeIntervalSinceNow > 0

        /*
        #beginchange
        if (!pending && !running) {

            if (event.begin2.used) {
                result.index = "begin2"
                result.currentDate = event.begin2
                pending = event.begin2.date.timeIntervalSinceNow > 0
                running = !pending && event.begin2.date.addingTimeInterval(event.duration).timeIntervalSinceNow > 0
            }

            if (!pending && !running) {

                if (event.begin3.used) {
                    result.index = "begin3"
                    result.currentDate = event.begin3
                    pending = event.begin3.date.timeIntervalSinceNow > 0
                    running = !pending && event.begin3.date.addingTimeInterval(event.duration).timeIntervalSinceNow > 0
                }

            }

        }
        */

        result.running = running
        result.pending = pending

        return result

    }

    open func play(_ eventId: Int, controller: UIViewController) {

        SVProgressHUD.show()

        // first fetch cluster config
        HTTPRequest.getConfiguration() { (env) in

            // get up-to-date event data
            HTTPRequest.fetchEvent(with: eventId) { (event) in

                /*
                let livestreamingParticipantVC = Storyboards.ViewControllers.livestreamingParticipant
                livestreamingParticipantVC.event = event
                livestreamingParticipantVC.env = env
                livestreamingParticipantVC.playRecorded = true

                controller.show(livestreamingParticipantVC, modally: true)
                */
                self.playEvent(event, env, controller)

                SVProgressHUD.dismiss()

            }

        }

    }

    fileprivate func playEvent(_ event : Event, _ env : EnviormentConfiguration?, _ controller: UIViewController) {

        let livestreamingParticipantVC = Storyboards.ViewControllers.livestreamingParticipant
        livestreamingParticipantVC.event = event
        livestreamingParticipantVC.env = env
        livestreamingParticipantVC.playRecorded = true

        controller.show(livestreamingParticipantVC, modally: true)

    }

    open func launch(_ eventId: Int, controller: UIViewController) {

        SVProgressHUD.show()

        // first fetch cluster config
        HTTPRequest.getConfiguration() { (env) in

            // get up-to-date event data
            HTTPRequest.fetchEvent(with: eventId) { (event) in

                // show webinar view

                if (event.live == 1) {

                    if (event.coachEMail == User.current?.email) {
                        // that's the coach.. show coach live stream
                        let livestreamingCoachVC = Storyboards.ViewControllers.livestreamingCoach
                        livestreamingCoachVC.event = event
                        livestreamingCoachVC.env = env
                        controller.show(livestreamingCoachVC, modally: true)

                    } else {

                        let livestreamingParticipantVC = Storyboards.ViewControllers.livestreamingParticipant
                        livestreamingParticipantVC.event = event
                        livestreamingParticipantVC.env = env

                        controller.show(livestreamingParticipantVC, modally: true)
                    }
                } else {

                    self.playEvent(event, env, controller)
                }


                SVProgressHUD.dismiss()

            }

        }
    }


    open func launch_deprecated(_ eventId: Int, controller: UIViewController) {

        SVProgressHUD.show()

        HTTPRequest.getConfiguration() { (env) in

            HTTPRequest.fetchEvent(with: eventId) { (event) in

                // check if begins already started
                var pending = event.begin.date.timeIntervalSinceNow > 0
                var running = !pending && event.begin.date.addingTimeInterval(event.duration).timeIntervalSinceNow > 0
                if (!pending && !running) {

                    if (event.begin2.used) {
                        pending = event.begin2.date.timeIntervalSinceNow > 0
                        running = !pending && event.begin2.date.addingTimeInterval(event.duration).timeIntervalSinceNow > 0
                    }

                    if (!pending && !running) {

                        if (event.begin3.used) {
                            pending = event.begin3.date.timeIntervalSinceNow > 0
                            running = !pending && event.begin3.date.addingTimeInterval(event.duration).timeIntervalSinceNow > 0
                        }

                    }

                }

                //todo: debug!!
                pending = false
                running = true

                // event is running, show live stream
                if (!pending && running) {

                    if (event.coachEMail == User.current?.email) {
                        // that's the coach.. show coach live stream
                        let livestreamingCoachVC = Storyboards.ViewControllers.livestreamingCoach
                        livestreamingCoachVC.event = event
                        livestreamingCoachVC.env = env
                        controller.show(livestreamingCoachVC, modally: true)

                    } else {

                        let livestreamingParticipantVC = Storyboards.ViewControllers.livestreamingParticipant
                        livestreamingParticipantVC.event = event
                        livestreamingParticipantVC.env = env

                        controller.show(livestreamingParticipantVC, modally: true)
                    }
                } else if (pending) {

                    // event is not running, show countdown screen

                    let countDownVC = Storyboards.ViewControllers.countDown
                    countDownVC.event = event
                    controller.show(countDownVC, modally: true)

                } else {

                    controller.present(UIAlertController("Schon vorbei.".localized, "Das Coaching ist leider schon vorbei.".localized,
                            actions: UIAlertAction(.OK)), animated: true)
                }

                SVProgressHUD.dismiss()

            }

        }
    }


}
