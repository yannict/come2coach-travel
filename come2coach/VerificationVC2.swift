//
// Created by Bernd Freier on 21.02.17.
// Copyright (c) 2017 MaytecNet. All rights reserved.
//

import Foundation
import SVProgressHUD

class VerificationVC2 : UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        SVProgressHUD.show()

        HTTPRequest.getUser { (user, errors) in

            SVProgressHUD.dismiss()

            if let u = user {

                if (u.isVerified > 3) {

                    let eventAdmin = Storyboards.ViewControllers.eventAdmin
                    self.navigationController?.push(eventAdmin, animated: true, completion: nil)


                }

            }

        }

    }

}
