//
//  Events.swift
//  come2coach
//
//  Created by Marc Ortlieb on 05.08.16.
//  Copyright © 2016 MaytecNet. All rights reserved.
//

import Foundation
import RealmSwift
import Unbox
import SwiftyJSON

// Placeholders with MOCK data

struct Events {
    static var sharedInstance = Events()
    fileprivate init() {
    }


}


enum UserType {
    case coach, participant
}

class Event: CustomStringConvertible {


    var userType = UserType.participant
    /// If there is a code detached on Website for this particular event one can subscribe for this event for free
    var voucherCode = 123456

    var recordedVideoFile = ""

    var id = 0
    // 123
    var owner = ""
    // vorname des coach
    var ownerImage = ""

    var ownerImageURL: URL? {
        if owner.hasPrefix("http") {
            return URL(string: ownerImage)
        }
        return URL(string: "https://c2c-app.s3.amazonaws.com/" + ownerImage)
    }

    var tmpUser: User? = nil
    var userImage = ""
    var tmpUserImageUrl: String = ""
    var userImageURL: URL? {
        if owner.hasPrefix("http") {
            return URL(string: userImage)
        }
        return URL(string: "https://c2c-app.s3.amazonaws.com/" + userImage)

    }

    var userAvatar = ""
    var ownerRate = 0.0
    var coachEMail = ""
    var coachFirstName = ""
    var coachLastName = ""
    var coachId = 0
    var live = 1

    var handout_url = ""
    var handout_desc = ""
    var ext_affiliate_url = ""
    var ext_affiliate_desc = ""

    var slug = ""
    // "my-webinar"
    var title = ""
    // "My Webinar"
    var img1PathExtension = ""
    var img1x1PathExtension = ""
    var img1RemoteFolder = ""
    var img2RemoteFolder = ""

    // "https://s3.eu-central-1.amazonaws.com/c2c-storage/IMAGE.png"
    var image: UIImage?
    var tmpImageUrl: String = ""
    var imageURL: URL? {
        if img1PathExtension.hasPrefix("http") {
            return URL(string: img1PathExtension)
        }
        return URL(string: "https://c2c-app.s3.amazonaws.com/" + img1PathExtension) //https://c2c-app.s3.eu-central-1.amazonaws.com/
    }

    var image1xURL: URL? {
        if img1x1PathExtension.hasPrefix("http") {
            return URL(string: img1x1PathExtension)
        }
        return URL(string: "https://c2c-app.s3.amazonaws.com/" + img1x1PathExtension) //https://c2c-app.s3.eu-central-1.amazonaws.com/

    }

    var img2PathExtension = ""
    // "https://s3.eu-central-1.amazonaws.com/c2c-storage/IMAGE.png"

    var video = ""

    var image2URL: URL? {
        if img2PathExtension.hasPrefix("http") {
            return URL(string: img2PathExtension)
        }
        return URL(string: "https://c2c-app.s3.amazonaws.com/" + img2PathExtension)
        //        return NSURL(string: img1PathExtension)

    }
    var img3PathExtension = ""
    // "https://s3.eu-central-1.amazonaws.com/c2c-storage/IMAGE.png"

    var image3URL: URL? {
        if img3PathExtension.hasPrefix("http") {
            return URL(string: img3PathExtension)
        }
        return URL(string: "https://c2c-app.s3.amazonaws.com/" + img3PathExtension)
        //        return NSURL(string: img1PathExtension)

    }

    var cat1 = ""
    // "gaming"
    var cat2 = ""
    // "hobby"
    var descriptionText = "DUMMY DESCRIPTION TEXT"
    // ""
    var text_do = "DUMMY DO TEXT"
    var text_learn = "DUMMY LEARN TEXT"

    var tags = [String]()
    // ["computer", "shooter", "games"] // Realm crashes
    var isWebinar = true
    // true || false
    var isPublic = true
    var isLiked = false
    var isWatching = false
    //same as isLiked; hi, dear backend developer
    var price = 0.00
    var minParticipants = 0
    var maxParticipants = 0
    var joinedParticipants = 0


    var duration: TimeInterval = 0
    var adult = 0

    var participants = 0


    var address: Address = Address()
    var begin: EventTimeStamp = EventTimeStamp()
    var begin2: EventTimeStamp  = EventTimeStamp()
    var begin3: EventTimeStamp = EventTimeStamp()
    var otherEvents: [Event] = [Event]()

    var durationArrString: NSString {
        return "\(Int(duration / (60.0 * 60.0))):\(Int((duration / 60.0).truncatingRemainder(dividingBy: 60)))" as NSString
    }

    // dynamic properties

    var endDate: Date {
        // #swift3date - begin.date + duration
        return begin.date.addingTimeInterval(duration)
    }
    var isHappeningNow: Bool {
        return now as Date >= begin.date && !didEnd
    }
    var isInCountDownPhaseNow: Bool {

        if let d = Calendar.current.date(byAdding: .minute, value: -10, to: begin.date) {
            return now.timeIntervalSince1970 > d.timeIntervalSince1970
                    && now.timeIntervalSince1970 < begin.date.timeIntervalSince1970
        } else {

            return false
        }



        // #swift3
        // return now  > (begin.date - 10*60) && now  < begin.date
    }
    var didEnd: Bool {
        return now >= endDate
    }

    func testTimer(_ minutes: Double) {
        begin.date = now.addingTimeInterval(minutes * 60.0) as Date
        HTTPRequest.updateEvent(with: self) { (errors) in
            OnDebugging.printError("Errors updating Event: \(errors)")
        }
    }

    var description: String {
        return "EVENT: \(self.title) starts At: \(self.begin.date) end: \(self.endDate)"
    }

}

// use: after fetching NSData with alamofire
// try unbox(data) as Event

//extension Event: Unboxable {
//    
//    init(unboxer: Unboxer) {
//        id = unboxer.unbox("id")
//        slug = unboxer.unbox("slug")
//        title = unboxer.unbox("title")
//    }
//}

//class Coach {
//    var ratingVotes = 0.0
//    var image = ""
//    var name = ""
//    var id : AnyObject? = nil
//}

class Address {
    var location = ""
    // "IIK Office"
    var street = ""
    // "Schürerstraße 3"
    var city = ""
    // "Würzburg"
    var zip = 00000
    // 97080
    var latitude = 0.00
    // 0.123456789
    var longitude = 0.00
    // 0.123456789
}

class EventTimeStamp {

    init() {

    }

    init(_ dateStr: String, _ tZ: String, _ tZt: String) {

        dateString = dateStr
        setDate(dateStr)
        timeZone = tZ
        timeZoneType = tZt
    }

    func setDate(_ dateStr: String) {

        if let eventDate = Date.makeDate(dateString) {
            date = eventDate
            shortDateString = Date.makeDateString(date)
            shortTimeString = Date.makeTimeString(date)
            used = true
        } else {
            date = Date.errorDate
            used = false
        }
    }

    var dateString = "" {
        // "2016-08-31 08:00:00.000000"
        didSet {
            setDate(dateString)
        }
    }

    // does not work for API
//    var json: [String:AnyObject] {
//        return ["date" : date.makeDateString(),
//         "timezone" : "Europe/Berlin",
//         "timezone_type" : 3]
//    }
    var shortDateString = ""
    var shortTimeString = ""

    var timeZone = ""
    // "Europe\/Berlin"
    var timeZoneType = ""
    // 3

    var date = Date.errorDate
    var used = false

    /// calculates seconds for navigation Logic

    var secTillEventStart: Int? {
        let remainingTime = date.timeIntervalSince(now as Date)
        return Int(remainingTime)
    }

    /// calculates seconds for countDownVC

    var secTillCountDownStart: Int? {
        let tenMin = 10 * 60
        if let secTillEventStart = secTillEventStart {
            return secTillEventStart - tenMin
        }
        return nil
    }
}

extension Date {

    static var errorDate: Date {
        return Date.distantPast
    }
}







