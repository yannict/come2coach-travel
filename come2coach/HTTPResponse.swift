//
//  Alamofire+ValidateStatusCode.swift
//  come2coach
//
//  Created by Marc Ortlieb on 10.08.16.
//  Copyright © 2016 MaytecNet. All rights reserved.
//

import Foundation

class HTTPResponse {
    
    static func validateStatusCode(_ statusCode: Int?) {
        guard let statusCode = statusCode else {
            OnDebugging
                .printError("statusCode == nil", title: "HTTPResponse.validateStatusCode()")
            return
        }
        switch statusCode { // https://httpstatuses.com
        //case 401: break
        default:
            OnDebugging
                .print("statusCode: \(statusCode)", title: "HTTPResponse.validateStatusCode()")
        }
    }
}
