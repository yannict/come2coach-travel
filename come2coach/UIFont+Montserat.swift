//
//  UIFont+Montserrat.swift
//  come2coach
//
//  Created by Marc Ortlieb on 08.08.16.
//  Copyright © 2016 MaytecNet. All rights reserved.
//

import Foundation


extension UIFont {
    
    struct Montserrat {
        
        static func semiBold(_ size: CGFloat = 15) -> UIFont {
            return UIFont(name: "Montserrat-SemiBold", size: size)!
        }
        static func bold(_ size: CGFloat = 15) -> UIFont {
            return UIFont(name: "Montserrat-Bold", size: size)!
        }
        static func black(_ size: CGFloat = 15) -> UIFont {
            return UIFont(name: "Montserrat-Black", size: size)!
        }
        static func regular(_ size: CGFloat = 15) -> UIFont {
            return UIFont(name: "Montserrat-Regular", size: size)!
        }
        static func hairline(_ size: CGFloat = 15) -> UIFont {
            return UIFont(name: "Montserrat-Hairline", size: size)!
        }
        static func ultraLight(_ size: CGFloat = 15) -> UIFont {
            return UIFont(name: "Montserrat-UltraLight", size: size)!
        }
        static func light(_ size: CGFloat = 15) -> UIFont {
            return UIFont(name: "Montserrat-Light", size: size)!
        }
        static func extraBold(_ size: CGFloat = 15) -> UIFont {
            return UIFont(name: "Montserrat-ExtraBold", size: size)!
        }
    }
}

