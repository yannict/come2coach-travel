//
//  EventManager.swift
//  come2coach
//
//  Created by Marc Ortlieb on 23.08.16.
//  Copyright © 2016 MaytecNet. All rights reserved.
//

import Foundation


class EventManager {

    static let sharedInstance = EventManager()
    
    var allEvents = [Event]() // ParticipantEvents & CoachEvents NOTICE: might have double entries
    
    var upcomingBookedEvents = [Event]()
    
    var coachOnlyEvents = [Event]()
    
    var pushNotificationDeviceToken : String?
    
    func checkUpcomingEvents(_ viewController:UIViewController? = nil, noUpcomingEventHandler: (((() -> Void))?) = nil)
    {
        
        OnDebugging.print("checkUpcomingEvents")
        
        // NOTIFICATIONS
        LocalNotificationManager.sharedInstance.manageNotifications(For: allEvents)
        
        guard let _vc = viewController else {return}

        if let firstUpcomingEvent = EventManager.firstUpcomingEvent(allEvents) {
            // LIVE
            if firstUpcomingEvent.isHappeningNow {
                switch firstUpcomingEvent.userType {
                case .coach:
                    let livestreamingCoachVC = Storyboards.ViewControllers.livestreamingCoach
                    livestreamingCoachVC.event = firstUpcomingEvent
                    //                                            viewController.show(livestreamingCoachVC, modally: true)
                    showEventIsHappeningNowAlert(onNextButtonPressed: livestreamingCoachVC, viewController: _vc)
                case .participant:
                    let livestreamingParticipantVC = Storyboards.ViewControllers.livestreamingParticipant
                    livestreamingParticipantVC.event = firstUpcomingEvent
                    //                                            viewController.show(livestreamingParticipantVC, modally: true)
                    showEventIsHappeningNowAlert(onNextButtonPressed: livestreamingParticipantVC, viewController: _vc)
                }
            }
                // COUNTDOWN
            else if firstUpcomingEvent.isInCountDownPhaseNow {
                let countDownVC = Storyboards.ViewControllers.countDown
                countDownVC.event = firstUpcomingEvent
                _vc.show(UINavigationController(countDownVC), modally: true)
            } else if let _noUpcomingEventHandler = noUpcomingEventHandler
            {
                // TABLEVIEW
                _noUpcomingEventHandler()
            }
        }
        
    }
    
    func loadAllEvents(_ viewController:UIViewController? = nil, alsoCheckUpcomingEvents:Bool = false, unwind2VCBeforeRedirect:Bool = false,  noUpcomingEventHandler: (() -> Void)? = nil, finishedLoadingHandler: (() -> Void)? = nil)
    {
        //delete all events and reload them totally
        
        
        HTTPRequest
            .fetchUpcomingBookedEvents { [weak self] in
                guard let self_ = self else {return}
                self_.allEvents.removeAll()
                self_.upcomingBookedEvents.removeAll()
                self_.coachOnlyEvents.removeAll()
                self_.allEvents += $0
                self_.upcomingBookedEvents += $0
                HTTPRequest
                    .fetchCoachEvents() {
                        
                        self_.allEvents += $0
                        self_.coachOnlyEvents.removeAll()
                        self_.coachOnlyEvents += $0
                
                        if let _finishedLoadingHandler = finishedLoadingHandler
                        {
                            _finishedLoadingHandler()
                        }
                        
                        if (alsoCheckUpcomingEvents)
                        {
                            if unwind2VCBeforeRedirect
                            {
                                guard let _vc = viewController else {return}
                                _vc.unwind(to: _vc)
                                {
                                    self_.checkUpcomingEvents(_vc, noUpcomingEventHandler: noUpcomingEventHandler)
                                }
                            }
                            else
                            {
                                self_.checkUpcomingEvents(viewController, noUpcomingEventHandler: noUpcomingEventHandler)
                            }
                        }
                        
                        if self_.allEvents.isEmpty {
                            if let _noUpcomingEventHandler = noUpcomingEventHandler
                            {
                                _noUpcomingEventHandler()
                            }
                        }
//                        else {
//                            self_.checkUpcomingEvents(viewController, noUpcomingEventHandler: noUpcomingEventHandler)
//                        }
                
                }
        }
    
    }


    static func firstUpcomingEvent(_ events:[Event]) -> Event? {
        
        let nowStreamingEvents =
            events
                .filter{ $0.isHappeningNow == true}
                .sorted {descending(($0.0.begin.date, $0.1.begin.date))}
        
        if let item = nowStreamingEvents.first {
            return item
        }

        let nowCountDownEvents =
            events
                .filter{$0.isInCountDownPhaseNow == true}
                .sorted {descending(($0.0.begin.date, $0.1.begin.date))}
        
        if let item = nowCountDownEvents.first {
            return item
        }
        let events = events
            .filter{ !$0.didEnd }
            .filter{ !$0.isHappeningNow }
            .filter{ !$0.isInCountDownPhaseNow }
            .sorted {descending(($0.0.begin.date, $0.1.begin.date))}
        
        return
            events.isEmpty ? nil : events.first
    }
    
    func showEventIsHappeningNowAlert(onNextButtonPressed liveStreamingVC: UIViewController, viewController: UIViewController) {
        
        let alertViewController = UIAlertController(title: "Coaching ist LIVE".localized, message: "", preferredStyle: .alert)
        alertViewController += UIAlertAction(title: "Weiter zur Coaching-Liste".localized, style: .cancel, handler: { _ in
            viewController.show(Storyboards.ViewControllers.coachings)
        })
        alertViewController += UIAlertAction(title: "Weiter zum Livestream".localized, style: .default, handler: { _ in
            viewController.show(liveStreamingVC, modally: true)
        })
        viewController.show(alertViewController)
    }
    
    
    
    /// Handle EventLogic for appropriate ViewController-redirections on localNotifications-userSwipe and AppLaunch
    
    func processEventsSequence<T>(on viewController: T) where T:UIViewController {
        
        // --------------------------------------------------------------------------------------------------
        // MARK: Internet Connection
        // --------------------------------------------------------------------------------------------------

        /*   #inetmanager
        guard InternetConnectionManager.sharedInstance.isConnected == true else
        {
            let vcLoginPreview = Storyboards.ViewControllers.loginPreview
            viewController.show(vcLoginPreview)
            {
                vcLoginPreview.present(vcLoginPreview.defaultNoInternetAlertView())
            }
            return
        }
        */

            
        
        
       
    
    
        // --------------------------------------------------------------------------------------------------
        // MARK: LoginHandler - Come2Coach
        // --------------------------------------------------------------------------------------------------
//        func come2CoachLoginHandler(errors: [String], user:User?) {
//            if errors.isContaining {
//                OnDebugging
//                    .printError("loginCome2Coach - errors \(errors)", title: "InitialStartVC.viewDidLoad()")
//            }
//            guard let user = user else {
//                viewController.show(Storyboards.ViewControllers.loginPreview)
//                return
//            }
//            user.save()
//            loadAllEvents(viewController, noUpcomingEventHandler: {
//                viewController.show(Storyboards.ViewControllers.coachings, animated: false)
//                }, finishedLoadingHandler:
//                {
//                    self.checkUpcomingEvents(viewController, noUpcomingEventHandler: {
//                        viewController.show(Storyboards.ViewControllers.coachings, animated: false)
//                    })
//                }
//            )
//        }
//            
        
        

        
        // --------------------------------------------------------------------------------------------------
        // MARK: LoginHandler - facebook
        // --------------------------------------------------------------------------------------------------
        func loginHandler(_ errors: [String], user:User?) {
            if errors.isContaining {
                OnDebugging
                    .printError("loginCome2Coach - errors \(errors)", title: "InitialStartVC.viewDidLoad()")

            }
            guard let user = user else {
                viewController.show(Storyboards.ViewControllers.loginPreview)
                return
            }
            user.save()
            
            let coachingsVC = Storyboards.ViewControllers.mainTabBar
            viewController.show(coachingsVC)
            
            // todo: deactivated for test reaons..
            // loadAllEvents(coachingsVC, alsoCheckUpcomingEvents: true)
            
        }
        
        
        
        // --------------------------------------------------------------------------------------------------
        // MARK: handle Initial Login, if user never signed in before
        // --------------------------------------------------------------------------------------------------
        
        
        guard let user = User.current else {
            // WK: should not happen - function is only called with valid accounts from login views
                viewController.show(Storyboards.ViewControllers.loginPreview)
                return
        }
        
        
        
        // --------------------------------------------------------------------------------------------------
        // MARK: handle differenciated Login sequences
        // --------------------------------------------------------------------------------------------------

        if (LoginManagerOld.AutoLoginAllowed) {

            switch user.lastLoginType {
            case User.LoginType.come2coach.rawValue:
                LoginManagerOld.sharedInstance.loginCome2Coach(user, handler: loginHandler)

            case User.LoginType.facebook.rawValue:
                LoginManagerOld.sharedInstance.loginFB(user.facebookToken, handler: loginHandler)

            default:
                LoginManagerOld.AutoLoginAllowed = true
                viewController.show(Storyboards.ViewControllers.login)
                OnDebugging
                        .printError("switch user.lastLoginType -> case is default/ user.lastLoginType == empty or nil",
                        title: "EventManager.processEventsSequence()")
            }
        } else
        {
            LoginManagerOld.AutoLoginAllowed = true
            viewController.show(Storyboards.ViewControllers.login)
        }
        
    }
    
    
    
    
}





