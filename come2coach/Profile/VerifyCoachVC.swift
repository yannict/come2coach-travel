//
//  VerifyCoachVC.swift
//  come2coach
//
//  Created by Alex Kupchak on 11/14/16.
//  Copyright © 2016 MaytecNet. All rights reserved.
//

import UIKit
import SVProgressHUD


private let HeaderId = "HeaderCellId"
private let InputId = "InputCellId"
private let TapId = "TabCellId"
private let ButtonId = "ButtonCellId"


private struct Cell {
    var title = ""
    var value = ""
    var cellId = ""
    var contentType: ContentType
    var number = ""
    var interaction = false
}

private enum ContentType {
    case header, mobile, code, paypalEmail, vita, save
}

class VerifyCoachVC: UIViewController {
    @IBOutlet weak var tableView: UITableView!

    fileprivate var data = [Cell]()
    var user: User? = nil

    @IBOutlet weak var topNavigation: UINavigationItem!
    var codeEntered = false
    var codeValue = ""
    var requestedCodeAlready = false

    @IBAction func clickBack(_ sender: AnyObject) {

        view.endEditing(true)

        let alertMessage = UIAlertController(title: "Als Coach verifizieren", message: "Möchtest du den Vorgang abbrechen?", preferredStyle: .actionSheet)
        alertMessage.addAction(UIAlertAction(title: "Nein", style: .default, handler: nil))
        alertMessage.addAction(UIAlertAction(title: "Ja, abbrechen", style: .default, handler:
        { (alert: UIAlertAction!) in


            let coachVerification1 = Storyboards.ViewControllers.verification1
            self.navigationController?.push(coachVerification1, animated: true, completion: nil)

            //self.dismiss(animated: true, completion: nil)

        }))


        self.present(alertMessage, animated: true, completion: nil)

    }


    func showAlert(_ title: String, _ message: String) {

        let msg = UIAlertController(title: title, message: message, preferredStyle: .alert)
        msg.addAction(UIAlertAction(.OK))

        self.present(msg, animated: false)
    }

    @IBAction func clickSave(_ sender: AnyObject) {

        self.view.endEditing(true)

        if let u = user {

            if (u.mobile.isEmpty) {

                showAlert("Mobilnummer fehlt".localized, "Bitte gebe deine Mobilnummer ein.".localized)

                return

            }

            //todo: #payment
            /*
            if (u.paypalEmail.isEmpty) {
                showAlert("PayPal E-Mail fehlt".localized, "Bitte gebe deine PayPal Adresse ein.".localized)

                return
            }


            // validate paypal email
            if (!RegistrationVC.isValidEmail(u.paypalEmail)) {
                showAlert("PayPal E-Mail nicht korrekt".localized, "Die PayPal-Adresse ist nicht korrekt oder enthält Sonderzeichen.".localized)

                return

            }
             */

            if (u.about.isEmpty) {
                showAlert("Deine Vita fehlt".localized, "Bitte schreibe ein wenig über dich.".localized)

                return
            }

            if (!codeEntered) {
                //self.present(UIAlertController("Opps!".localized, "Der Verifizierungs-Code stimmt leider nicht. Probiers nochmal.".localized,
                //        actions: UIAlertAction(.OK)))

                showAlert("Verifizierungs-Code".localized, "Bitte gebe deinen Code aus der SMS ein, die du bekommen hast.".localized)

                return


            } else {

                SVProgressHUD.show()
                // Int(codeValue)!
                HTTPRequest.verificationStep2(codeValue, handler: { (result, error) in

                    if (error != nil) {
                        SVProgressHUD.dismiss()

                        self.showAlert("Opps!".localized, "Der Verifizierungs-Code stimmt leider nicht. Bitte nochmal prüfen.".localized)


                    } else {

                        SVProgressHUD.show()

                        // u.paypalEmail
                        HTTPRequest.updateUser(self.user, password: nil, paypalEmail: nil, handler: { (result, error) in
                            SVProgressHUD.dismiss()
                            if error == nil {

                                let coachVerification1 = Storyboards.ViewControllers.verification2
                                self.navigationController?.push(coachVerification1, animated: true, completion: nil)

                            } else {

                                self.showAlert("Oh das tut uns leid".localized, "Es ist ein Problem aufgetreten, bitte probiers nochmal.".localized)

                                let coachVerification1 = Storyboards.ViewControllers.verification2
                                self.navigationController?.push(coachVerification1, animated: true, completion: nil)

                            }
                        })
                    }
                })

                return

            }
        }

    }

    // MARK: - Life cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        // Get User Data
        //
        SVProgressHUD.show()
        HTTPRequest.getUser { (user, error) in

            SVProgressHUD.dismiss()

            if (error != nil) {
                User.checkConnection(self)
            } else if let u = user {

                self.tableView.isHidden = false

                self.user = u
                self.setupView()

                self.tableView.reloadData()

            } else {
                User.checkConnection(self)
            }
        }

    }

    func setupView() {

        if (data.count == 0) {

            //todo: #payment
            /*
            let header3 = Cell(title: "PAYPAL MAILADRESSE".localized, value: "", cellId: HeaderId, contentType: .header, number: "1", interaction: false)
            data.append(header3)

            let email = Cell(title: "PayPal E-Mail", value: user!.paypalEmail, cellId: InputId, contentType: .paypalEmail, number: "", interaction: true)
            data.append(email)
            */

            let header1 = Cell(title: "MOBILNUMMER EINGEBEN".localized, value: "", cellId: HeaderId, contentType: .header, number: "1", interaction: false)
            data.append(header1)

            let mobile = Cell(title: "0049 1234 56789", value: user!.mobile, cellId: InputId, contentType: .mobile, number: "", interaction: true)
            data.append(mobile)

            let header2 = Cell(title: "VERIFIZIERUNGSCODE".localized, value: "", cellId: HeaderId, contentType: .header, number: "2", interaction: false)
            data.append(header2)

            let code = Cell(title: "Code", value: "", cellId: InputId, contentType: .code, number: "", interaction: true)
            data.append(code)

            let header4 = Cell(title: "DEINE VITA".localized, value: "", cellId: HeaderId, contentType: .header, number: "3", interaction: false)
            data.append(header4)

            let descr = Cell(title: "", value: "", cellId: TapId, contentType: .vita, number: "", interaction: true)
            data.append(descr)

            let btn = Cell(title: "", value: "", cellId: ButtonId, contentType: .save, number: "", interaction: true)
            data.append(btn)

            setupKeyboardObservers()

            tableView.tableFooterView = UIView()
        }

    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)


    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    fileprivate func setupKeyboardObservers() {
        NotificationCenter.default
                .addObserver(self, selector: #selector(keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default
                .addObserver(self, selector: #selector(keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }


    // SHOW Keyboard
    func keyboardWillShow(_ notification: Notification) {
        guard let info = notification.userInfo else {
            OnDebugging
                    .printError("notification.userInfo == nil", title: "LoginVC.keyboardWillShow()")
            return
        }
        let keyboardFrame: CGRect = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue

        var contentInsets: UIEdgeInsets
        contentInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardFrame.height, 0.0)

        tableView.contentInset = contentInsets
    }

    // HIDE Keyboard
    func keyboardWillHide(_ notification: Notification) {

        var contentInsets: UIEdgeInsets
        contentInsets = UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0)

        tableView.contentInset = contentInsets
    }
}

// MARK: - UITableViewDelegate, UITableViewDataSource

extension VerifyCoachVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if data[indexPath.row].cellId == HeaderId {
            return 40
        }
        if data[indexPath.row].cellId == ButtonId {
            return 45
        }
        return 54
    }
}

extension VerifyCoachVC: UITableViewDataSource {

    func addSeparator(_ cell: UITableViewCell) {

        let additionalSeparatorThickness = CGFloat(1)
        let additionalSeparator = UIView(frame: CGRect(x: 0, y: cell.frame.size.height - additionalSeparatorThickness, width: cell.frame.size.width, height: additionalSeparatorThickness))
        additionalSeparator.backgroundColor = UIColor(netHex: 0x6C6C6C)
        cell.addSubview(additionalSeparator)
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count;
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if data[indexPath.row].cellId == HeaderId {
            let cell = tableView.dequeueReusableCell(withIdentifier: HeaderId, for: indexPath) as! VerificationHeaderCell
            cell.setData(data[indexPath.row].title, number: data[indexPath.row].number)
            addSeparator(cell)
            return cell
        }

        if data[indexPath.row].cellId == InputId {
            let cell = tableView.dequeueReusableCell(withIdentifier: InputId, for: indexPath) as! VerificationInputCell
            cell.delegate = self

            // #swift3


            /*

            else if (ct == "paypalEmail") {
                cell.setData(user!.paypalEmail, contentType: ct, placeholder: data[indexPath.row].title)
            }
            */

            let ct = String(describing: data[indexPath.row].contentType)

            if (ct == "mobile") {
                cell.setData(user!.mobile, contentType: ct, placeholder: data[indexPath.row].title)
            }  else if (ct == "code") {
                cell.setData(codeValue, contentType: ct, placeholder: data[indexPath.row].title)
            } else {
                cell.setData(data[indexPath.row].value, contentType: ct, placeholder: data[indexPath.row].title)
            }

            cell.isUserInteractionEnabled = data[indexPath.row].interaction
            addSeparator(cell)
            return cell
        }

        if data[indexPath.row].cellId == TapId {
            let cell = tableView.dequeueReusableCell(withIdentifier: TapId, for: indexPath) as! VerificationTapCell
            cell.delegate = self
            addSeparator(cell)
            return cell
        }

        if data[indexPath.row].cellId == ButtonId {
            let cell = tableView.dequeueReusableCell(withIdentifier: ButtonId, for: indexPath) as! VerificationButtonCell
            addSeparator(cell)
            cell.pressed = { () in
                self.clickSave(self)
            }
            return cell
        }

        return UITableViewCell()
    }
}

extension VerifyCoachVC: VerificationInputCellDelegate {


    func checkSendCode() {

        if let m = self.user?.mobile {
            if (!m.isEmpty) {

                if (!checkNumber(m)) {
                    return
                }

                if (!requestedCodeAlready) {

                    requestedCodeAlready = true

                    // request verification code
                    SVProgressHUD.show()
                    HTTPRequest.verificationStep1(m, handler: { (result, error) in

                        SVProgressHUD.dismiss()

                    })
                }

            }
        }
    }

    func checkNumber(_ value: String) -> Bool {

        //debugPrint("Check number " + value)

        if (value.isEmpty) {
            return false
        } else if (value.characters.count <= 2) {

            let msg = UIAlertController(title: "Mobilnummer".localized, message: "Die Mobilnummer ist zu kurz.".localized, preferredStyle: .alert)
            msg.addAction(UIAlertAction(.OK))


            self.present(msg,
                    animated: false)


            return false
        } else {

            let startIndex = value.index(value.startIndex, offsetBy: 0)
            let endIndex = value.index(value.startIndex, offsetBy: 1)

            if (value[startIndex ... endIndex] != "00") {

                let msg = UIAlertController(title: "Mobilnummer".localized, message: "Deine Mobilnummer muss mit 00 beginnen, gefolgt von der Länderkennung. z.B. 0049 123456".localized, preferredStyle: .alert)
                msg.addAction(UIAlertAction(.OK))


                self.present(msg,
                        animated: false)


                return false
            }

        }

        return true

    }

    func verifyStep(_ sender: AnyObject, value: String) {
        let cell = sender as! VerificationInputCell
        let ip = tableView.indexPath(for: cell)


        if let indexPath = ip {

            if data[indexPath.row].contentType == .mobile {

                if (!value.isEmpty) {

                    // save value
                    self.user?.write(mobile: value)
                    var item = self.data[indexPath.row - 1]
                    item.number = ""
                    item.value = self.user!.mobile
                    self.data[indexPath.row - 1] = item

                    // validate
                    if (!checkNumber(value)) {
                        return
                    }

                    // request sms code
                    requestedCodeAlready = true

                    SVProgressHUD.show()
                    HTTPRequest.verificationStep1(value, handler: { (result, error) in
                        SVProgressHUD.dismiss()
                        if (error != nil) {

                            let msg = UIAlertController(title: "Upps".localized, message: "Die Verifizierung ist leider fehlgeschlagen. Bitte versuche es erneut oder kontaktiere unseren Support.".localized, preferredStyle: .alert)
                            msg.addAction(UIAlertAction(.OK))


                            self.present(msg,
                                    animated: false)

                        } else {


                            // Update header cell
                            //
                            let headerIndexPath = IndexPath(row: indexPath.row - 1, section: indexPath.section)
                            self.tableView.reloadRows(at: [headerIndexPath], with: UITableViewRowAnimation.none)
                        }
                    })
                }

            } else if data[indexPath.row].contentType == .code {
                if value.characters.count > 0 {

                    codeEntered = true
                    codeValue = value

                    var item = self.data[indexPath.row - 1]
                    item.number = ""
                    item.value = value
                    self.data[indexPath.row - 1] = item
                    // Update header cell
                    //
                    let headerIndexPath = IndexPath(row: indexPath.row - 1, section: indexPath.section)
                    self.tableView.reloadRows(at: [headerIndexPath], with: UITableViewRowAnimation.none)


                }
            } /* else if data[indexPath.row].contentType == .paypalEmail {

                self.user?.write(ppEmail: value)

                var item = self.data[indexPath.row - 1]
                item.number = ""
                item.value = value
                self.data[indexPath.row - 1] = item
                // Update header cell
                //
                let headerIndexPath = IndexPath(row: indexPath.row - 1, section: indexPath.section)
                self.tableView.reloadRows(at: [headerIndexPath], with: UITableViewRowAnimation.none)

            } */
            //todo: #payment
        }

    }
}

extension VerifyCoachVC: VerificationDescrCellDelegate {

    func descrStep(_ sender: AnyObject, value: String) {

        let cell = sender as! VerificationTapCell
        let indexPath = tableView.indexPath(for: cell)

        if data[indexPath!.row].contentType == .vita {

            let vc = Storyboards.ViewControllers.coachingDescription
            vc.placeholder = "Gib hier bitte Deinen Text ein ...".localized
            vc.titleText = "DEINE VITA"
            vc.descrValue = user!.about
            self.show(vc)
            vc.completion = { (txt) in

                self.user?.write(about: txt)

                var item = self.data[indexPath!.row - 1]
                item.number = ""
                item.value = value
                self.data[indexPath!.row - 1] = item
                // Update header cell
                //
                let headerIndexPath = IndexPath(row: indexPath!.row - 1, section: indexPath!.section)
                self.tableView.reloadRows(at: [headerIndexPath], with: UITableViewRowAnimation.none)

            }
        }

    }

}


