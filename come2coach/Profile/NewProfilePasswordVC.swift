//
//  NewPasswordVC.swift
//  come2coach
//
//  Created by Alex Kupchak on 11/11/16.
//  Copyright © 2016 MaytecNet. All rights reserved.
//

import UIKit
import RealmSwift
import SVProgressHUD

private let concurrentFalse = "concurrent_false"
private let concurrentTrue = "concurrent_true"

class NewProfilePasswordVC: UIViewController {

    @IBOutlet weak var passwordValidationImage: UIImageView!
    @IBOutlet weak var passwordConcurrentImage: UIImageView!

    @IBOutlet weak var textViewBottomConstrain: NSLayoutConstraint!
    
    var user: User!

    @IBOutlet weak var newPasswordTextField: UITextField!
    @IBOutlet weak var repeatPasswordTextField: UITextField!

    @IBAction func pwd2Button(_ sender: AnyObject) {
        repeatPasswordTextField.becomeFirstResponder()
    }

    @IBAction func pwdButton(_ sender: AnyObject) {
        newPasswordTextField.becomeFirstResponder()
    }

    @IBAction func clickBackButton(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
    }


    @IBAction func clickSave(_ sender: AnyObject) {

        if newPasswordTextField.text?.characters.count == 0 {
            self.present(UIAlertController("Bitte korrigieren:".localized, "Bitte gebe ein neues Passwort ein.".localized,
                    actions: UIAlertAction(.OK)), animated: true)
            return
        }

        if newPasswordTextField.text == repeatPasswordTextField.text {

            SVProgressHUD.show()
            HTTPRequest.updateUser(self.user, password: self.newPasswordTextField.text, paypalEmail: nil, handler: { (result, error) in
                SVProgressHUD.dismiss()
                if error == nil {
                    //self.user.write(password: self.newPasswordTextField.text!)
                    OnDebugging.print("Update password: success")

                    self.dismiss(animated: true, completion: nil)

                } else {
                    self.present(UIAlertController("Oh, tut uns leid.".localized, "Dein Passwort konnte leider nicht geändert werden. Bitte probier es nochmal.".localized,
                            actions: UIAlertAction(.OK)), animated: true)
                }
            })

        } else {

            self.present(UIAlertController("Bitte korrigieren:".localized, "Deine Passwörter stimmen nicht überein.".localized,
                    actions: UIAlertAction(.OK)), animated: true)

            return
        }

    }


    // MARK: - Life cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardNotification(notification:)), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)


        if let current = User.current {
            user = current
        } else {
            user = User()
        }

        self.setupTitle(self.navigationItem.title)

        if user.password.characters.count == 0 {
            newPasswordTextField.isHidden = true
            repeatPasswordTextField.isHidden = true
        }

        passwordValidationImage.isHidden = true
        passwordConcurrentImage.isHidden = true

        passwordValidationImage.image = UIImage(named: concurrentTrue)
        passwordConcurrentImage.image = UIImage(named: concurrentTrue)

        newPasswordTextField.delegate = self
        repeatPasswordTextField.delegate = self

        //newPasswordTextField.text = user.password
        //repeatPasswordTextField.text = user.password

        newPasswordTextField.addTarget(self, action: #selector(NewProfilePasswordVC.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
        repeatPasswordTextField.addTarget(self, action: #selector(NewProfilePasswordVC.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Actions



    // MARK: - Others

    func textFieldDidChange(_ textField: UITextField) {
        passwordValidationImage.isHidden = false
        passwordConcurrentImage.isHidden = false

        if textField == newPasswordTextField {
            newPasswordTextField.isHidden = false
            passwordValidationImage.image = UIImage(named: concurrentTrue)
            if repeatPasswordTextField.text == textField.text {
                passwordConcurrentImage.image = UIImage(named: concurrentTrue)
            } else {
                passwordConcurrentImage.image = UIImage(named: concurrentFalse)
            }
        } else if textField == repeatPasswordTextField {
            repeatPasswordTextField.isHidden = false
            if newPasswordTextField.text == textField.text {
                passwordConcurrentImage.image = UIImage(named: concurrentTrue)
            } else {
                passwordConcurrentImage.image = UIImage(named: concurrentFalse)
            }
        }
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    func keyboardNotification(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            let endFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            let duration: TimeInterval = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurveRawNSN = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIViewAnimationOptions.curveEaseInOut.rawValue
            let animationCurve: UIViewAnimationOptions = UIViewAnimationOptions(rawValue: animationCurveRaw)
            if (endFrame?.origin.y)! >= UIScreen.main.bounds.size.height {
                textViewBottomConstrain?.constant = 0.0
            } else {
                textViewBottomConstrain?.constant = endFrame?.size.height ?? 0.0
            }
            UIView.animate(withDuration: duration,
                    delay: TimeInterval(0),
                    options: animationCurve,
                    animations: { self.view.layoutIfNeeded() },
                    completion: nil)
        }
    }
}

extension NewProfilePasswordVC: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true
    }

    func textFieldShouldReturn(_ textField: UITextField!) -> Bool
    {
        self.view.endEditing(true)
        return false
    }
}
