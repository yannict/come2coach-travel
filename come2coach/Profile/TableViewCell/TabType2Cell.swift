//
//  TabType2Cell.swift
//  come2coach
//
//  Created by Thomas Mayer on 29.03.17.
//  Copyright © 2017 MaytecNet. All rights reserved.
//

import Foundation

class TabType2Cell : UITableViewCell {

    @IBOutlet weak var label1: UILabel!
    
    @IBOutlet weak var label2: UILabel!
    
    @IBOutlet weak var icon: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }

    func setData(txt : String, txt2 : String, visible : Bool) {

        label1.text = txt
        label2.text = txt2
        icon.isHidden = visible

    }

    
}
