//
//  TapTypeCell.swift
//  come2coach
//
//  Created by Alex Kupchak on 11/9/16.
//  Copyright © 2016 MaytecNet. All rights reserved.
//

import UIKit

class TapTypeCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(_ title: String) {
        titleLabel.text = title
    }
    
}
