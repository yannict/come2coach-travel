//
//  AvatarCell.swift
//  come2coach
//
//  Created by Alex Kupchak on 11/10/16.
//  Copyright © 2016 MaytecNet. All rights reserved.
//

import UIKit

protocol AvatarCellDelegate: class {
    func takeNewAvatar()
}

class AvatarCell: UITableViewCell {
    weak var delegate: AvatarCellDelegate?
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var name2Label: UILabel!
    
    
    @IBOutlet weak var profileImageView: RoundedImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        //profileImageView.layer.masksToBounds = true
        //profileImageView.layer.cornerRadius = profileImageView.frame.size.height / 2
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func clickTakeNewAvatar(_ sender: AnyObject) {
        delegate?.takeNewAvatar()
    }
    
    func setData(_ user: User = User()) {
        nameLabel.text = user.name
        name2Label.text = user.lname

        if (user.image != nil) {
            let image = user.userImage()
            profileImageView.image = image
        } else
        if let surl = user.imageStringUrl {

            if (!surl.isEmpty) {

                profileImageView.sd_setImage(with: URL( string:surl ) )

            }
        }
    }
}
