//
//  InputTypeCell.swift
//  come2coach
//
//  Created by Alex Kupchak on 11/9/16.
//  Copyright © 2016 MaytecNet. All rights reserved.
//

import UIKit

protocol InputTypeCellDelegete: class {
    func saveData(_ sender: AnyObject, value: String)
}

class InputTypeCell: UITableViewCell {
    weak var delegate: InputTypeCellDelegete?
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var valueTextField: PaddedTextField!
    
    @IBAction func titleButton(_ sender: AnyObject) {
        valueTextField.becomeFirstResponder()
    }
    
    @IBOutlet weak var titleButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        valueTextField.addTarget(self, action: #selector(InputTypeCell.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
    }
    
    func textFieldDidChange(_ textField: UITextField) {
        delegate?.saveData(self, value: textField.text!)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setData(_ title: String, value: String) {
        valueTextField.delegate = self
        valueTextField.placeholder = title
        titleLabel.text = title
        valueTextField.text = value
    }
    
}

extension InputTypeCell: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        //valueTextField.resignFirstResponder()
        endEditing(true)
        return false
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        //delegate?.saveData(self, value: textField.text!)
    }


}
