//
//  VerificationInputCell.swift
//  come2coach
//
//  Created by Alex Kupchak on 11/14/16.
//  Copyright © 2016 MaytecNet. All rights reserved.
//

import UIKit

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


protocol VerificationInputCellDelegate: class {
    func verifyStep(_ sender: AnyObject, value: String)
    func checkSendCode()
}

class VerificationInputCell: UITableViewCell {
    
    weak var delegate: VerificationInputCellDelegate?
    var contentType: String!

    @IBOutlet weak var valueTextField: UITextField!

    override func awakeFromNib() {
        super.awakeFromNib()
        valueTextField.delegate = self
        valueTextField.enablesReturnKeyAutomatically = false
        //valueTextField.addTarget(self, action: #selector(InputTypeCell.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }


    func setData(_ value: String, contentType: String, placeholder: String) {

        //debugPrint("WERT: " + value + " C: " + contentType)

        self.contentType = contentType
        /*
        if contentType == "paypalEmail" {
            valueTextField.becomeFirstResponder()
        }
        */
        valueTextField.text = value
        valueTextField.placeholder = placeholder

    }
}

//func validate(value: String) -> Bool {
//    let PHONE_REGEX = "^\\d{3}-\\d{3}-\\d{4}$"
//    let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
//    let result =  phoneTest.evaluateWithObject(value)
//    return result
//}

extension VerificationInputCell: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        valueTextField.resignFirstResponder()
        return true
    }

    /*
    func textFieldShouldBeginEditing(_ textField: UITextField!) -> Bool {    //delegate method

        //debugPrint(contentType)


        if (contentType == "code") {
            self.endEditing(true)
            debugPrint("check code!")
            delegate?.checkSendCode()
        }

        return true
    }
    */

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        /*if contentType == "paypalEmail" {
            return true
        }
        
        // Create an `NSCharacterSet` set which includes everything *but* the digits
        let inverseSet = CharacterSet(charactersIn:"0123456789").inverted
        
        // At every character in this "inverseSet" contained in the string,
        // split the string up into components which exclude the characters
        // in this inverse set
        let components = string.components(separatedBy: inverseSet)
        
        // Rejoin these components
        let filtered = components.joined(separator: "")  // use join("", components) if you are using Swift 1.2
        
        // If the original string is equal to the filtered string, i.e. if no
        // inverse characters were present to be eliminated, the input is valid
        // and the statement returns true; else it returns false
        return string == filtered
        */

        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {

        //debugPrint("did ending :" + contentType + " val = " + valueTextField.text!)

        if textField.text?.characters.count > 0 {
            //self.endEditing(true)
            delegate?.verifyStep(self, value: textField.text!)
        }
    }

}
