//
//  VerificationHeaderCell.swift
//  come2coach
//
//  Created by Alex Kupchak on 11/14/16.
//  Copyright © 2016 MaytecNet. All rights reserved.
//

import UIKit

class VerificationHeaderCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var numberLabel: UILabel!
    @IBOutlet weak var circleImageView: UIImageView!
    @IBOutlet weak var numberView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        numberView.layer.masksToBounds = true
        //numberView.layer.borderWidth = 1.0
        numberView.layer.cornerRadius = 9.0
        //numberView.layer.borderColor = UIColor.white.cgColor
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setData(_ title: String, number: String) {
        titleLabel.text = title
        
        if number.characters.count > 0 {
            numberLabel.text = number
            circleImageView.isHidden = true
        } else {
            circleImageView.isHidden = false
        }
    }

}
