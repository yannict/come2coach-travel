//
//  BottomEditCell.swift
//  come2coach
//
//  Created by Alex Kupchak on 11/10/16.
//  Copyright © 2016 MaytecNet. All rights reserved.
//

import UIKit

class BottomEditCell: UITableViewCell {

    var pressed: (() -> Void)!

    @IBOutlet weak var button: UIButton!

    @IBAction func buttonPressed(_ sender: Any) {

        pressed()

    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(_ title: String) {
        button.setTitle(title, for: .normal)
    }
    
    @IBAction func clickBottomButton(_ sender: AnyObject) {
    }
}
