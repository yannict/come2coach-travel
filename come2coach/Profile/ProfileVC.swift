//
//  ProfileVC.swift
//  come2coach
//
//  Created by Alex Kupchak on 11/8/16.
//  Copyright © 2016 MaytecNet. All rights reserved.
//

import UIKit
import SVProgressHUD
import MRProgress
import AWSS3
import Photos

private let HeaderCellId = "HeaderId"
private let InputCellId = "InputCellId"
private let TapCellId = "TapCellId"
//private let CreateCoachingCellId = "CreateCochingId"
private let AvatarCellId = "AvatarId"
private let BottomEditCellId = "BottomEditId"
private let TapCell2Id = "Tap2CellId"

private struct Cell {
    var title = ""
    var value = ""
    var cellId = ""
    var contentType: ContentType
}

private enum ContentType {
    case avatar, header, name, lname, email, password, number, paypalEmail, notification, creation, logout, delete, save, vita,
         coins, earnings
}

class ProfileVC: UIViewController {
    fileprivate var data = [Cell]()
    var user = User()

    var selectedImageUrl: URL!
    var avatarImage: UIImage!

    @IBOutlet weak var profileButton: UIButton!
    @IBOutlet weak var helpButton: UIButton!
    @IBOutlet weak var leftSegmentView: UIView!
    @IBOutlet weak var rightSegmentView: UIView!

    //@IBOutlet weak var profileImageView: UIImageView!

    @IBOutlet weak var tableView: UITableView!

    // MARK: - Life cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.register(UINib(nibName: "InputTypeCell", bundle: nil), forCellReuseIdentifier: InputCellId)
        tableView.register(UINib(nibName: "TapTypeCell", bundle: nil), forCellReuseIdentifier: TapCellId)
        //tableView.register(UINib(nibName: "CreateFirstCoachingCell", bundle: nil), forCellReuseIdentifier: CreateCoachingCellId)
        tableView.register(UINib(nibName: "BottomEditCell", bundle: nil), forCellReuseIdentifier: BottomEditCellId)
        tableView.register(UINib(nibName: "TapType2Cell", bundle: nil), forCellReuseIdentifier: TapCell2Id)


        self.setupTitle(self.navigationItem.title)

        setupKeyboardObservers()

        tableView.tableFooterView = UIView()



        user = User.current!

        tableView.separatorColor = UIColor(netHex: 0x6C6C6C)

        // Get User Data
        //
        SVProgressHUD.show()
        HTTPRequest.getUser { (user, error) in

            SVProgressHUD.dismiss()

            if let u = user {

                self.user = u

                if (u.image == nil) {

                    S3Manager.downloadFileFromS3(self.user.imageName, progress: { (totalBytesSent, totalBytesExpectedToSend) in
                    }) { (success, contentType, body) in
                        if success {
                            DispatchQueue.main.async {
                                let url = URL(string: body!.absoluteString)
                                let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                                let image = UIImage(data: data!)
                                self.user.saveImage(image!)
                                self.tableView.reloadData()
                            }
                        }
                    }
                }

            }
            // Setup cells
            //
            self.setupDataTableCells()
            self.tableView.reloadData()
        }
    }

    override func viewWillAppear(_ animated: Bool) {

        //setupDataTableCells()
        //tableView.reloadData()

        // Protocol actions
        ProtocolEventRequest.showEvent = { (eventId) in

            HTTPRequest.fetchEvent(with: eventId) { (event) in

                ProtocolEventRequest.eventId = nil

                let detailsVC = Storyboards.ViewControllers.eventDetails
                detailsVC.mode = "normal"
                detailsVC.event = event

                self.navigationController?.push(detailsVC, animated: true, completion: nil)

            }

        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    internal func validateProfile() -> Bool {


        // check e-mail

        if (self.user.email.characters.count == 0) {
            self.present(UIAlertController("Bitte korrigieren:".localized, "Bitte gebe eine E-Mail ein.".localized,
                    actions: UIAlertAction(.OK)), animated: true)

            return false
        }

        if !(self.user.email.validateEmail()) {

            self.present(UIAlertController("Bitte korrigieren:".localized, "Deine E-Mail hat kein korrektes Format oder enthält unerlaubte Sonderzeichen.".localized,
                    actions: UIAlertAction(.OK)), animated: true)

            return false;
        }

        /*
        // check paypal address
        if (!self.user.paypalEmail.isEmpty) {

            if (!RegistrationVC.isValidEmail(self.user.paypalEmail)) {

                self.present(UIAlertController("Bitte korrigieren:".localized, "Deine E-Mail hat kein korrektes Format oder enthält unerlaubte Sonderzeichen.".localized,
                        actions: UIAlertAction(.OK)), animated: true)

                return false;
            }
        }
        */

        if (self.user.lname.isEmpty || self.user.name.isEmpty) {
            self.present(UIAlertController("Bitte korrigieren:".localized, "Bitte gebe einen Vor- und Nachnamen ein.".localized,
                    actions: UIAlertAction(.OK)), animated: true)

            return false
        }

        if (!RegistrationVC.isValidNameWithSpace(self.user.lname)) {
            self.present(UIAlertController("Bitte korrigieren:".localized, "Der Nachname enthält ungültige Zeichen.".localized,
                    actions: UIAlertAction(.OK)), animated: true)

            return false

        }

        if (!RegistrationVC.isValidNameWithSpace(self.user.name)) {
            self.present(UIAlertController("Bitte korrigieren:".localized, "Der Vorname enthält ungültige Zeichen.".localized,
                    actions: UIAlertAction(.OK)), animated: true)

            return false

        }

        /*
        if (!self.user.mobile.isEmpty) {
            // check mobile
            let numberCharacters = CharacterSet.decimalDigits.inverted
            if (self.user.mobile.rangeOfCharacter(from: numberCharacters) != nil) {
                self.present(UIAlertController("Bitte korrigieren:".localized, "Die Handynummer darf nur Zahlen enthalten".localized,
                        actions: UIAlertAction(.OK)), animated: true)

                return false
            }


        }
        */

        return true;

    }

    fileprivate func setupKeyboardObservers() {
        NotificationCenter.default
                .addObserver(self, selector: #selector(keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default
                .addObserver(self, selector: #selector(keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }

    // MARK: - Actions

    @IBAction func clickProfileSegment(_ sender: AnyObject) {
        leftSegmentView.alpha = 0.5
        rightSegmentView.alpha = 0
    }

    @IBAction func clickHelpSegment(_ sender: AnyObject) {
        leftSegmentView.alpha = 0
        rightSegmentView.alpha = 0.5
    }

    // MARK: - Others

    /*
    func setUpSegmentControl() {
        // Set up left
        //
        let buttonPathLeft = UIBezierPath(roundedRect: leftSegmentView.bounds, byRoundingCorners: [UIRectCorner.topLeft, UIRectCorner.bottomLeft], cornerRadii: CGSize(width: 15.0, height: 15.0))
        let maskLayerLeft = CAShapeLayer()
        maskLayerLeft.frame = self.view.bounds
        maskLayerLeft.path = buttonPathLeft.cgPath
        leftSegmentView.layer.mask = maskLayerLeft

        // Set up right
        //
        let buttonPathRight = UIBezierPath(roundedRect: rightSegmentView.bounds, byRoundingCorners: [UIRectCorner.topRight, UIRectCorner.bottomRight], cornerRadii: CGSize(width: 15.0, height: 15.0))
        let maskLayerRight = CAShapeLayer()
        maskLayerRight.frame = self.view.bounds
        maskLayerRight.path = buttonPathRight.cgPath
        rightSegmentView.layer.mask = maskLayerRight
    }

    func setUpBorderForSegment() {
        // Rounded profile button
        //
        let buttonPathLeft = UIBezierPath(roundedRect: profileButton.bounds, byRoundingCorners: [UIRectCorner.topLeft, UIRectCorner.bottomLeft], cornerRadii: CGSize(width: 15.0, height: 15.0))
        let maskLayerLeft = CAShapeLayer()
        maskLayerLeft.frame = self.view.bounds
        maskLayerLeft.path = buttonPathLeft.cgPath
        profileButton.layer.mask = maskLayerLeft

        // set border in profile button
        //
        let borderLayerLeft = CAShapeLayer()
        borderLayerLeft.frame = self.view.bounds;
        borderLayerLeft.path = buttonPathLeft.cgPath;
        borderLayerLeft.lineWidth = 2
        borderLayerLeft.strokeColor = UIColor.white.cgColor
        borderLayerLeft.fillColor = UIColor.clear.cgColor
        profileButton.layer.addSublayer(borderLayerLeft)

        // Rounded help button
        //
        let buttonPathRight = UIBezierPath(roundedRect: helpButton.bounds, byRoundingCorners: [UIRectCorner.topRight, UIRectCorner.bottomRight], cornerRadii: CGSize(width: 15.0, height: 15.0))
        let maskLayerRight = CAShapeLayer()
        maskLayerRight.frame = self.view.bounds
        maskLayerRight.path = buttonPathRight.cgPath
        helpButton.layer.mask = maskLayerRight;

        // set border in help button
        //
        let borderLayerRight = CAShapeLayer()
        borderLayerRight.frame = self.view.bounds;
        borderLayerRight.path = buttonPathRight.cgPath;
        borderLayerRight.lineWidth = 2
        borderLayerRight.strokeColor = UIColor.white.cgColor
        borderLayerRight.fillColor = UIColor.clear.cgColor
        helpButton.layer.addSublayer(borderLayerRight)
    }
    */

    func setupDataTableCells() {
        data = [Cell]()
        let avatar = Cell(title: "", value: "", cellId: AvatarCellId, contentType: ContentType.avatar)

        data.append(avatar)

        let header1 = Cell(title: "ALLGEMEINE DATEN".localized, value: "", cellId: HeaderCellId, contentType: ContentType.header)

        data.append(header1)

        let firstName = Cell(title: "Vorname".localized, value: "\(user.name)", cellId: InputCellId, contentType: ContentType.name)

        data.append(firstName)

        let lastName = Cell(title: "Nachname".localized, value: "\(user.lname)", cellId: InputCellId, contentType: ContentType.lname)

        data.append(lastName)

        let mail = Cell(title: "E-Mail".localized, value: "\(user.email)", cellId: InputCellId, contentType: ContentType.email)

        data.append(mail)

        let password = Cell(title: "Passwort ändern".localized, value: "", cellId: TapCellId, contentType: ContentType.password)

        data.append(password)

        let number = Cell(title: "Handynummer".localized, value: user.mobile, cellId: InputCellId, contentType: ContentType.number)

        data.append(number)


        let header2 = Cell(title: "KONTOSTAND".localized, value: "", cellId: HeaderCellId, contentType: ContentType.header)

        data.append(header2)


//        let credits = Cell(title: "come2coach Coins".localized, value: "", cellId: TapCell2Id, contentType: ContentType.coins)
//
//        data.append(credits)
//
//        if (user.isVerified > 2) {
//
//            let earnings = Cell(title: "".localized, value: "", cellId: TapCell2Id, contentType: ContentType.earnings)
//
//            data.append(earnings)
//
//        }

        let header3 = Cell(title: "PUSH BENACHRICHTIGUNGEN".localized, value: "", cellId: HeaderCellId, contentType: ContentType.header)

        data.append(header3)

        let notification = Cell(title: "Teilnehmer Benachrichtigungen".localized, value: "", cellId: TapCellId, contentType: ContentType.notification)

        data.append(notification)

        let header4 = Cell(title: "KURZBESCHREIBUNG".localized, value: "", cellId: HeaderCellId, contentType: ContentType.header)

        data.append(header4)

        let vita = Cell(title: "Beschreibung eingeben".localized, value: "", cellId: TapCellId, contentType: ContentType.vita)

        data.append(vita)


        //let createCoaching = Cell(title: "", value: "", cellId: CreateCoachingCellId, contentType: ContentType.creation)
        //data.append(createCoaching)

        let save = Cell(title: "Speichern".localized, value: "", cellId: BottomEditCellId, contentType: ContentType.save)

        data.append(save)


        let logout = Cell(title: "Abmelden".localized, value: "", cellId: BottomEditCellId, contentType: ContentType.logout)

        data.append(logout)

        let delete = Cell(title: "Profil löschen".localized, value: "", cellId: BottomEditCellId, contentType: ContentType.delete)

        data.append(delete)

    }

    // SHOW Keyboard
    func keyboardWillShow(_ notification: Notification) {
        guard let info = notification.userInfo else {
            OnDebugging
                    .printError("notification.userInfo == nil", title: "LoginVC.keyboardWillShow()")
            return
        }
        let keyboardFrame: CGRect = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue

        var contentInsets: UIEdgeInsets
        contentInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardFrame.height, 0.0)

        tableView.contentInset = contentInsets
    }

    // HIDE Keyboard
    func keyboardWillHide(_ notification: Notification) {

        var contentInsets: UIEdgeInsets
        contentInsets = UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0)

        tableView.contentInset = contentInsets
    }

    func resizeImage(_ image: UIImage, newValue: CGFloat) -> UIImage {
        //        var scale: CGFloat
        //        var newWidth = newValue
        //        var newHeight: CGFloat
        //        if image.size.width > image.size.height {
        //            scale = newWidth / image.size.width
        //            newHeight = image.size.height * scale
        //        } else {
        //            newHeight = newWidth
        //            scale = newHeight / image.size.height
        //            newWidth = image.size.width * scale
        //        }

        // Old
        //        let scale = newWidth / image.size.width
        //        let newHeight = image.size.height * scale

        let scale = newValue / image.size.width
        let newHeight = image.size.height * scale

        UIGraphicsBeginImageContext(CGSize(width: newValue, height: newHeight))
        image.draw(in: CGRect(x: 0, y: 0, width: newValue, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        /*
        UIGraphicsBeginImageContext(CGSizeMake(224, 224))
        image.drawInRect(CGRectMake(0, 0, 224, 224))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        */

        return newImage!
    }

    // MARK: Networking

    func startUploadingImage() {
        // Get local image name
        //
        var localFileName: String?
        //        if let imageToUploadUrl = selectedImageUrl {
        //            let phResult = PHAsset.fetchAssetsWithALAssetURLs([imageToUploadUrl], options: nil)
        //            localFileName = phResult.firstObject?.filename
        //        } else {
        let date = Date().timeIntervalSince1970
        localFileName = "user_avatar-\(date).jpg"
        //        }

        if localFileName == nil {
            return
        }

        //todo: handle empty imageStringUrl

        //let remoteName = localFileName!
        let remoteName = User.current?.imageStringUrl

        // Progress view
        //
        let overlayView = MRProgressOverlayView.showOverlayAdded(to: self.view, title: "Bitte warten....".localized, mode: MRProgressOverlayViewMode.indeterminate, animated: true)

        S3Manager.uploadFileToS3(remoteName, generatedImageUrl: generateImageUrl(remoteName!), contentType: "image/jpeg", progress: { (totalBytesSent: Int64, totalBytesExpectedToSend: Int64) in

            // Prepare for progress view
            //
            if overlayView?.mode != MRProgressOverlayViewMode.determinateCircular {
                overlayView?.mode = MRProgressOverlayViewMode.determinateCircular
                overlayView?.titleLabelText = "Übertrage Daten...".localized
            }
            let progress = Float(totalBytesSent) / Float(totalBytesExpectedToSend)
            overlayView?.setProgress(progress, animated: true)

        }, completion: { (success, s3URL) in
            DispatchQueue.main.async {
                MRProgressOverlayView.dismissOverlay(for: self.view, animated: true)
            }
            // Remove locally stored file
            //
            self.remoteImageWithUrl(remoteName!)

            if success {
                HTTPRequest.uploadUserImage(s3URL!, handler: { (error, success) in
                    if success {
                        OnDebugging.print("Success upload avatar path")
                    }
                })
            }
        })
    }

    func generateImageUrl(_ fileName: String) -> URL {
        let fileURL = URL(fileURLWithPath: NSTemporaryDirectory() + fileName)
        let data = UIImageJPEGRepresentation(avatarImage, 0.6)
        try? data!.write(to: fileURL, options: [.atomic])

        return fileURL
    }

    func remoteImageWithUrl(_ fileName: String) {
        let fileURL = URL(fileURLWithPath: NSTemporaryDirectory() + fileName)
        do {
            try FileManager.default.removeItem(at: fileURL)
        } catch {
            OnDebugging.print(error)
        }
    }


    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */

}

// MARK: - UITableViewDelegate, UITableViewDataSource

extension ProfileVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if data[indexPath.row].cellId == HeaderCellId {
            return 40
        }

        /*
        if data[indexPath.row].cellId == CreateCoachingCellId {
            if (self.user.isVerified) {
                return 0
            } else {
                return 260
            }
        }
        */

        if data[indexPath.row].cellId == AvatarCellId {
            return 290
        }

        if data[indexPath.row].cellId == BottomEditCellId {
            return 45
        }

        return 54
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

        if data[indexPath.row].cellId == TapCellId {

            // Select new password controller
            //
            if data[indexPath.row].contentType == ContentType.password {

                show(Storyboards.ViewControllers.newProfilePassword, animated: true, modally: true)

            } else if data[indexPath.row].contentType == ContentType.notification {

                // Select set notification controller
                //
                let vc = Storyboards.ViewControllers.profileNotification
                vc.PushEnabled = (self.user.wishlistReminder == 1)
                show(vc, animated: true)
                vc.completion = { (state) in

                    self.user.write(wishListReminder: state ? 1 : 0);

                    //SVProgressHUD.show()
                    HTTPRequest.notificationState(state ? 1 : 0) { (s) in
                        //SVProgressHUD.dismiss()
                    }

                }

            } else if data[indexPath.row].contentType == ContentType.vita {

                // Select set description controller
                //

                if (validateProfile()) {


                    let vc = Storyboards.ViewControllers.coachingDescription
                    vc.placeholder = "Gib hier bitte Deinen Text ein ...".localized
                    vc.titleText = "KURZBESCHREIBUNG"
                    vc.descrValue = self.user.about
                    vc.completion = { (txt) in

                        self.user.write(about: txt)

                        SVProgressHUD.show()
                        HTTPRequest.updateUser(self.user, password: nil, paypalEmail: nil, handler: { (result, error) in
                            SVProgressHUD.dismiss()
                        })

                    }
                    self.show(vc, animated: true)


                }
            }


        }

        if data[indexPath.row].cellId == TapCell2Id {

            if data[indexPath.row].contentType == ContentType.coins {
                BuyCoinsVC.show(fromVC: self, userCoinsCount: user.coins, eventId: -1) { () in

                    // refresh user data (eventually coin amount changed)

                    SVProgressHUD.show()
                    HTTPRequest.getUser { (user, error) in

                        SVProgressHUD.dismiss()

                        if let u = user {

                            self.user = u
                            self.tableView.reloadData()
                        }
                    }

                }
            }

        }

        // Select bottom button
        //
        if data[indexPath.row].cellId == BottomEditCellId {
            if data[indexPath.row].contentType == ContentType.logout {

                let alertController = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
                alertController.addAction(UIAlertAction(title: "Abmelden".localized, style: UIAlertActionStyle.destructive) { action -> Void in
                    // Go to login preview controller
                    //
                    User.loginManager.AutoLoginAllowed = false
                    //LoginManagerOld.AutoLoginAllowed = false
                    self.user.logout()
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    let window = appDelegate.window
                    window!.rootViewController = UINavigationController(Storyboards.ViewControllers.loginPreview)
                })

                alertController.addAction(UIAlertAction(title: "Abbrechen".localized, style: UIAlertActionStyle.cancel) { action -> Void in
                    // Cancel

                })

                self.present(alertController, animated: true, completion: nil)
            } else if data[indexPath.row].contentType == ContentType.delete {
                let alertController = UIAlertController(title: nil, message: "Möchtest Du das Profil wirklich löschen?".localized, preferredStyle: UIAlertControllerStyle.actionSheet)
                alertController.addAction(UIAlertAction(title: "Profil löschen".localized, style: UIAlertActionStyle.destructive) { action -> Void in
                    SVProgressHUD.show()
                    HTTPRequest.deleteUser({ (result, error) in
                        SVProgressHUD.dismiss()
                        if result as! Bool == true {
                            OnDebugging.print("User deleted")
                            // Go to login preview controller
                            //
                            //self.user.delete()
                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            let window = appDelegate.window

                            //LoginManagerOld.AutoLoginAllowed = false
                            User.loginManager.AutoLoginAllowed = false
                            window!.rootViewController = UINavigationController(Storyboards.ViewControllers.loginPreview)
                        } else {
                            OnDebugging.print(error)
                        }
                    })
                })

                alertController.addAction(UIAlertAction(title: "Abbrechen".localized, style: UIAlertActionStyle.cancel) { action -> Void in
                    // Cancel

                })

                self.present(alertController, animated: true, completion: nil)
            } else if data[indexPath.row].contentType == ContentType.save {

                // validate input

                if (validateProfile()) {

                    SVProgressHUD.show()
                    HTTPRequest.updateUser(self.user, password: nil, paypalEmail: nil, handler: { (result, error) in
                        SVProgressHUD.dismiss()

                        /*
                        if result {
                            OnDebugging.print("Success update profile - isVerified = " + self.user.isVerified.description)
                            self.tableView.reloadData()
                        }
                        */
                    })

                }
            }
        }

        // Select verification controller
        //
        //todo: verification
        /*
        if data[indexPath.row].cellId == CreateCoachingCellId {
            show(Storyboards.ViewControllers.verifyCoach, animated: true, modally: true)
        }
        */
    }
}

extension ProfileVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count;
    }

    func addSeparator(_ cell: UITableViewCell) {

        let additionalSeparatorThickness = CGFloat(1)
        let additionalSeparator = UIView(frame: CGRect(x: 0, y: cell.frame.size.height - additionalSeparatorThickness, width: cell.frame.size.width, height: additionalSeparatorThickness))
        additionalSeparator.backgroundColor = UIColor(netHex: 0x6C6C6C)
        cell.addSubview(additionalSeparator)
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        //tableView.separatorColor = UIColor(netHex: 0x6C6C6C)

        if data[indexPath.row].cellId == AvatarCellId {
            let cell = tableView.dequeueReusableCell(withIdentifier: AvatarCellId, for: indexPath) as! AvatarCell
            cell.delegate = self
            cell.setData(user)
            addSeparator(cell)
            return cell
        }

        if data[indexPath.row].cellId == HeaderCellId {
            let cell = tableView.dequeueReusableCell(withIdentifier: HeaderCellId, for: indexPath) as! TableHeaderCell
            cell.setData(data[indexPath.row].title)
            addSeparator(cell)
            return cell
        }

        if data[indexPath.row].cellId == TapCell2Id {
            let cell = tableView.dequeueReusableCell(withIdentifier: TapCell2Id, for: indexPath) as! TabType2Cell
            //cell.delegate = self

            if (data[indexPath.row].contentType == ContentType.coins) {
                cell.setData(txt: "come2coach Coins".localized, txt2: String(user.coins), visible: false)
            } else {
                cell.setData(txt: "Erlöse aus Deinen Coachings".localized, txt2: String(format: "%.2f €", user.earnings), visible: true)
            }

            addSeparator(cell)
            return cell
        }


        if data[indexPath.row].cellId == InputCellId {
            let cell = tableView.dequeueReusableCell(withIdentifier: InputCellId, for: indexPath) as! InputTypeCell
            cell.delegate = self
            cell.setData(data[indexPath.row].title, value: data[indexPath.row].value)
            addSeparator(cell)
            return cell
        }

        if data[indexPath.row].cellId == TapCellId {
            let cell = tableView.dequeueReusableCell(withIdentifier: TapCellId, for: indexPath) as! TapTypeCell
            cell.setData(data[indexPath.row].title)
            addSeparator(cell)
            return cell
        }

        /*
        if (data[indexPath.row].cellId == CreateCoachingCellId && !user.isVerified) {
            let cell = tableView.dequeueReusableCell(withIdentifier: CreateCoachingCellId, for: indexPath) as! CreateFirstCoachingCell
            addSeparator(cell)
            return cell
        }
        */


        if data[indexPath.row].cellId == BottomEditCellId {
            let cell = tableView.dequeueReusableCell(withIdentifier: BottomEditCellId, for: indexPath) as! BottomEditCell
            cell.setData(data[indexPath.row].title)

            if data[indexPath.row].contentType == ContentType.logout {

                cell.pressed = { () in

                    let alertController = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
                    alertController.addAction(UIAlertAction(title: "Abmelden".localized, style: UIAlertActionStyle.destructive) { action -> Void in
                        // Go to login preview controller
                        //
                        //LoginManagerOld.AutoLoginAllowed = false
                        User.loginManager.AutoLoginAllowed = false
                        self.user.logout()
                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                        let window = appDelegate.window
                        window!.rootViewController = UINavigationController(Storyboards.ViewControllers.loginPreview)
                    })

                    alertController.addAction(UIAlertAction(title: "Abbrechen".localized, style: UIAlertActionStyle.cancel) { action -> Void in
                        // Cancel

                    })

                    self.present(alertController, animated: true, completion: nil)

                }

            } else if data[indexPath.row].contentType == ContentType.delete {

                cell.pressed = { () in

                    let alertController = UIAlertController(title: nil, message: "Möchtest Du das Profil wirklich löschen?".localized, preferredStyle: UIAlertControllerStyle.actionSheet)
                    alertController.addAction(UIAlertAction(title: "Profil löschen".localized, style: UIAlertActionStyle.destructive) { action -> Void in
                        SVProgressHUD.show()
                        HTTPRequest.deleteUser({ (result, error) in
                            SVProgressHUD.dismiss()
                            if result as! Bool == true {
                                OnDebugging.print("User deleted")
                                // Go to login preview controller
                                //
                                //self.user.delete()
                                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                let window = appDelegate.window

                                //LoginManagerOld.AutoLoginAllowed = false
                                User.loginManager.AutoLoginAllowed = false
                                window!.rootViewController = UINavigationController(Storyboards.ViewControllers.loginPreview)
                            } else {
                                OnDebugging.print(error)
                            }
                        })
                    })

                    alertController.addAction(UIAlertAction(title: "Abbrechen".localized, style: UIAlertActionStyle.cancel) { action -> Void in
                        // Cancel

                    })

                    self.present(alertController, animated: true, completion: nil)
                }

            } else if data[indexPath.row].contentType == ContentType.save {

                cell.pressed = { () in

                    // validate input

                    if (self.validateProfile()) {

                        SVProgressHUD.show()
                        HTTPRequest.updateUser(self.user, password: nil, paypalEmail: nil, handler: { (result, error) in
                            SVProgressHUD.dismiss()

                            /*result
                            if result {
                                OnDebugging.print("Success update profile - isVerified = " + self.user.isVerified.description)
                                self.tableView.reloadData()
                            }*/
                        })

                    }
                }

            }

            addSeparator(cell)

            return cell
        }

        return UITableViewCell()
    }
}

extension ProfileVC: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offset = scrollView.contentOffset.y
        //        var avatarTransform = CATransform3DIdentity
        //        var headerTransform = CATransform3DIdentity

        if offset < 0 {
            //            let headerScaleFactor:CGFloat = -(offset) / header.bounds.height
            //            let headerSizevariation = ((header.bounds.height * (1.0 + headerScaleFactor)) - header.bounds.height) / 2.0
            //            headerTransform = CATransform3DTranslate(headerTransform, 0, headerSizevariation, 0)
            //            headerTransform = CATransform3DScale(headerTransform, 1.0 + headerScaleFactor, 1.0 + headerScaleFactor, 0)
            //
            //            header.layer.transform = headerTransform
        }
    }
}

extension ProfileVC: AvatarCellDelegate {
    func takeNewAvatar() {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.allowsEditing = false
        imagePicker.navigationBar.isTranslucent = false
        imagePicker.navigationBar.barTintColor = UIColor.black

        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)

        alertController.addAction(UIAlertAction(title: "Foto auswählen".localized, style: UIAlertActionStyle.default) { action -> Void in
            imagePicker.sourceType = .photoLibrary

            self.present(imagePicker, animated: true, completion: nil)
        })

        alertController.addAction(UIAlertAction(title: "Foto aufnehmen".localized, style: UIAlertActionStyle.default) { action -> Void in
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                if UIImagePickerController.availableCaptureModes(for: .rear) != nil {
                    imagePicker.sourceType = .camera

                    self.present(imagePicker, animated: true, completion: nil)
                }
            }
        })

        alertController.addAction(UIAlertAction(title: "Abbrechen".localized, style: UIAlertActionStyle.cancel) { action -> Void in
            // Cancel
        })

        self.present(alertController, animated: true, completion: nil)
    }
}

extension ProfileVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String: Any]) {
        if (info[UIImagePickerControllerReferenceURL] != nil) {
            selectedImageUrl = info[UIImagePickerControllerReferenceURL] as! URL
        }

        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {

            // Scale to 200 and rotate if needed
            //
            let scalePickture = ImageTools.resizeImage(pickedImage, newValue: 180)
            avatarImage = scalePickture

            // Save image to storage
            //
            user.saveImage(avatarImage)
            //profileImageView.image = scalePickture//fixImageOrientation(scalePickture)

            // Clear image cache and avoid displaying profile pics in views
            ImageTools.clearImageWebCache();

            tableView.reloadData()

        }

        dismiss(animated: true, completion: { [weak self] _ in
            guard let self_ = self else {
                return
            }

            self_.startUploadingImage()
        })
    }
}

extension ProfileVC: InputTypeCellDelegete {
    func saveData(_ sender: AnyObject, value: String) {
        let cell = sender as! InputTypeCell
        let indexPath = tableView.indexPath(for: cell)
        if indexPath != nil {
            if data[indexPath!.row].contentType == .name {
                self.user.write(value)
                data[indexPath!.row].value = value
            }

            if data[indexPath!.row].contentType == .lname {
                self.user.write(lname: value)
                data[indexPath!.row].value = value
            }

            if data[indexPath!.row].contentType == .email {
                self.user.write(email: value)
                data[indexPath!.row].value = value
            }

            if data[indexPath!.row].contentType == .number {
                self.user.write(mobile: value)
                data[indexPath!.row].value = value
            }

            if data[indexPath!.row].contentType == .paypalEmail {
                self.user.write(ppEmail: value)
                data[indexPath!.row].value = value
            }
        }
    }
}

