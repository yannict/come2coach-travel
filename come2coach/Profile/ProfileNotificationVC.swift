//
//  ProfileNotificationVC.swift
//  come2coach
//
//  Created by Alex Kupchak on 11/11/16.
//  Copyright © 2016 MaytecNet. All rights reserved.
//

import UIKit

class ProfileNotificationVC: UIViewController {
    @IBOutlet weak var notificationSwitch: UISwitch!

    var PushEnabled: Bool = true
    var completion: ((_ state: Bool) -> Void)!


    // MARK: - Life cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        notificationSwitch.isOn = PushEnabled

        self.navigationController?.navigationBar.isHidden = true
        self.setupTitle(self.navigationItem.title)

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Actions

    @IBAction func notificationValueChange(_ sender: AnyObject) {

    }

    @IBAction func clickBackButton(_ sender: AnyObject) {

        self.completion(notificationSwitch.isOn)

        self.navigationController?.navigationBar.isHidden = true

        if (self.navigationController == nil) {
            dismissViewController()
        } else {
            self.navigationController?.popViewController(animated: true)
        }

    }


    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */

}
