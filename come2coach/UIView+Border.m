//
//  UIView+Border.m
//  MaytecNet
//
//  Created by Werner Kratochwil on 16.06.15.
//  Copyright (c) 2015 MaytecNet All rights reserved.
//
//

#import "UIView+Border.h"

@implementation UIView (Border)

@dynamic borderColor,borderWidth,cornerRadius;

-(void)setBorderColor:(UIColor *)borderColor{
    [self.layer setBorderColor:borderColor.CGColor];
}

-(void)setBorderWidth:(NSInteger)borderWidth{
    [self.layer setBorderWidth:borderWidth];
}

-(void)setCornerRadius:(NSInteger)cornerRadius{
    [self.layer setCornerRadius:cornerRadius];
}

@end
