//
//  UIAlertAction.swift
//  come2coach
//
//  Created by Marc Ortlieb on 01.08.16.
//  Copyright © 2016 Marc Ortlieb. All rights reserved.
//

import Foundation

extension UIAlertAction {
    
    /// convenience OK button
    enum ButtonTitles: String {
        case OK, Weiter, Abbrechen, Zurück
    }
    /// fast init with ButtonTitle
    convenience init(_ buttonTitle: ButtonTitles, style: UIAlertActionStyle = .default, onTouch: ((_ action: UIAlertAction)-> Void)? = nil)  {
        
        // automatically treat .Abbrechen as UIAlertActionStyle.Cancel
        var styleCopy = style
        if buttonTitle == .Abbrechen && (style == .default || style == .cancel) {
            styleCopy = .cancel
        }
        self.init(title: buttonTitle.rawValue, style: styleCopy, handler: onTouch)
    }
    convenience init(_ title: String, style: UIAlertActionStyle = .default, onTouch: ((_ action: UIAlertAction)-> Void)? = nil)  {
        
        self.init(title: title, style: style, handler: onTouch)
    }
    
    
    
    @available(*, deprecated : 1.0.0, message : "use: UIAlertAction(.OK) / UIAlertAction(_ buttonTitle: ButtonTitles, style: UIAlertActionStyle = .Default, onTouch: ((action: UIAlertAction)-> Void)? = nil)")
    static func OK() -> UIAlertAction {
        return UIAlertAction(title: "OK".localized, style: .default, handler: nil)
    }
    @available(*, deprecated : 1.0.0, message : "use: UIAlertAction(.Abbrechen) / UIAlertAction(_ buttonTitle: ButtonTitles, style: UIAlertActionStyle = .Default, onTouch: ((action: UIAlertAction)-> Void)? = nil)")
    static func Cancel() -> UIAlertAction {
        return UIAlertAction(title: "Abbrechen".localized, style: .cancel, handler: nil)
    }
    
    
    
    
//    func onTouch(action: UIAlertAction, ) {
//        
//        if let title = action.title {
//            switch title {
//            case "Coaching beginnen":
//                ViewControllerManager.sharedInstance
//                    .topViewController()
//                    .show(Storyboards.ViewControllers.initialStart, animated: false)
//            default: break
//            }
//        }
//    }
}
