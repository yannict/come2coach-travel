//
// Created by Bernd Freier on 20.02.17.
// Copyright (c) 2017 MaytecNet. All rights reserved.
//

import Foundation

protocol PublisherPopoverDelegate {
    func republish(_ type : Int)
}

class PublisherPopover : UIView {

    var selectedTyp : Int = 0
    var delegate : PublisherPopoverDelegate?

    @IBOutlet weak var pickerView: UIView!
    
    @IBOutlet weak var popoverView: UIView!

    @IBOutlet weak var btnType: UIButton!
    
    @IBOutlet weak var pickerType: UIPickerView!
    
    @IBAction func selectTypePressed(_ sender: Any) {
        
        pickerView.isHidden = false
        pickerType.dataSource = self
        pickerType.delegate = self
        pickerType.reloadAllComponents()
        pickerType.selectRow(0, inComponent: 0, animated: false)
        
    }
    
    @IBAction func selectTypeFinished(_ sender: Any) {

        selectedTyp = pickerType.selectedRow(inComponent: 0) + 1
        let title = selectedTyp == 1 ?  "Livestream".localized : "Videostream".localized
        btnType.setTitle(title, for: UIControlState())
        pickerView.isHidden = true

    }

    @IBAction func btnDismissPopover(_ sender: Any) {

        self.removeFromSuperview()
    }
    
    
    @IBAction func selectTypeCancel(_ sender: Any) {
        
        pickerView.isHidden = true
    }
    
    @IBAction func btnContinuePressed(_ sender: Any) {
        
        self.removeFromSuperview()
        if let _ = delegate {
            delegate?.republish(selectedTyp)
        }
        
    }

    func showPopover() {

        let path = UIBezierPath.init(rect: self.bounds)
        let fillLayer = CAShapeLayer.init()
        fillLayer.path = path.cgPath
        fillLayer.fillRule = kCAFillRuleEvenOdd
        fillLayer.fillColor = UIColor.black.cgColor
        fillLayer.opacity = 0.5
        self.layer.insertSublayer(fillLayer, at: 0)

        self.layoutSubviews()
    }
    @IBAction func hideThisPopover(_ sender: Any) {
        self.removeFromSuperview()
    }


}



extension PublisherPopover : UIPickerViewDataSource, UIPickerViewDelegate {

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 2
    }

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {

        return row == 0 ? "Livestream".localized : "Videostream".localized
    }

}
