//
// Created by Bernd Freier on 18.01.17.
// Copyright (c) 2017 MaytecNet. All rights reserved.
//

import Foundation
import EVReflection

class msgBase: EVObject {

    var type: String = ""
    var data: String = ""

    init(_ type: String, _ data: String) {

        self.type = type
        self.data = data
        super.init()
    }

    required override init() {
        super.init()
    }

    required convenience init?(coder: NSCoder) {
        self.init()
    }

}