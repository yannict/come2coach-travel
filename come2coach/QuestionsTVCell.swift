//
//  QuestionsTVCell.swift
//  come2coach
//
//  Created by Marc Ortlieb on 08.08.16.
//  Copyright © 2016 MaytecNet. All rights reserved.
//

import UIKit


protocol QuestionsTVCellDelegate {
    func didTapCheckmark(at indexPath: IndexPath)
}
class QuestionsTVCell: UITableViewCell, Reusable {

    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var questionLabel: UILabel!
    @IBAction func checkmarkButtonAction(_ sender: AnyObject) {
        OnDebugging.print("QuestionsTVCell.checkmarkButtonAction() - .indexPath: \(indexPath)")
        
        if let indexPath = indexPath {
            delegate?.didTapCheckmark(at: indexPath)
        }
    }
    
    
    var question: Question? {
        didSet {
            if let question = question {
                nameLabel.text = question.displayName
                questionLabel.text = question.text
            }
        }
    }
    var indexPath: IndexPath?
    
    var delegate: QuestionsTVCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    

}
