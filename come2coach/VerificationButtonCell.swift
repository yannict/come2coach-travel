//
// Created by Bernd Freier on 21.02.17.
// Copyright (c) 2017 MaytecNet. All rights reserved.
//

import Foundation

class VerificationButtonCell: UITableViewCell {

    var pressed: (() -> Void)!

    override func awakeFromNib() {
        super.awakeFromNib()

    }

    @IBAction func buttonPressed(_ sender: Any) {

        pressed()
    }

}
