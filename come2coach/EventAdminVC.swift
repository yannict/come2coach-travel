//
//  EventAdminVC.swift
//  come2coach
//
//  Created by Victor on 04.11.16.
//  Copyright © 2016 MaytecNet. All rights reserved.
//

import Foundation
import MessageUI

class EventAdminVC: UIViewController {

    @IBOutlet weak var eventCollectionView: UICollectionView!
    @IBOutlet weak var toggleButton: UISegmentedControl!
    @IBOutlet weak var toggleWidthConstraint: NSLayoutConstraint!


    @IBOutlet weak var labelCreateEvent: UILabel!

    var coachEvents: [Event]! = [Event]()
    var archiveEvents: [Event]! = [Event]()
    var isArchive: Bool = false


    var coachingsPage = 0
    var coachingsCount = 0
    var archivePage = 0
    var archiveCount = 0

    //MARK: Life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.isHidden = true

        // check if user is already registered
        if let u = User.current {

            if (u.isVerified == 0 || u.isVerified == 1) {

                let coachVerification1 = Storyboards.ViewControllers.verification1
                self.navigationController?.push(coachVerification1, animated: true, completion: nil)
            } else if (u.isVerified == 2) {

                let coachVerification1 = Storyboards.ViewControllers.verification2
                self.navigationController?.push(coachVerification1, animated: true, completion: nil)

            }

        }

      let tFont = NSDictionary(object: Styleguide.Color.text, forKey: NSForegroundColorAttributeName as NSCopying)
      let tFont2 = NSDictionary(object: UIColor.white, forKey: NSForegroundColorAttributeName as NSCopying)
      toggleButton.setTitleTextAttributes(tFont2 as! [AnyHashable: Any], for: .selected)
      toggleButton.setTitleTextAttributes(tFont as! [AnyHashable: Any], for: UIControlState())

        toggleWidthConstraint.constant = 90
        toggleButton.removeSegment(at: 1, animated: false)

        toggleButton.layer.cornerRadius = 10.0;
        toggleButton.layer.borderColor = Styleguide.Color.text.cgColor
        toggleButton.layer.borderWidth = 1.0;
        toggleButton.layer.masksToBounds = true;


        eventCollectionView.reloadData()

        // load data
        //getAllCoachEvents()
        //getAllArchiveEvents(false)


    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        // load data
        getAllCoachEvents()
        getAllArchiveEvents(false)

        // Protocol actions
        ProtocolEventRequest.showEvent = { (eventId) in

            HTTPRequest.fetchEvent(with: eventId) { (event) in

                ProtocolEventRequest.eventId = nil

                let detailsVC = Storyboards.ViewControllers.eventDetails
                detailsVC.mode = "normal"
                detailsVC.event = event

                self.navigationController?.push(detailsVC, animated: true, completion: nil)

            }

        }

        /*getAllCoachEvents()
        getAllArchiveEvents(false)*/
    }

    //MARK: Actions

    @IBAction func switchDisplayMode() {
        isArchive = !isArchive
        isArchive ? getAllArchiveEvents(true) : getAllCoachEvents()
        eventCollectionView.reloadData()
    }

    func getCoachEvents(_ page: Int, perPage: Int) {


        HTTPRequest.fetchCoachOnlyEvents(page, perPage: perPage) { (events, currentPage, count) in
            OnDebugging.print(events)
            self.coachingsPage = currentPage
            self.coachingsCount = count
            self.coachEvents.append(contentsOf: events)
            self.eventCollectionView.reloadData()
        }

    }

    func getAllCoachEvents() {

        coachingsPage = 0
        coachingsCount = 0

        HTTPRequest.fetchCoachOnlyEvents(0, perPage: (coachingsPage + 1) * HTTPRequest.coachingsPerPage) { (events, page, count) in
            OnDebugging.print(events)
            self.coachEvents = events
            self.coachingsCount = count
            self.eventCollectionView.reloadData()
        }

    }

    func getArchiveEvents(_ fromSwitch: Bool, page: Int, perPage: Int) {

        //debugPrint("REQUEST " + String(page) + " - " + String(perPage))

        HTTPRequest.fetchArchiveEvents(page, perPage: perPage) { (events, currentPage, count) in
            self.archivePage = currentPage
            self.archiveCount = count
            self.archiveEvents.append(contentsOf: events)

            if fromSwitch == false && self.archiveEvents.count > 0 && self.toggleButton.numberOfSegments == 1 {
                self.toggleButton.insertSegment(withTitle: "ARCHIV".localized, at: 1, animated: false)
                self.toggleWidthConstraint.constant = 180
                self.eventCollectionView.reloadData()
            } else {
                self.eventCollectionView.reloadData()
            }
        }

    }

    func getAllArchiveEvents(_ fromSwitch: Bool) {

        archivePage = 0
        archiveCount = 0

        HTTPRequest.fetchArchiveEvents(0, perPage: HTTPRequest.coachingsPerPage) { (events, page, count) in
            self.archiveCount = count
            self.archiveEvents = events
            if fromSwitch == false && self.archiveEvents.count > 0 && self.toggleButton.numberOfSegments == 1 {
                self.toggleButton.insertSegment(withTitle: "ARCHIV".localized, at: 1, animated: false)
                self.toggleWidthConstraint.constant = 180
                self.eventCollectionView.reloadData()
            } else {
                self.eventCollectionView.reloadData()
            }
        }

    }

    func changeParticipants(_ event: Event) {
        let rootVC = UIApplication.shared.delegate?.window!!.rootViewController
        let popup = Bundle.main.loadNibNamed("ParticipantsPopup", owner: nil, options: nil)![0] as! ParticipantsPopup
        popup.event = event
        popup.delegate = self
        popup.frame = CGRect.init(origin: CGPoint.zero, size: CGSize(width: Constants.Sizes.screenWidth, height: Constants.Sizes.screenHeight))
        rootVC!.view.addSubview(popup)
        popup.showPopover()
    }

    func createNewCoaching() {

        if (User.current!.isVerified > 2) {

            CreateManager.sharedInstance.resetData()
            let coachingVC = Storyboards.ViewControllers.createCoachingNavCtrl
            coachingVC.navigationBar.isHidden = true
            //self.show(coachingVC, animated: true)
            // self.navigationController?.push(coachingVC, animated: true, completion: nil)

            // self.show(coachingVC, animated: true)
            present(coachingVC, animated: true)
        } else {

            self.show(UIAlertController("Verifiziere dich als Coach".localized, "Bitte verifiziere dich zuerst im Profil, bevor du selbst Coachings erstellst".localized,
                    actions: UIAlertAction(.OK)))

        }
    }

    func updateEventParticipantsAction(_ event: Event) {
        HTTPRequest.updateEvent(with: event) { (errors: [String]) in
            OnDebugging.print(errors)
        }
    }

    func republish(_ event: Event) {
        CreateManager.sharedInstance.resetData()
        CreateManager.sharedInstance.isRepublishing = true
        //CreateManager.sharedInstance.eventId = event.id
        CreateManager.sharedInstance.isOnline = event.isWebinar
        CreateManager.sharedInstance.titleText = event.title
        CreateManager.sharedInstance.description = event.descriptionText
        var address = DetailAddress()
        address.street = event.address.street
        address.location = event.address.location
        address.city = event.address.city
        address.zip = event.address.zip
        CreateManager.sharedInstance.address = address
        CreateManager.sharedInstance.minParticipant = event.minParticipants
        CreateManager.sharedInstance.maxParticipant = event.maxParticipants
        CreateManager.sharedInstance.price = Int(event.price)
        CreateManager.sharedInstance.ageRestriction = event.adult
        CreateManager.sharedInstance.duration = event.durationArrString as String
        CreateManager.sharedInstance.begin = event.begin.date

        // S3
        //
        CreateManager.sharedInstance.existingImg1 = event.img1PathExtension
        if event.img2PathExtension != "" {
            CreateManager.sharedInstance.existingImg2 = event.img2PathExtension
        }

        let coachingVC = UINavigationController(Storyboards.ViewControllers.chooseCoachingMode)
        coachingVC.navigationBar.isHidden = true
        self.present(coachingVC, animated: true, completion: nil)
    }

}

//MARK: UICollectionViewDataSource

extension EventAdminVC: UICollectionViewDataSource {

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if isArchive == false {
            //var sections = 0
            let count = coachEvents.count + 1
            /*
            if count > 0 && count < 3 {
                sections = 1
            } else {
                let sectionsPrint: Int = count % 2 == 0 ? Int(count / 2) : Int(count / 2 + 1)
                print("Sections: " + String(sectionsPrint))
                sections = coachEvents.count % 2 == 0 ? Int(count / 2) : Int(count / 2 + 1)
            } */
            //print("Sections: " + String(count % 2 == 0 ? Int(count / 2) : Int(count / 2 + 1)))
            return count % 2 == 0 ? Int(count / 2) : Int(count / 2 + 1)
            //return sections + 1
        } else {
            if archiveEvents.count > 0 && archiveEvents.count < 3 {
                return 1
            }
            let sections: Int = archiveEvents.count % 2 == 0 ? Int(archiveEvents.count / 2) : Int(archiveEvents.count / 2 + 1)
            print("Sections: " + String(sections))
            return archiveEvents.count % 2 == 0 ? Int(archiveEvents.count / 2) : Int(archiveEvents.count / 2 + 1)

        }
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if isArchive == false {

            let count = coachEvents.count + 1

            labelCreateEvent.isHidden = coachEvents.count > 0

            if count > 0 && count < 3 && section == 0 {
                return count
            }
            var items: Int = 2
            if count % 2 == 1 && section >= Int(count / 2) {
                items = 1
            } else if count % 2 == 0 && section == Int(count / 2) {
                items = 1
            }
            return items
        } else {

            labelCreateEvent.isHidden = true

            if archiveEvents.count > 0 && archiveEvents.count < 3 {
                return archiveEvents.count
            }
            var items: Int = 2
            if archiveEvents.count % 2 == 1 && section == Int(archiveEvents.count / 2) {
                items = 1
            }
            return items
        }


    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if isArchive == false {
            if coachEvents.count == 0 {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.ViewControllers.createCoachingCell, for: indexPath) as! CreateCoachingCell
                cell.delegate = self
                return cell
            } else {

                let count = coachEvents.count + 1

                if count % 2 == 1 {

                    // if indexPath.section > Int(coachEvents.count / 2) {
                    if indexPath.section == 0 && indexPath.item == 0 {

                        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.ViewControllers.createCoaching2Cell, for: indexPath) as! CreateCoachingCell
                        cell.delegate = self
                        return cell

                        //let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.ViewControllers.createCoachingCell, for: indexPath) as! CreateCoachingCell
                        //cell.delegate = self
                        //return cell
                    } else {

                        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.ViewControllers.eventAdminCell, for: indexPath) as! EventAdminCell
                        cell.cleanInfo()

                        let event = coachEvents[indexPath.section * 2 + (indexPath.item - 1)]
                        cell.fillEventInfo(event)
                        cell.delegate = self
                        if (indexPath.section * 2 + indexPath.item) == (coachingsPage + 1) * HTTPRequest.coachingsPerPage - 1 {
                            getCoachEvents(coachingsPage + 1, perPage: HTTPRequest.coachingsPerPage)
                        }
                        return cell
                    }

                } else {
                    /*
                    if indexPath.section == Int(coachEvents.count / 2) {
                        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.ViewControllers.createCoachingCell, for: indexPath) as! CreateCoachingCell
                        cell.delegate = self
                        return cell
                    } else {
                    */
                    if indexPath.section == 0 && indexPath.item == 0 {

                        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.ViewControllers.createCoaching2Cell, for: indexPath) as! CreateCoachingCell
                        cell.delegate = self
                        return cell

                    } else {
                        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.ViewControllers.eventAdminCell, for: indexPath) as! EventAdminCell
                        cell.cleanInfo()

                        let event = coachEvents[indexPath.section * 2 + (indexPath.item - 1)]
                        cell.fillEventInfo(event)
                        cell.delegate = self
                        if (indexPath.section * 2 + indexPath.item) == (coachingsPage + 1) * HTTPRequest.coachingsPerPage - 1 {
                            getCoachEvents(coachingsPage + 1, perPage: HTTPRequest.coachingsPerPage)
                        }
                        return cell
                    }

                }
            }
        } else {

            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.ViewControllers.eventArchiveCell, for: indexPath) as! EventAdminCell
            cell.cleanInfo()
            let event = archiveEvents[indexPath.section * 2 + indexPath.item]
            cell.fillEventInfo(event)
            cell.delegate = self

            if (indexPath.section * 2 + indexPath.item) == (archivePage + 1) * HTTPRequest.coachingsPerPage - 1 {
                getArchiveEvents(true, page: archivePage + 1, perPage: HTTPRequest.coachingsPerPage)
            }
            return cell

        }

    }

}

//MARK: UICollectionViewDelegateFlowLayout

extension EventAdminVC: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        //let cellHeight = 165 // 265

        if isArchive == false {
            if coachEvents.count == 0 {
                return CGSize.init(width: Constants.Sizes.screenWidth - 30, height: (Constants.Sizes.screenHeight - 185))
            } else {
                if indexPath.section != Int(coachEvents.count / 2) {
                    /*
                    if indexPath.section > Int(coachEvents.count / 2) {
                        return CGSize.init(width: Constants.Sizes.screenWidth - 30, height: 265)
                    }*/
                    return CGSize.init(width: (Constants.Sizes.screenWidth - 45) / 2, height: Constants.Sizes.eventAdminCellHeight)
                } else {
                    //if coachEvents.count % 2 == 1 {
                    return CGSize.init(width: (Constants.Sizes.screenWidth - 45) / 2, height: Constants.Sizes.eventAdminCellHeight)
                    //} else {
                    //    return CGSize.init(width: Constants.Sizes.screenWidth - 30, height: 265)
                    //}
                }
            }
        } else {
            return CGSize.init(width: (Constants.Sizes.screenWidth - 45) / 2, height: Constants.Sizes.eventAdminCellHeight)
        }
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        if isArchive == false {
            /*if coachEvents.count == 0 {
                return UIEdgeInsetsMake(4, 15, 4, 15)
            } else { */

            let count = coachEvents.count + 1

            if section < Int((count) / 2) {
                return UIEdgeInsetsMake(4, 15, 4, 15)
            } else {
                if (count) % 2 == 1 && coachEvents.count == coachingsCount {
                    let leftInset: CGFloat = (Constants.Sizes.screenWidth - ((Constants.Sizes.screenWidth - 45) / 2)) / 2
                    return UIEdgeInsetsMake(4, leftInset - 15, 4, leftInset - 15)
                } else {
                    return UIEdgeInsetsMake(4, 15, 4, 15)
                }
            }
            //}
        } else {
            if section != Int(archiveEvents.count / 2) {
                return UIEdgeInsetsMake(4, 15, 4, 15)
            } else {
                if archiveEvents.count % 2 == 1 && archiveEvents.count == archiveCount {
                    let leftInset: CGFloat = (Constants.Sizes.screenWidth - ((Constants.Sizes.screenWidth - 45) / 2)) / 2
                    return UIEdgeInsetsMake(4, leftInset - 15, 4, leftInset - 15)
                } else {
                    return UIEdgeInsetsMake(4, 15, 4, 15)
                }
            }
        }

    }
}

//MARK: UICollectionViewDelegate

extension EventAdminVC: UICollectionViewDelegate {

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath)
        if cell?.classForCoder == EventAdminCell.self {
            let detailsVC = Storyboards.ViewControllers.eventDetails
            detailsVC.mode = isArchive == true ? "archive" : "admin"
            let event = isArchive == true ? archiveEvents[indexPath.section * 2 + indexPath.item] : coachEvents[indexPath.section * 2 + indexPath.item - 1]
            detailsVC.event = event
            self.navigationController?.push(detailsVC, animated: true, completion: nil)
        }
    }

}

//MARK: CreateCoachingCellDelegate

extension EventAdminVC: CreateCoachingCellDelegate {

    func didInitiateNewCoachingCreating() {
        createNewCoaching()
    }

}

//MARK: EventAdminCellDelegate

extension EventAdminVC: EventAdminCellDelegate {

    func changeParticipantsForEvent(_ event: Event) {
        changeParticipants(event)
    }

    func shareEvent(_ event: Event) {
        var description = event.descriptionText
        if description.characters.count > 255 {
            description = description.substring(to: description.characters.index(description.startIndex, offsetBy: 255))
        }
        let shareURL = HTTPRequest.rootUrl + "event/" + event.slug

        //let shareItems : [AnyObject] = [event.title, description, NSURL.init(string: event.img1PathExtension)!, shareURL]

        let shareItems: [AnyObject] = [event.title as AnyObject, description as AnyObject, shareURL as AnyObject]

        let shareController = UIActivityViewController.init(activityItems: shareItems, applicationActivities: nil)
        shareController.popoverPresentationController?.sourceView = self.view
        self.present(shareController, animated: true, completion: nil)
    }

    func republishEvent(_ event: Event) {
        republish(event)
    }

    func sendMail(_ event: Event) {
        let emailNC = UINavigationController(Storyboards.ViewControllers.emailVC)
        let emailVC = emailNC.viewControllers.first as! EmailViewController
        emailVC.event = event
        emailNC.navigationBar.isHidden = true
        self.present(emailNC, animated: true, completion: nil)
    }

}

//MARK: ParticipantsPopupDelegate

extension EventAdminVC: ParticipantsPopupDelegate {

    func updateEventParticipants(_ event: Event) {
        updateEventParticipantsAction(event)
    }

}

