//
//  PaddedLabel.m
//  come2coach
//
//  Created by Werner Kratochwil Full on 04.08.16.
//  Copyright © 2016 MaytecNet. All rights reserved.
//
// taken from here: http://stackoverflow.com/questions/3476646/uilabel-text-margin

#warning TODO move to Swift with code from above


#import "PaddedLabel.h"

@implementation PaddedLabel

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        self.edgeInsets = UIEdgeInsetsMake(self.topEdge, self.leftEdge, self.bottomEdge, self.rightEdge);
    }
    return self;
}

- (void)drawTextInRect:(CGRect)rect
{
    self.edgeInsets = UIEdgeInsetsMake(self.topEdge, self.leftEdge, self.bottomEdge, self.rightEdge);
    
    [super drawTextInRect:UIEdgeInsetsInsetRect(rect, self.edgeInsets)];
}

- (CGSize)intrinsicContentSize
{
    CGSize size = [super intrinsicContentSize];
    size.width  += self.edgeInsets.left + self.edgeInsets.right;
    size.height += self.edgeInsets.top + self.edgeInsets.bottom;
    return size;
}


@end
