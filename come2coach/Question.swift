//
//  Question.swift
//  come2coach
//
//  Created by Werner Kratochwil Full on 19.09.16.
//  Copyright © 2016 MaytecNet. All rights reserved.
//

import Foundation
import RealmSwift
import Unbox

class Question: Object {
    dynamic var displayName:String = ""
    dynamic var text:String = "" //= "(\(arc4random())) Kannst du den Trick mit dem Wasserkochen, nachmal kurz zeigen das war super!"
    dynamic var timestamp: Date = Date()
    dynamic var userID = -1
    dynamic var eventID = -1
    
    dynamic var realmID = UUID().uuidString
//    /** REALM: primaryKey - https://realm.io/docs/swift/latest/#primary-keys
//     primaryKey needs to be a class function which returns the name of the property which is the primary key, not an instance method which returns the value of the primary key.
//     additionally dynamic var realmID = Constants.Realm.PrimaryKeys.user
//     */
    override class func primaryKey() -> String? {
        return "realmID"
    }


    
    func loadDict(_ dict: NSDictionary) -> Question?
    {
        if let valUserID = dict["user_id"] as? Double
        {
            userID = Int(valUserID)
        }
        else
        {
            return nil
        }
        if let strText = dict["message"] as? String
        {
            text = strText
        }
        else
        {
            return nil
        }
        if let valTimestamp = dict["date"] as? Double
        {
            timestamp = Date(timeIntervalSince1970: valTimestamp)
        }
        else
        {
            return nil
        }
        if let strDisplayName = dict["displayName"] as? String
        {
            displayName = strDisplayName
        }
        else
        {
            displayName = "Unbekannter User \(userID)"
        }
        return self
    }

    /*required init(unboxer: Unboxer) {
        userID = unboxer.unbox("user_id")
        text = unboxer.unbox("message")
        timestamp = NSDate(timeIntervalSince1970: unboxer.unbox("date"))
        displayName = unboxer.unbox("displayName")
    }*/
    
    //TODO: FIX THIS
    func add() {
        defaultRealm
            .add(self, update: false)
            .printErrors()
    }
 
    // REALM: Delete Object
    func delete() {
        defaultRealm
            .delete(self)
            .printErrors()
    }


    
}
