//
//  RoundedImageView.m
//  MaytecNet
//
//  Created by Werner Kratochwil on 16.06.15.
//  Copyright (c) 2015 MaytecNet All rights reserved.
//
//

#import "RoundedImageView.h"

@implementation RoundedImageView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)layoutSubviews
{
    self.clipsToBounds = YES;
    self.layer.cornerRadius = floor(self.bounds.size.height/2.0f);
    [super layoutSubviews];
}

@end
