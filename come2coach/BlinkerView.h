//
//  BlinkerView.h
//
//  Created by Werner Kratochwil Full on 22.11.09.
//  Copyright 2009 MaytecNet. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface BlinkerView : UIView {
	bool running;
}

#define BLINK_DURATION 1.2
#define MIN_ALPHA 0.2
#define MAX_ALPHA 1.0

-(void)startAnimation;
-(void)stopAnimation;
-(void)doAnimate;
@end
