//
//  KeychainWrapper.h
//  come2coach
//
//  Created by Werner Kratochwil on 31.08.15.
//  Copyright (c) MaytecNet. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KeychainWrapper : NSObject

- (void)mySetObject:(id)inObject forKey:(id)key;
- (id)myObjectForKey:(id)key;
- (void)resetKeychainItem;
- (NSMutableDictionary *)dictionaryToSecItemFormat:(NSDictionary *)dictionaryToConvert;
- (NSMutableDictionary *)secItemFormatToDictionary:(NSDictionary *)dictionaryToConvert;

- (void)writeToKeychain;



@end
