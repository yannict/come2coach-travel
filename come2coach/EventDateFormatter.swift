//
//  EventDateFormatter.swift
//  come2coach
//
//  Created by Victor on 31.10.16.
//  Copyright © 2016 MaytecNet. All rights reserved.
//

import Foundation
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class EventDateFormatter {
    
    class func timeLeftToDate(_ date: Date) -> String {
        let currentDate = Date()
        let calendar = Calendar.init(identifier: Calendar.Identifier.gregorian)
        let components = (calendar as NSCalendar?)?.components([.year, .month, .weekOfMonth, .day, .hour, .minute], from: currentDate, to: date, options: NSCalendar.Options(rawValue: UInt(0)))
        if let _  = components {
            if components?.year > 0 {
                return displayYears(components!)
            }
            else if components?.month > 0 {
                return displayMonthes(components!)
            }
            else if components?.weekOfMonth > 0 {
                return displayWeeks(components!)
            }
            else if components?.day > 0 {
                return displayDays(components!)
            }
            else if components?.hour > 0 {
                return displayHours(components!)
            }
            else if components?.minute > 0 {
                return displayMinutes(components!)
            }
        }
        return ""
    }
    
    class fileprivate func displayYears(_ components: DateComponents) -> String {
        if components.year == 1 {
            return "in 1 Jahr"
        }
        else {
            return "in " + String(components.year!) + " Jahren"
        }
    }
    
    class fileprivate func displayMonthes(_ components: DateComponents) -> String {
        if components.month == 1 {
            return "in 1 Monat"
        }
        else {
            return "in " + String(components.month!) + " Monate"
        }
    }
    
    class fileprivate func displayWeeks(_ components: DateComponents) -> String {
        if components.weekOfMonth == 1 {
            return "in 1 Woche"
        }
        else {
            return "in " + String(components.weekOfMonth!) + " Wochen"
        }
    }
    
    class fileprivate func displayDays(_ components: DateComponents) -> String {
        if components.day == 1 {
            return "in 1 Tag"
        }
        else {
            return "in " + String( components.day! ) + " Tagen"
        }
    }
    
    class fileprivate func displayHours(_ components: DateComponents) -> String {
        if components.hour == 1 {
            return "in 1 Stunde"
        }
        else {
            return "in " + String(components.hour!) + " Stunden"
        }
    }
    
    class fileprivate func displayMinutes(_ components: DateComponents) -> String {
        if components.minute == 1 {
            return "in 1 Minute"
        }
        else {
            return "in " + String(components.minute!) + " Minuten"
        }
    }
    
}
