//
//  AppDelegate.swift
//  come2coach
//
//  Created by Marc Ortlieb on 31.07.16.
//  Copyright © 2016 Marc Ortlieb. All rights reserved.
//

import UIKit
import RealmSwift
import AWSS3
import Braintree
import BraintreeDropIn

//import FacebookCore
//import FacebookLogin

import CoreData
import FBSDKCoreKit
import FBSDKLoginKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {

        // button color (nav bar) for uiactivityview
        UINavigationBar.appearance().tintColor = UIColor.black

        // init braintree sdk - url scheme
        BTAppSwitch.setReturnURLScheme("de.come2coach.ios.payments")

        // Set the theme before initializing Drop-In
        BTUIKAppearance.darkTheme()

      
      UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName: Styleguide.Color.text, NSFontAttributeName: Styleguide.Font.medium(size: 13)!]
      UINavigationBar.appearance().tintColor = Styleguide.Color.text
      UINavigationBar.appearance().barTintColor = Styleguide.Color.text
      
/*
#if DEBUG
        // filter out print() statements if in release Mode / on debuging show print() statements
        OnDebugging
                .enableDebugginMode()
#endif
*/

        // moved to app context
        //initPushNotifications(application)

        //LocalNotificationManager.sharedInstance
        //  .registerCoachingBeginsWithOptionalUserActions()


        UINavigationBar.systemWideSetup()

        // Realm
        defaultRealm
                .onSuccess {
                    var config = $0.configuration
                    config.deleteRealmIfMigrationNeeded = true
                    Realm.Configuration.defaultConfiguration = config
                }
                .onError { (error) in
                    debugPrint("couldn't get defaultRealm - error: \(error)")
                }
        /*
        OnDebugging.perform { onDebugging in

        }*/

        // Init Instabug
        // Instabug.start(withToken: "7143e0c1427391c845680d1b900cd88f", invocationEvent: IBGInvocationEvent.shake)

        // Set credential for S3
        //
        let credentialsProvider: AWSCognitoCredentialsProvider = AWSCognitoCredentialsProvider(regionType: AWSRegionType.euCentral1, identityPoolId: Constants.myIdentityPoolId)

        let configuration = AWSServiceConfiguration(region: AWSRegionType.euCentral1, credentialsProvider: credentialsProvider)

        AWSServiceManager.default().defaultServiceConfiguration = configuration

        //Facebook
        // #facebook
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)

        ViewControllerManager.sharedInstance.instantiate()

        // setup initial VC uncomment for RELEASE | comment out if want to test other storyboards than login.storyboard
        // needed for auotLogin after AppTermination, where user did successfully login before at least one time

        IAPManager.instance.initialize()
        
        window?.rootViewController = UINavigationController(Storyboards.ViewControllers.loginPreview)
        return true
    }


    func applicationWillResignActive(_ application: UIApplication) {
        // OnDebugging.print("applicationWillResignActive", title: "APPDELEGATE")
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // OnDebugging.print("applicationDidEnterBackground", title: "APPDELEGATE")
        let topViewController = ViewControllerManager.sharedInstance.topViewController()
        if (topViewController is LivestreamingCoachVC) {
            (topViewController as! LivestreamingCoachVC).didEnterBackground()
        }
        if (topViewController is LivestreamingParticipantVC) {
            (topViewController as! LivestreamingParticipantVC).didEnterBackground()
        }
    }


    func applicationWillEnterForeground(_ application: UIApplication) {
        // OnDebugging.print("applicationWillEnterForeground", title: "APPDELEGATE")

        // #facebook
        FBSDKAppEvents.activateApp()

        let topViewController = ViewControllerManager.sharedInstance.topViewController()
        if (topViewController is LoginPreviewVC || topViewController is LoginVC) {
            OnDebugging
                    .print("topViewController is LoginPreviewVC -> don't show(Storyboards.ViewControllers.initialStart) but DO 'FBSDKAppEvents.activateApp()'",
                    title: "APPDELEGATE.applicationDidBecomeActive")
        } else if (topViewController is LivestreamingCoachVC) {
            (topViewController as! LivestreamingCoachVC).willEnterForeground()
        } else if (topViewController is LivestreamingParticipantVC) {
            (topViewController as! LivestreamingParticipantVC).willEnterForeground()
        } /* else if (topViewController is CoachingsVC) {
            topViewController.show(Storyboards.ViewControllers.initialStart) {

                //TODO: TESTING without unwind2VCBeforeRedirect
                EventManager.sharedInstance.loadAllEvents(topViewController, alsoCheckUpcomingEvents: true, unwind2VCBeforeRedirect: true, noUpcomingEventHandler: {
                    topViewController.show(Storyboards.ViewControllers.coachings)
                })
            }

        } */
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        //OnDebugging.print("applicationDidBecomeActive", title: "APPDELEGATE")

    }

    // MARK: Launch app via url

    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey: Any]) -> Bool {

        if url.scheme?.localizedCaseInsensitiveCompare("de.come2coach.ios.payments") == .orderedSame {
            return BTAppSwitch.handleOpen(url, options: options)
        }

        if (processProtocol( url: url)) {
            return true
        }

        return FBSDKApplicationDelegate.sharedInstance().application(
                app,
                open: url as URL!,
                sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as! String,
                annotation: options[UIApplicationOpenURLOptionsKey.annotation]
        )

        //FBSDKApplicationDelegate.sharedInstance().application(app, open: url, sourceApplication: options["UIApplicationOpenURLOptionsSourceApplicationKey"] as! String,
        //      annotation: nil)
    }

    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {

        if url.scheme?.localizedCaseInsensitiveCompare("de.come2coach.ios.payments") == .orderedSame {
            return BTAppSwitch.handleOpen(url, sourceApplication: sourceApplication)
        }

        if (processProtocol( url: url)) {
            return true
        }

        // #facebook
        return FBSDKApplicationDelegate.sharedInstance().application(
                application,
                open: url as URL!,
                sourceApplication: sourceApplication,
                annotation: annotation)

    }


    // MARK: Find top view controller

    func topViewController() -> UIViewController {
        return self.topViewController((self.window?.rootViewController)!)
    }

    func topViewController(_ rootViewController: UIViewController) -> UIViewController {
        if rootViewController.presentedViewController == nil {
            return rootViewController
        }

        // #swift3
        // if rootViewController.presentedViewController? (of: UINavigationController) == true {
        if rootViewController.presentedViewController is UINavigationController {
            let navigationController = rootViewController.presentedViewController as! UINavigationController
            let lastViewController = navigationController.viewControllers.last! as UIViewController
            return self.topViewController(lastViewController)
        }
        let presentedViewController = rootViewController.presentedViewController! as UIViewController
        return self.topViewController(presentedViewController)
    }

    // MARK: Handle Notifications
    // NOT PART of UIApplicationDelegate

//    func application(application: UIApplication,  launchOptions: [NSObject: AnyObject]?) -> Bool { OnDebugging.print("APPDELEGATE: launchOptions")
//        guard let options    = launchOptions,
//                notification = options[UIApplicationLaunchOptionsLocalNotificationKey] as? UILocalNotification else {
//                OnDebugging
//                    .printError("couldn't get userInfoForKey", title: "AppDelegate.launchOptions)")
//                return true
//        }
//
//        OnDebugging
//            .print("launching App from LocalNotification", title: "Appdelegate.application:launchOptions:()")
//        
//        if notification.category == Constants.LocalNotifications.Category.coachingBegins {
//            window?.rootViewController = UINavigationController(rootViewController: Storyboards.ViewControllers.initialStart)
//            window?.makeKeyAndVisible()
//        }
//        ViewControllerManager.sharedInstance
//            .topViewController()
//            .show(Storyboards.ViewControllers.initialStart, animated: false)
//        
//        return true
//    }

/*
    func application(_ application: UIApplication, didReceive notification: UILocalNotification) {
        OnDebugging.print("APPDELEGATE: didReceiveLocalNotification")
        guard let userInfo = notification.userInfo,
              let eventID = userInfo[Constants.LocalNotifications.UserInfoKeys.coachingID] as? Int else {
            OnDebugging
                    .printError("couldn't get eventID from userInfoForKey[\(Constants.LocalNotifications.UserInfoKeys.coachingID)]",
                    title: "AppDelegate.didReceiveLocalNotification)")
            return
        }
        OnDebugging
                .print("Local Push Notification received", title: "Appdelegate.didReceiveLocalNotification()")



        if notification.category == Constants.LocalNotifications.Category.coachingBegins {
            OnDebugging
                    .print("application.applicationState == \(application.applicationState)", title: "Appdelegate.didReceiveLocalNotification()")

            switch application.applicationState {
            case .inactive, .background:

                let topViewController = ViewControllerManager.sharedInstance.topViewController()
                if (topViewController is CoachingsVC || topViewController is DetailsWebVC) {
                    EventManager.sharedInstance.checkUpcomingEvents(topViewController)
                } else if topViewController is UIAlertController {
                    topViewController.unwind() {
                        EventManager.sharedInstance.checkUpcomingEvents(topViewController)
                    }
                }

            case .active:



                //TODO: check if still necessary, if yes search for a solution
//                guard InternetConnectionManager.sharedInstance.isConnected == true else
//                {
//                    topViewController.show(topViewController.defaultNoInternetAlertView())
//                    return
//                }

                if let event = EventManager.sharedInstance.allEvents
                        .filter({ $0.id == eventID })
                        .safeIndex(0) {
                    self.showAlertStartCoaching(event)
                } else {
                    // Show ALERT: "Coaching beginnen"
                    HTTPRequest
                            .fetchEvent(with: eventID) { [weak self] event in
                        self?.showAlertStartCoaching(event)
                    }
                }


            }
        } else {
            OnDebugging
                    .printError("notification.category != Constants.LocalNotifications.Category.coachingBegins", title: "AppDelegate.didReceiveLocalNotification()")
        }
    }


    func showAlertStartCoaching(_ event: Event) {
        let eventTitle = event.title.isEmpty ? "" : "\"\(event.title)\""
        let topViewController = ViewControllerManager.sharedInstance.topViewController()

        let beginCoaching = UIAlertAction(title: "Coaching beginnen".localized, style: UIAlertActionStyle.default, handler: { _ in
            if event.isHappeningNow {
                switch event.userType {
                case .coach:
                    topViewController
                            .present(Storyboards.ViewControllers.livestreamingCoach, animated: true)
                case .participant:
                    topViewController
                            .present(Storyboards.ViewControllers.livestreamingParticipant, animated: true)
                }
            } else if event.isInCountDownPhaseNow {
                let countdownVC = Storyboards.ViewControllers.countDown
                countdownVC.event = event
                topViewController.show(UINavigationController(countdownVC), modally: true)
            } else if event.didEnd {
                // TODO: @Werner !bitte testen ob sich das hier wie gewünscht verhält.
                topViewController
                        .show(UIAlertController("Coaching verpasst".localized, "Tut mir leid, das Coaching ist leider schon vorüber.".localized, actions: UIAlertAction(.OK)))
            }
        })

        topViewController
                .show(UIAlertController("Gleich gehts los".localized, "Dein Coaching \(eventTitle) beginnt gleich.".localized, actions: UIAlertAction(.Abbrechen, style: .cancel), beginCoaching))

    }

    */


    // ---------------------------
    // MARK: [INTERNET CONNECTION] // InternetSensitive
    // ---------------------------

//    private func guard_hasInternetConnection_else(@noescape performBlock: (noInternetAC: UIAlertController)->Void ) {
//        guard InternetConnectionManager.sharedInstance.isConnected == true else {
//            OnDebugging.print("InternetConnectionManager.sharedInstance.isConnected == false", title: "LoginVC.login()")
//            
//            performBlock(noInternetAC: self.defaultNoInternetALertView())
//            return
//        }
//    }

    func defaultNoInternetALertView() -> UIAlertController {
        return
        UIAlertController("Kein Internet:".localized, "Bitte stelle sicher, dass du eine Internetverbindung hast.".localized, actions: UIAlertAction(.OK))
    }


//MARK: APNS

    func application(_ application: UIApplication, didRegister notificationSettings: UIUserNotificationSettings) {

        if notificationSettings.types != UIUserNotificationType() {
            application.registerForRemoteNotifications()
        }
    }


    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {

        var token = ""
        for i in 0 ..< deviceToken.count {
            token = token + String(format: "%02.2hhx", arguments: [deviceToken[i]])
        }
        User.newDeviceToken = token

        //debugPrint("DEVICE TOKEN" + token)

        if let t = User.newDeviceToken {
            HTTPRequest.updatePushToken(t)
        }

    }

    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        OnDebugging.print("APNS: Error with remote notifications: \(error.localizedDescription)")
    }

    func processNotification(userInfo: [AnyHashable: Any]) {

        if let aps = userInfo["aps"] as? NSDictionary {

            var notifiAlert = UIAlertView()
            notifiAlert.title = "Nachricht".localized
            notifiAlert.message = aps["alert"] as! String
            notifiAlert.addButton(withTitle: "OK")
            notifiAlert.show()
        }

        if let action = userInfo["action"] as? String {

            var userId = userInfo["user_id"] as? Int
            var eventId = userInfo["event_id"] as? Int

            if (action == "coach-reminder" ||
                    action == "user-reminder") {

                if let eId = eventId {

                    CoachReminder.active = true
                    CoachReminder.eventId = eId
                    if CoachReminder.remindMe != nil {
                        CoachReminder.remindMe()
                    }

                    let systemSoundID: SystemSoundID = 1005
                    AudioServicesPlayAlertSound(systemSoundID)
                }
            }

        }

    }

    func processProtocol(url: URL) -> Bool {

        // Deep link for password reset will be like come2coach://password/<token>
        if url.scheme == "come2coach" {
            let param = url.absoluteString.components(separatedBy: "/")
            if (param.count == 4) {

                if (param[2] == "passwort") {

                    User.token = param[3]
                    topViewController().present(Storyboards.ViewControllers.newPassword, animated: true)

                } else if (param[2] == "affiliate") {


                    HTTPRequest.postShareLink(param[3]) { (success, eventId) in

                        if (success) {

                            if (!User.loginManager.isAuthenticated) {

                                ProtocolEventRequest.eventId = eventId

                            } else {

                                ProtocolEventRequest.showEvent(eventId)

                            }

                        }


                    }

                } else if (param[2] == "event") {

                    if let eventId = Int(param[3]) {

                        if (!User.loginManager.isAuthenticated) {

                            ProtocolEventRequest.eventId = eventId

                        } else {

                            ProtocolEventRequest.showEvent(eventId)

                        }

                    }
                }

            }

            return true
        }


        return false

    }

    func application(_ application: UIApplication, didReceiveRemoteNotification data: [AnyHashable: Any]) {
        // Print notification payload data
        print("Push notification received: \(data)")

        processNotification(userInfo: data)
    }


    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        OnDebugging.print("didReceiveRemoteNotification");

        processNotification(userInfo: userInfo)

        /*
            let notification = UILocalNotification()
            notification.alertBody = aps["alert"] as! String
            //notification.alertAction = "Streiche, um zu deinem Coaching zu kommen.".localized
            notification.soundName = UILocalNotificationDefaultSoundName
            notification.fireDate = Date()
            //notification.userInfo = [Constants.LocalNotifications.UserInfoKeys.coachingID: event.id]

            //UIApplication.shared.scheduleLocalNotification(notification)
            UIApplication.shared.presentLocalNotificationNow(notification)
            */


    }


    func initPushNotifications(_ application: UIApplication) {
        let notificationTypes: UIUserNotificationType = [UIUserNotificationType.alert, UIUserNotificationType.badge, UIUserNotificationType.sound]
        let pushNotificationSettings = UIUserNotificationSettings(types: notificationTypes, categories: nil)
        application.registerUserNotificationSettings(pushNotificationSettings)
        application.registerForRemoteNotifications()
    }


}





