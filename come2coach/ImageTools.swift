import Foundation
import SDWebImage

class ImageTools {

    static func resizeImage(_ image: UIImage, newValue: CGFloat) -> UIImage {

        let scale = newValue / image.size.width
        let newHeight = image.size.height * scale

        UIGraphicsBeginImageContext(CGSize(width: newValue, height: newHeight))
        image.draw(in: CGRect(x: 0, y: 0, width: newValue, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return newImage!
    }

    static func clearImageWebCache() {


        SDWebImageManager.shared().imageCache .cleanDisk()
        SDWebImageManager.shared()!.imageCache.clearMemory()
        SDWebImageManager.shared()!.imageCache.clearDisk()



    }
}
