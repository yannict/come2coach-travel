//
//  JSONWebToken.swift
//  come2coach
//
//  Created by Marc Ortlieb on 04.08.16.
//  Copyright © 2016 Marc Ortlieb. All rights reserved.
//

import Foundation
import RealmSwift



class JSONWebToken {
    static let sharedInstance = JSONWebToken()
    fileprivate init(){}
    
    // "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjU2LCJpc3MiOiJodHRwczpcL1wvYXBpLmNvbWUyY29hY2guZGVcL3YxXC9hdXRoXC9sb2dpbiIsImlhdCI6MTQ3MDIxNzgyOCwiZXhwIjoxNDcxNDI3NDI4LCJuYmYiOjE0NzAyMTc4MjgsImp0aSI6IjRkMDM2NmU3MTA0MDQyMjM4ZGUwOTVmZWU3NGFhMTUyIn0.pKXOmKMwsZUzj7sncZTPoeJoz4V7w8LOFQ9mh2q9LBQ"
    
    //"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjcxLCJpc3MiOiJodHRwczpcL1wvYXBpLmNvbWUyY29hY2guZGVcL3YxXC9hdXRoXC9sb2dpbiIsImlhdCI6MTQ3OTQ2NjAxNywiZXhwIjoxNDgwNjc1NjE3LCJuYmYiOjE0Nzk0NjYwMTcsImp0aSI6ImNlYzQyZGJiZGYyOTgzMjdiYzZhMzYyNWMyNmYzNDNiIn0.nMdb6S8bbT-S_Oa7u4LqtFU2RYPGI0zgNN1C5c__lcA"
    
    var value: String  {
        
        get {
            if let jwtOptional: String? = try? Realm().objects(User.self).resultsSafeIndex(0)?.JSONWebToken,
                let jwt = jwtOptional {
                return jwt
            } else {
                return ""
            }
        }
        // TODO: Make this CRASH-SAVE
        //        set {
        //            let realm = try! Realm()
        //            if let user = User.current {
        //                user.JSONWebToken = value
        //
        //                try! realm.write {
        //                    realm.add(user, update: true)
        //                }
        //            }
        //        }
        //        set {
        //            do {
        //                let realm = try Realm()
        //
        //                if let user = realm.objectForPrimaryKey(User.self, key: Constants.RealmID.user) {
        //                    user.JSONWebToken = value
        //
        //                    try realm.write {
        //                        realm.add(user, update: true)
        //                    }
        //
        //                } else {
        //                    OnDebugging
        //                        .printError("couldnt fetch user from realm", title: "JSONWebToken.value.set")
        //                }
        //
        //            } catch  {
        //                OnDebugging
        //                    .printError("ERROR: \(error)", title: "JSONWebToken.value.set")
        //            }
        //        }
        
    }
    
    
    // TODO: make NetworkRequest - check if JWT is valid
    // SUCCESS, isValid = "true" - simulated for Login - integrate Network request here
    
    static func validate(_ JSONWebToken: String, handler:(_ isValid:Bool, _ error: NSError?)->Void) {
        
        handler(true, nil)
    }
    
    
}
