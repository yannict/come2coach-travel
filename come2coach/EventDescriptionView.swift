//
//  EventDescriptionView.swift
//  come2coach
//
//  Created by Timotheus Laubengaier on 26.04.17.
//  Copyright © 2017 MaytecNet. All rights reserved.
//

import Foundation
import UIKit

class EventDescriptionView: UIView {
  
  let locationImageView: UIImageView = {
    let view = UIImageView()
    view.contentMode = .scaleAspectFill
    view.backgroundColor = Styleguide.Color.lightGray
    view.clipsToBounds = true
    return view
  }()
  
  let headingView: UIView = {
    let view = UIView()
    view.backgroundColor = Styleguide.Color.lightGray
    return view
  }()
  
  let headingLabel: UILabel = {
    let label = UILabel()
    label.text = "KURZBESCHREIBUNG"
    label.font = Styleguide.Font.light(size: 13)
    label.textColor = Styleguide.Color.text
    return label
  }()
  
  let descriptionLabel: UILabel = {
    let label = UILabel()
    label.text = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut"
    label.font = Styleguide.Font.light(size: 13)
    label.textColor = Styleguide.Color.text
    label.numberOfLines = 0
    return label
  }()
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    
    addSubview(locationImageView)
    locationImageView.snp.makeConstraints { (make) in
      make.top.equalTo(self)
      make.leading.equalTo(self)
      make.trailing.equalTo(self)
      make.height.equalTo(213)
    }
    
    addSubview(headingView)
    headingView.snp.makeConstraints { (make) in
      make.top.equalTo(locationImageView.snp.bottom)
      make.leading.equalTo(self)
      make.trailing.equalTo(self)
      make.height.equalTo(40)
    }
    
    headingView.addSubview(headingLabel)
    headingLabel.snp.makeConstraints { (make) in
      make.leading.equalTo(headingView).offset(15)
      make.centerY.equalTo(headingView)
    }
    
    addSubview(descriptionLabel)
    descriptionLabel.snp.makeConstraints { (make) in
      make.top.equalTo(headingView.snp.bottom).offset(20)
      make.leading.equalTo(self).offset(20)
      make.trailing.equalTo(self).offset(-20)
      make.bottom.equalTo(self).offset(-20)
    }
    
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
}
