//
// Created by Bernd Freier on 12.02.17.
// Copyright (c) 2017 MaytecNet. All rights reserved.
//

import Foundation


class ProfileDescriptionVC : UIViewController {

    var placeholder : String = "Gib hier bitte Deinen Text ein ...".localized

    var completion: ((_ txt : String ) -> Void)!
    var text : String?

    @IBOutlet weak var textView: UITextView!
    
    @IBAction func clickBackButton(_ sender: Any) {

        self.completion(textView.text)

        show(Storyboards.ViewControllers.profile, animated: true)

    }
    
    override func viewDidLoad() {

        textView.delegate = self
        textView.returnKeyType = UIReturnKeyType.default
        if let t = text {
            textView.text = t
        }

        if (textView.text == "")
        {
            // set placeholder in case no text was entered
            textView.textColor = UIColor.lightGray
            textView.text = placeholder
        }
    }

}


extension ProfileDescriptionVC: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        /*
        guard let enteredText = textView.text else { return true }

        let newLength = enteredText.characters.count + text.characters.count - range.length
        return newLength <= 125
        */
        return true
    }

    func textViewDidBeginEditing(_ textView: UITextView) {
        if (textView.text == placeholder.localized)
        {
            textView.text = ""
            textView.textColor = UIColor.black
        }
    }

    func textViewDidEndEditing(_ textView: UITextView) {
        if (textView.text == placeholder.localized)
        {
            textView.text = ""
            textView.textColor = UIColor.black
        }
    }
}
