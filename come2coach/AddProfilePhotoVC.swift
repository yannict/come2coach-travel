//
//  AddProfilePhotoVC.swift
//  come2coach
//
//  Created by Alex Kupchak on 10/21/16.
//  Copyright © 2016 MaytecNet. All rights reserved.
//

import Foundation
import UIKit
import AWSS3
import AWSCore
import Photos
import MRProgress

class AddProfilePhotoVC: UIViewController {
    
    @IBOutlet weak var profileImageView: UIImageView!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var title2Label: UILabel!


    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var buttonBottomAnchor: NSLayoutConstraint!
    
    @IBOutlet weak var controllerTitleLabel: UILabel!
    
    @IBOutlet weak var successImageView: UIImageView!
    
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var cancelIcon: UIImageView!
    
    var selectedImageUrl: URL!
    
    // MARK: Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupControllerTitle(controllerTitleLabel.text!)
        
        let deltaHeight: CGFloat = 45
        buttonBottomAnchor.constant -= deltaHeight
        view.layoutIfNeeded()
        
        profileImageView.layer.masksToBounds = true
        //profileImageView.layer.borderWidth = 1.0
        profileImageView.layer.cornerRadius = 50.0
        //profileImageView.layer.borderColor = UIColor.white.cgColor
        
        successImageView.alpha = 0
    }
    
    // MARK: Actions
    
    @IBAction func clickCancel(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func clickNext(_ sender: AnyObject) {
        self.show(Storyboards.ViewControllers.registrationConfirm)
        //self.show(Storyboards.ViewControllers.login)
    }
    
    // Set photo to profile
    //
    @IBAction func clickTakeNewPhoto(_ sender: AnyObject) {
        //subTitleLabel.text = "Während du wartest kannst du gleich dein Profilbild einstellen!".localized
        
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.allowsEditing = false
        imagePicker.navigationBar.isTranslucent = false
        imagePicker.navigationBar.barTintColor = Designs.Colors.lightBlue
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle:UIAlertControllerStyle.actionSheet)
        
        alertController.addAction(UIAlertAction(title: "Foto auswählen".localized, style: UIAlertActionStyle.default) { action -> Void in
            imagePicker.sourceType = .photoLibrary
            
            self.present(imagePicker, animated: true, completion: nil)
            })
        
        alertController.addAction(UIAlertAction(title: "Foto aufnehmen".localized, style: UIAlertActionStyle.default) { action -> Void in
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                if UIImagePickerController.availableCaptureModes(for: .rear) != nil {
                    imagePicker.sourceType = .camera
                    
                    self.present(imagePicker, animated: true, completion: nil)
                }
            }
            })
        
        alertController.addAction(UIAlertAction(title: "Abbrechen".localized, style: UIAlertActionStyle.cancel) { action -> Void in
            // Cancel

            })
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    // Show bottom "next" button
    func showNextButton() -> Void {
        let deltaHeight: CGFloat = 45
        
        UIView
            .animate(withDuration: 0.5, delay: 0, options: UIViewAnimationOptions(), animations: { [weak self] in
                guard let self_ = self else {return}
                self_.buttonBottomAnchor.constant += deltaHeight
                self_.view.layoutIfNeeded()
                }, completion: nil)
    }
    
    // MARK: Others
    
    func setupControllerTitle(_ title: String) -> Void {
        let attributes: NSDictionary = [
            NSFontAttributeName:Designs.FontVars.navigationBarTitleFont,
            NSForegroundColorAttributeName:Designs.FontVars.navigationBarTitleColor,
            NSKernAttributeName:Designs.FontVars.navigationBarTitleKern
        ]
        
        let attributedTitle = NSAttributedString(string: title, attributes: attributes as? [String : AnyObject])
        
        controllerTitleLabel.attributedText = attributedTitle
        controllerTitleLabel.sizeToFit()
    }
    
    // MARK: Networking
    
    func startUploadingImage() {
        // Get local image name
        //
        var localFileName:String?
        //        if let imageToUploadUrl = selectedImageUrl {
        //            let phResult = PHAsset.fetchAssetsWithALAssetURLs([imageToUploadUrl], options: nil)
        //            localFileName = phResult.firstObject?.filename
        //        } else {
        let date = Date().timeIntervalSince1970
        localFileName = "user_avatar-\(date).jpg"
        //        }
        
        if localFileName == nil {
            return
        }
        
        let remoteName = localFileName!
        
        // Progress view
        //
        let overlayView = MRProgressOverlayView.showOverlayAdded(to: self.view, title: "Bitte warten....".localized, mode: MRProgressOverlayViewMode.indeterminate, animated: true)
        
        S3Manager.uploadFileToS3(localFileName, generatedImageUrl: generateImageUrl(remoteName), contentType: "image/jpeg", progress: { (totalBytesSent, totalBytesExpectedToSend) in
            // Prepare for progress view
            //
            if overlayView?.mode != MRProgressOverlayViewMode.determinateCircular {
                overlayView?.mode = MRProgressOverlayViewMode.determinateCircular
                //overlayView?.titleLabelText = "Übertrage Daten...".localized
            }
            let progress = Float(totalBytesSent) / Float(totalBytesExpectedToSend)
            overlayView?.setProgress(progress, animated: true)
            
        }) { (success, s3URL) in
            
            // Assign user pic path to profile
            //
            RegistrationVC.user?.imageStringUrl = remoteName
            
            // Remove locally stored file
            //
            self.remoteImageWithUrl(remoteName)
            
            // Trigger registration endpoint
            //
            LoginController.register(RegistrationVC.user!) { (errors) in
                
                /* todo: move validation logic to RegistrationVC
                if errors.isContaining {
                    var message = ""
                    errors.forEach { message += "\($0),\n" }
                    message.removeLast()
                    message.removeLast()
                    message += "."
                    
                    self.present(UIAlertController("Bitte korrigieren:".localized, message,
                        actions: UIAlertAction(.OK)))
                    return
                } else if errors.isEmpty {
                    self.show(Storyboards.ViewControllers.profilePhoto, animated: true, modally: true)
                }
                */
            }

            
            if success {
                // Show button and success
                //
                DispatchQueue.main.async {
                    self.cancelButton.isHidden = true
                    self.cancelIcon.isHidden = true
                    MRProgressOverlayView.dismissOverlay(for: self.view, animated: true)
                    if self.buttonBottomAnchor.constant != 0 {
                        self.showNextButton()
                    }
                    
                    self.successImageView.alpha = 0
                    UIView.animate(withDuration: 1, delay: 0, options: UIViewAnimationOptions(), animations: { [weak self] in
                        guard let self_ = self else {return}
                        self_.successImageView.alpha = 1
                        self_.view.layoutIfNeeded()
                        }, completion: nil)
                }
            }
        }
    }
    
    func generateImageUrl(_ fileName: String) -> URL {
        let fileURL = URL(fileURLWithPath: NSTemporaryDirectory() + fileName)
        let data = UIImageJPEGRepresentation(profileImageView.image!, 0.6)
        try? data!.write(to: fileURL, options: [.atomic])
        
        return fileURL
    }
    
    func remoteImageWithUrl(_ fileName: String) {
        let fileURL = URL(fileURLWithPath: NSTemporaryDirectory() + fileName)
        do {
            try FileManager.default.removeItem(at: fileURL)
        } catch
        {
            OnDebugging.print(error)
        }
    }
    
    // MARK: Work with image
    
    func resizeImage(_ image: UIImage, newWidth: CGFloat) -> UIImage {
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        image.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    func fixImageOrientation(_ src:UIImage)->UIImage {
        if src.imageOrientation == UIImageOrientation.up {
            return src
        }
        
        var transform: CGAffineTransform = CGAffineTransform.identity
        
        switch src.imageOrientation {
        case UIImageOrientation.down, UIImageOrientation.downMirrored:
            transform = transform.translatedBy(x: src.size.width, y: src.size.height)
            transform = transform.rotated(by: CGFloat(M_PI))
            break
        case UIImageOrientation.left, UIImageOrientation.leftMirrored:
            transform = transform.translatedBy(x: src.size.width, y: 0)
            transform = transform.rotated(by: CGFloat(M_PI_2))
            break
        case UIImageOrientation.right, UIImageOrientation.rightMirrored:
            transform = transform.translatedBy(x: 0, y: src.size.height)
            transform = transform.rotated(by: CGFloat(-M_PI_2))
            break
        case UIImageOrientation.up, UIImageOrientation.upMirrored:
            break
        }
        
        switch src.imageOrientation {
        case UIImageOrientation.upMirrored, UIImageOrientation.downMirrored:
            transform.translatedBy(x: src.size.width, y: 0)
            transform.scaledBy(x: -1, y: 1)
            break
        case UIImageOrientation.leftMirrored, UIImageOrientation.rightMirrored:
            transform.translatedBy(x: src.size.height, y: 0)
            transform.scaledBy(x: -1, y: 1)
        case UIImageOrientation.up, UIImageOrientation.down, UIImageOrientation.left, UIImageOrientation.right:
            break
        }
        
        let ctx:CGContext = CGContext(data: nil, width: Int(src.size.width), height: Int(src.size.height), bitsPerComponent: src.cgImage!.bitsPerComponent, bytesPerRow: 0, space: src.cgImage!.colorSpace!, bitmapInfo: CGImageAlphaInfo.premultipliedLast.rawValue)!
        
        ctx.concatenate(transform)
        
        switch src.imageOrientation {
        case UIImageOrientation.left, UIImageOrientation.leftMirrored, UIImageOrientation.right, UIImageOrientation.rightMirrored:
            ctx.draw(src.cgImage!, in: CGRect(x: 0, y: 0, width: src.size.height, height: src.size.width))
            break
        default:
            ctx.draw(src.cgImage!, in: CGRect(x: 0, y: 0, width: src.size.width, height: src.size.height))
            break
        }
        
        let cgimg:CGImage = ctx.makeImage()!
        let img:UIImage = UIImage(cgImage: cgimg)
        
        return img
    }
}

extension AddProfilePhotoVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if (info[UIImagePickerControllerReferenceURL] != nil) {
            selectedImageUrl = info[UIImagePickerControllerReferenceURL] as! URL
        }
        
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {

            profileImageView.isHidden = false
            profileImageView.contentMode = .scaleAspectFill
            
            // Scale to 200 and rotate if needed
            //
            // let scalePickture = resizeImage(pickedImage, newWidth: 200)

            let scalePicture = ImageTools.resizeImage(pickedImage, newValue: 180)
            profileImageView.image = scalePicture;

            //profileImageView.image = fixImageOrientation(scalePickture)
        }
        
        dismiss(animated: true, completion: { [weak self] _ in
            guard let self_ = self else {return}
            
            self_.titleLabel.text = "Profilbild wurde".localized
            self_.title2Label.text = "erfolgreich angelegt!".localized


            self_.startUploadingImage()
            })
    }
}
