//
//  Font.swift
//  come2coach
//
//  Created by Marc Ortlieb on 08.08.16.
//  Copyright © 2016 MaytecNet. All rights reserved.
//
// http://stackoverflow.com/questions/29862179/how-to-set-uibutton-font-via-appearance-proxy-in-ios-8


import UIKit

/* Drop the theme setup in app delegate
func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
    SystemFont.setup()
    
    // ...
    
    return true
}
*/

//class SystemFont {
//    
//    private static let defaultFont = "Montserrat-Regular.otf"
//    private static let defaultFontSize: CGFloat = 15.0
//    
//    
//    static func setup() {
//        
//        UILabel.appearance().font = UIFont(name: defaultFont, size: defaultFontSize)
//        UIButton.appearance().titleLabel?.font = UIFont(name: defaultFont, size: defaultFontSize)
//    }
//    
//    
//    
//}
//
//
//
//
//
//extension UIButton {
//    
//    dynamic var defaultFont: UIFont! {
//        get { return self.titleLabel?.font }
//        set { self.titleLabel?.font = newValue }
//    }
//}
//
//extension UILabel{
//    
//    /// use: UILabel.appearance().defaultFont = UIFont.systemFontOfSize(25)
//    
//    dynamic var defaultFont: UIFont? {
//        get { return self.font }
//        set { self.font = newValue }
//    }
//}