//
//  BuyCoinsVC.swift
//  come2coach
//
//  Created by Igor Prysyazhnyuk on 3/20/17.
//  Copyright © 2017 MaytecNet. All rights reserved.
//

import UIKit
import SVProgressHUD

class BuyCoinsVC: UIViewController {
    @IBOutlet weak var buyCoinsButton: CoinsCountButton!
    
    fileprivate var selectedProductIndex: Int?
    fileprivate var userCoinsCount = 0
    fileprivate var eventId = -1
    fileprivate var onClose : (() -> Void)?

    fileprivate lazy var coinsProducts: [CoinsProduct] = {
        return [CoinsProduct(coinsCount: 25, price: 24.99, appleProductId: "25coins"),
                CoinsProduct(coinsCount: 50, price: 49.99, appleProductId: "50coins"),
                CoinsProduct(coinsCount: 100, price: 99.99, appleProductId: "100coins"),
                CoinsProduct(coinsCount: 250, price: 249.99, appleProductId: "250coins"),
                CoinsProduct(coinsCount: 500, price: 499.99, appleProductId: "500coins"),
                CoinsProduct(coinsCount: 1000, price: 999.99, appleProductId: "1000coins")]
    }()
    
    typealias ErrorHandler = (String) -> ()
    private lazy var errorHandler: ErrorHandler = { error in
        SVProgressHUD.dismiss()
        UIAlertController.show(fromVC: self, error: error)
    }
    
    static func show(fromVC: UIViewController, userCoinsCount: Int, eventId: Int, completion: (() -> Void)?  ) {
        let storyboard = UIStoryboard(name: Constants.Storyboards.coachings)
        let vc: BuyCoinsVC = storyboard.instanciateVCByName()
        vc.userCoinsCount = userCoinsCount
        vc.eventId = eventId
        vc.onClose = completion
        fromVC.present(vc, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        buyCoinsButton.coins = userCoinsCount
    }
    
    private func handlePayment(transactionId: String, coinsCount: Int) {
        HTTPRequest.handlePayment(eventId: eventId, transactionId: transactionId, coinsCount: coinsCount, onSuccess: { coinsCount in
            SVProgressHUD.dismiss()
            // delegate: view was closed
            if let oc = self.onClose {
                oc()
            }
            self.dismissViewController()
            NotificationCenter.postCoinsBoughtNotification(coinsCount: coinsCount)
        }, onError: errorHandler)
    }
    
    @IBAction func closeTapped(_ sender: Any) {
        // delegate: view was closed
        if let oc = onClose {
            oc()
        }
        dismissViewController()
    }
    
    @IBAction func buyCoinsTapped(_ sender: Any) {
        guard let selectedProductIndex = selectedProductIndex else {
            UIAlertController.show(fromVC: self, error: "Bitte wähle die Anzahl der gewünschten Coins".localized)
            return
        }
        let coinsProduct = coinsProducts[selectedProductIndex]
        let productId = coinsProduct.appleProductId
        SVProgressHUD.show()
        IAPManager.instance.buyProduct(productId: productId, onSuccess: { transactionId in
            self.handlePayment(transactionId: transactionId, coinsCount: coinsProduct.coinsCount)
//            NotificationCenter.postCoinsBoughtNotification(coinsCount: self.userCoinsCount + coinsProduct.coinsCount)
//            SVProgressHUD.dismiss()
//            self.dismissViewController()
        }, onError: errorHandler,
           onCancel: {
            SVProgressHUD.dismiss()
        })
    }
}

struct CoinsProduct {
    var coinsCount: Int
    var price: Float
    var appleProductId: String
}

extension BuyCoinsVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: CoinsPriceCell = tableView.cell(for: indexPath)
        cell.setData(coinsProduct: coinsProducts[indexPath.row], selected: indexPath.row == selectedProductIndex)
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return coinsProducts.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedProductIndex = indexPath.row
        tableView.reloadData()
    }
}
