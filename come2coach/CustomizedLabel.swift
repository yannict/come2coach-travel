//
//  CustomizedLabel.swift
//  come2coach
//
//  Created by Bernd Freier on 12.12.16.
//  Copyright © 2016 MaytecNet. All rights reserved.
//

import Foundation

open class C2CLabel : UILabel {
    
    func letterSpace(_ text : String?){

        if let t = text {
                //let strA = NSMutableAttributedString(string: t)
                //strA.addAttribute(NSKernAttributeName, value: CGFloat(1.4), range: NSRange(location: 0, length: t.characters.count))
                //self.attributedText = strA;

                let attributedString = NSMutableAttributedString(string: t)
                attributedString.addAttribute(NSKernAttributeName, value: CGFloat(1.4), range: NSRange(location: 0, length: attributedString.length))
                self.attributedText = attributedString

        }
    }
    
    override open func awakeFromNib() {
        letterSpace(self.text)

    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    override open var text: String? {
        didSet {
            letterSpace(text);
        }
    }
}
