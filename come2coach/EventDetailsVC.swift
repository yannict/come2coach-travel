//
//  EventDetailsVC.swift
//  come2coach
//
//  Created by Victor on 02.11.16.
//  Copyright © 2016 MaytecNet. All rights reserved.
//

import Foundation
import Player
import SVProgressHUD

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func <<T:Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l < r
    case (nil, _?):
        return true
    default:
        return false
    }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func ><T:Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l > r
    default:
        return rhs < lhs
    }
}


let AppointmentCellIdentifier = "AppointmentCell"

class EventDetailsVC: UIViewController {

    var event: Event!

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var ownerNameLabel: UILabel!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var ownerPhoto: UIImageView!
    @IBOutlet weak var eventImageView: UIImageView!
    @IBOutlet weak var eventImage2View: UIImageView!
    @IBOutlet weak var eventMediaView: UIView!
    @IBOutlet weak var eventMediaHeightConstraint: NSLayoutConstraint!

    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var bookTimeCountLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var descriptionSection: UIView!
    @IBOutlet weak var appointmentsButton: UIButton!
    @IBOutlet weak var bookButton: CoinsCountButton!
    @IBOutlet weak var appointmentsWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var image2HeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var similarSectionHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var similarEventsCollectionView: UICollectionView!
    @IBOutlet weak var similarSection: UIView!

    @IBOutlet weak var extButtonView: UIView!

    @IBOutlet weak var buttonSendMail: UIButton!

    @IBOutlet weak var buttonEditRepublish: UIButton!


    /*
    @IBOutlet weak var firstStar: UIImageView!
    @IBOutlet weak var secondStar: UIImageView!
    @IBOutlet weak var thirdStar: UIImageView!
    @IBOutlet weak var fourthStar: UIImageView!
    @IBOutlet weak var fifthStar: UIImageView!
    */

    @IBOutlet weak var btnPlayRecordedStream: UIImageView!

    @IBOutlet weak var viewTitleLabel: C2CLabel!

    @IBOutlet weak var labelCoachName: UILabel!
    @IBOutlet weak var labelParticipants: UILabel!

    @IBOutlet weak var progressParticipants: UIProgressView!

    @IBOutlet weak var labelParticipants2: UILabel!
    @IBOutlet weak var labelRating: UILabel!

    var appointmentsTable: UITableView!

    var appointments: [EventTimeStamp]? = [EventTimeStamp]()
    var selectedAppointment = 0

    var player: Player?
    var playerFullScreen = false

    var mode = "normal"
    var isBooked = false

    var userCoinsCount: Int = 0
    var isCoinsEnough: Bool {
        return userCoinsCount >= Int(event.price)
    }
    
    //MARK: Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        bookButton.isHidden = true

        isBooked = false

        getEventAction()

        setupRatingInteraction()

        let tapRec = UITapGestureRecognizer()

        tapRec.addTarget(self, action: "tappedOwnerImage")
        ownerPhoto.addGestureRecognizer(tapRec)


        btnPlayRecordedStream.isHidden = true

        /*
        // check if event is prerecorded - every expired event has a prerecorded stream
        let stats = LiveStreamingHandler.getEventStatus(event)


        if (!stats.running && !stats.pending) {

            // if so configure and show play button

            let btnPlayRecordedStreamRec = UITapGestureRecognizer()
            btnPlayRecordedStreamRec.addTarget(self, action: "btnPlayRecordedStreamPressed")
            btnPlayRecordedStream.addGestureRecognizer(btnPlayRecordedStreamRec)

            btnPlayRecordedStream.isHidden = false
        } else {

        }
        */

        progressParticipants.trackTintColor = UIColor(netHex: 0xD2D2D2)

        registerLocalNotifications()
    }

    private func registerLocalNotifications() {
        NotificationCenter.register(observer: self, localNotificationType: .coinsBought, selector: #selector(coinsBought))
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func coinsBought(notification: Notification) {
        guard let coinsCount = notification.coinsCount else { return }
        userCoinsCount = coinsCount
        switchMode(self.mode)
        NotificationCenter.postNotification(localNotificationType: .updateBookingView)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        MainTabBarController.sharedInstance?.tabBar.isHidden = true

        //switchMode(mode)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        //Storyboards.ViewControllers.mainTabBar.tabBar.hidden = false
        MainTabBarController.sharedInstance?.tabBar.isHidden = false


        /*
        if let p = player {
            p.removeFromParentViewController()
        }
        */
    }

    func switchMode(_ mode: String) {

        bookButton.isHidden = true

        if (mode == "archive") {
            bookButton.isHidden = true
            extButtonView.isHidden = false
            buttonEditRepublish.setTitle("Neu veröffentlichen".localized, for: .normal)

            // play button
            btnPlayRecordedStream.isHidden = false
            let btnPlayRecordedStreamRec = UITapGestureRecognizer()
            btnPlayRecordedStreamRec.addTarget(self, action: "btnPlayRecordedStreamPressed")
            btnPlayRecordedStream.addGestureRecognizer(btnPlayRecordedStreamRec)

        } else if (mode == "admin") {
            bookButton.isHidden = true
            extButtonView.isHidden = false
            buttonEditRepublish.setTitle("Coaching editieren".localized, for: .normal)
        } else if (mode == "joined") {
            extButtonView.isHidden = true
            self.bookButton.setTitle("Jetzt bewerten".localized, for: .normal)
            bookButton.isHidden = false

            if (event.live == 0) {
                let btnPlayRecordedStreamRec = UITapGestureRecognizer()
                btnPlayRecordedStreamRec.addTarget(self, action: "btnPlayRecordedStreamPressed")
                self.btnPlayRecordedStream.addGestureRecognizer(btnPlayRecordedStreamRec)

                self.btnPlayRecordedStream.isHidden = false
            }

        } else {
            extButtonView.isHidden = true

            if let u = User.current {
                if event.coachId == u.id {

                    isBooked = true
                    self.bookButton.setTitle("Coaching beitreten".localized, for: .normal)

                    self.bookButton.isHidden = false

                    return
                } else {

                    HTTPRequest.isBooked(event.id) { (success, json) in

                        if (success) {

                            debugPrint(json)

                            // if event was booked, change book button
                            if (json["begin"].boolValue == true ||
                                    json["begin1"].boolValue == true ||
                                    json["begin2"].boolValue == true ||
                                    json["begin3"].boolValue == true
                               ) {

                                self.isBooked = true

                                self.bookButton.isHidden = false
                                // show booked button
                                self.bookButton.setTitle("Coaching beitreten".localized, for: .normal)

                                self.appointmentsWidthConstraint.constant = 0

                                // show play button in case of video streaming
                                if (self.event.live == 0) {

                                    let btnPlayRecordedStreamRec = UITapGestureRecognizer()
                                    btnPlayRecordedStreamRec.addTarget(self, action: "btnPlayRecordedStreamPressed")
                                    self.btnPlayRecordedStream.addGestureRecognizer(btnPlayRecordedStreamRec)

                                    self.btnPlayRecordedStream.isHidden = false
                                }

                            } else {

                                self.isBooked = false

                                //todo: #payment
                                self.updateBookButton()
                                
//                                self.bookButton.setTitle("In Kürze verfügbar".localized, for: .normal)

                                // show appointments if applicable
                                if self.appointments?.count > 1 {
                                    self.appointmentsWidthConstraint.constant = Constants.Sizes.screenWidth / 2
                                    self.view.layoutSubviews()
                                }

                                self.bookButton.isHidden = false
                            }

                        }

                    }

                }
            }

        }

    }


    //MARK: Actions

    func tappedOwnerImage() {
        let coachVC = Storyboards.ViewControllers.coachDetails
        coachVC.event = event

        self.navigationController?.push(coachVC, animated: true, completion: nil)
    }

    func btnPlayRecordedStreamPressed() {

        let ls = LiveStreamingHandler()
        ls.play(event.id, controller: self)

    }

    @IBAction func back() {
        navigationController?.popViewController(animated: true)
    }

    @IBAction func likeEvent() {
        if event.isLiked == false {
            HTTPRequest.likeEvent(with: self.event.id) { (success) in
                if success == true {
                    self.event.isLiked = true
                    self.likeButton.setImage(UIImage.init(named: "icon_like"), for: UIControlState())
                }
            }
        } else {
            HTTPRequest.unlikeEvent(with: self.event.id) { (success) in
                if success == true {
                    self.event.isLiked = false
                    self.likeButton.setImage(UIImage.init(named: "icon_like_white"), for: UIControlState())
                }
            }
        }
    }

    @IBAction func shareEvent() {
        
        
        //let rootVC = UIApplication.shared.delegate?.window!!.rootViewController
        let popup = Bundle.main.loadNibNamed("SharePopup", owner: nil, options: nil)![0] as! SharePopup
        popup.delegate = self
        popup.frame = CGRect.init(origin: CGPoint.zero, size: CGSize(width: Constants.Sizes.screenWidth, height: Constants.Sizes.screenHeight))
        self.view.addSubview(popup)
        popup.showPopover()

    }

    fileprivate func setEventInfo() {
        appointments?.removeAll()
        if event.begin.dateString != "" {
            appointments?.append(event.begin)
        }
        if event.begin2.used && event.begin2.dateString != "" {
            appointments?.append(event.begin2)
        }
        if event.begin3.used && event.begin3.dateString != "" {
            appointments?.append(event.begin3)
        }
        bookTimeCountLabel.text = String(format: "%i", appointments!.count)
        bookTimeCountLabel.isHidden = false

        /* see switchMode
        if appointments?.count > 1 {
            appointmentsWidthConstraint.constant = Constants.Sizes.screenWidth / 2
        }
        */

        titleLabel.text = event.title
        if event.img1PathExtension != "" {
            eventImageView.sd_setImage(with: event.imageURL as URL!)
        }

        if event.ownerImage != "" {
            ownerPhoto.sd_setImage(with: event.ownerImageURL as URL!)
        } else if event.userAvatar != "" {
            ownerPhoto.sd_setImage(with: URL.init(string: event.userAvatar))
        }

        if event.isLiked == false {
            likeButton.setImage(UIImage.init(named: "icon_like_white"), for: UIControlState())
        } else {
            likeButton.setImage(UIImage.init(named: "icon_like"), for: UIControlState())
        }
        //setRating()
        if event.img2PathExtension != "" {
            //image2HeightConstraint.constant = 215.0

            eventMediaHeightConstraint.constant = 215.0

            if (event.img2PathExtension.hasSuffix(".mov")) {

                eventImage2View.isHidden = true

                // get in-content player and play
                if let p = player {
                    p.stop()
                }

                player = getInContentPlayer(event.image2URL!)
                player?.playFromBeginning()


            } else {
                eventImage2View.isHidden = false

                eventImage2View.sd_setImage(with: event.image2URL as URL!)
            }
        }
        if event.otherEvents.count == 0 {
            similarSectionHeightConstraint.constant = 0
        } else {
            similarSectionHeightConstraint.constant = 288
            similarEventsCollectionView.reloadData()
            let gradient: CAGradientLayer = CAGradientLayer()
            let startColor = UIColor.init(white: 0, alpha: 0.0)
            let endColor = UIColor.init(white: 0, alpha: 0.1)
            gradient.colors = [startColor.cgColor, endColor.cgColor]
            gradient.locations = [0.0, 1.0]
            gradient.startPoint = CGPoint(x: 0.0, y: 0.9)
            gradient.endPoint = CGPoint(x: 0.0, y: 1.0)
            gradient.frame = CGRect(x: 0.0, y: 0.0, width: self.similarSection.frame.size.width, height: self.similarSection.frame.size.height)

            self.similarSection.layer.insertSublayer(gradient, at: 0)
        }
        ownerNameLabel.text = event.owner
        durationLabel.text = (event.durationArrString as String) + " h"
        dateLabel.text = event.begin.shortDateString
        timeLabel.text = event.begin.shortTimeString
        priceLabel.text = String(event.price) + " Coins".localized
        descriptionLabel.text = event.descriptionText
        let size = descriptionLabel.sizeThatFits(CGSize(width: Constants.Sizes.screenWidth - 80, height: CGFloat.greatestFiniteMagnitude))
        descriptionLabel.translatesAutoresizingMaskIntoConstraints = true
        descriptionLabel.frame = CGRect(x: 18, y: 30, width: Constants.Sizes.screenWidth - 80, height: size.height)
        descriptionSection.layoutSubviews()
        if appointments?.count > 1 {
            appointmentsButton.setTitle(appointments![selectedAppointment].shortDateString + " | " + appointments![selectedAppointment].shortTimeString, for: UIControlState())
        }

        labelCoachName.text = event.coachFirstName + " " + event.coachLastName

        viewTitleLabel.text = event.live == 1 ? "LIVESTREAMING" : "VIDEOSTREAMING"

        labelParticipants.text = String(event.joinedParticipants) + " von " + String(event.maxParticipants) + " Teilnehmer".localized

        progressParticipants.progress = Float(event.joinedParticipants) / Float(event.maxParticipants)

        if event.joinedParticipants >= event.minParticipants {

            progressParticipants.progressImage = UIImage.init(named: "event-progress-ok")
            labelParticipants2.text = "Das Coaching findet statt!".localized

        } else {
            progressParticipants.progressImage = UIImage.init(named: "event-progress-fail")
            labelParticipants2.text = "Das Coaching findet nicht statt!".localized
        }

        labelRating.text = String(event.ownerRate) + " | 5 Sterne".localized
    }
    
    private func updateBookButton() {
        if (isCoinsEnough) {
            bookButton.isCoinsEnoughTitle = "Jetzt buchen".localized
            bookButton.setBlueBackground()
        } else {
            bookButton.isCoinsEnoughTitle = "Coins kaufen".localized
            bookButton.setRedBackground()
        }
        bookButton.coins = userCoinsCount
    }
    
    internal func getInContentPlayer(_ url: URL) -> Player {


        /*
        if let p = player {
            p.removeFromParentViewController()
        }
        */

        let p = Player()

        let playerTapped = UITapGestureRecognizer(target: self, action: #selector(EventDetailsVC.playerTapped))
        playerTapped.numberOfTapsRequired = 1

        let rect = CGRect(x: eventMediaView.bounds.origin.x,
                y: eventMediaView.bounds.origin.y,
                width: eventMediaView.bounds.size.width, height: 215.0)

        p.view.frame = rect
        p.view.addGestureRecognizer(playerTapped)

        self.addChildViewController(p)
        eventMediaView.addSubview(p.view)

        p.didMove(toParentViewController: self)
        p.fillMode = "AVLayerVideoGravityResizeAspectFill"
        //p.setUrl(url)
        p.url = url
        p.muted = false
        p.playbackResumesWhenEnteringForeground = false

        //debugPrint(" Volume : " + String(p.muted))
        return p;
    }

    internal func playerTapped() {
        if let p = player {
            if (p.playbackState == .paused) {
                p.playFromCurrentTime()
            } else if (p.playbackState == .stopped) {
                p.playFromBeginning()
            } else if (p.playbackState == .playing) {
                p.pause()
            }
        }
    }

    fileprivate func setupRatingInteraction() {
        /*
        let ratingone = UITapGestureRecognizer(target: self, action: #selector(EventDetailsVC.ratingone))
        ratingone.numberOfTapsRequired = 1

        let ratingtwo = UITapGestureRecognizer(target: self, action: #selector(EventDetailsVC.ratingtwo))
        ratingtwo.numberOfTapsRequired = 1

        let ratingthree = UITapGestureRecognizer(target: self, action: #selector(EventDetailsVC.ratingthree))
        ratingthree.numberOfTapsRequired = 1

        let ratingfour = UITapGestureRecognizer(target: self, action: #selector(EventDetailsVC.ratingfour))
        ratingfour.numberOfTapsRequired = 1

        let ratingfive = UITapGestureRecognizer(target: self, action: #selector(EventDetailsVC.ratingfive))
        ratingfive.numberOfTapsRequired = 1


        firstStar.isUserInteractionEnabled = true
        firstStar.addGestureRecognizer(ratingone)

        secondStar.isUserInteractionEnabled = true
        secondStar.addGestureRecognizer(ratingtwo)

        thirdStar.isUserInteractionEnabled = true
        thirdStar.addGestureRecognizer(ratingthree)

        fourthStar.isUserInteractionEnabled = true
        fourthStar.addGestureRecognizer(ratingfour)

        fifthStar.isUserInteractionEnabled = true
        fifthStar.addGestureRecognizer(ratingfive)
        */

    }

    func rateThisEvent(_ rateValue: Int) {

        HTTPRequest.rateEvent(with: event.id, rate: rateValue) { (success) in

            if (!success) {
                self.present(UIAlertController("Ohh, das tut uns leid.".localized, "Dein Rating konnte soeben nicht " +
                        "gespeichert werden. Versuchs einfach nochmal, Danke!".localized,
                        actions: UIAlertAction(.OK)), animated: true)
            }
        }

    }

    /*
    func ratingone() {
        firstStar.image = UIImage.init(named: "icon_star_filled")
        secondStar.image = UIImage.init(named: "icon_star_empty")
        thirdStar.image = UIImage.init(named: "icon_star_empty")
        fourthStar.image = UIImage.init(named: "icon_star_empty")
        fifthStar.image = UIImage.init(named: "icon_star_empty")

        rateThisEvent(1)

    }

    func ratingtwo() {
        firstStar.image = UIImage.init(named: "icon_star_filled")
        secondStar.image = UIImage.init(named: "icon_star_filled")
        thirdStar.image = UIImage.init(named: "icon_star_empty")
        fourthStar.image = UIImage.init(named: "icon_star_empty")
        fifthStar.image = UIImage.init(named: "icon_star_empty")

        rateThisEvent(2)
    }

    func ratingthree() {
        firstStar.image = UIImage.init(named: "icon_star_filled")
        secondStar.image = UIImage.init(named: "icon_star_filled")
        thirdStar.image = UIImage.init(named: "icon_star_filled")
        fourthStar.image = UIImage.init(named: "icon_star_empty")
        fifthStar.image = UIImage.init(named: "icon_star_empty")

        rateThisEvent(3)
    }

    func ratingfour() {
        firstStar.image = UIImage.init(named: "icon_star_filled")
        secondStar.image = UIImage.init(named: "icon_star_filled")
        thirdStar.image = UIImage.init(named: "icon_star_filled")
        fourthStar.image = UIImage.init(named: "icon_star_filled")
        fifthStar.image = UIImage.init(named: "icon_star_empty")

        rateThisEvent(4)
    }

    func ratingfive() {
        firstStar.image = UIImage.init(named: "icon_star_filled")
        secondStar.image = UIImage.init(named: "icon_star_filled")
        thirdStar.image = UIImage.init(named: "icon_star_filled")
        fourthStar.image = UIImage.init(named: "icon_star_filled")
        fifthStar.image = UIImage.init(named: "icon_star_filled")

        rateThisEvent(5)
    }
    */

    /*
    fileprivate func setRating() {
        let rating = Int(event.ownerRate)  // ownerRate is mapped to rating_points
        if rating > 0 {
            firstStar.image = UIImage.init(named: "icon_star_filled")
        }
        if rating > 1 {
            secondStar.image = UIImage.init(named: "icon_star_filled")
        }
        if rating > 2 {
            thirdStar.image = UIImage.init(named: "icon_star_filled")
        }
        if rating > 3 {
            fourthStar.image = UIImage.init(named: "icon_star_filled")
        }
        if rating > 4 {
            fifthStar.image = UIImage.init(named: "icon_star_filled")
        }
    }
    */

    @IBAction func showAppointmentsDropdown() {
        if let _ = appointmentsTable {
            appointmentsTable.removeFromSuperview()
            appointmentsTable = nil
        } else {
            let tableHeight = 44 * (appointments?.count)!
            appointmentsTable = UITableView.init(frame: CGRect.init(x: 0, y: bookButton.frame.origin.y - CGFloat(tableHeight), width: bookButton.bounds.size.width, height: CGFloat(tableHeight)))
            appointmentsTable.dataSource = self
            appointmentsTable.delegate = self
            appointmentsTable.register(UINib(nibName: "AppointmentCell", bundle: nil), forCellReuseIdentifier: AppointmentCellIdentifier)
            appointmentsTable.backgroundColor = UIColor.white
            self.view.addSubview(appointmentsTable)
        }
    }

    @IBAction func bookCoaching() {

        if (mode == "joined") {

            // Rate Coaching
            if (event.live == 1) {

                if (event.begin.date <= Date()) {

                    let coachRating = Storyboards.ViewControllers.coachRatings
                    coachRating.event = event
                    self.navigationController?.push(coachRating, animated: true, completion: nil)

                } else {

                    self.present(UIAlertController("Bewertung".localized, "Du kannst ein Coaching erst bewerten, wenn es begonnen hat.".localized,
                            actions: UIAlertAction(.OK)), animated: true)
                }

            } else {

                let coachRating = Storyboards.ViewControllers.coachRatings
                coachRating.event = event
                self.navigationController?.push(coachRating, animated: true, completion: nil)
            }

        } else {

            if (!isBooked) {
                if isCoinsEnough {
                    //let rootVC = UIApplication.shared.delegate?.window!!.rootViewController
                    let popup = Bundle.main.loadNibNamed("BookingPopup", owner: nil, options: nil)![0] as! BookingPopup
                    popup.event = event
                    popup.delegate = self
                    popup.frame = CGRect.init(origin: CGPoint.zero, size: CGSize(width: Constants.Sizes.screenWidth, height: Constants.Sizes.screenHeight))
                    //rootVC!.view.addSubview(popup)
                    self.view.addSubview(popup)
                    popup.showPopover()
                    
                    //todo: reactivate
                    // joinEvent(event, participants: 1)
                } else { showBuyCoinsVC() }
            } else {
                // start streaming
                let ls = LiveStreamingHandler()
                ls.launch(event.id, controller: self)
            }
        }
    }

    func showBuyCoinsVC() {
        BuyCoinsVC.show(fromVC: self, userCoinsCount: userCoinsCount, eventId: event.id, completion: nil)
    }
    
    @IBAction func btnRepublishPressed(_ sender: Any) {

        if (mode == "archive") {

            //let rootVC = UIApplication.shared.delegate?.window!!.rootViewController
            let popup = Bundle.main.loadNibNamed("PublishPopover", owner: nil, options: nil)![0] as! PublisherPopover
            popup.delegate = self
            popup.frame = CGRect.init(origin: CGPoint.zero, size: CGSize(width: Constants.Sizes.screenWidth, height: Constants.Sizes.screenHeight))
            self.view.addSubview(popup)
            popup.showPopover()

        } else if (mode == "admin") {

            //todo: edit
            CreateManager.sharedInstance.closeEvent = {

                CreateManager.sharedInstance.closeEvent = nil
                self.getEventAction()
            }

            CreateManager.sharedInstance.resetData()
            CreateManager.sharedInstance.sender = self
            CreateManager.sharedInstance.isRepublishing = true
            CreateManager.sharedInstance.mode = "edit"
            CreateManager.sharedInstance.eventId = event.id
            CreateManager.sharedInstance.recordedFile = event.recordedVideoFile
            CreateManager.sharedInstance.isOnline = event.live == 1
            CreateManager.sharedInstance.titleText = event.title
            CreateManager.sharedInstance.description = event.descriptionText
            CreateManager.sharedInstance.minParticipant = event.minParticipants
            CreateManager.sharedInstance.maxParticipant = event.maxParticipants
            CreateManager.sharedInstance.price = Int(event.price)
            CreateManager.sharedInstance.ageRestriction = event.adult
            CreateManager.sharedInstance.duration = event.durationArrString as String
            CreateManager.sharedInstance.begin = event.begin.date
            if event.begin2.used && event.begin2.dateString != "" {
                CreateManager.sharedInstance.begin1 = event.begin2.date
            }
            if event.begin3.used && event.begin3.dateString != "" {
                CreateManager.sharedInstance.begin2 = event.begin3.date
            }
            CreateManager.sharedInstance.affiliateDescr = event.ext_affiliate_desc
            CreateManager.sharedInstance.affiliateURL = event.ext_affiliate_url
            CreateManager.sharedInstance.handoutURL = event.handout_url
            CreateManager.sharedInstance.handoutDescr = event.handout_desc

            CreateManager.sharedInstance.existingImg1 = event.img1PathExtension
            CreateManager.sharedInstance.existingImg1Folder = event.img1RemoteFolder

            if event.img2PathExtension != "" {
                CreateManager.sharedInstance.existingImg2 = event.img2PathExtension
                CreateManager.sharedInstance.existingImg2Folder = event.img2RemoteFolder
                CreateManager.sharedInstance.existingVideo = event.video
            }

            let coachingVC = Storyboards.ViewControllers.createCoachingNavCtrl
            coachingVC.navigationBar.isHidden = true
            present(coachingVC, animated: true)

        }

    }

    @IBAction func btnSendMailPressed(_ sender: Any) {

        /*
        let emailNC = UINavigationController(Storyboards.ViewControllers.emailVC)
        let emailVC = emailNC.viewControllers.first as! EmailViewController
        emailVC.event = event
        emailNC.navigationBar.isHidden = true
        self.present(emailNC, animated: true, completion: nil)
        */


        let emailNC = UINavigationController(Storyboards.ViewControllers.coachingDescription)
        emailNC.navigationBar.isHidden = true
        let vc = emailNC.viewControllers.first as! CoachingDescriptionVC
        vc.placeholder = "Gib hier bitte Deinen Text ein ...".localized
        vc.titleText = "MAIL AN TEILNEHMER"
        vc.descrValue = ""
        vc.completion = { (txt) in

            if (!txt.isEmpty) {
                SVProgressHUD.show()
                HTTPRequest.mailEvent(with: self.event, text: txt) { (success) in
                    SVProgressHUD.dismiss()
                }
            }

            emailNC.dismissViewController(true, completion: nil)
        }
        self.present(emailNC, animated: true, completion: nil)
        //self.show(emailNC,animated: true)


    }


    fileprivate func getEventAction() {
        // The network connection was lost.
        let eventID = String(event.id)
        getEvent(eventID)
    }

    fileprivate func getEvent(_ eventID: String) {

        HTTPRequest.fetchEvent(with: self.event.id) { (event) in
            self.event = event
            self.getUserCoinsCount()
        }
    }

    private func getUserCoinsCount() {
        HTTPRequest.getUserCoinsCount(onSuccess: { (userCoinsCount) in
            self.userCoinsCount = userCoinsCount
            self.setEventInfo()
            self.switchMode(self.mode)
        }) { (error) in
            UIAlertController.show(fromVC: self, error: error)
        }
    }
}

extension EventDetailsVC: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let _ = appointments {
            return appointments!.count
        } else {
            return 0
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: AppointmentCellIdentifier, for: indexPath) as! AppointmentCell
        cell.timeLabel.text = appointments![indexPath.row].shortDateString + " | " + appointments![indexPath.row].shortTimeString
        cell.selectionStyle = .none
        return cell
    }

}

extension EventDetailsVC: UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedAppointment = indexPath.row
        appointmentsButton.setTitle(appointments![selectedAppointment].shortDateString + " | " + appointments![selectedAppointment].shortTimeString, for: UIControlState())
        appointmentsTable.removeFromSuperview()
        appointmentsTable = nil
    }

}

//MARK: UICollectionViewDataSource

extension EventDetailsVC: UICollectionViewDataSource {

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return event.otherEvents.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.ViewControllers.eventCellDefault, for: indexPath) as! DefaultEventCell
        cell.cleanInfo()
        print(indexPath.section * 2 + indexPath.item)
        cell.event = event.otherEvents[indexPath.item]
        cell.delegate = self
        cell.fillEventInfoExt(event.otherEvents[indexPath.item], event.ownerImageURL)
        return cell
    }

}

//MARK: UICollectionViewDelegateFlowLayout

extension EventDetailsVC: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: (Constants.Sizes.screenWidth - 42) / 2, height: Constants.Sizes.defaultCollectionCellHeight)
    }
}

//MARK: DefaultEventCellDelegate

extension EventDetailsVC: DefaultEventCellDelegate {

    func defaultEventCell(_ cell: DefaultEventCell, didLikeEvent like: Bool, eventWithId eventId: Int) {

        if like == true {
            HTTPRequest.likeEvent(with: eventId) { (success) in
                if success == true {
                    for event in self.event.otherEvents {
                        if event.id == eventId {
                            event.isLiked = like
                            event.isWatching = like
                        }
                    }
                    self.similarEventsCollectionView.reloadData()
                }
            }
        } else {
            HTTPRequest.unlikeEvent(with: eventId) { (success) in
                if success == true {
                    for event in self.event.otherEvents {
                        if event.id == eventId {
                            event.isLiked = like
                            event.isWatching = like
                        }
                    }
                    self.similarEventsCollectionView.reloadData()
                }
            }
        }
    }
}

//MARK: UICollectionViewDelegate

extension EventDetailsVC: UICollectionViewDelegate {

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let detailsVC = Storyboards.ViewControllers.eventDetails
        detailsVC.mode = mode
        detailsVC.event = event.otherEvents[indexPath.item]
        self.navigationController?.push(detailsVC, animated: true, completion: nil)
    }

}

//MARK: ParticipantsEmailsDelegate

extension EventDetailsVC: ParticipantsEmailsVCDelegate {
    func participantsEmailsFilled(emails: [String]) {
        joinEventRequest(participantsEmails: emails)
    }
}

//MARK: BookingPopupDelegate

extension EventDetailsVC: BookingPopupDelegate {

    fileprivate func joinEventRequest(participantsEmails: [String] = []) {
        SVProgressHUD.show()
        HTTPRequest.joinEvent(eventId: event.id, participantsEmails: participantsEmails, onSuccess: {
            SVProgressHUD.dismiss()
            self.getEventAction()
        }) { (error) in
            SVProgressHUD.dismiss()
            UIAlertController.show(fromVC: self, error: error)
        }
    }
    
    func joinEvent(_ event: Event, participants: Int) {
        if participants > 1 {
            ParticipantsEmailsVC.show(participantsCount: participants - 1, coinsCount: userCoinsCount, fromVC: self, delegate: self)
        } else { joinEventRequest() }
    }
}


extension EventDetailsVC: SharePopupDelegate {

    func shareEventAction() {

        SVProgressHUD.show()

        HTTPRequest.shareLink(event.id) { (success, json) in

            SVProgressHUD.dismiss()

            if (success) {

                var description = self.event.descriptionText
                if description.characters.count > 255 {
                    description = description.substring(to: description.characters.index(description.startIndex, offsetBy: 255))
                }
                let shareURL = json["url"].stringValue

                let shareItems: [AnyObject] = [self.event.title as AnyObject, description as AnyObject, shareURL as AnyObject]

                let shareController = UIActivityViewController.init(activityItems: shareItems, applicationActivities: nil)
                shareController.popoverPresentationController?.sourceView = self.view
                self.present(shareController, animated: true, completion: nil)

            }

        }

    }

}


extension EventDetailsVC: PublisherPopoverDelegate {

    func republish(_ type: Int) {

        CreateManager.sharedInstance.resetData()
        CreateManager.sharedInstance.isRepublishing = true
        CreateManager.sharedInstance.mode = "repub"
        CreateManager.sharedInstance.eventId = event.id
        CreateManager.sharedInstance.recordedFile = event.recordedVideoFile
        CreateManager.sharedInstance.isOnline = type == 0
        CreateManager.sharedInstance.titleText = event.title
        CreateManager.sharedInstance.description = event.descriptionText
        CreateManager.sharedInstance.minParticipant = event.minParticipants
        CreateManager.sharedInstance.maxParticipant = event.maxParticipants
        CreateManager.sharedInstance.price = Int(event.price)
        CreateManager.sharedInstance.ageRestriction = event.adult
        CreateManager.sharedInstance.duration = event.durationArrString as String
        CreateManager.sharedInstance.begin = event.begin.date
        if event.begin2.used && event.begin2.dateString != "" {
            CreateManager.sharedInstance.begin1 = event.begin2.date
        }
        if event.begin3.used && event.begin3.dateString != "" {
            CreateManager.sharedInstance.begin2 = event.begin3.date
        }
        //CreateManager.sharedInstance.begin1 = event.begin2.date
        //CreateManager.sharedInstance.begin2 = event.begin3.date
        CreateManager.sharedInstance.affiliateDescr = event.ext_affiliate_desc
        CreateManager.sharedInstance.affiliateURL = event.ext_affiliate_url
        CreateManager.sharedInstance.handoutURL = event.handout_url
        CreateManager.sharedInstance.handoutDescr = event.handout_desc

        // S3
        //
        CreateManager.sharedInstance.existingImg1 = event.img1PathExtension
        CreateManager.sharedInstance.existingImg1Folder = event.img1RemoteFolder

        if event.img2PathExtension != "" {
            CreateManager.sharedInstance.existingImg2 = event.img2PathExtension
            CreateManager.sharedInstance.existingImg2Folder = event.img2RemoteFolder
            CreateManager.sharedInstance.existingVideo = event.video
        }


        let coachingVC = Storyboards.ViewControllers.createCoachingNavCtrl
        coachingVC.navigationBar.isHidden = true
        //self.show(coachingVC, animated: true)
        // self.navigationController?.push(coachingVC, animated: true, completion: nil)

        // self.show(coachingVC, animated: true)
        present(coachingVC, animated: true)

        /*

        let coachingVC = Storyboards.ViewControllers.coachingDetails
        coachingVC.mode = "edit"
        //let coachingVC = UINavigationController(coachingVC)
        UINavigationController(coachingVC).navigationBar.isHidden = true
        self.present(coachingVC, animated: true, completion: nil)
        */

    }

}
