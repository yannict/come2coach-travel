//
// Created by Bernd Freier on 23.01.17.
// Copyright (c) 2017 MaytecNet. All rights reserved.
//

import Foundation

class CoachDetailsVC: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!

    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!

    @IBOutlet weak var eventCountLabel: UILabel!

    @IBOutlet weak var followerLabel: UILabel!

    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var ownerPhoto: UIImageView!
    @IBOutlet weak var eventImageView: UIImageView!
    @IBOutlet weak var eventImage2View: UIImageView!
    @IBOutlet weak var eventMediaView: UIView!
    @IBOutlet weak var eventMediaHeightConstraint: NSLayoutConstraint!

    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var bookTimeCountLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var descriptionSection: UIView!
    @IBOutlet weak var appointmentsButton: UIButton!
    @IBOutlet weak var bookButton: UIButton!
    @IBOutlet weak var appointmentsWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var image2HeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var similarSectionHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var similarEventsCollectionView: UICollectionView!
    @IBOutlet weak var similarSection: UIView!


    var event: Event!
    var otherEvents: [Event] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        setEventInfo()

        HTTPRequest.getSpecificUser(event.coachId) { (success, j) in

            if (success) {

                if let json = j {
                    self.eventCountLabel.text = String(json["user"]["total_events"].intValue) + " Coachings".localized
                    self.followerLabel.text = String(json["user"]["total_followers"].intValue) + " Follower"
                    self.descriptionLabel.text = json["user"]["about"].stringValue

                    self.otherEvents =
                            json["events"]
                                    .arrayValue
                                    .map {
                                JSONDecoder.makeEvent($0)
                            }


                    if self.otherEvents.count == 0 {
                        self.similarSectionHeightConstraint.constant = 0
                    } else {
                        self.similarEventsCollectionView.dataSource = self
                        self.similarSectionHeightConstraint.constant = 288
                        self.similarEventsCollectionView.reloadData()
                        let gradient: CAGradientLayer = CAGradientLayer()
                        let startColor = UIColor.init(white: 0, alpha: 0.0)
                        let endColor = UIColor.init(white: 0, alpha: 0.1)
                        gradient.colors = [startColor.cgColor, endColor.cgColor]
                        gradient.locations = [0.0, 1.0]
                        gradient.startPoint = CGPoint(x: 0.0, y: 0.9)
                        gradient.endPoint = CGPoint(x: 0.0, y: 1.0)
                        gradient.frame = CGRect(x: 0.0, y: 0.0, width: self.similarSection.frame.size.width, height: self.similarSection.frame.size.height)

                        self.similarSection.layer.insertSublayer(gradient, at: 0)
                    }


                }


            }


        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        MainTabBarController.sharedInstance?.tabBar.isHidden = true
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        MainTabBarController.sharedInstance?.tabBar.isHidden = false

    }


    @IBAction func back() {
        navigationController?.popViewController(animated: true)
    }

    func setEventInfo() {

        titleLabel.text = event.coachFirstName
        subTitleLabel.text = event.coachLastName

        if event.ownerImage != "" {
            ownerPhoto.sd_setImage(with: event.ownerImageURL as URL!)
        }

        ratingLabel.text = String(event.ownerRate) + " | 5 Sterne".localized


    }


}


extension CoachDetailsVC: UICollectionViewDataSource {

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return otherEvents.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.ViewControllers.eventCellDefault, for: indexPath) as! DefaultEventCell
        cell.cleanInfo()
        print(indexPath.section * 2 + indexPath.item)
        cell.event = otherEvents[indexPath.item]
        cell.delegate = self
        cell.fillEventInfoExt(otherEvents[indexPath.item],  event.ownerImageURL)
        return cell
    }

}

//MARK: DefaultEventCellDelegate

extension CoachDetailsVC: DefaultEventCellDelegate {

    func defaultEventCell(_ cell: DefaultEventCell, didLikeEvent like: Bool, eventWithId eventId: Int) {

        if like == true {
            HTTPRequest.likeEvent(with: eventId) { (success) in
                if success == true {
                    for event in self.otherEvents {
                        if event.id == eventId {
                            event.isLiked = like
                            event.isWatching = like
                        }
                    }
                    self.similarEventsCollectionView.reloadData()
                }
            }
        } else {
            HTTPRequest.unlikeEvent(with: eventId) { (success) in
                if success == true {
                    for event in self.otherEvents {
                        if event.id == eventId {
                            event.isLiked = like
                            event.isWatching = like
                        }
                    }
                    self.similarEventsCollectionView.reloadData()
                }
            }
        }
    }
}
