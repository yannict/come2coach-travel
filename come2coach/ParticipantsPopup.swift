//
//  ParticipantsPopup.swift
//  come2coach
//
//  Created by Victor on 10.11.16.
//  Copyright © 2016 MaytecNet. All rights reserved.
//

import Foundation

protocol ParticipantsPopupDelegate {
    func updateEventParticipants(_ event: Event)
}

class ParticipantsPopup: UIView {
    
    let maxParticipantsCount = 99
    
    @IBOutlet weak var popoverView : UIView!
    @IBOutlet weak var popoverViewBottomConstraint : NSLayoutConstraint!
    @IBOutlet weak var minButton : UIButton!
    @IBOutlet weak var maxButton : UIButton!
    @IBOutlet weak var pickerView : UIView!
    @IBOutlet weak var countPickerView : UIPickerView!
    
    var delegate : ParticipantsPopupDelegate?
    
    var event : Event?
    var minSelect = true
    
    func showPopover() {
        self.layoutSubviews()
        
        let path = UIBezierPath.init(rect: self.bounds)
        let roundedPath = UIBezierPath.init(roundedRect: popoverView.frame, cornerRadius: 4)
        
        path.append(roundedPath)
        path.usesEvenOddFillRule = true
        
        let fillLayer = CAShapeLayer.init()
        fillLayer.path = path.cgPath
        fillLayer.fillRule = kCAFillRuleEvenOdd
        fillLayer.fillColor = UIColor.black.cgColor
        fillLayer.opacity = 0.5
        self.layer.insertSublayer(fillLayer, at: 0)
    }
    
    @IBAction func selectMinParticipants() {
        minSelect = true
        pickerView.isHidden = false
        countPickerView.reloadAllComponents()
        countPickerView.selectRow(event!.minParticipants - 1, inComponent: 0, animated: false)
    }
    
    @IBAction func selectMaxParticipants() {
        minSelect = false
        pickerView.isHidden = false
        countPickerView.reloadAllComponents()
        countPickerView.selectRow(event!.maxParticipants - event!.minParticipants, inComponent: 0, animated: false)
    }
    
    @IBAction func saveSelectedParticipants() {
        if minSelect == true {
            event!.minParticipants = countPickerView.selectedRow(inComponent: 0) + 1
        }
        else {event!.maxParticipants = countPickerView.selectedRow(inComponent: 0) + event!.minParticipants}
        pickerView.isHidden = true
    }
    
    @IBAction func cancel() {
        pickerView.isHidden = true
    }
    
    @IBAction func saveEvent() {
        self.removeFromSuperview()
        if let _ = delegate {
            delegate!.updateEventParticipants(event!)
        }
    }
    
    @IBAction func hidePopover() {
        self.removeFromSuperview()
    }
    
}

extension ParticipantsPopup : UIPickerViewDataSource, UIPickerViewDelegate {
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if minSelect == true {
            return event!.maxParticipants
        }
        else {return maxParticipantsCount - event!.minParticipants + 1}
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if minSelect {
            return String(row + 1)
        }
        else {return String(row + event!.minParticipants)}
    }
    
}
