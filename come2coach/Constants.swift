//
//  Constants.swift
//  come2coach
//
//  Created by Marc Ortlieb on 31.07.16.
//  Copyright © 2016 Marc Ortlieb. All rights reserved.
//

import Foundation

import R5Streaming

struct Constants {
    
    static let S3BucketName = "c2c-app" //"come2coach"
    static let myIdentityPoolId = "eu-central-1:c4ad72db-1150-4ff7-8a4c-114211de61d1" //"eu-west-1:9d763862-7b8e-41d5-98ca-c5fe1e44d484"
    
    struct API {
        
        /**
         Since the API should only be accessed through authorized endpoints, every request MUST contain a Token in the header. A token can be obtained from the head of development of this project - Alexander Fuchs (Skype: mi-alex.f). If the token is invalid or not provided, the server will respond with status code 406
         */
        static let accessToken = "DK12ANSG3928DDGGAAD32SF12DSFD"
        static let jwtSecret = "3gER8BJLPONDmrYM9BLqflSzJrwntUgA"
        static let apiServer = "api.come2coach.de"
        //TODO: read IP from apiServer name
        static let liveRecordingIP = "35.157.59.253" // "5.35.253.148" // "52.59.224.105" // "5.35.253.148"  // "5.35.253.148"
        static let liveRecordingServer = "come2coach.de"
        static let liveRecordingPort:Int32 = 8554
        static let liveRecordingAppName = "live"
        //static let liveRecordingBitrate:Int32 = 256 //256 //OK: 512, 300, NOTOK: 1024   , test 64
        static let liveRecordingWidth:Int32 = 320
        static let liveRecordingHeight:Int32 = 568

        //for TESTING only
        static let liveRecordingStreamName = "c2c"
    }
    
    struct Sizes {
        static let screenHeight = UIScreen.main.bounds.size.height
        static let screenWidth = UIScreen.main.bounds.size.width
        static let defaultCollectionCellHeight : CGFloat = 185.0
        static let eventAdminCellHeight : CGFloat = 185.0 // 245.0
    }
    
    /**
     use constants for StorybordIdentifiers
     */
    struct Storyboards {
        static let login = "Login"
        static let coachings = "Coachings"
        static let livestreaming = "Livestreaming"
        static let createCoaching = "CreateCoaching"
        static let profile = "Profile"
    }
    
    struct ViewControllers {
        
        // Registration & Login Process
        
        static let initialStart = "InitialStartVC"
        static let loginPreview = "LoginPreviewVC"
        static let registration = "RegistrationVC"
        static let login = "LoginVC"
        static let requestPassword = "RequestPasswordVC"
        static let resetPassword = "ResetPasswordVC"
        static let newPassword = "NewPasswordVC"
        
        // Main Tab Bar
        
        static let mainTabBar = "MainTabBarController"
        
        // Coachings
        
        static let coachings = "CoachingVC"
        static let coachingDetailsWeb = "DetailsWebVC"
        static let eventOverview = "EventOverviewVC"
        static let favoriteEvents = "FavoriteEventsVC"
        static let eventDetails = "EventDetailsVC"
        static let eventAdmin = "EventAdminVC"
        static let emailViewController = "EmailViewController"
        
        // Cells
        
        static let eventCellDefault = "EventCellDefault"
        static let eventTableCell = "EventTableCell"
        static let eventAdminCell = "EventAdminCell"
        static let eventArchiveCell = "EventArchiveCell"
        static let createCoachingCell = "CreateCoachingCell"
        static let createCoaching2Cell = "CreateCoaching2Cell"
        
        // LiveStreaming
        
        static let livestreamingNavigation = "LivestreamingNC" // UINavigationController

        static let countDown = "CountDownVC"
        static let coachPublish = "CoachPublicVC"
        static let participantSubscribe = "ParticipantSubscribeVC"
        
        static let livestreamingCoach = "LivestreamingCoachVC"
        static let livestreamingParticipant = "LivestreamingParticipantVC"
        
        // Create Coaching
        
        static let chooseCoachingMode = "ChooseCoachingModeVC"
        static let coachingDetails = "CoachingDetailsVC"
        static let coachingPrice = "CoachingPriceVC"
        static let coachingDescription = "CoachingDescriptionVC"
        static let coachingPlanning = "CoachingPlanningVC"
        
        // Profile
        
        static let profile = "ProfileVC"
        static let newProfilePassword = "NewProfilePasswordVC"
        
        static let verifyCoach = "VerifyCoachVC"
    }
    
    
    // LiveStreamingConfig
    
    static func getLiveStreamingConfig(_ env : EnviormentConfiguration)->R5Configuration{
        // Set up the configuration
        let config = R5Configuration()
        config.host = env.red5Edge1 // API.liveRecordingIP
        config.port = Int32( env.red5Port ) // Int32(API.liveRecordingPort)
        config.contextName = API.liveRecordingAppName
        config.`protocol` = 1
        config.buffer_time = env.red5bufferSize
        OnDebugging.print("Config: \(config.host) \(config.port) \(config.contextName)")
        return config
    }
    
    
    /// UILocalNotifications - used to inform user about upcoming coachigns, & redirect to CountDownScreen
    
    struct LocalNotifications {
        struct UserInfoKeys {
            static let coachingID = "coachingID"
            //static let coachingStartDate = "coachingStartDate"
        }
        struct Category {
            static let coachingBegins = "coachingBegins"
        }
    }
    
    /// used to track when internet Connection status changes
    
    struct Notifications {
        static let internetConnectionStatusChanged = "internetConnectionStatusChangedNotificationKey"
    }
    

    struct RealmID {
        static let user = "Constants.RealmID.user"
        static let event = "Constants.RealmID.event"

    }
}

















