//
//  RegistrationVC.swift
//  come2coach
//
//  Created by Alex Kupchak on 10/21/16.
//  Copyright © 2016 MaytecNet. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift
import SVProgressHUD
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func >= <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l >= r
  default:
    return !(lhs < rhs)
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class RegistrationVC: UIViewController {
    
    
    @IBOutlet weak var subscribeView: UIView!
    @IBOutlet weak var subscribeLabel: UILabel!
    @IBOutlet weak var subscribeSwitch: UISwitch!
    @IBOutlet weak var checkedAGB: UISwitch!
    
    @IBOutlet weak var inputContainerView: UIView!
    @IBOutlet weak var inputContainerBottomAnchor: NSLayoutConstraint!
    
    @IBOutlet weak var acbLabel: UILabel!
    @IBOutlet weak var privacyLabel: UILabel!
    
    @IBOutlet weak var fullName: PaddedTextField!
    @IBOutlet weak var mailTF: PaddedTextField!
    @IBOutlet weak var passwordTF: PaddedTextField!
    @IBOutlet weak var passwordRepeatTF: PaddedTextField!
    @IBOutlet weak var btnRegister: UIButton!
    
    var isShow: Bool = false
    
    // MARK: Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupKeyboardObservers()
        formatACBLabelBlue()
        formatPrivacyLabelBlue()
        self.setupTitle(self.navigationItem.title)

        let backgroundTap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view += backgroundTap
    }

    override func viewWillAppear(_ animated: Bool) {

        super.viewWillAppear(animated)

        navigationController?.isNavigationBarHidden = true

    }
    
    deinit {
        NotificationCenter.default
            .removeObserver(self)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        subscribeSwitch.onTintColor = Designs.Colors.lightBlue
        checkedAGB.onTintColor = Designs.Colors.lightBlue
        
        if isShow == false {
            slideInKeyboardFromBottom()
        }
    }
    
    fileprivate func setupKeyboardObservers() {
        NotificationCenter.default
            .addObserver(self, selector: #selector(keyboardWillShow(_:)), name:NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default
            .addObserver(self, selector: #selector(keyboardWillHide(_:)), name:NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    // onViewDidAppaer
    func slideInKeyboardFromBottom() {
        isShow = true
        
        let passwordForgottenButtonHeight: CGFloat = 40
        let subscribeForgottenButtonHeight: CGFloat = 40
        let deltaHeight: CGFloat = inputContainerView.frame.height + passwordForgottenButtonHeight + 8 + subscribeForgottenButtonHeight
        inputContainerBottomAnchor.constant -= deltaHeight
        view.layoutIfNeeded()
        
        UIView
            .animate(withDuration: 0.5, delay: 0, options: UIViewAnimationOptions(), animations: { [weak self] in
                self?.inputContainerBottomAnchor.constant += deltaHeight
                self?.view.layoutIfNeeded()
                }, completion: nil)
    }
    
    // MARK: Actions
    
    @IBAction func backToLoginPreview(_ sender: AnyObject) {
        slideOutKeyboardIntoBottomThenShowLoginPreviewVC()
    }
    
    @IBAction func backButtonPressed(_ sender: Any) {
        slideOutKeyboardIntoBottomThenShowLoginPreviewVC()
    }

    var tempInputContainerBottomAnchorConstant: CGFloat = 0 // handles case where didTap(X)-Button while Keyboard was present
    func slideOutKeyboardIntoBottomThenShowLoginPreviewVC() {
        isShow = false
        
        let passwordForgottenButtonHeight: CGFloat = 40
        let subscribeForgottenButtonHeight: CGFloat = 40
        let deltaHeight: CGFloat = inputContainerView.frame.height + passwordForgottenButtonHeight + 8 + subscribeForgottenButtonHeight
        inputContainerBottomAnchor.constant = tempInputContainerBottomAnchorConstant
        dismissKeyboard()
        
        view.layoutIfNeeded()
        
        UIView
            .animate(withDuration: 0.5, delay: 0, options: UIViewAnimationOptions(), animations: { [weak self] in
                guard let self_ = self else {return}
                self_.inputContainerBottomAnchor.constant = 8 - deltaHeight
                self_.view.layoutIfNeeded()
                }, completion: { _ in
                    self.show(Storyboards.ViewControllers.loginPreview)
            })
    }
    
    @IBAction func changeSubscriptionStatus(_ sender: AnyObject) {
        
    }
    
    // MARK: Button Registration click
    
    static var user : User?
    
    @IBAction func clickRegistration(_ sender: AnyObject) {
        //let user = User()
        RegistrationVC.user = User();

        /*   #inetmanager
        guard InternetConnectionManager.sharedInstance.isConnected == true else {
            self.present(self.defaultNoInternetAlertView())
            return
        }
        */

        // Check AGB

        if !checkedAGB!.isOn {

            self.present(UIAlertController("AGBs akzeptieren".localized, "Bitte akzeptiere zuerst unsere Nutzungsbedingungen.".localized,
                    actions: UIAlertAction(.OK)), animated: true)
            return
        }

        RegistrationVC.user!.subscribeNewsletter = subscribeSwitch!.isOn

        // Name validation
        //
        var fullNameArr = fullName.text?.components(separatedBy: " ")
        fullNameArr = fullNameArr!.filter({$0.characters.count > 0})
        
        if fullNameArr?.count >= 2 {
            RegistrationVC.user!.name = fullNameArr![0]
            RegistrationVC.user!.lname = fullNameArr![1]
            
            for i in 2..<fullNameArr!.count {
                if fullNameArr![i].characters.count > 0 {
                    RegistrationVC.user!.lname = RegistrationVC.user!.lname + String(format: " %@", fullNameArr![i])
                }
            }
        }  else {
            self.present(UIAlertController("Bitte korrigieren:".localized, "Gebe deinen Vor- und Nachnamen bitte ein.".localized,
                actions: UIAlertAction(.OK)), animated: true)
            return
        }

        // Name Validation
        if (!RegistrationVC.isValidName(RegistrationVC.user!.name)) {

            self.present(UIAlertController("Bitte korrigieren:".localized, "Dein Vorname ist zu kurz oder enthält unerlaubte Sonderzeichen.".localized,
                    actions: UIAlertAction(.OK)), animated: true)
            return

        }

        if (!RegistrationVC.isValidName(RegistrationVC.user!.lname)) {

            self.present(UIAlertController("Bitte korrigieren:".localized, "Dein Nachname ist zu kurz oder enthält unerlaubte Sonderzeichen.".localized,
                    actions: UIAlertAction(.OK)), animated: true)
            return

        }


        // Mail validation
        //
        if mailTF.text?.characters.count > 0 {
            if mailTF.text!.validateEmail()  {
                RegistrationVC.user!.email = mailTF.text!
            } else {
                self.present(UIAlertController("Bitte korrigieren:".localized, "Deine E-Mail hat kein korrektes Format oder enthält unerlaubte Sonderzeichen.".localized,
                    actions: UIAlertAction(.OK)), animated: true)
                return
            }
        } else {
            self.present(UIAlertController("Bitte korrigieren:".localized, "Gib deine Mail an".localized,
                actions: UIAlertAction(.OK)), animated: true)
            return
        }
        
        // Pass validation
        //
        if (passwordTF.text?.characters.count < 6)
        {
            self.present(UIAlertController("Bitte korrigieren:".localized, "Das Passwort ist zu kurz".localized,
                    actions: UIAlertAction(.OK)), animated: true)
        }

        if passwordTF.text?.characters.count > 0 || passwordRepeatTF.text?.characters.count > 0 {
            if passwordTF.text == passwordRepeatTF.text {
                RegistrationVC.user!.password = passwordTF.text!
            } else {
                self.present(UIAlertController("Bitte korrigieren:".localized, "Deine Passwörter stimmen nicht überein".localized,
                    actions: UIAlertAction(.OK)), animated: true)
                return
            }
        } else {
            self.present(UIAlertController("Bitte korrigieren:".localized, "Setze ein Passwort".localized,
                actions: UIAlertAction(.OK)), animated: true)
            return
        }
        
        self.show(Storyboards.ViewControllers.profilePhoto, animated: true, modally: true)
        
        /*
         
         change: the registration endpoint was changed by backend to allow uploading profile pic with one rest call..
         moved this logic to AddProfilePhotoVC.
         
         todo: add validation of name here
         todo: link up newsletter switch to user.subscribeNewsletter
        
        SVProgressHUD.show()
        LoginManager.sharedInstance.register(RegistrationVC.user!) { (errors) in
            SVProgressHUD.dismiss()
            if errors.isContaining {
                var message = ""
                errors.forEach { message += "\($0),\n" }
                message.removeLast()
                message.removeLast()
                message += "."
                
                self.present(UIAlertController("Bitte korrigieren:".localized, message,
                    actions: UIAlertAction(.OK)))
                return
            } else if errors.isEmpty {
                self.show(Storyboards.ViewControllers.profilePhoto, animated: true, modally: true)
            }
        }
        */
        
        
    }
    
    // MARK: ACB
    
    @IBAction func didTapACB(_ sender: AnyObject) {
        formatACBLabelBlue()
    }
    @IBAction func didCancelTapACB(_ sender: AnyObject) {
        formatACBLabelBlue()
    }
    @IBAction func didTouchACB(_ sender: AnyObject) {
        acbLabel.textColor = UIColor.white
        UIApplication.shared.openURL(URL(string: "https://app.come2coach.de/tos")!)
    }
    @IBAction func didCancelTouch(_ sender: AnyObject) {
        formatACBLabelBlue()
    }
    
    // MARK: Policy
    
    @IBAction func didCancelTouchPolicy(_ sender: AnyObject) {
        formatPrivacyLabelBlue()
    }
    
    @IBAction func didTouchPolicy(_ sender: AnyObject) {
        privacyLabel.textColor = UIColor.white
    }
    
    @IBAction func didTapPolicy(_ sender: AnyObject) {
        formatPrivacyLabelBlue()
        UIApplication.shared.openURL(URL(string: "https://app.come2coach.de/privacy-policy")!)
    }
    
    @IBAction func didCancelTapPolicy(_ sender: AnyObject) {
        formatPrivacyLabelBlue()
    }
    
    
    // MARK: NSNotificationCenter
    //==================================================================
    
    // SHOW Keyboard
    func keyboardWillShow(_ notification: Notification) {
        guard let info = notification.userInfo else {
            OnDebugging
                .printError("notification.userInfo == nil",title: "LoginVC.keyboardWillShow()")
            return
        }
        let keyboardFrame: CGRect = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let slideDuration = info[UIKeyboardAnimationDurationUserInfoKey] as! Double
        let curve = info[UIKeyboardAnimationCurveUserInfoKey] as! UInt
        let options = UIViewAnimationOptions(rawValue: curve)
        
        let keyboardWordsSuggestionsFrameHeight: CGFloat = 48
        inputContainerBottomAnchor.constant = keyboardFrame.height - keyboardWordsSuggestionsFrameHeight
        tempInputContainerBottomAnchorConstant = keyboardFrame.height - keyboardWordsSuggestionsFrameHeight
        view.layoutIfNeeded()
        
        
        UIView
            .animate(withDuration: slideDuration, delay: 0, options: options, animations: { [weak self] in
                self?.view.layoutIfNeeded()
                }, completion: nil)
    }
    
    // HIDE Keyboard
    func keyboardWillHide(_ notification: Notification) {
        guard let info = notification.userInfo else {
            OnDebugging
                .printError("notification.userInfo == nil",title: "LoginVC.keyboardWillHide()")
            return
        }
        let slideDuration = info[UIKeyboardAnimationDurationUserInfoKey] as! Double
        let curve = info[UIKeyboardAnimationCurveUserInfoKey] as! UInt
        let options = UIViewAnimationOptions(rawValue: curve)
        
        inputContainerBottomAnchor.constant = 8
        tempInputContainerBottomAnchorConstant = 8
        UIView
            .animate(withDuration: slideDuration, delay: 0, options: options, animations: { [weak self] in
                self?.view.layoutIfNeeded()
                }, completion: nil)
    }
    
    func dismissKeyboard() {
        fullName.resignFirstResponder()
        mailTF.resignFirstResponder()
        passwordRepeatTF.resignFirstResponder()
        passwordTF.resignFirstResponder()
    }
    
    // MARK: Others
    
    func formatACBLabelBlue() {
        acbLabel?.textColor = Designs.Colors.lightBlue
    }
    
    func formatPrivacyLabelBlue() {
        privacyLabel?.textColor = Designs.Colors.lightBlue
    }
    
    static func isValidEmail(_ testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-üäöÜÄÖ]+@[A-Za-z0-9.-üäöÜÄÖ]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }

    static func isValidName(_ testStr:String) -> Bool {
        let nameRegEx = "[A-Z0-9a-z-üäöÜÄÖ]{2,}"

        let nameTest = NSPredicate(format:"SELF MATCHES %@", nameRegEx)
        return nameTest.evaluate(with: testStr)
    }

    static func isValidNameWithSpace(_ testStr:String) -> Bool {
        let nameRegEx = "[A-Z0-9a-z-üäöÜÄÖ ]{2,}"

        let nameTest = NSPredicate(format:"SELF MATCHES %@", nameRegEx)
        return nameTest.evaluate(with: testStr)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        dismissKeyboard()
        return true
    }
}

extension RegistrationVC: UITextFieldDelegate {
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.layoutIfNeeded()
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return true
    }
}
