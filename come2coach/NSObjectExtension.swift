//
//  Extensions.swift
//  
//
//  Created by Igor Prysyazhnyuk on 1/16/17.
//  Copyright © 2017 Steelkiwi. All rights reserved.
//

import Foundation

extension NSObject {
    static var name: String {
        return String(describing: self)
    }
}
