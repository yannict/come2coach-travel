//
//  UIViewController+IDs.swift
//  come2coach
//
//  Created by Marc Ortlieb on 02.08.16.
//  Copyright © 2016 Marc Ortlieb. All rights reserved.
//

import Foundation


//extension UIViewController {
//    
//   
//}
/** Usecase
 
class UnicornBrowserViewController: UIViewController, SegueHandlerType {
    
    enum SegueIdentifier: String {
        case ShowImportUnicornID
        case ShowCreateNewUnicornID
        case ShowAddUnicornID
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        switch segueIdentifier(For: segue) {
        case .ShowImportUnicornID: break
        case .ShowCreateNewUnicornID: break
        case .ShowAddUnicornID: break
        }
    }
    
    func handleAction(sender: AnyObject?) {
        performSegue(with: .ShowAddUnicornID, sender: sender)
    }
}
 
*/


//protocol SegueHandlerType {
//    associatedtype SegueIdentifier: RawRepresentable
//}
//
//extension SegueHandlerType where Self: UIViewController, SegueIdentifier.RawValue == String {
//    
//    // performSegue
//    
//    func performSegue(with identifier: SegueIdentifier, sender: AnyObject?) {
//        performSegueWithIdentifier(identifier.rawValue, sender: sender)
//    }
//    
//    // segueID
//    
//    func segueIdentifier(For segue: UIStoryboardSegue) -> SegueIdentifier {
//        
//        guard let identifier = segue.identifier, segueIdentifier = SegueIdentifier(rawValue: identifier)
//            else { fatalError("Invalid segue Identifier: \(segue.identifier)") }
//        
//        return segueIdentifier
//    }
//}


