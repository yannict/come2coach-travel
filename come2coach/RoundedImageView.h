//
//  RoundedImageView.h
//  MaytecNet
//
//  Created by Werner Kratochwil on 16.06.15.
//  Copyright (c) 2015 MaytecNet All rights reserved.
//
//

#import <UIKit/UIKit.h>

@interface RoundedImageView : UIImageView

@end
