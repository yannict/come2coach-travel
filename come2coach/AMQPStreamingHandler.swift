//
// Created by Bernd Freier on 16.01.17.
// Copyright (c) 2017 MaytecNet. All rights reserved.
//

import Foundation
import RMQClient

class AMQPStreamingHandler: NSObject, RMQConnectionDelegate {

    var conn: RMQConnection?
    var channel: RMQChannel?
    var topic: RMQExchange?
    var queue: RMQQueue?

    var msgDelegate: AMQPDelegate?
    var env :  EnviormentConfiguration?
    var userId : String?
    var eventId : String?

    var triedEdge1 : Bool = false
    var triedEdge2 : Bool = false

    open func connect(_ env: EnviormentConfiguration, delegate: AMQPDelegate?) -> AMQPStreamingHandler {

        self.env = env

        msgDelegate = delegate

        var amqpHost =  env.amqpEdge1 + ":" + String(env.amqpPort)

        if (triedEdge2) {
             amqpHost = env.amqpEdge2 + ":" + String(env.amqpPort)
        }

        // let rmq_delegate = RMQConnectionDelegateLogger() // implement RMQConnectionDelegate yourself to react to errors
        conn = RMQConnection(uri: "amqp://c2cclient:c2c!!Client@" + amqpHost, delegate: self)

        triedEdge1 = true

        conn?.start()

        user()

        return self

    }

    open func close() {

        if let t = topic {
            queue?.unbind(t)
        }

        channel?.close()

        conn?.close()
    }

    static func getCoachRoutingKey(_ event : Event) -> String {
        return String(event.id) + "-" + String(event.coachId)
    }

    open func user() -> AMQPStreamingHandler {

        if let uId = userId {
            if let eId = eventId {

                channel = conn?.createChannel()

                // let exchange = channel?.direct("c2c-chat", options: .autoDelete)

                topic = channel?.topic("c2c.chat." + eId, options: [.durable, .autoDelete])
                queue = channel?.queue(eId + "-" + uId)

                queue?.bind(topic, routingKey: eId + "-" + uId)
                queue?.bind(topic, routingKey: "all")

                // receive message and process by amqp delegate
                queue?.subscribe({ (_ message: RMQMessage) -> Void in

                    print("Received \(String(data: message.body, encoding: .utf8))")

                    if let d = self.msgDelegate {

                        let msg = String(data: message.body, encoding: .utf8)!

                        let m = msgBase(json: msg)

                        d.process(m.type, m.data)
                    }
                })



            }
        }

        return self

    }

    open func sendMessage(_ message: String?) {

        if let msg = message {

            let m = msgChat()
            m.msg = msg
            m.user = User.current!.name + " " + User.current!.lname

            let base = msgBase("chat", m.toJsonString())

            topic?.publish(base.toJsonString().data(using: .utf8), routingKey: "all")
        }
    }

    open func sendEnteredRoom(_ userId: String, _ eventId: String, _ firstName: String, _ lastName: String) {

        let m = msgRoom()
        m.action = "entered"
        m.firstName = firstName
        m.lastName = lastName
        m.userID = userId

        let base = msgBase("room", m.toJsonString())

        topic?.publish(base.toJsonString().data(using: .utf8), routingKey: eventId)

    }

    open func sendExitRoom(_ userId: String, _ eventId: String, _ firstName: String, _ lastName: String) {

        let m = msgRoom()
        m.action = "left"
        m.firstName = firstName
        m.lastName = lastName
        m.userID = userId

        let base = msgBase("room", m.toJsonString())

        topic?.publish(base.toJsonString().data(using: .utf8), routingKey: eventId)

    }



    /// @brief Called when a socket cannot be opened, or when AMQP handshaking times out for some reason.
    func connection(_ connection: RMQConnection, failedToConnectWithError error: Error?) {

        if (!triedEdge2) {
            triedEdge2 = true

            self.connect(self.env!, delegate: msgDelegate)

        }

    }

    /// @brief Called when a connection disconnects for any reason
    func connection(_ connection: RMQConnection, disconnectedWithError error: Error?) {

    }
    /// @brief Called before the configured <a href="http://www.rabbitmq.com/api-guide.html#recovery">automatic connection recovery</a> sleep.

    func willStartRecovery(with connection: RMQConnection) {

    }
    /// @brief Called after the configured <a href="http://www.rabbitmq.com/api-guide.html#recovery">automatic connection recovery</a> sleep.

    func startingRecovery(with connection: RMQConnection) {

    }

    func recoveredConnection(_ connection: RMQConnection) {

    }

    func channel(_ channel: RMQChannel, error: Error?) {

    }


}
