//
//  EventTableCell.swift
//  come2coach
//
//  Created by Victor on 01.11.16.
//  Copyright © 2016 MaytecNet. All rights reserved.
//

import Foundation

protocol EventTableCellDelegate {
    func eventTableCell(_ cell: EventTableCell, didLikeEvent like: Bool, eventWithId eventId: Int)

    func sendMail(_ event: Event)

    func liveHandler(_ event: Event)
}

class EventTableCell: UITableViewCell {

    @IBOutlet weak var eventImageView: UIImageView!
    @IBOutlet weak var eventTitleLabel: UILabel!
    @IBOutlet weak var eventStateLabel: UILabel!

    @IBOutlet weak var eventState2Label: UILabel!


    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var mailButton: UIButton?
    @IBOutlet weak var broadcastButton: UIButton!

    @IBOutlet weak var broadcastImageView: UIImageView?
    var event: Event?
    var delegate: EventTableCellDelegate? = nil

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func cleanInfo() {
        eventTitleLabel.text = ""
        eventImageView.image = UIImage.init(named: "placeholder")
        eventStateLabel.attributedText = NSAttributedString.init(string: "")
    }

    @IBAction func mailButtonClicked(_ sender: AnyObject) {
        delegate?.sendMail(event!)
    }

    @IBAction func broadCastClick(_ sender: AnyObject) {
        delegate?.liveHandler(event!)
    }

    func fillEventInfo(_ event: Event, isLiked: Bool) {

        broadcastButton?.isHidden = true

        eventTitleLabel.text = event.title
        if event.img1PathExtension != "" {
            eventImageView.sd_setImage(with: event.imageURL as URL!)
        }
        eventStateLabel.attributedText = stateStringForEvent(event)

        //todo: auf live umstellen
        eventState2Label.text = event.live == 1 ? "Livestream".localized : "Videostream".localized

        if isLiked == true {
            likeButton.isHidden = false
            mailButton?.isHidden = true
            broadcastButton?.isHidden = true

            var isLiked = self.event?.isLiked
            if self.event?.isWatching == true {
                isLiked = true
            }
            likeButton.setImage(isLiked == true ? UIImage.init(named: "icon_liked") : UIImage.init(named: "icon_like_empty"), for: UIControlState())
        } else {
            likeButton.isHidden = true
            if (event.live == 1) {
                broadcastButton?.isHidden = false
            }
            mailButton?.isHidden = false

            /* #lvdebug
            if event.isHappeningNow {
                mailButton?.hidden = false
                broadcastImageView?.hidden = true
            } else {
                mailButton?.hidden = true
                broadcastImageView?.hidden = false
            }
            */
        }

    }

    @IBAction func like() {
        var isLiked = self.event?.isLiked
        if self.event?.isWatching == true {
            isLiked = true
        }
        if let _ = delegate {
            delegate?.eventTableCell(self, didLikeEvent: !isLiked!, eventWithId: (self.event?.id)!)
        }
    }

    @IBAction func sendMail() {
        if let _ = delegate {
            delegate!.sendMail(event!)
        }
    }

    fileprivate func stateStringForEvent(_ event: Event) -> NSAttributedString {

        let currentEvent = LiveStreamingHandler.getEventStatus(event)


        if let time = currentEvent.currentDate {

            let timeAttributes: NSDictionary = [NSForegroundColorAttributeName: UIColor.black]
            let timeString = NSMutableAttributedString(string: time.shortTimeString + " Uhr | ".localized + "mit ".localized +
                    event.coachFirstName + " " + event.coachLastName
                    , attributes: timeAttributes as? [String: AnyObject])
            return timeString

        } else {

            let timeAttributes: NSDictionary = [NSForegroundColorAttributeName: UIColor.black]
            let timeString = NSMutableAttributedString(string: event.coachFirstName + " " + event.coachLastName
                    , attributes: timeAttributes as? [String: AnyObject])
            return timeString

        }

        /*
        let stateAttributes: NSDictionary = [NSForegroundColorAttributeName: Designs.Colors.coachingTypeBlue]
        let stateString = NSMutableAttributedString(string: event.isWebinar == true ? "Online" : event.address.city, attributes: stateAttributes as? [String: AnyObject])
        let timeAttributes: NSDictionary = [NSForegroundColorAttributeName: Designs.Colors.coachingTimeGray]
        let timeString = NSAttributedString(string: ", " + EventDateFormatter.timeLeftToDate(event.begin.date), attributes: timeAttributes as? [String: AnyObject])
        stateString.append(timeString)
        return stateString
        */
    }

    fileprivate func isOnline() {

    }

}
