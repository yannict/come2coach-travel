//
//  CoinsCountButton.swift
//  come2coach
//
//  Created by Igor Prysyazhnyuk on 3/20/17.
//  Copyright © 2017 MaytecNet. All rights reserved.
//

import UIKit

class CoinsCountButton: UIButton {
    
    @IBInspectable
    var isCoinsEnoughTitle: String = "" {
        didSet { updateUI() }
    }
    
    var coins: Int = 0 {
        didSet { updateUI() }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        updateUI()
    }
    
    private func updateUI() {
        guard !isCoinsEnoughTitle.isEmpty else { return }
        let buttonTitle = "\(isCoinsEnoughTitle) | \("Deine Coins".localized): \(coins)"
        let buttonTitleNSString = buttonTitle as NSString
        let whiteColorRange = buttonTitleNSString.range(of: isCoinsEnoughTitle)
        let blackColorRange = buttonTitleNSString.range(of: buttonTitle.replacingOccurrences(of: isCoinsEnoughTitle, with: ""))
        let attributedString = NSMutableAttributedString.init(string: buttonTitle)
        attributedString.addAttribute(NSForegroundColorAttributeName, value: UIColor.white , range: whiteColorRange)
        attributedString.addAttribute(NSForegroundColorAttributeName, value: UIColor.black , range: blackColorRange)
        setAttributedTitle(attributedString, for: .normal)
    }
    
    override func setTitle(_ title: String?, for state: UIControlState) {
        super.setTitle(title, for: state)
        setAttributedTitle(nil, for: .normal)
        isCoinsEnoughTitle = ""
        setBlueBackground()
    }
    
    func setRedBackground() {
        setBackground(name: "cc-btn-reject")
    }
    
    func setBlueBackground() {
        setBackground(name: "button-background-verlauf")
    }
    
    private func setBackground(name: String) {
        setBackgroundImage(UIImage(named: name), for: .normal)
    }
}
