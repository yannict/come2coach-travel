//
//  UIView+Tools.h
//  MaytecNet
//
//  Created by Werner Kratochwil on 16.06.15.
//  Copyright (c) 2015 MaytecNet All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Tools)

- (void)pin2View:(UIView *)otherView withAttribute:(NSLayoutAttribute)attribute;
- (void)pinAllEdges2View:(UIView *)otherView;
- (void)pin2View:(UIView *)otherView withAttribute:(NSLayoutAttribute)attribute distance:(CGFloat)distance;

- (void)pin2SuperViewWithAttribute:(NSLayoutAttribute)attribute distance:(CGFloat)distance;
- (void)pin2SuperViewWithAttribute:(NSLayoutAttribute)attribute;
- (void)pin2View:(UIView *)otherView withAttribute:(NSLayoutAttribute)attribute
  otherAttribute:(NSLayoutAttribute)otherAttribute distance:(CGFloat)distance;
- (void)pin2View:(UIView *)otherView withAttribute:(NSLayoutAttribute)attribute
  otherAttribute:(NSLayoutAttribute)otherAttribute;

@end
