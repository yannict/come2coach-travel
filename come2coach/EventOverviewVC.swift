//
//  EventOverviewVC.swift
//  come2coach
//
//  Created by Victor on 31.10.16.
//  Copyright © 2016 MaytecNet. All rights reserved.
//

import Foundation
import SVProgressHUD

class EventOverviewVC: UIViewController {

    @IBOutlet weak var todayEventsCollectionView: UICollectionView!
    @IBOutlet weak var allEventsCollectionView: UICollectionView!
    @IBOutlet weak var allEventsTableView: UITableView!
    @IBOutlet weak var displayModeButton: UIButton!
    @IBOutlet weak var allEventsTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var todayEventsHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var backScrollView: UIScrollView!
    @IBOutlet weak var toggleButton: UISegmentedControl!

    @IBOutlet weak var buttonCoachReminder: UIButton!
    
    
    
    var allEvents: [Event]! = [Event]()
    var todayEvents: [Event]! = [Event]()

    var upcomingPage = 0
    var upcomingCount = 0
    var todayPage = 0
    var todayCount = 0

    //MARK: Life cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        // register push notification
        (UIApplication.shared.delegate as! AppDelegate).initPushNotifications(UIApplication.shared);

        LocalNotificationManager.sharedInstance
                .registerCoachingBeginsWithOptionalUserActions()

        // setup view
        navigationController?.navigationBar.isHidden = true
        allEventsTableView.tableFooterView = UIView.init(frame: CGRect.zero)
        allEventsTopConstraint.constant = 0
        self.view.layoutSubviews()

        // clear image cache
        ImageTools.clearImageWebCache()

        // Toogle Button
        toggleButton.setTitleTextAttributes(
                [
                        NSFontAttributeName: UIFont(name: "Montserrat-Regular", size: 11.0)!,
                        NSForegroundColorAttributeName: UIColor.white
                ], for: .normal)

        toggleButton.setTitleTextAttributes([
                NSForegroundColorAttributeName: UIColor.white
        ], for: .selected)

        toggleButton.layer.cornerRadius = 10.0;
        toggleButton.layer.borderColor = UIColor.white.cgColor
        toggleButton.layer.borderWidth = 1.0;
        toggleButton.layer.masksToBounds = true;

        // load event data
        //getAllTodayEvents()
        //getAllUpcomingEvents()


    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        // load event data
        getAllTodayEvents()
        getAllUpcomingEvents()

        if (CoachReminder.active)
        {
            buttonCoachReminder.isHidden = false
        } else {
            buttonCoachReminder.isHidden = true
        }

        CoachReminder.remindMe = { () in

            if self != nil {

                 self.buttonCoachReminder.isHidden = false
            }
        }

        // Protocol actions
        ProtocolEventRequest.showEvent = { (eventId) in

            HTTPRequest.fetchEvent(with: eventId) { (event) in

                ProtocolEventRequest.eventId = nil

                let detailsVC = Storyboards.ViewControllers.eventDetails
                detailsVC.mode = "normal"
                detailsVC.event = event

                self.navigationController?.push(detailsVC, animated: true, completion: nil)

            }

        }


        if let redirectEvent = ProtocolEventRequest.eventId {

            HTTPRequest.fetchEvent(with: redirectEvent) { (event) in

                ProtocolEventRequest.eventId = nil

                let detailsVC = Storyboards.ViewControllers.eventDetails
                detailsVC.mode = "normal"
                detailsVC.event = event
                self.navigationController?.push(detailsVC, animated: true, completion: nil)

            }

        }

        //getAllTodayEvents()
        //For testing - start
//        todayEvents = generateTestEvents()
//        self.allEventsTopConstraint.constant = 348
//        self.todayEventsHeightConstraint.constant = 274
//        self.allEventsCollectionView.scrollEnabled = false
//        self.allEventsTableView.scrollEnabled = false
//        self.view.layoutSubviews()
//        todayEventsCollectionView.reloadData()
        //For testing - end
        //getAllUpcomingEvents()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        CoachReminder.remindMe = nil
    }

    //MARK: Actions

    @IBAction func buttonCoachReminderPressed(_ sender: Any) {

        if let eId = CoachReminder.eventId {

            let ls = LiveStreamingHandler()
            ls.launch(eId, controller: self)

        }

        CoachReminder.active = false
    }
    
    
    @IBAction func switchDisplayMode() {
        allEventsCollectionView.isHidden = !allEventsCollectionView.isHidden
        allEventsTableView.isHidden = !allEventsTableView.isHidden
        if allEventsTableView.isHidden == false {
            allEventsTableView.reloadData()
            displayModeButton.setImage(UIImage.init(named: "icon_collection"), for: UIControlState())
        } else {
            allEventsCollectionView.reloadData()
            displayModeButton.setImage(UIImage.init(named: "icon_table"), for: UIControlState())
        }
    }

    var live = true

    @IBAction func toggleSwitched(_ sender: Any) {
        live = !live
        getAllTodayEvents()
        getAllUpcomingEvents()
    }

    func getTodayEvents(_ page: Int, perPage: Int) {

         HTTPRequest.fetchTodayEvents(page, perPage: perPage, live : live ? 1 : 0) { (events, page, count) in
            self.todayCount = count
            self.todayPage = page
            self.todayEvents.append(contentsOf: events)
            self.todayEventsCollectionView.reloadData()
            //OnDebugging.print(events)
            if events.count > 0 {
                self.allEventsTopConstraint.constant = 348
                self.todayEventsHeightConstraint.constant = 266
                self.allEventsCollectionView.isScrollEnabled = false
                self.allEventsTableView.isScrollEnabled = false
                self.view.layoutSubviews()
            }
        }

    }

    func getAllTodayEvents() {

        todayPage = 0
        todayCount = 0

         HTTPRequest.fetchTodayEvents(0, perPage: (todayPage + 1) * HTTPRequest.coachingsPerPage, live : live ? 1 : 0) { (events, page, count) in
            self.todayEvents = events
            self.todayEventsCollectionView.reloadData()
            //OnDebugging.print(events)
            if events.count > 0 {
                self.allEventsTopConstraint.constant = 348
                self.todayEventsHeightConstraint.constant = 266
                self.allEventsCollectionView.isScrollEnabled = false
                self.allEventsTableView.isScrollEnabled = false
                self.view.layoutSubviews()
            } else {
                self.allEventsTopConstraint.constant = 16
                self.todayEventsHeightConstraint.constant = 0
                self.allEventsCollectionView.isScrollEnabled = false
                self.allEventsTableView.isScrollEnabled = false
                self.view.layoutSubviews()
            }
        }

    }

    func getUpcomingEvents(_ page: Int, perPage: Int) {


        HTTPRequest.fetchUpcomingEvents(page, perPage: perPage, live: live ? 1 : 0 ) { (events, page, count) in
            self.upcomingCount = count
            self.upcomingPage = page
            self.allEvents.append(contentsOf: events)
            self.allEventsCollectionView.isHidden == false ? self.allEventsCollectionView.reloadData() : self.allEventsTableView.reloadData()
            OnDebugging.print(events)

        }

    }

    func getAllUpcomingEvents() {

        upcomingPage = 0
        upcomingCount = 0

        SVProgressHUD.show()
        HTTPRequest.fetchUpcomingEvents(0, perPage: (upcomingPage + 1) * HTTPRequest.coachingsPerPage, live: live ? 1 : 0) { (events, page, count) in

            SVProgressHUD.dismiss()
            self.upcomingCount = count
            self.allEvents = events
            self.allEventsCollectionView.isHidden == false ? self.allEventsCollectionView.reloadData() : self.allEventsTableView.reloadData()
            OnDebugging.print(events)

        }
    }



}

//MARK: DefaultEventCellDelegate

extension EventOverviewVC: DefaultEventCellDelegate {

    func defaultEventCell(_ cell: DefaultEventCell, didLikeEvent like: Bool, eventWithId eventId: Int) {

        if like == true {
            HTTPRequest.likeEvent(with: eventId) { (success) in
                if success == true {
                    for event in self.todayEvents {
                        if event.id == eventId {
                            event.isLiked = like
                            event.isWatching = like
                        }
                    }
                    self.todayEventsCollectionView.reloadData()
                    for event in self.allEvents {
                        if event.id == eventId {
                            event.isLiked = like
                            event.isWatching = like
                        }
                    }
                    self.allEventsCollectionView.isHidden == false ? self.allEventsCollectionView.reloadData() : self.allEventsTableView.reloadData()
                }
            }
        } else {
            HTTPRequest.unlikeEvent(with: eventId) { (success) in
                if success == true {
                    for event in self.todayEvents {
                        if event.id == eventId {
                            event.isLiked = like
                            event.isWatching = like
                        }
                    }
                    self.todayEventsCollectionView.reloadData()
                    for event in self.allEvents {
                        if event.id == eventId {
                            event.isLiked = like
                            event.isWatching = like
                        }
                    }
                    self.allEventsCollectionView.isHidden == false ? self.allEventsCollectionView.reloadData() : self.allEventsTableView.reloadData()
                }
            }
        }
    }

}

//MARK: EventTableCellDelegate

extension EventOverviewVC: EventTableCellDelegate {

    func eventTableCell(_ cell: EventTableCell, didLikeEvent like: Bool, eventWithId eventId: Int) {
        if like == true {
            HTTPRequest.likeEvent(with: eventId) { (success) in
                if success == true {
                    for event in self.todayEvents {
                        if event.id == eventId {
                            event.isLiked = like
                            event.isWatching = like
                        }
                    }
                    self.todayEventsCollectionView.reloadData()
                    for event in self.allEvents {
                        if event.id == eventId {
                            event.isLiked = like
                            event.isWatching = like
                        }
                    }
                    self.allEventsCollectionView.isHidden == false ? self.allEventsCollectionView.reloadData() : self.allEventsTableView.reloadData()
                }
            }
        } else {
            HTTPRequest.unlikeEvent(with: eventId) { (success) in
                if success == true {
                    for event in self.todayEvents {
                        if event.id == eventId {
                            event.isLiked = like
                            event.isWatching = like
                        }
                    }
                    self.todayEventsCollectionView.reloadData()
                    for event in self.allEvents {
                        if event.id == eventId {
                            event.isLiked = like
                            event.isWatching = like
                        }
                    }
                    self.allEventsCollectionView.isHidden == false ? self.allEventsCollectionView.reloadData() : self.allEventsTableView.reloadData()
                }
            }
        }
    }

    func sendMail(_ event: Event) {
        // never used by view
    }

    func liveHandler(_ event: Event) {
        // never used by view
    }




}

//MARK: UICollectionViewDataSource

extension EventOverviewVC: UICollectionViewDataSource {

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if collectionView == allEventsCollectionView {
            if allEvents.count > 0 && allEvents.count < 3 {
                return 1
            }
            let sections: Int = allEvents.count % 2 == 0 ? Int(allEvents.count / 2) : Int(allEvents.count / 2 + 1)
            print("Sections: " + String(sections))
            return allEvents.count % 2 == 0 ? Int(allEvents.count / 2) : Int(allEvents.count / 2 + 1)
        } else {
            return 1
        }
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == todayEventsCollectionView {
            return todayEvents.count
        } else {
            if allEvents.count > 0 && allEvents.count < 3 {
                return allEvents.count
            }
            var items: Int = 2
            if allEvents.count % 2 == 1 && section == Int(allEvents.count / 2) {
                items = 1
            }
            return items
        }
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.ViewControllers.eventCellDefault, for: indexPath) as! DefaultEventCell
        cell.cleanInfo()
        //print(indexPath.section * 2 + indexPath.item)
        let event = collectionView == todayEventsCollectionView ? todayEvents[indexPath.item] : allEvents[indexPath.section * 2 + indexPath.item]
        cell.event = event
        cell.delegate = self
        if collectionView == todayEventsCollectionView {
            cell.fillEventInfo(todayEvents[indexPath.row])
            if (indexPath.item) == (todayPage + 1) * HTTPRequest.coachingsPerPage - 1 {
                getTodayEvents(todayPage + 1, perPage: HTTPRequest.coachingsPerPage)
            }
        } else {

            debugPrint("Pages " + String(upcomingPage) + "  " +  String(upcomingCount) )

            cell.fillEventInfo(allEvents[indexPath.section * 2 + indexPath.item])
            if (indexPath.section * 2 + indexPath.item) == (upcomingPage + 1) * HTTPRequest.coachingsPerPage - 1 {
                getUpcomingEvents(upcomingPage + 1, perPage: HTTPRequest.coachingsPerPage)
            }
        }
        return cell
    }

}

//MARK: UICollectionViewDelegateFlowLayout

extension EventOverviewVC: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == todayEventsCollectionView {
            return CGSize.init(width: Constants.Sizes.screenWidth - 65, height: collectionView.bounds.size.height - 8)
        } else {
            return CGSize.init(width: (Constants.Sizes.screenWidth - 42) / 2, height: Constants.Sizes.defaultCollectionCellHeight)
        }
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        if collectionView == todayEventsCollectionView {
            return UIEdgeInsetsMake(4, 20, 4, 20)
        } else {
            if allEvents.count % 2 == 1 && section == Int(allEvents.count / 2) && allEvents.count == upcomingCount {
                let leftInset: CGFloat = (Constants.Sizes.screenWidth - ((Constants.Sizes.screenWidth - 42) / 2)) / 2
                return UIEdgeInsetsMake(4, leftInset - 16, 4, leftInset - 16)
            } else {
                return UIEdgeInsetsMake(4, 16, 4, 16)
            }
        }

    }
}

//MARK: UICollectionViewDelegate

extension EventOverviewVC: UICollectionViewDelegate {

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let detailsVC = Storyboards.ViewControllers.eventDetails
        detailsVC.mode = "normal"
        if collectionView == todayEventsCollectionView {
            detailsVC.event = todayEvents[indexPath.item]
        } else {
            detailsVC.event = allEvents[indexPath.section * 2 + indexPath.item]
        }
        self.navigationController?.push(detailsVC, animated: true, completion: nil)
    }

}

//MARK: UITableViewDataSource

extension EventOverviewVC: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return allEvents.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.ViewControllers.eventTableCell, for: indexPath) as! EventTableCell
        cell.cleanInfo()
        cell.event = allEvents[indexPath.row]
        cell.delegate = self
        cell.fillEventInfo(allEvents[indexPath.row], isLiked: true)
        if indexPath.row == (upcomingPage + 1) * HTTPRequest.coachingsPerPage - 1 {
            getUpcomingEvents(upcomingPage + 1, perPage: HTTPRequest.coachingsPerPage)
        }
        return cell
    }

}

//MARK: UITableViewDelegate

extension EventOverviewVC: UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let detailsVC = Storyboards.ViewControllers.eventDetails
        detailsVC.mode = "normal"
        detailsVC.event = allEvents[indexPath.row]
        self.navigationController?.push(detailsVC, animated: true, completion: nil)
    }
  
}


extension EventOverviewVC: UIScrollViewDelegate {
//    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        if scrollView == backScrollView {
//            if scrollView.contentOffset.y < (allEventsTopConstraint.constant - 50) {
//                self.allEventsCollectionView.isScrollEnabled = false
//                self.allEventsTableView.isScrollEnabled = false
//            } else {
//                self.allEventsCollectionView.isScrollEnabled = true
//                self.allEventsTableView.isScrollEnabled = true
//            }
//            print(backScrollView.contentOffset)
//        }
//    }
}
