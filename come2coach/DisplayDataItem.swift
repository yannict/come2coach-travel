import Foundation

typealias CellHeaderCallback = (_ d : DisplayDataItem) -> Void

class DisplayDataItem {

    var cell : UITableViewCell?
    var header : DisplayDataItem?
    var isHidden = false
    var type: CellType?
    var isOpen = false
    var isChecked = false
    var isImage = false
    var image: UIImage?
    var text: String?
    var number : String?
    var callback: CellHeaderCallback?
    var indexPath : IndexPath?
    var isVideo = false
    var VideoUrl : URL?

    func update() {

        if let c = cell as? HeaderCell
        {
            c.setDisplayData(self)
        }
    }
}
