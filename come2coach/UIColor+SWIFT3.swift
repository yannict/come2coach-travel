//
//  UIColor+SWIFT3.swift
//  Dreamcatcher
//
//  Created by Marc Orlieb on 17.06.16.
//  Copyright © 2016 Marc Ortlieb. All rights reserved.
//

import UIKit

/*
extension UIColor {
    
    static var tableViewHeaderGray: UIColor { return UIColor(red: 0.937, green: 0.925, blue: 0.914, alpha: 1.00) }
    
    static var black:     UIColor {return .black}
    static var white:     UIColor {return .white}
    static var clear:     UIColor {return .clear}

    static var gray:      UIColor {return .gray}
    static var lightGray: UIColor {return .lightGray}
    static var darkGray:  UIColor {return .darkGray}
    static var groupTableView: UIColor {return .darkGray}
    
    static var red:       UIColor {return .red}
    static var yellow:    UIColor {return .yellow}
    static var green:     UIColor {return .green}
    static var blue:      UIColor {return .blue}
    
    static var orange:    UIColor {return .orange}
    static var purple:    UIColor {return .purple}
    static var cyan:      UIColor {return .cyan}
    static var magenta:   UIColor {return .magenta}
    static var brown:     UIColor {return .brown}
}
*/

