//
//  UIAlertController+ex.swift
//  come2coach
//
//  Created by Marc Ortlieb on 01.08.16.
//  Copyright © 2016 Marc Ortlieb. All rights reserved.
//

import UIKit


func += (left: UIAlertController, right: UIAlertAction) {
    left.addAction(right)
}

extension UIAlertController {
    
    static func Alert(_ alertControllerStyle: UIAlertControllerStyle = .actionSheet, _ title: String = "", _ message: String = "", actions: UIAlertAction...) -> UIAlertController {
        let alertController = self.init(title: title, message: message, preferredStyle: .alert)
        actions.forEach(alertController.addAction)
        return alertController
    }
    
    convenience init(alertControllerStyle: UIAlertControllerStyle = .actionSheet, _ title: String = "", _ message: String = "", actions: UIAlertAction...) {
        self.init(title: title, message: message, preferredStyle: alertControllerStyle)
        actions.forEach(self.addAction)
    }
    
    convenience init(_ title: String = "", _ message: String = "", actions: [UIAlertAction]) {
        self.init(title: title, message: message, preferredStyle: .alert)
    }
    
    convenience init(errorMessage: String = "") {
        self.init(title: nil, message: errorMessage, preferredStyle: .alert)
        addAction(UIAlertAction(.Abbrechen))
    }
    
    static func show(fromVC: UIViewController, error: String) {
        fromVC.present(UIAlertController(errorMessage: error), animated: true)
    }
}
