//
//  CoachingDescriptionVC.swift
//  come2coach
//
//  Created by Alex Kupchak on 10/29/16.
//  Copyright © 2016 MaytecNet. All rights reserved.
//

import Foundation


class CoachingDescriptionVC: UIViewController {

    var completion: ((_ txt: String) -> Void)!

    @IBOutlet weak var textView: UITextView!

    @IBOutlet weak var titleLabel: C2CLabel!
    @IBOutlet weak var textViewBottomConstrain: NSLayoutConstraint!

    var placeholder: String?
    var titleText: String?
    var descrValue: String?

    override func viewDidLoad() {

        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardNotification(notification:)), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)

        titleLabel.text = titleText

        textView.delegate = self
        textView.returnKeyType = UIReturnKeyType.default
        textView.text = descrValue // CreateManager.sharedInstance.description

        if let p = placeholder {
            if (textView.text == "") {

                // set placeholder in case no text was entered
                textView.textColor = UIColor.lightGray
                textView.text = placeholder // "Kurzbeschreibung (max. 125 Zeichen)".localized
            }
        }

    }

    // MARK: Actions
    // =========================================================================================

    @IBAction func clickBackButton(_ sender: AnyObject) {

        /*
        dismiss(animated: true, completion: { [weak self] _ in
            guard let self_ = self else {return}
            CreateManager.sharedInstance.description = self_.textView.text
            self_.completion()
        })
        */

        if textView.text == placeholder {
            self.completion("")
        } else {
            self.completion(textView.text)
        }

        //        dismissViewControllerAnimated(false, completion: {[weak self] _ in

        if (self.navigationController == nil) {
            dismissViewController()
        } else {
            self.navigationController?.popViewController(animated: false, completion: { [weak self] _ in
                //guard let self_ = self else {return}
                //CreateManager.sharedInstance.description = self_.textView.text
                //self_.completion()
            })
        }
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    func keyboardNotification(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            let endFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            let duration: TimeInterval = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurveRawNSN = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIViewAnimationOptions.curveEaseInOut.rawValue
            let animationCurve: UIViewAnimationOptions = UIViewAnimationOptions(rawValue: animationCurveRaw)
            if (endFrame?.origin.y)! >= UIScreen.main.bounds.size.height {
                textViewBottomConstrain?.constant = 0.0
            } else {
                textViewBottomConstrain?.constant = endFrame?.size.height ?? 0.0
            }
            UIView.animate(withDuration: duration,
                    delay: TimeInterval(0),
                    options: animationCurve,
                    animations: { self.view.layoutIfNeeded() },
                    completion: nil)
        }
    }

}

extension CoachingDescriptionVC: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        /*
        guard let enteredText = textView.text else {
            return true
        }

        let newLength = enteredText.characters.count + text.characters.count - range.length
        return newLength <= 125
        */
        return true
    }

    func textViewDidBeginEditing(_ textView: UITextView) {
        if let p = placeholder {
            if (textView.text == p) {
                textView.text = ""
                textView.textColor = UIColor.black
            }
        }
    }

    func textViewDidEndEditing(_ textView: UITextView) {
        if let p = placeholder {
            if (textView.text == p) {
                textView.text = ""
                textView.textColor = UIColor.black
            }
        }

    }
}
