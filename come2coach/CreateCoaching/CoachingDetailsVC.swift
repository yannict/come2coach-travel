//
//  CoachingDetailsVC.swift
//  come2coach
//
//  Created by Alex Kupchak on 10/26/16.
//  Copyright © 2016 MaytecNet. All rights reserved.
//

import Foundation
import MediaPlayer
import MobileCoreServices
import CoreLocation
import SVProgressHUD

enum CellType {
    case header
    case titleImage
    case imageOrVideo
    case coachingTitle
    case description
    case Address
}

// Constants
//
let HeaderCellIdentifier = "HeaderCellId"
let CoachingMediaIdentifier = "CoachingMediaCellId"
let CoachingTitleIdentifier = "CoachingTitleCell"
let DescriptionIdentifier = "DescriptionCell"
let AddressIdentifier = "AddressCell"


class CoachingDetailsVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!

    @IBOutlet weak var ButtonViewHeightConstrain: NSLayoutConstraint!

    @IBOutlet weak var ButtonView: UIView!
    @IBOutlet weak var BtnReject: UIButton!
    @IBOutlet weak var BtnRejectSeparator: UIView!

    @IBOutlet weak var rejectView: UIView!

    @IBOutlet weak var rejectBackground: UIView!

    var data: Array<AnyObject> = []
    var displayData: Array<DisplayDataItem> = []

    var insertIndex = 3
    var choosenObject = [String: AnyObject]()
    var isAvailable: Bool = false

    var selectedCellIndex = 1

    var locationManager: CLLocationManager!

    func rejectHideView() {
        rejectView.isHidden = true
        rejectBackground.isHidden = true
    }


    @IBAction func rejectDismissView(_ sender: Any) {
        rejectHideView()
    }

    @IBAction func rejectBtnPressed(_ sender: Any) {

        SVProgressHUD.show()
        HTTPRequest.rejectEvent(CreateManager.sharedInstance.eventId!) { (success, json) in

            SVProgressHUD.dismiss()

            CreateManager.sharedInstance.closeCreation()
            self.dismissViewController(true) { () in

                if let s = CreateManager.sharedInstance.sender {
                    s.back()
                }

            }
        }

    }

    @IBOutlet weak var btnNext: UIButton!

    @IBAction func btnNextPressed(_ sender: Any) {

        if isAvailable {
            CreateManager.sharedInstance.detailsData = data
            CreateManager.sharedInstance.insertIndex = insertIndex
            show(Storyboards.ViewControllers.coachingPrice)
        }

    }

    @IBAction func btnRejectPressed(_ sender: Any) {

        rejectView.isHidden = false
        rejectBackground.isHidden = false

    }


    func switchViewMode(_ mode: String) {


        self.view.layoutIfNeeded()


        if (mode == "normal" || mode == "repub") {
            //ButtonView.frame.size.height = 45.0
            BtnReject.isHidden = true
            BtnRejectSeparator.isHidden = true
            ButtonViewHeightConstrain.constant = 45.0
        } else if (mode == "edit") {
            //ButtonView.frame.size.height = 91.0
            BtnReject.isHidden = false
            BtnRejectSeparator.isHidden = false
            ButtonViewHeightConstrain.constant = 91.0
        } else if (mode == "rejectEvent") {
            //ButtonView.frame.size.height = 91.0
            BtnReject.isHidden = false
            BtnRejectSeparator.isHidden = false
            ButtonViewHeightConstrain.constant = 91.0
            rejectView.isHidden = false
            rejectBackground.isHidden = false
        }

        self.view.layoutIfNeeded()

    }


    var item1stMedia = DisplayDataItem()
    var item2ndMedia = DisplayDataItem()
    var itemDescr = DisplayDataItem()
    var itemTitleText = DisplayDataItem()
    var itemAddress = DisplayDataItem()

    func open2ndMedia(_ d: DisplayDataItem) {

        if let c = item2ndMedia.cell {
            c.isHidden = !item2ndMedia.isHidden
            item2ndMedia.isHidden = !item2ndMedia.isHidden
            tableView.reloadData()

            if let i = item2ndMedia.indexPath {
                self.tableView.scrollToRow(at: i as IndexPath, at: .top, animated: true)
            }
        }

    }

    func openDescr(_ d: DisplayDataItem) {
        if let c = itemDescr.cell {

            c.isHidden = !itemDescr.isHidden
            itemDescr.isHidden = !itemDescr.isHidden

            tableView.reloadData()

            if let i = itemDescr.indexPath {
                self.tableView.scrollToRow(at: i as IndexPath, at: .top, animated: true)
            }
        }
    }

    func openTitleText(_ d: DisplayDataItem) {
        if let c = itemTitleText.cell {
            c.isHidden = !itemTitleText.isHidden
            itemTitleText.isHidden = !itemTitleText.isHidden
            tableView.reloadData()
        }

        if let i = itemTitleText.indexPath {

            self.tableView.scrollToRow(at: i as IndexPath, at: .top, animated: true)
        }
    }


    func initDisplayDataFirstCreateEvent() {

        var d = DisplayDataItem()
        d.type = CellType.header
        d.text = "TITELBILD AUSWÄHLEN".localized
        d.isOpen = true
        d.isChecked = false
        d.number = "1"

        displayData.append(d)

        item1stMedia.type = CellType.titleImage
        item1stMedia.isImage = false
        item1stMedia.isHidden = false
        item1stMedia.image = UIImage()
        item1stMedia.header = d;

        displayData.append(item1stMedia)

        d = DisplayDataItem()
        d.type = CellType.header
        d.text = "WEITERES BILD ODER VIDEO".localized
        d.isOpen = false
        d.isChecked = false
        d.number = "2"
        d.callback = open2ndMedia

        displayData.append(d)

        item2ndMedia.type = CellType.imageOrVideo
        item2ndMedia.isImage = false
        item2ndMedia.isVideo = false
        // #swift3 item2ndMedia.VideoUrl = nil
        item2ndMedia.isHidden = true
        item2ndMedia.header = d;

        displayData.append(item2ndMedia)

        d = DisplayDataItem()
        d.type = CellType.header
        d.text = "TITEL DES COACHINGS".localized
        d.isOpen = false
        d.isChecked = false
        d.number = "3"
        d.callback = openTitleText

        displayData.append(d)

        itemTitleText.type = CellType.coachingTitle
        itemTitleText.text = CreateManager.sharedInstance.titleText
        itemTitleText.isHidden = true
        itemTitleText.header = d;

        displayData.append(itemTitleText)


        d = DisplayDataItem()
        d.type = CellType.header
        d.text = "BESCHREIBUNG DES COACHINGS".localized
        d.isOpen = false
        d.isChecked = false
        d.number = "4"
        d.callback = openDescr

        displayData.append(d)

        itemDescr.type = CellType.description
        itemDescr.isHidden = true
        itemDescr.header = d;

        displayData.append(itemDescr)

        /*
        if !CreateManager.sharedInstance.isOnline {

            itemAddress.type = CellType.Address
            itemAddress.isHidden = true
            itemAddress.header = d

            displayData.append(itemAddress)

        }
        */


    }


    override func viewDidLoad() {

        switchViewMode(CreateManager.sharedInstance.mode)

        // configure reject dismiss event
        let rejectBackgroundTap = UITapGestureRecognizer()
        rejectBackgroundTap.addTarget(self, action: "rejectHideView")
        rejectBackground.addGestureRecognizer(rejectBackgroundTap)

        // configure view
        if CreateManager.sharedInstance.isRepublishing == true {
            fillDataExt()
        } else {
            initDisplayDataFirstCreateEvent()
        }

        /*
        if CreateManager.sharedInstance.detailsData.count == 0 {
            let firstDetails = ["type" : CellType.Header.rawValue,
                                "text" : "TITELBILD".localized,
                                "isOpen" : true,
                                "number" : "1",
                                "callback" : show2ndMediaCell,
                                "isCheck" : false]
            
            let secondDetails = ["type" : CellType.TitleImage.rawValue,
                                 "isImage" : false,
                                 "hidden" : true,
                                 "image" : UIImage()]
            
            let thirdDetails = ["type" : CellType.Header.rawValue,
                                "text" : "WEITERES BILD ODER VIDEO".localized,
                                "isOpen" : false,
                                "number" : "2",
                                "isCheck" : false]
            
            
            let fourthDetails = ["type" : CellType.Header.rawValue,
                                 "text" : "COACHINGTITEL".localized,
                                 "isOpen" : false,
                                 "number" : "3",
                                 "isCheck" : false]
            
            let fifthDetails = ["type" : CellType.Header.rawValue,
                                "text" : "ERGÄNZENDE BESCHREIBUNG".localized,
                                "isOpen" : false,
                                "number" : "4",
                                "isCheck" : false]
            
            data = [firstDetails, secondDetails,thirdDetails,fourthDetails,fifthDetails]
            CreateManager.sharedInstance.detailsData = data
        } else {
            data = CreateManager.sharedInstance.detailsData
            if data.count > 7 {
                if !CreateManager.sharedInstance.isOnline {
                    if CreateManager.sharedInstance.address.isStreetFill() && CreateManager.sharedInstance.address.isCityFill() && CreateManager.sharedInstance.address.isLocationFill() && CreateManager.sharedInstance.address.isZipFill() {
                        isAvailable = true
                        nextArrow.image = UIImage(named: "next_blue")
                        nextLabel.textColor = Designs.Colors.blueMedium
                    }
                } else {
                    isAvailable = true
                    nextArrow.image = UIImage(named: "next_blue")
                    nextLabel.textColor = Designs.Colors.blueMedium
                }
            }
            insertIndex = CreateManager.sharedInstance.insertIndex
        }
        */

        tableView.register(UINib(nibName: "CoachingMediaCell", bundle: nil), forCellReuseIdentifier: CoachingMediaIdentifier)
        tableView.register(UINib(nibName: "HeaderCell", bundle: nil), forCellReuseIdentifier: HeaderCellIdentifier)
        tableView.register(UINib(nibName: "CoachingTitleCell", bundle: nil), forCellReuseIdentifier: CoachingTitleIdentifier)
        tableView.register(UINib(nibName: "DescriptionCell", bundle: nil), forCellReuseIdentifier: DescriptionIdentifier)
        tableView.register(UINib(nibName: "AddressCell", bundle: nil), forCellReuseIdentifier: AddressIdentifier)


        tableView.reloadData()

        tableView.tableFooterView = UIView()

        setupKeyboardObservers()

        // load image and/or video data in case it's a prefilled event
        if CreateManager.sharedInstance.isRepublishing == true {
            fillDataAsyncLoadMedia()
        }

    }

    deinit {
        NotificationCenter.default
                .removeObserver(self)
    }

    fileprivate func setupKeyboardObservers() {
        NotificationCenter.default
                .addObserver(self, selector: #selector(keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default
                .addObserver(self, selector: #selector(keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }

    override func viewWillAppear(_ animated: Bool) {

    }

    // MARK: Actions
    // =========================================================================================


    @IBAction func closeButton(_ sender: AnyObject) {
        CreateManager.sharedInstance.closeCreation()
        dismissViewController(true, completion: nil)
    }

    // Fill data

    fileprivate func fillDataExt() {

        var d = DisplayDataItem()
        d.type = CellType.header
        d.text = "TITELBILD AUSWÄHLEN".localized
        d.isOpen = true
        d.isChecked = true
        d.number = "1"

        displayData.append(d)


        item1stMedia.type = CellType.titleImage
        item1stMedia.isImage = true
        item1stMedia.isHidden = false
        item1stMedia.image = UIImage()
        item1stMedia.header = d;

        displayData.append(item1stMedia)


        let media2 = CreateManager.sharedInstance.existingImg2 == "";

        d = DisplayDataItem()
        d.type = CellType.header
        d.text = "WEITERES BILD ODER VIDEO".localized
        d.isOpen = !media2
        d.isChecked = !media2
        d.number = "2"
        d.callback = open2ndMedia

        displayData.append(d)

        item2ndMedia.type = CellType.imageOrVideo
        item2ndMedia.isImage = false
        item2ndMedia.isVideo = false
        // #swift3 item2ndMedia.VideoUrl = URL()
        item2ndMedia.isHidden = media2
        item2ndMedia.header = d;

        displayData.append(item2ndMedia)

        d = DisplayDataItem()
        d.type = CellType.header
        d.text = "TITEL DES COACHINGS".localized
        d.isOpen = true
        d.isChecked = true
        d.number = "3"
        d.callback = openTitleText

        displayData.append(d)

        itemTitleText.type = CellType.coachingTitle
        itemTitleText.text = CreateManager.sharedInstance.titleText
        itemTitleText.isHidden = false
        itemTitleText.header = d;

        displayData.append(itemTitleText)


        d = DisplayDataItem()
        d.type = CellType.header
        d.text = "BESCHREIBUNG DES COACHINGS".localized
        d.isOpen = true
        d.isChecked = true
        d.number = "4"
        d.callback = openDescr

        displayData.append(d)

        itemDescr.type = CellType.description
        itemDescr.isHidden = false
        itemDescr.header = d;

        displayData.append(itemDescr)

        /*
        if !CreateManager.sharedInstance.isOnline {

            itemAddress.type = CellType.Address
            itemAddress.isHidden = false
            itemAddress.header = d

            displayData.append(itemAddress)

        }
        */

        // setup next button
        //
        //nextArrow.image = UIImage(named: "next_blue")
        //nextLabel.textColor = Designs.Colors.blueMedium

        btnNext.isEnabled = true
        isAvailable = true

    }

    fileprivate func fillDataAsyncLoadMedia() {

        // load title image
        if CreateManager.sharedInstance.existingImg1 != "" {

            let imageView = UIImageView.init()

            imageView.sd_setImage(with: URL(string: CreateManager.sharedInstance.getExistingImg1URL()), completed: { (image, error, cacheType, url) in

                if let _ = image {
                    CreateManager.sharedInstance.imageAdded = true

                    self.item1stMedia.isImage = true
                    self.item1stMedia.image = image

                    if let c = self.item1stMedia.cell {
                        (c as! CoachingMediaCell).setDisplayData(self.item1stMedia)
                    }
                }
            })
        }


        if CreateManager.sharedInstance.existingImg2 != "" {

            // switch between video or image
            if (CreateManager.sharedInstance.existingImg2.hasSuffix(".mov")) {
                // Video
                self.item2ndMedia.isVideo = true
                self.item2ndMedia.isImage = false
                self.item2ndMedia.VideoUrl = URL(string: CreateManager.sharedInstance.getExistingImg2URL())

                if let c = self.item2ndMedia.cell {
                    (c as! CoachingMediaCell).setDisplayData(self.item2ndMedia)
                }

            } else {

                let imageView = UIImageView.init()
                imageView.sd_setImage(with: URL(string: CreateManager.sharedInstance.getExistingImg2URL()), completed: { (image, error, cacheType, url) in
                    if let _ = image {
                        CreateManager.sharedInstance.mediaAdded = true

                        self.item2ndMedia.isImage = true
                        self.item2ndMedia.isVideo = false
                        self.item2ndMedia.image = image

                        if let c = self.item2ndMedia.cell {
                            (c as! CoachingMediaCell).setDisplayData(self.item2ndMedia)
                        }
                    }
                })
            }
        }


    }


    /*
    fileprivate func fillData() {

        let firstDetails = ["type": CellType.header.rawValue,
                            "text": "TITELBILD".localized,
                            "isOpen": true,
                            "number": "1",
                            "isCheck": false] as [String : Any]

        let firstDetailsData = ["type": CellType.titleImage.rawValue,
                                "isImage": false,
                                "image": UIImage()] as [String : Any]

        let secondDetails = ["type": CellType.header.rawValue,
                             "text": "WEITERES BILD ODER VIDEO".localized,
                             "isOpen": true,
                             "number": "2",
                             "isCheck": false] as [String : Any]

        let secondDetailsData = ["type": CellType.imageOrVideo.rawValue,
                                 "isImage": false,
                                 "isVideo": false,
                                 "videoUrl": URL()]

        let thirdDetails = ["type": CellType.header.rawValue,
                            "text": "COACHINGTITEL".localized,
                            "isOpen": true,
                            "number": "3",
                            "isCheck": true] as [String : Any]

        let thirdDetailsData = ["type": CellType.coachingTitle.rawValue,
                                "text": CreateManager.sharedInstance.titleText] as [String : Any]

        let fourthDetails = ["type": CellType.header.rawValue,
                             "text": "ERGÄNZENDE BESCHREIBUNG".localized,
                             "isOpen": true,
                             "number": "4",
                             "isCheck": false] as [String : Any]

        let description = ["type": CellType.description.rawValue]

        data = [firstDetails, firstDetailsData, secondDetails, secondDetailsData, thirdDetails, thirdDetailsData, fourthDetails, description]

        if !CreateManager.sharedInstance.isOnline {
            let address = ["type": CellType.address.rawValue,
                           "location": "",
                           "street": "",
                           "city": "",
                           "zip": ""] as [String : Any]
            data.append(address as AnyObject)
        }

        if CreateManager.sharedInstance.titleImageS3Url != "" {
            let imageView = UIImageView.init()
            imageView.sd_setImage(with: URL(string: CreateManager.sharedInstance.titleImageS3Url), completed: { (image, error, cacheType, url) in
                if let _ = image {
                    CreateManager.sharedInstance.imageAdded = true
                    var firstHeader: Dictionary<String, AnyObject>
                    firstHeader = self.data[2] as! Dictionary<String, AnyObject>
                    firstHeader["isCheck"] = true as AnyObject?
                    self.data[2] = firstHeader as AnyObject
                    var firstDetails: Dictionary<String, AnyObject>
                    firstDetails = self.data[3] as! Dictionary<String, AnyObject>
                    firstDetails["isImage"] = true as AnyObject?
                    firstDetails["image"] = image
                    self.data[3] = firstDetails as AnyObject
                    self.tableView.reloadData()
                }
            })
        }

        // TODO: Filling of 2nd media

        if CreateManager.sharedInstance.mediaS3Url != "" {
            let imageView = UIImageView.init()
            imageView.sd_setImage(with: URL(string: CreateManager.sharedInstance.mediaS3Url), completed: { (image, error, cacheType, url) in
                if let _ = image {
                    CreateManager.sharedInstance.mediaAdded = true
                    var secondHeader: Dictionary<String, AnyObject>
                    secondHeader = self.data[2] as! Dictionary<String, AnyObject>
                    secondHeader["isCheck"] = true as AnyObject?
                    self.data[2] = secondHeader as AnyObject
                    var secondDetails: Dictionary<String, AnyObject>
                    secondDetails = self.data[3] as! Dictionary<String, AnyObject>
                    secondDetails["isImage"] = true as AnyObject?
                    secondDetails["image"] = image
                    self.data[3] = secondDetails as AnyObject
                    self.tableView.reloadData()
                }
            })
        }

        // setup next button
        //
        nextArrow.image = UIImage(named: "next_blue")
        nextLabel.textColor = Designs.Colors.blueMedium
        isAvailable = true

    }
    */

    // SHOW Keyboard
    func keyboardWillShow(_ notification: Notification) {
        guard let info = notification.userInfo else {
            OnDebugging
                    .printError("notification.userInfo == nil", title: "LoginVC.keyboardWillShow()")
            return
        }
        let keyboardFrame: CGRect = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue

        var contentInsets: UIEdgeInsets
        contentInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardFrame.height, 0.0);

        tableView.contentInset = contentInsets
    }

    // HIDE Keyboard
    func keyboardWillHide(_ notification: Notification) {

        var contentInsets: UIEdgeInsets
        contentInsets = UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0);

        tableView.contentInset = contentInsets
    }

    // MARK: Others

    // Geocode location which user entered
    //
    func setupLocationIfNeeded() {
        let address = "\(CreateManager.sharedInstance.address.street), \(CreateManager.sharedInstance.address.zip), \(CreateManager.sharedInstance.address.city)"
        let geocoder = CLGeocoder()
        geocoder.geocodeAddressString(address) { (placemarks, error) in
            if let placemark = placemarks?[0] {
                CreateManager.sharedInstance.address.geoX = CGFloat(placemark.location!.coordinate.latitude)
                CreateManager.sharedInstance.address.geoY = CGFloat(placemark.location!.coordinate.longitude)
            }
        }
    }

}

// MARK: UITableViewDelegate
// =========================================================================================


extension CoachingDetailsVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

        let d = displayData[indexPath.row]
        if d.type == CellType.description {
            let vc = Storyboards.ViewControllers.coachingDescription
            vc.placeholder = "Gib hier bitte Deinen Text ein ...".localized
            vc.titleText = "BESCHREIBUNG DES COACHINGS".localized
            vc.descrValue = CreateManager.sharedInstance.description
            show(vc)
            vc.completion = { (txt) in

                CreateManager.sharedInstance.description = txt

                if let h = d.header {

                    // check for placeholder.. not so beautiful, but works;)
                    if (CreateManager.sharedInstance.description == "Gib hier bitte Deinen Text ein ...".localized) {
                        return
                    }

                    // check header
                    h.isChecked = CreateManager.sharedInstance.description.count > 0
                    h.update()
                }



                if CreateManager.sharedInstance.isFillData() {
                    // setup next button
                    //
                    //self.nextArrow.image = UIImage(named: "next_blue")
                    //self.nextLabel.textColor = Designs.Colors.blueMedium
                    self.btnNext.isEnabled = true
                    self.isAvailable = true
                } else {
                    //self.nextArrow.image = UIImage(named: "next_grey")
                    //self.nextLabel.textColor = Designs.Colors.grayForNextButton
                    self.btnNext.isEnabled = false
                    self.isAvailable = false
                }
            }
        }

    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        if displayData[indexPath.row].isHidden {
            return 0
        }

        switch (displayData[indexPath.row].type!) {
        case CellType.titleImage: return 215
        case CellType.imageOrVideo: return 215
        case CellType.description: return 54
        case CellType.coachingTitle: return 54
        case CellType.Address: return 200
        default: return 40
        }

    }

    // resize image
    //
    func resizeImage(_ image: UIImage, newWidth: CGFloat) -> UIImage {
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        image.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return newImage!
    }
}

// MARK: UITableViewDataSource
// =========================================================================================


extension CoachingDetailsVC: UITableViewDataSource {

    func addSeparator(_ cell: UITableViewCell) {

        let additionalSeparatorThickness = CGFloat(1)
        let additionalSeparator = UIView(frame: CGRect(x: 0, y: cell.frame.size.height - additionalSeparatorThickness, width: cell.frame.size.width, height: additionalSeparatorThickness))
        additionalSeparator.backgroundColor = UIColor(netHex: 0x95989A)
        cell.addSubview(additionalSeparator)

    }


    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let ddata = displayData[indexPath.row] as DisplayDataItem
        ddata.indexPath = indexPath

        switch (ddata.type!) {
        case CellType.imageOrVideo:
            let cell: CoachingMediaCell = tableView.dequeueReusableCell(withIdentifier: CoachingMediaIdentifier, for: indexPath) as! CoachingMediaCell
            ddata.cell = cell
            cell.delegate = self
            cell.setDisplayData(ddata)
            //cell.setData(dictionary as! Dictionary<String, AnyObject>)
            cell.isHidden = ddata.isHidden
            addSeparator(cell)
            return cell

        case CellType.titleImage:
            let cell: CoachingMediaCell = tableView.dequeueReusableCell(withIdentifier: CoachingMediaIdentifier, for: indexPath) as! CoachingMediaCell
            ddata.cell = cell
            cell.delegate = self
            cell.setDisplayData(ddata)
            //cell.setData(dictionary as! Dictionary<String, AnyObject>)
            cell.isHidden = ddata.isHidden
            addSeparator(cell)
            return cell

        case CellType.coachingTitle:
            let cell: CoachingTitleCell = tableView.dequeueReusableCell(withIdentifier: CoachingTitleIdentifier, for: indexPath) as! CoachingTitleCell
            ddata.cell = cell

            cell.delegate = self
            cell.setDisplayData(ddata)
            cell.isHidden = ddata.isHidden
            addSeparator(cell)
            return cell


        case CellType.description:

            let cell: DescriptionCell = tableView.dequeueReusableCell(withIdentifier: DescriptionIdentifier, for: indexPath) as! DescriptionCell
            ddata.cell = cell

            cell.isHidden = ddata.isHidden
            addSeparator(cell)
            return cell

        case CellType.Address:

            let cell: AddressCell = tableView.dequeueReusableCell(withIdentifier: AddressIdentifier, for: indexPath) as! AddressCell
            ddata.cell = cell

            cell.delegate = self
            cell.setDisplayData(ddata)
            cell.isHidden = ddata.isHidden
            addSeparator(cell)
            return cell


        default:
            let cell: HeaderCell = tableView.dequeueReusableCell(withIdentifier: HeaderCellIdentifier, for: indexPath) as! HeaderCell
            ddata.cell = cell
            cell.setDisplayData(ddata)
            addSeparator(cell)
            return cell

        }

    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return displayData.count
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
}

// MARK: UIImagePickerControllerDelegate
// =========================================================================================


extension CoachingDetailsVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {


    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String: Any]) {
        let mediaType = info[UIImagePickerControllerMediaType] as! String

        picker.dismiss(animated: true) {
            [weak self] _ in
            guard let self_ = self else {
                return
            }


            let ddata: DisplayDataItem
            ddata = self_.displayData[self_.selectedCellIndex]

            if let dh = ddata.header {
                if !dh.isChecked {
                    dh.isChecked = true
                    dh.update()
                }
            }

            // if video
            //
            if mediaType == kUTTypeMovie as String {

                //Handle a movie capture
                //
                ddata.VideoUrl = info[UIImagePickerControllerMediaURL] as? URL
                ddata.isVideo = true
                ddata.isImage = false

                // remember data
                //
                // CreateManager.sharedInstance.media.videoUrl = info[UIImagePickerControllerMediaURL] as? URL
                CreateManager.sharedInstance.img2.type = "video"
                CreateManager.sharedInstance.img2.changed = true
                CreateManager.sharedInstance.img2.videoURL = info[UIImagePickerControllerMediaURL] as? URL

                self_.tableView.reloadData()

            } else {
                var takenImg: UIImage?

                if let editedImage = info[UIImagePickerControllerEditedImage] as? UIImage {
                    takenImg = editedImage
                } else if let otherImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
                    takenImg = otherImage
                }

                if let toresizeImage = takenImg {


                    // let resizedImage = ImageTools.resizeImage(toresizeImage, newValue: 300)

                    ddata.image = toresizeImage // resizedImage
                    ddata.isImage = true
                    ddata.isVideo = false

                    // remember data
                    //
                    if self_.selectedCellIndex == CreateManager.sharedInstance.imageIndex {
                        CreateManager.sharedInstance.img1.changed = true
                        CreateManager.sharedInstance.img1.originalImage = toresizeImage // resizedImage

                        /*  #deprecated
                        if let imageUrl = info[UIImagePickerControllerReferenceURL] {
                            CreateManager.sharedInstance.selectedTitleImageUrl = imageUrl as! URL
                        }
                        */

                    } else if self_.selectedCellIndex == CreateManager.sharedInstance.mediaIndex {

                        //CreateManager.sharedInstance.media.image = toresizeImage // resizedImage
                        //CreateManager.sharedInstance.media.videoUrl = nil

                        CreateManager.sharedInstance.img2.changed = true
                        CreateManager.sharedInstance.img2.originalImage = toresizeImage

                        /*
                        if let imageUrl = info[UIImagePickerControllerReferenceURL] {
                            CreateManager.sharedInstance.media.selectedMediaImageUrl = info[UIImagePickerControllerReferenceURL] as! URL
                        }
                        */
                    }

                    self_.tableView.reloadData()
                }
            }

            if self_.selectedCellIndex == CreateManager.sharedInstance.imageIndex {
                if CreateManager.sharedInstance.imageAdded == false {
                    CreateManager.sharedInstance.imageAdded = true

                }
            }

            // check navigation
            if CreateManager.sharedInstance.isFillData() {
                // setup next button
                //
                //self_.nextArrow.image = UIImage(named: "next_blue")
                //self_.nextLabel.textColor = Designs.Colors.blueMedium
                self_.btnNext.isEnabled = true
                self_.isAvailable = true
            } else {
                //self_.nextArrow.image = UIImage(named: "next_grey")
                //self_.nextLabel.textColor = Designs.Colors.grayForNextButton
                self_.btnNext.isEnabled = false
                self_.isAvailable = false
            }
        }


        /*

        picker.dismissViewControllerAnimated(true) {
            [weak self] _ in
            guard let self_ = self else {
                return
            }

            var checkHeader: Dictionary<String, AnyObject>
            checkHeader = self_.data[self_.selectedCellIndex - 1] as! Dictionary<String, AnyObject>
            if checkHeader["isCheck"] as! Bool == false {
                checkHeader["isCheck"] = true
                self_.data[self_.selectedCellIndex - 1] = checkHeader
            }

            // if video
            //
            if mediaType == kUTTypeMovie as String {
                //Handle a movie capture
                //
                self_.choosenObject["videoUrl"] = info[UIImagePickerControllerMediaURL]
                self_.choosenObject["isVideo"] = true
                self_.choosenObject["isImage"] = false
                self_.data[self_.selectedCellIndex] = self_.choosenObject

                // remember data
                //
                CreateManager.sharedInstance.media.videoUrl = info[UIImagePickerControllerMediaURL] as? NSURL
                self_.tableView.reloadData()
            } else {
                if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
                    let resizedImage = self_.resizeImage(pickedImage, newWidth: 1024)
                    self_.choosenObject["image"] = resizedImage
                    self_.choosenObject["isImage"] = true
                    self_.choosenObject["isVideo"] = false
                    self_.choosenObject["videoUrl"] = NSURL()

                    // remember data
                    //
                    if self_.selectedCellIndex == CreateManager.sharedInstance.imageIndex {
                        CreateManager.sharedInstance.titleImage = resizedImage
                        if let imageUrl = info[UIImagePickerControllerReferenceURL] {
                            CreateManager.sharedInstance.selectedTitleImageUrl = imageUrl as! NSURL
                        }
                    } else if self_.selectedCellIndex == CreateManager.sharedInstance.mediaIndex {
                        CreateManager.sharedInstance.media.image = resizedImage
                        CreateManager.sharedInstance.media.videoUrl = nil
                        if let imageUrl = info[UIImagePickerControllerReferenceURL] {
                            CreateManager.sharedInstance.media.selectedMediaImageUrl = info[UIImagePickerControllerReferenceURL] as! NSURL
                        }
                    }

                    self_.data[self_.selectedCellIndex] = self_.choosenObject
                    self_.tableView.reloadData()
                }
            }

            if self_.selectedCellIndex == CreateManager.sharedInstance.imageIndex {
                if CreateManager.sharedInstance.imageAdded == false {
                    CreateManager.sharedInstance.imageAdded = true
                    // open header for first media file
                    //

                    var openHeader: Dictionary<String, AnyObject>
                    openHeader = self_.data[self_.insertIndex - 1] as! Dictionary<String, AnyObject>
                    openHeader["isOpen"] = true
                    self_.data[self_.insertIndex - 1] = openHeader

                    let details = ["type" : CellType.ImageOrVideo.rawValue,
                                   "isImage" : false,
                                   "isVideo" : false,
                                   "videoUrl" : NSURL()]

                    self_.tableView.beginUpdates()
                    self_.data.insert(details, atIndex: self_.insertIndex)
                    self_.tableView.insertRowsAtIndexPaths([NSIndexPath(forRow: self_.insertIndex, inSection: 0)], withRowAnimation: UITableViewRowAnimation.Fade)
                    self_.tableView.endUpdates()
                    self_.insertIndex += 2
                    self_.tableView.reloadData()



                }
            } else if self_.selectedCellIndex == CreateManager.sharedInstance.mediaIndex {
                if CreateManager.sharedInstance.mediaAdded == false {
                    // CreateManager.sharedInstance.mediaAdded = true
                    // open header for second media file
                    //

                    var openHeader: Dictionary<String, AnyObject>
                    openHeader = self_.data[self_.insertIndex - 1] as! Dictionary<String, AnyObject>
                    if openHeader["isOpen"] as? Bool == false {
                        openHeader["isOpen"] = true
                        self_.data[self_.insertIndex - 1] = openHeader

                        let details = ["type" : CellType.CoachingTitle.rawValue,
                                       "text" : ""]

                        self_.tableView.beginUpdates()
                        self_.data.insert(details, atIndex: self_.insertIndex)
                        self_.tableView.insertRowsAtIndexPaths([NSIndexPath(forRow: self_.insertIndex, inSection: 0)], withRowAnimation: UITableViewRowAnimation.Fade)
                        self_.tableView.endUpdates()
                        self_.insertIndex += 2
                        self_.tableView.reloadData()
                    }
                }
            }
        }
    */
    }

}

// MARK: CoachingMediaCellDelegate
// =========================================================================================


extension CoachingDetailsVC: CoachingMediaCellDelegate {
    func addMedia(_ sender: AnyObject, cell: AnyObject) {
        let selectCellIndex = tableView.indexPath(for: cell as! CoachingMediaCell)?.row
        selectedCellIndex = selectCellIndex!

        //choosenObject = sender as! Dictionary
        if selectCellIndex == CreateManager.sharedInstance.imageIndex {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.allowsEditing = true
            imagePicker.navigationBar.isTranslucent = false
            imagePicker.navigationBar.barTintColor = UIColor.black

            let alertController = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)

            alertController.addAction(UIAlertAction(title: "Foto auswählen".localized, style: UIAlertActionStyle.default) { action -> Void in
                imagePicker.sourceType = .photoLibrary

                self.present(imagePicker, animated: true, completion: nil)
            })

            alertController.addAction(UIAlertAction(title: "Foto aufnehmen".localized, style: UIAlertActionStyle.default) { action -> Void in
                if UIImagePickerController.isSourceTypeAvailable(.camera) {
                    if UIImagePickerController.availableCaptureModes(for: .rear) != nil {
                        imagePicker.sourceType = .camera



                        self.present(imagePicker, animated: true, completion: nil)
                    }
                }
            })

            alertController.addAction(UIAlertAction(title: "Abbrechen".localized, style: UIAlertActionStyle.cancel) { action -> Void in
                // Cancel

            })

            self.present(alertController, animated: true, completion: nil)
        } else {
            let mediaUI = UIImagePickerController()
            mediaUI.navigationBar.isTranslucent = false
            mediaUI.navigationBar.barTintColor = Designs.Colors.lightBlue

            let alertController = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)

            alertController.addAction(UIAlertAction(title: "Foto oder Video auswählen".localized, style: UIAlertActionStyle.default) { action -> Void in
                if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum) == false {
                    return
                }
                mediaUI.sourceType = .savedPhotosAlbum
                mediaUI.mediaTypes = UIImagePickerController.availableMediaTypes(for: .savedPhotosAlbum)!
                mediaUI.allowsEditing = true
                mediaUI.delegate = self
                self.present(mediaUI, animated: true, completion: nil)
                return
            }
            )

            alertController.addAction(UIAlertAction(title: "Foto oder Video aufnehmen".localized, style: UIAlertActionStyle.default) { action -> Void in
                if UIImagePickerController.isSourceTypeAvailable(.camera) == false {
                    return
                }
                mediaUI.sourceType = .camera
                mediaUI.mediaTypes = UIImagePickerController.availableMediaTypes(for: .camera)!
                mediaUI.allowsEditing = false
                mediaUI.delegate = self

                self.present(mediaUI, animated: true, completion: nil)
                return
            }
            )

            alertController.addAction(UIAlertAction(title: "Abbrechen".localized, style: UIAlertActionStyle.cancel) { action -> Void in
                // Cancel

            })

            self.present(alertController, animated: true, completion: nil)
        }
    }

    func playVideo(_ sender: AnyObject) {
        let moviePlayer = MPMoviePlayerViewController(contentURL: sender as! URL)
        presentMoviePlayerViewControllerAnimated(moviePlayer)
    }
}

// MARK: CoachingTitleCellDelegate
// =========================================================================================


extension CoachingDetailsVC: CoachingTitleCellDelegate {
    func endEnter(_ sender: AnyObject) {
        let text = sender as! String

        if text.characters.count > 0 {
            // check header

            if let h = itemTitleText.header {
                h.isChecked = text.characters.count > 0;
                h.update()
            }

            itemTitleText.text = text;

        }

    }

    func titleTextFieldDidChange(_ isEnableNext: Bool) {
        if CreateManager.sharedInstance.isFillData() {
            //data.count > 7 && CreateManager.sharedInstance.address.isAddressFill() {
            // setup next button
            //
            if isEnableNext {
                //nextArrow.image = UIImage(named: "next_blue")
                //nextLabel.textColor = Designs.Colors.blueMedium
                btnNext.isEnabled = true
                isAvailable = true
            } else {
                //nextArrow.image = UIImage(named: "next_grey")
                //nextLabel.textColor = Designs.Colors.grayForNextButton
                btnNext.isEnabled = false
                isAvailable = false
            }
        } else {
            //nextArrow.image = UIImage(named: "next_grey")
            //nextLabel.textColor = Designs.Colors.grayForNextButton
            btnNext.isEnabled = false
            isAvailable = false
        }
    }
}

// MARK: AddressCellDelegate
// =========================================================================================


extension CoachingDetailsVC: AddressCellDelegate {
    func endEnterAddress(_ open: Bool) {
        if open && CreateManager.sharedInstance.isFillData() {
            //CreateManager.sharedInstance.titleText.characters.count > 0 {
            // setup next button
            //
            //nextArrow.image = UIImage(named: "next_blue")
            //nextLabel.textColor = Designs.Colors.blueMedium
            btnNext.isEnabled = true
            isAvailable = true
        } else {
            //nextArrow.image = UIImage(named: "next_grey")
            //nextLabel.textColor = Designs.Colors.grayForNextButton
            btnNext.isEnabled = false
            isAvailable = false

        }
    }
}

