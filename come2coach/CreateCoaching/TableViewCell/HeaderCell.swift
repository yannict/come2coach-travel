//
//  HeaderCell.swift
//  come2coach
//
//  Created by Alex Kupchak on 10/27/16.
//  Copyright © 2016 MaytecNet. All rights reserved.
//

import UIKit
import Foundation

class HeaderCell: UITableViewCell {

    @IBOutlet weak var numderLabel: UILabel!
    @IBOutlet weak var numberView: UIView!
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var openCheckImage: UIImageView!
    var callBack : CellHeaderCallback!
    var ddata : DisplayDataItem!
    @IBOutlet weak var cView: UIView!
    
    @IBAction func headerClicked(_ sender: AnyObject) {

        if let c = callBack {
            callBack(ddata)
        }

    }

    override func awakeFromNib() {
        super.awakeFromNib()
        numberView.layer.masksToBounds = true
        numberView.layer.borderWidth = 0.0
        numberView.layer.cornerRadius = 9.0
        numberView.layer.borderColor = UIColor.white.cgColor
        prepareForReuse()
    }

    override func prepareForReuse() {
        contentLabel?.text = ""
        numderLabel?.text = ""
    }
    
    // MARK: Initializers
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }


    
    
    func setDisplayData( _ d: DisplayDataItem) {

        ddata = d;

        contentLabel.text = d.text
        numderLabel.text = d.number

        callBack = d.callback

        if d.isOpen {
            numberView.backgroundColor = Designs.Colors.blueMedium
        } else {
            numberView.backgroundColor = UIColor(netHex: 0x969696)
        }

        if d.isChecked {
            openCheckImage.isHidden = false
        } else {
            openCheckImage.isHidden = true
        }
    }
    
    func setData(_ dictionary: Dictionary<String, AnyObject>) -> Void {
        contentLabel.text = dictionary["text"] as? String
        numderLabel.text = dictionary["number"] as? String
        
        let isOpen: Bool = dictionary["isOpen"] as! Bool
        let isCheck: Bool = dictionary["isCheck"] as! Bool
        callBack = dictionary["callback"] as! CellHeaderCallback
        
        if isOpen {
            numberView.backgroundColor = Designs.Colors.blueMedium
        } else {
            numberView.backgroundColor = UIColor(netHex: 0x969696)
        }
        
        if isCheck {
            openCheckImage.isHidden = false
        } else {
            openCheckImage.isHidden = true
        }
    }


    
}
