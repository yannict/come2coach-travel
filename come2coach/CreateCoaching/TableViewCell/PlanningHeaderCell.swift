//
//  PlanningHeaderCell.swift
//  come2coach
//
//  Created by Alex Kupchak on 10/31/16.
//  Copyright © 2016 MaytecNet. All rights reserved.
//

import UIKit

class PlanningHeaderCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(_ dictionary: Dictionary<String, AnyObject>) {
        titleLabel.text = dictionary["title"] as? String
    }
}
