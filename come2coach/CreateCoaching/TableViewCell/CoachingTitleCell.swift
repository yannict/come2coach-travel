//
//  CoachingTitleCell.swift
//  come2coach
//
//  Created by Alex Kupchak on 10/28/16.
//  Copyright © 2016 MaytecNet. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


protocol CoachingTitleCellDelegate: class {
    func endEnter(_ sender: AnyObject)
    func titleTextFieldDidChange(_ isEnableNext: Bool)
}

class CoachingTitleCell: UITableViewCell {
    weak var delegate: CoachingTitleCellDelegate?
    
    @IBOutlet weak var titleTextField: PaddedTextField!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        titleTextField.addTarget(self, action: #selector(NewProfilePasswordVC.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setDisplayData(_ d: DisplayDataItem) {
        titleTextField.text = d.text
        titleTextField.delegate = self

    }

    func setData(_ dictionary: Dictionary<String, AnyObject>) -> Void {
        titleTextField.text = dictionary["text"] as? String
        titleTextField.delegate = self
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidChange(_ textField: UITextField) {
        if textField == titleTextField {
            CreateManager.sharedInstance.titleText = textField.text!
            if textField.text?.characters.count > 0 {
                delegate?.titleTextFieldDidChange(true)
            } else {
                delegate?.titleTextFieldDidChange(false)
            }
        }
    }
}

extension CoachingTitleCell: UITextFieldDelegate {
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.layoutIfNeeded()
        if textField.text?.characters.count > 0 {
            delegate?.endEnter(textField.text! as AnyObject)
        } else {
            delegate?.endEnter("" as AnyObject)
        }
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        
        let newLength = text.characters.count + string.characters.count - range.length
        return newLength <= 30
    }
    
}
