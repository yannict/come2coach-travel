//
//  PlanningAgeRestrictionCell.swift
//  come2coach
//
//  Created by Alex Kupchak on 10/31/16.
//  Copyright © 2016 MaytecNet. All rights reserved.
//

import UIKit

protocol PlanningAgeRestrictionCellDelegate: class {
    func showPicker()
}


class PlanningAgeRestrictionCell: UITableViewCell {
    weak var delegate: PlanningAgeRestrictionCellDelegate?
    
    @IBOutlet weak var restrictionLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setData(_ dictionary: Dictionary<String, AnyObject>) {
        restrictionLabel.text = dictionary["restriction"] as? String
    }
    
    @IBAction func clickRestrictionButton(_ sender: AnyObject) {
        delegate?.showPicker()
    }
   
}
