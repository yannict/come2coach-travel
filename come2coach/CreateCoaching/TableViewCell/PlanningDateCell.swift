//
//  PlanningDateCell.swift
//  come2coach
//
//  Created by Alex Kupchak on 10/31/16.
//  Copyright © 2016 MaytecNet. All rights reserved.
//

import UIKit

protocol PlanningDateCellDelegate: class {
    func addNewTime(_ sender: AnyObject)
    func removeTime(_ sender: AnyObject)

    func showDatePicker(_ isDuration: Bool, indexPath: IndexPath)
}


class PlanningDateCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var addImageView: UIImageView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var addButton: UIButton!
    
    var isDefault: Bool = true
    var isDuration: Bool = false
    weak var delegate: PlanningDateCellDelegate?
    var indexPath: IndexPath = IndexPath()
    
    var dictionary: Dictionary<String, AnyObject> = [:]

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        addButton.isHidden = true
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setData(_ dictionary: Dictionary<String, AnyObject>, isDefault:Bool = true, isDuration:Bool = false, indexPath: IndexPath) {

        // #beginchange
        addImageView.isHidden = true
        addButton.isHidden = true

        self.dictionary = dictionary
        self.isDefault = isDefault
        self.indexPath = indexPath
        self.isDuration = isDuration
        
        titleLabel.text = dictionary["title"] as? String
        timeLabel.text = dictionary["time"] as? String
        dateLabel.text = dictionary["date"] as? String
        
        if isDuration {
            addImageView.isHidden = true
            addButton.isHidden = true
        } else {
            //addImageView.isHidden = false
            // addButton.isHidden = false
            // #beginchange
            addButton.isHidden = true

            /*
            #beginchange
            if isDefault {
                addImageView.image = UIImage(named: "add_time")
            } else {
                addImageView.image = UIImage(named: "remove_time")
            }
            */
        }
    }
    
    @IBAction func clickDateButton(_ sender: AnyObject) {
        delegate?.showDatePicker(isDuration, indexPath: self.indexPath)
    }
    
    @IBAction func clickTimeButton(_ sender: AnyObject) {
    }

    @IBAction func clickAddButton(_ sender: AnyObject) {
        if isDefault {
            if CreateManager.sharedInstance.currentBeginIndex < 3 {
                // add new
                delegate?.addNewTime(sender)
            }
        } else {
            // delete
            delegate?.removeTime(self)
        }
    }
}
