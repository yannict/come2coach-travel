//
//  AddressCell.swift
//  come2coach
//
//  Created by Alex Kupchak on 10/28/16.
//  Copyright © 2016 MaytecNet. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


protocol AddressCellDelegate: class {
    func endEnterAddress(_ open: Bool)
}

class AddressCell: UITableViewCell {
    weak var delegate: AddressCellDelegate?

    @IBOutlet weak var addressLabel: UILabel!
    
    @IBOutlet weak var locationTextField: PaddedTextField!
    @IBOutlet weak var streetTextField: PaddedTextField!
    @IBOutlet weak var cityTextField: PaddedTextField!
    @IBOutlet weak var zipTextField: PaddedTextField!
    
    var dictionary: Dictionary<String, AnyObject> = [:]
    var isLocation = false
    var isStreet = false
    var isCity = false
    var isZip = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        locationTextField.delegate = self
        streetTextField.delegate = self
        cityTextField.delegate = self
        zipTextField.delegate = self
        
        zipTextField.keyboardType = UIKeyboardType.numberPad
        
        let backgroundTap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        contentView += backgroundTap
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func dismissKeyboard() {
        locationTextField.resignFirstResponder()
        streetTextField.resignFirstResponder()
        cityTextField.resignFirstResponder()
        zipTextField.resignFirstResponder()
    }

    func setDisplayData( _ d: DisplayDataItem) {
        locationTextField.text = CreateManager.sharedInstance.address.location //ditctionary["location"] as? String
        streetTextField.text = CreateManager.sharedInstance.address.street //ditctionary["street"] as? String
        cityTextField.text = CreateManager.sharedInstance.address.city //ditctionary["city"] as? String
        zipTextField.text = CreateManager.sharedInstance.address.zip > 0 ? String(CreateManager.sharedInstance.address.zip) : "" //ditctionary["zip"] as? String

        checkEmptyAddress()

    }
    func setData(_ ditctionary: Dictionary<String, AnyObject>) {
        //        self.dictionary = ["type" : CellType.Address.rawValue,
        //                           "location" : CreateManager.sharedInstance.address.location,
        //                           "street" : CreateManager.sharedInstance.address.street,
        //                           "city" : CreateManager.sharedInstance.address.city,
        //                           "zip" : CreateManager.sharedInstance.address.zip]
        
        locationTextField.text = CreateManager.sharedInstance.address.location //ditctionary["location"] as? String
        streetTextField.text = CreateManager.sharedInstance.address.street //ditctionary["street"] as? String
        cityTextField.text = CreateManager.sharedInstance.address.city //ditctionary["city"] as? String
        zipTextField.text = CreateManager.sharedInstance.address.zip > 0 ? String(CreateManager.sharedInstance.address.zip) : "" //ditctionary["zip"] as? String
        
        checkEmptyAddress()
    }
    
    func checkEmptyAddress() {
        if CreateManager.sharedInstance.address.location.characters.count > 0 {
            isLocation = true
        } else {
            isLocation = false
        }
        if CreateManager.sharedInstance.address.street.characters.count > 0 {
            isStreet = true
        } else {
            isStreet = false
        }
        if CreateManager.sharedInstance.address.city.characters.count > 0 {
            isCity = true
        } else {
            isCity = false
        }
        if CreateManager.sharedInstance.address.zip > 0 {
            isZip = true
        } else {
            isZip = false
        }
        
        if isLocation && isStreet && isCity && isZip {
            delegate?.endEnterAddress(true)
        } else {
            delegate?.endEnterAddress(false)
        }
    }
}

extension AddressCell: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        //        if textField.text?.characters.count > 0 {
        //            delegate?.endEnter(textField.text!)
        //        } else {
        //            delegate?.endEnter("")
        //        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == zipTextField {
            let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            if Int(text) != nil {
                // Text field converted to an Int
                return true
            } else if text == "" {
                // Text field is not an Int
                return true
            } else {
                return false
            }
        }
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.layoutIfNeeded()
        
        if textField == locationTextField {
            //dictionary["location"] = textField.text
            CreateManager.sharedInstance.address.location = textField.text!
        } else if textField == streetTextField {
            //dictionary["street"] = textField.text
            CreateManager.sharedInstance.address.street = textField.text!
        } else if textField == cityTextField {
            //dictionary["city"] = textField.text
            CreateManager.sharedInstance.address.city = textField.text!
        } else if textField == zipTextField {
            //dictionary["zip"] = textField.text
            CreateManager.sharedInstance.address.zip = textField.text?.characters.count > 0 ? Int(textField.text!)! : 0
        }
        
        checkEmptyAddress()
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return true
    }
}
