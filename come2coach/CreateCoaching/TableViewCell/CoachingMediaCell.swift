//
//  CoachingMediaCell.swift
//  come2coach
//
//  Created by Alex Kupchak on 10/27/16.
//  Copyright © 2016 MaytecNet. All rights reserved.
//

import UIKit
import Player

protocol CoachingMediaCellDelegate: class {
    func addMedia(_ sender: AnyObject, cell: AnyObject)

    func playVideo(_ sender: AnyObject)
}

class CoachingMediaCell: UITableViewCell {

    @IBOutlet weak var contentImageView: UIImageView!
    @IBOutlet weak var addMediaImageView: UIImageView!
    @IBOutlet weak var addMediaButton: UIButton!
    @IBOutlet weak var panelView: UIView!

    @IBOutlet weak var playVideoButton: UIButton!
    @IBOutlet weak var playImageView: UIImageView!

    @IBOutlet weak var retakeButton: UIButton!

    
    @IBOutlet weak var cView: UIView!
    
    
    
    @IBOutlet weak var reSelectVideoBtn: UIButton!
    weak var delegate: CoachingMediaCellDelegate?
    var dictionary: Dictionary<String, AnyObject> = [:]
    var player: Player?
    var lastVideoURL = String()

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    internal func getInContentPlayer(_ url: URL) -> Player {

        let p = Player()

        let playerTapped = UITapGestureRecognizer(target: self, action: #selector(CoachingMediaCell.playerTapped))
        playerTapped.numberOfTapsRequired = 1

        let playerDblTapped = UITapGestureRecognizer(target: self, action: #selector(CoachingMediaCell.playerDblTapped))
        playerDblTapped.numberOfTapsRequired = 2

        p.view.frame = cView.bounds //self.bounds
        p.view.addGestureRecognizer(playerTapped)
        p.view.addGestureRecognizer(playerDblTapped)

        //self.addChildViewController(p)
        cView.addSubview(p.view)
        //self.addSubview(p.view)

        //p.didMoveToParentViewController(self)
        p.fillMode = "AVLayerVideoGravityResizeAspectFill"
        //p.setUrl(url)
        p.url = url
        p.playbackResumesWhenEnteringForeground = false

        return p;
    }

    func playerDblTapped() {
        if let p = player {
            p.pause()
        }

        delegate?.addMedia(self.dictionary as AnyObject, cell: self)
    }

    func playerTapped() {
        if let p = player {
            if (p.playbackState == .paused) {
                p.playFromCurrentTime()
            } else if (p.playbackState == .stopped) {
                p.playFromBeginning()
            } else if (p.playbackState == .playing) {
                p.pause()
            }
        }

    }

    @IBAction func reSelectVideo(_ sender: AnyObject) {
        playerDblTapped()
    }

    func setDisplayData(_ data: DisplayDataItem) {

        // When cell is image or video
        //
        if data.isVideo {

            retakeButton.isHidden = true
            reSelectVideoBtn.isHidden = false
            contentImageView.isHidden = true
            addMediaImageView.isHidden = true
            addMediaButton.isHidden = true
            playImageView.isHidden = true
            playVideoButton.isHidden = true

            if (player == nil) {
                player = getInContentPlayer(data.VideoUrl!)
                lastVideoURL = data.VideoUrl!.description

                cView.sendSubview(toBack: player!.view)
                cView.bringSubview(toFront: reSelectVideoBtn)


                player?.playFromBeginning()
            } else {
                player?.view.isHidden = false

                if (lastVideoURL != data.VideoUrl!.description) {
                    player?.stop()
                    //player?.setUrl(data.VideoUrl!)
                    player?.url = data.VideoUrl!


                    cView.sendSubview(toBack: player!.view)
                    cView.bringSubview(toFront: reSelectVideoBtn)
                    player?.playFromBeginning()
                }
            }


            /*

            self.sendSubviewToBack(reSelectVideoBtn)
            self.sendSubviewToBack(player!.view)
            self.sendSubviewToBack(retakeButton)
            self.sendSubviewToBack(contentImageView)
            */



            /*

            previous code before in-content video player

            addMediaImageView.hidden = true
            addMediaButton.hidden = true
            playImageView.hidden = false
            playVideoButton.hidden = false
            let image = videoSnapshot( data.VideoUrl! )
            CreateManager.sharedInstance.media.previewImage = image!
            data.image = image
            contentImageView.image = image
            */


        } else if data.isImage {

            if let p = player {
                // hide video player in case it's an image
                p.stop()
                p.view.isHidden = true
            }
            retakeButton.isHidden = false
            reSelectVideoBtn.isHidden = true
            contentImageView.isHidden = false
            contentImageView.image = data.image
            addMediaImageView.isHidden = true
            addMediaButton.isHidden = true
            playImageView.isHidden = true
            playVideoButton.isHidden = true
        } else {

            if let p = player {
                // hide video player in case it's an image
                p.stop()
                p.view.isHidden = true
            }
            retakeButton.isHidden = false
            reSelectVideoBtn.isHidden = true
            contentImageView.isHidden = false
            playImageView.isHidden = true
            playVideoButton.isHidden = true
            addMediaImageView.isHidden = false
            addMediaButton.isHidden = false
        }
        return


        // When cell is title image
        //
        playImageView.isHidden = true
        playVideoButton.isHidden = true

        if data.isImage {
            addMediaImageView.isHidden = true
            addMediaButton.isHidden = true
        } else {
            addMediaImageView.isHidden = false
            addMediaButton.isHidden = false
        }
        contentImageView.image = data.image
    }


    func setData(_ dictionary: Dictionary<String, AnyObject>) -> Void {
        self.dictionary = dictionary
        let isImage = dictionary["isImage"] as! Bool

        if let isVideo = dictionary["isVideo"] {
            // When cell is image or video
            //
            let isVideo = isVideo as! Bool

            if isVideo {
                addMediaImageView.isHidden = true
                addMediaButton.isHidden = true
                playImageView.isHidden = false
                playVideoButton.isHidden = false
                //let image = videoSnapshot((dictionary["videoUrl"] as? URL)!)
                //CreateManager.sharedInstance.media.previewImage = image!
                //self.dictionary["image"] = image
                //contentImageView.image = image
            } else if isImage {
                contentImageView.image = dictionary["image"] as? UIImage
                addMediaImageView.isHidden = true
                addMediaButton.isHidden = true
                playImageView.isHidden = true
                playVideoButton.isHidden = true
            } else {
                playImageView.isHidden = true
                playVideoButton.isHidden = true
                addMediaImageView.isHidden = false
                addMediaButton.isHidden = false
            }
            return
        }

        // When cell is title image
        //
        playImageView.isHidden = true
        playVideoButton.isHidden = true

        if isImage {
            addMediaImageView.isHidden = true
            addMediaButton.isHidden = true
        } else {
            addMediaImageView.isHidden = false
            addMediaButton.isHidden = false
        }
        contentImageView.image = dictionary["image"] as? UIImage
    }

    @IBAction func clickAddMedia(_ sender: AnyObject) {
        delegate?.addMedia(self.dictionary as AnyObject, cell: self)
    }

    @IBAction func clickPlayVideo(_ sender: AnyObject) {
        delegate?.playVideo(dictionary["videoUrl"]!)
    }

    @IBAction func retakePhotoOrVideo(_ sender: AnyObject) {
        delegate?.addMedia(self.dictionary as AnyObject, cell: self)
    }

    func videoSnapshot(_ filePathLocal: URL) -> UIImage? {
        let vidURL = filePathLocal as URL
        let asset = AVURLAsset(url: vidURL)
        let generator = AVAssetImageGenerator(asset: asset)
        generator.appliesPreferredTrackTransform = true

        let timestamp = CMTime(seconds: 1, preferredTimescale: 60)

        do {
            let imageRef = try generator.copyCGImage(at: timestamp, actualTime: nil)
            return UIImage(cgImage: imageRef)
        } catch let error as NSError {
            print("Image generation failed with error \(error)")
            return nil
        }
    }
}
