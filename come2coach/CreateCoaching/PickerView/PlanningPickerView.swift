//
//  PlanningPickerView.swift
//  come2coach
//
//  Created by Alex Kupchak on 11/2/16.
//  Copyright © 2016 MaytecNet. All rights reserved.
//

import UIKit


protocol PlanningPickerViewDelegate: class {
    func cancelPicker()
    func donePicker(_ date: Date)
    func donePickerFromRestriction(_ ageRestriction: String)
}

class PlanningPickerView: UIView {
    
    var isDuration = false
    var selectedRestriction = "keine".localized
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    
    weak var delegate: PlanningPickerViewDelegate?
    
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var pickerView: UIPickerView!
    
    var data: Array<AnyObject> = []
    // var dataDuration = []
    var isDate: Bool = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    @IBAction func clickCancel(_ sender: AnyObject) {
        delegate?.cancelPicker()
    }
    
    @IBAction func clickDone(_ sender: AnyObject) {
        if isDate {
            /*
            if (isDuration && NSDate.minutesFrom(datePicker.date) < 15)
            {
                NSDate()
            }
            */

            delegate?.donePicker(datePicker.date)
        } else {
            delegate?.donePickerFromRestriction(selectedRestriction)
        }
    }
    
    //    required init(frame: CGRect, isDate: Bool) {
    //        super.init(frame: frame)
    //
    //        self.isDate = isDate
    //    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
    internal override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    func show(_ isDate:Bool, isDuration: Bool) {
        self.isDuration = isDuration
        self.isDate = isDate
        if isDate {
            datePicker.isHidden = false
            pickerView.isHidden = true
            if isDuration {

                let dateFormatter = DateFormatter();
                dateFormatter.dateFormat = "hh:mm";

                datePicker.datePickerMode = UIDatePickerMode.time
                datePicker.minimumDate = dateFormatter.date(from: "00:15") ;
                datePicker.minuteInterval = 15
                datePicker.date = Date.makeDate(CreateManager.sharedInstance.duration, dateFormat: "HH:mm")!
            } else {
                datePicker.datePickerMode = UIDatePickerMode.dateAndTime
                datePicker.minimumDate = Date()
                datePicker.date = CreateManager.sharedInstance.begin as Date
            }
        } else {
            self.pickerView.isHidden = false
            self.datePicker.isHidden = true
            data = ["keine".localized as AnyObject, "ab 14 Jahren".localized as AnyObject, "ab 16 Jahren".localized as AnyObject, "ab 18 Jahren".localized as AnyObject]
            
            //            for i in 0...21 {
            //                self.data.append(String(i))
            //            }
            
            pickerView.delegate = self
            pickerView.reloadAllComponents()
            //pickerView.selectRow(0, inComponent: 0, animated: true)
        }
    }
}

extension PlanningPickerView: UIPickerViewDelegate, UIPickerViewDataSource {
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return data.count
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1;
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedRestriction = data[row] as! String
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let value = data[row] as! String
        return value
    }
}
