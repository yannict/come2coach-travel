//
//  CreateManager.swift
//  come2coach
//
//  Created by Alex Kupchak on 11/1/16.
//  Copyright © 2016 MaytecNet. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import MRProgress
import SVProgressHUD

struct Media {

    var videoUrl: URL!
    var previewImage = UIImage()

    var image: UIImage?
    var selectedMediaImageUrl: URL!
}

class MediaMetaItem {

    var remoteName = ""
    var localName: URL!
    var contentType = "image/jpeg"

    // Remove locally stored image
    func remove() {
        do {
            try FileManager.default.removeItem(at: localName)
        } catch {
            OnDebugging.print(error)
        }
    }
}

class MediaMetaData {

    var changed = false
    var type = "image"
    var create1X = true
    var create2X = true
    var create3X = true
    var filePrefix: String
    var remoteFolder = "event"

    init(_ prefix: String) {
        filePrefix = prefix
    }


    var originalImage = UIImage()
    var videoURL: URL!

    // Get Video
    func getVideo() -> MediaMetaItem {

        let result = MediaMetaItem()
        result.localName = videoURL
        result.remoteName = filePrefix + "-video.mov"
        result.contentType = "movie/mov"
        return result
    }

    // Get Thumb Nail for Collection/Table Views
    func getThumbNail() -> MediaMetaItem {

        let thumb = ImageTools.resizeImage(originalImage, newValue: 180)
        let result = MediaMetaItem()
        result.localName = local(filePrefix + "-1x.jpg", image: thumb)
        result.remoteName = filePrefix + "-1x.jpg"
        return result

    }

    func getEventDetailImage() -> MediaMetaItem {

        let thumb = ImageTools.resizeImage(originalImage, newValue: 300)

        let result = MediaMetaItem()
        result.localName = local(filePrefix + "-2x.jpg", image: thumb)
        result.remoteName = filePrefix + "-2x.jpg"
        return result
    }

    func getOriginal() -> MediaMetaItem {

        let result = MediaMetaItem()
        result.localName = local(filePrefix + "-org.jpg", image: originalImage)
        result.remoteName = filePrefix + "-org.jpg"
        return result

    }

    // Store image locally and returns URL
    func local(_ fileName: String, image: UIImage) -> URL {

        let fileURL = URL(fileURLWithPath: NSTemporaryDirectory() + fileName)
        let data = UIImageJPEGRepresentation(image, 0.6)
        try? data!.write(to: fileURL, options: [.atomic])

        return fileURL
    }

}


struct DetailAddress {
    var street = String()
    var location = String()
    var city = String()
    var zip = Int()
    var geoX = CGFloat()
    var geoY = CGFloat()

    func isStreetFill() -> Bool {
        return street.characters.count > 0
    }

    func isLocationFill() -> Bool {
        return location.characters.count > 0
    }

    func isCityFill() -> Bool {
        return city.characters.count > 0
    }

    func isZipFill() -> Bool {
        return zip > 0
    }

    func isAddressFill() -> Bool {
        if isStreetFill() && isLocationFill() && isCityFill() && isZipFill() {
            return true
        }
        return false
    }
}

class CreateManager {

    var closeEvent: (() -> Void)?

    // Event class (will be changed later)
    //

    var mode = "normal"

    var sender: EventDetailsVC?

    var isRepublishing: Bool = false

    var recordedFile: String = ""

    var eventId: Int?
    var isOnline: Bool = true

    // Image Data
    var img1 = MediaMetaData("img1")
    var existingImg1 = String()
    var existingImg1Folder = String()
    var existingImg2 = String()
    var existingImg2Folder = String()
    var existingVideo = String()


    // var media = Media()
    var img2 = MediaMetaData("img2")

    var titleText = String()
    var description = "My description"
    var address = DetailAddress()
    var minParticipant = 5
    var maxParticipant = 25
    var price = 25
    var ageRestriction = 0
    var duration = "1:30"
    var begin = Date()
    var begin1: Date?
    var begin2: Date?
    var isVideo = false
    var handoutURL = ""
    var handoutDescr = ""
    var affiliateURL = ""
    var affiliateDescr = ""

    var detailsData: Array<AnyObject> = []
    var planningData: Array<AnyObject> = []
    var imageAdded = false
    var mediaAdded = false
    var insertIndex = Int()

    let imageIndex = 1
    let mediaIndex = 3

    var currentBeginIndex = 1


    internal func getExistingImg1URL() -> String {
        return "https://c2c-app.s3.amazonaws.com/" + existingImg1
    }

    internal func getExistingImg2URL() -> String {
        return "https://c2c-app.s3.amazonaws.com/" + existingImg2
    }

    class var sharedInstance: CreateManager {

        struct Static {
            static let instance = CreateManager()
        }

        return Static.instance
    }

    /*
        Validate if event has all information
    */
    func isFillData() -> Bool {

        return titleText.characters.count > 0 && description.characters.count > 0 && imageAdded

        /*
        if !isOnline {
            if address.isAddressFill() && titleText.characters.count > 0 && description.characters.count > 0 && imageAdded {
                return true
            }
        } else {
            if titleText.characters.count > 0 && description.characters.count > 0 && imageAdded {
                return true
            }
        }

        return false
        */
    }

    func increaseCurrentBeginIndex() {
        currentBeginIndex += 1
    }

    func decreaseCurrentBeginIndex() {
        currentBeginIndex -= 1
    }

    func resetData() {
        isRepublishing = false
        sender = nil
        mode = "normal"

        eventId = nil
        recordedFile = ""
        isOnline = true
        img1 = MediaMetaData("img1")
        // selectedTitleImageUrl = nil  #deprecated
        // isTitleImage = Bool()  #deprecated

        img2 = MediaMetaData("img2")
        // media = Media()

        existingImg1 = String()
        existingImg1Folder = String()
        existingImg2 = String()
        existingImg2Folder = String()
        existingVideo = String()

        titleText = String()
        description = String()
        address = DetailAddress()
        minParticipant = 5
        maxParticipant = 25
        price = 25
        ageRestriction = 0
        duration = "1:30"
        begin = Date()
        begin1 = nil
        begin2 = nil
        detailsData = []
        planningData = []
        imageAdded = false
        mediaAdded = false
        insertIndex = Int()

        handoutURL = ""
        handoutDescr = ""
        affiliateURL = ""
        affiliateDescr = ""

        // S3
        //
        existingImg1 = String()
        existingImg2 = String()
    }

    func closeCreation() {
        // close all controllers
    }

    func createEvent(_ handler: @escaping (_ success: Bool, _ eventId: Int) -> Void) {
        // Production
        //
        
        var url = URL(string: HTTPRequest.rootUrl + "event")!

        if let eid = eventId {
            if (mode == "edit") {
                url = URL(string: HTTPRequest.rootUrl + "event/" + String(eid))!
            }
        }

        let headers = [
                "Token": Constants.API.accessToken,
                "Content-Type": "application/json",
                "Authorization": "Bearer \(JSONWebToken.sharedInstance.value)"
        ]

        var parameters: [String: Any] = [:]

        parameters = [
                "title": titleText as AnyObject,
                "description": description as AnyObject,
                "webinar": isOnline ? "1" : "0", //  Int(isOnline), #swift3
                "live": isOnline ? 1 : 0,
                "price": price,
                "min_part": minParticipant,
                "max_part": maxParticipant,
                "begin": begin.makeDateStringWith(), //"2016-01-01 14:30:00",
                "duration": duration,
                "adult": ageRestriction,
                "public": 1,
                "img1": img1.remoteFolder,
                "img2": img2.remoteFolder,
                "handout_url": handoutURL,
                "handout_desc": handoutDescr,
                "ext_affiliate_url": affiliateURL,
                "ext_affiliate_desc": affiliateDescr
        ]

        /*

        parameters = [
                "title": titleText as AnyObject,
                "cat1": "gaming" as AnyObject,
                "description": description as AnyObject,
                "text_do": "Lorem ipsum." as AnyObject, // req
                "text_learn": "Lorem ipsum." as AnyObject, // req
                "tags": [// req
                         "computer",
                         "games"
                ],
                "webinar": isOnline ? "1" : "0", //  Int(isOnline), #swift3
                "live": isOnline ? "1" : "0",
                "price": price,
                "min_part": minParticipant,
                "max_part": maxParticipant,

                "begin": begin.makeDateStringWith(), //"2016-01-01 14:30:00",
                "duration": duration,
                "adult": ageRestriction,

                "public": 1,
                "img1": titleImageS3Url,
                "handout_url": handoutURL,
                "handout_desc": handoutDescr,
                "ext_affiliate_url": affiliateURL,
                "ext_affiliate_desc": affiliateDescr
        ] */


        /*
        if mediaS3Url != "" {
            parameters["img2"] = mediaS3Url as AnyObject?
        }

        if isVideo {
            parameters["video"] = mediaS3Url as AnyObject?
        }
        */

        if (img2.type == "video") {
            parameters["video"] = img2.getVideo().remoteName
        } else if (img2.changed == false && !existingVideo.isEmpty) {
            parameters["video"] = existingVideo
        }



        if (!recordedFile.isEmpty) {
            parameters["img3"] = recordedFile
        } else {

            if let eid = eventId {
                if (mode == "repub") {
                    parameters["img3"] = "c2c\(eid).flv"
                }
            }
        }


        /*

        if begin1 != nil {
            parameters["begin2"] = begin1!.makeDateStringWith()
        }

        if begin2 != nil {
            parameters["begin3"] = begin2!.makeDateStringWith()
        }
        */

        /*
        if isOnline == false {
            parameters["address"] = [
                    "location": address.location,
                    "street": address.street,
                    "city": address.city,
                    "zip": address.zip,
                    "geo_x": address.geoX,
                    "geo_y": address.geoY
            ]
        }
        */

        Alamofire
                .request(url,
                        method: .post,
                        parameters: parameters,
                        encoding: JSONEncoding.default,
                        headers: headers)
                .validate()
                .debugRequest(url.absoluteString)
                .responseJSON { response in

                    OnDebugging.print("serverResponseStatusCode: \(response.response?.statusCode)")

                    switch response.result {
                    case .failure(let error):
                        OnDebugging.print(error.localizedDescription)


                        if let r = response.result.value {

                            debugPrint(r)

                        }

                        handler(false, -1)

                    case .success:

                        guard let value = response.result.value else {
                            OnDebugging.print("response.result.value == nil")
                            return
                        }

                        let json = JSON(value)

                        debugPrint("HTTP Result")
                        debugPrint(json)

                        let responseDictionary = json.dictionaryValue

                        if let eventId = responseDictionary["id"]?.intValue {
                            //self.eventId = eventId
                            handler(true, eventId)
                            OnDebugging.print(self.eventId)
                        } else if let responseErrors = responseDictionary["errors"]?.dictionaryValue {
                            // posible error
                            OnDebugging.print("Error - \(responseErrors)")
                            handler(false, -1)
                        } else {
                            handler(false, -1)
                        }
                    }
                }
    }


    /*
        Upload img1 to backend (S3)
    */

    func uploadImages(_ media: MediaMetaData, _ view: MRProgressOverlayView?, completion: @escaping (_ success: Bool) -> Void) {


        if (media.type == "video") {

            uploadImage(media.getVideo(), view, media.remoteFolder) { (success) in

                completion(success)
            }

        } else {

            // store all images locally

            if (media.create1X) {

                let low = media.getThumbNail()

                // upload thumbnail
                uploadImage(low, view, media.remoteFolder) { (success) in

                    if (success) {

                        if (media.create2X) {

                            // upload medium size
                            let medium = media.getEventDetailImage()

                            self.uploadImage(medium, view, media.remoteFolder) { (success2) in


                                if (success2) {

                                    if (media.create3X) {

                                        // upload high size

                                        let high = media.getOriginal()

                                        self.uploadImage(high, view, media.remoteFolder) { (success3) in

                                            completion(success3)

                                        }

                                    } else {
                                        completion(true)
                                    }


                                } else {
                                    completion(false)
                                }

                            }

                        } else {
                            completion(true)
                        }

                    } else {
                        completion(false)
                    }
                }
            } else {
                completion(true)
            }
        }
    }


    func uploadImage(_ media: MediaMetaItem, _ overlayView: MRProgressOverlayView?, _ folder: String, completion: @escaping (_ success: Bool) -> Void) {

        // Progress view
        //
        //let overlayView = MRProgressOverlayView.showOverlayAdded(to: view, title: "Bitte warten....".localized, mode: MRProgressOverlayViewMode.indeterminate, animated: true)

        S3Manager.uploadFileToS3(folder + "/" + media.remoteName,
                generatedImageUrl: media.localName,
                contentType: media.contentType,
                progress: { (totalBytesSent, totalBytesExpectedToSend) in
                    // Prepare for progress view
                    //
                    if overlayView?.mode != MRProgressOverlayViewMode.determinateCircular {
                        overlayView?.mode = MRProgressOverlayViewMode.determinateCircular
                        overlayView?.titleLabelText = "Übertrage Daten...".localized
                    }
                    let progress = (Float(totalBytesSent) / Float(totalBytesExpectedToSend))
                    debugPrint(progress)

                    overlayView?.setProgress(progress, animated: true)
                }) { (success, s3URL) in
            if success {

                /*
                DispatchQueue.main.async {
                    MRProgressOverlayView.dismissOverlay(for: view, animated: true)
                }
                */

                // Remove locally stored file
                //
                media.remove()

            }

            completion(success)
        }
    }
}
