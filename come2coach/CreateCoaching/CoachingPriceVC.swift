//
//  CoachingPriceVC.swift
//  come2coach
//
//  Created by Alex Kupchak on 10/28/16.
//  Copyright © 2016 MaytecNet. All rights reserved.
//

import Foundation

let euro = "€"

class CoachingPriceVC: UIViewController {
    var minValue: Int = 5
    var maxValue: Int = 25
    var price: Int = 25
    
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var increasePriceButton: UIButton!
    
    @IBOutlet weak var minValueLabel: UILabel!
    @IBOutlet weak var maxValueLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!

    override func viewDidLoad() {


        minValue = CreateManager.sharedInstance.minParticipant
        maxValue = CreateManager.sharedInstance.maxParticipant
        price = CreateManager.sharedInstance.price
        
        minValueLabel.text = String(minValue)
        maxValueLabel.text = String(maxValue)
        priceLabel.text = String(price) + euro
    }
    
    // MARK: Actions
    // =========================================================================================
    
    @IBAction func clickCloseButton(_ sender: AnyObject) {
//        CreateManager.sharedInstance.closeCreation()
        dismiss(animated: true, completion: { [weak self] _ in
            guard let self_ = self else {return}
            CreateManager.sharedInstance.minParticipant = self_.minValue
            CreateManager.sharedInstance.maxParticipant = self_.maxValue
            CreateManager.sharedInstance.price = self_.price
            })
    }
    
    @IBAction func clickBackButton(_ sender: AnyObject) {
        self.navigationController?.popViewController(animated: false, completion: { [weak self] _ in
            guard let self_ = self else {return}
            CreateManager.sharedInstance.minParticipant = self_.minValue
            CreateManager.sharedInstance.maxParticipant = self_.maxValue
            CreateManager.sharedInstance.price = self_.price
            })
//        dismissViewControllerAnimated(false, completion: { [weak self] _ in
//            guard let self_ = self else {return}
//            CreateManager.sharedInstance.minParticipant = self_.minValue
//            CreateManager.sharedInstance.maxParticipant = self_.maxValue
//            CreateManager.sharedInstance.price = self_.price
//            })
    }
    
    @IBAction func clickNext(_ sender: AnyObject) {
        CreateManager.sharedInstance.minParticipant = minValue
        CreateManager.sharedInstance.maxParticipant = maxValue
        CreateManager.sharedInstance.price = price
        show(Storyboards.ViewControllers.coachingPlanning)
    }
    
    @IBAction func clickIncreasePrice(_ sender: AnyObject) {
        price += 5
        priceLabel.text = String(price) + euro
    }
    
    @IBAction func clickDecreasePrice(_ sender: AnyObject) {
        if price > 10 {
            price -= 5
        }
        if price >= 0 {
            priceLabel.text = String(price) + euro
        }
    }
    
    @IBAction func clickDecreaseMinValue(_ sender: AnyObject) {
        if minValue > 1 {
            minValue -= 1
        }
        if minValue >= 1 {
            minValueLabel.text = String(minValue)
        }
    }
    
    @IBAction func clickIncreaseMinValue(_ sender: AnyObject) {
        minValue += 1
        if minValue > maxValue {
            maxValue = minValue
            maxValueLabel.text = String(maxValue)
        }
        minValueLabel.text = String(minValue)
    }
    
    @IBAction func clickDecreaseMaxValue(_ sender: AnyObject) {
        if maxValue > minValue {
            maxValue -= 1
        }
        if maxValue >= 0 {
            maxValueLabel.text = String(maxValue)
        }
    }
    
    @IBAction func clickIncreaseMaxValue(_ sender: AnyObject) {
        maxValue += 1
        maxValueLabel.text = String(maxValue)
    }
}
