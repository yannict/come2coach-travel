//
//  CoachingPlanningVC.swift
//  come2coach
//
//  Created by Alex Kupchak on 10/31/16.
//  Copyright © 2016 MaytecNet. All rights reserved.
//

import Foundation
import AWSS3
import AWSCore
import Photos
import MRProgress
import SVProgressHUD


enum PlanningCellType: Int {
    case header
    case dateDefault
    case dateAdded
    case duration
    case restriction
    case handoutURL
    case handoutDescr
    case affiliate
}

private let HeaderCellId = "HeaderId"
private let DateCellId = "DateId"
private let RestrictionCellId = "RestrictionId"
private let AffiliateId = "AffiliateId"
private let HandoutDescrId = "HandoutDescrId"
private let HandoutId = "HandoutId"

class CoachingPlanningVC: UIViewController {
    @IBOutlet weak var tableView: UITableView!

    var data: Array<AnyObject> = []
    var insertIndex: Int = 2
    var selectedIndexPath = IndexPath()
    // used to assign label for PlanningDateCell
    var addedAddDate = 0
    var defaultDateCell: PlanningDateCell?


    @IBOutlet var picker: PlanningPickerView!

    override func viewDidLoad() {

        picker.delegate = self

        if CreateManager.sharedInstance.planningData.count == 0 {
            var firstHeader: [String: Any] = [:]
            firstHeader = ["title": "DATUM WÄHLEN".localized,
                           "type": PlanningCellType.header]

            var secondHeader: [String: Any] = [:]
            secondHeader = ["title": "DAUER WÄHLEN".localized,
                            "type": PlanningCellType.header]

            var thirdHeader: [String: Any] = [:]
            thirdHeader = ["title": "HANDOUT (OPTIONAL)".localized,
                           "type": PlanningCellType.header]

            var handoutURL: [String: Any] = [:]
            handoutURL = ["title": "",
                          "type": PlanningCellType.handoutURL
            ]

            var handoutDescr: [String: Any] = [:]
            handoutDescr = ["title": "",
                            "type": PlanningCellType.handoutDescr
            ]

            var forthHeader: [String: Any] = [:]
            forthHeader = ["title": "AFFILIATE PARTNERLINK (OPTIONAL)".localized,
                           "type": PlanningCellType.header]

            var affiliate: [String: Any] = [:]
            affiliate = ["title": "",
                         "type": PlanningCellType.affiliate
            ]


            /*
            var dateDefault: [String: Any] = [:]
            dateDefault = ["title": "Termin wählen".localized,
                           "type": PlanningCellType.dateDefault,
                           "date": Date().makeDateStringWith("dd.MM.yy"),
                           "time": Date().makeDateStringWith("HH:mm"),
                           "index": CreateManager.sharedInstance.currentBeginIndex]
                           */


            var dateDefault: [String: Any] = [:]
            dateDefault = ["title": "Termin wählen".localized,
                           "type": PlanningCellType.dateDefault,
                           "date": CreateManager.sharedInstance.begin.makeDateStringWith("dd.MM.yy"),
                           "time": CreateManager.sharedInstance.begin.makeDateStringWith("HH:mm"),
                           "index": CreateManager.sharedInstance.currentBeginIndex]



            var duration: [String: Any] = [:]
            let d = CreateManager.sharedInstance.duration.components(separatedBy: ":")
            duration = ["title": "Dauer".localized,
                        "type": PlanningCellType.duration,
                        "date": d[0] + " h".localized,
                        "time": d[1] + " min".localized]


            var restriction: [String: Any] = [:]
            restriction = ["title": "Altersbeschränkung".localized,
                           "restriction": "keine".localized,
                           "type": PlanningCellType.restriction]


            /*
            #beginchange
            data.append(firstHeader as AnyObject)
            data.append(dateDefault as AnyObject)
            */

            data = [firstHeader as AnyObject,
                    dateDefault as AnyObject,
                    secondHeader as AnyObject,
                    duration as AnyObject,
                    thirdHeader as AnyObject,
                    handoutURL as AnyObject,
                    handoutDescr as AnyObject,
                    forthHeader as AnyObject,
                    affiliate as AnyObject]

            /*

            #beginchange

            if let begin2 = CreateManager.sharedInstance.begin1 {

                CreateManager.sharedInstance.increaseCurrentBeginIndex()

                var date2: [String: Any] = [:]
                date2 = ["title": "zweiter Termin".localized,
                         "type": PlanningCellType.dateAdded,
                         "date": begin2.makeDateStringWith("dd.MM.yy"),
                         "time": begin2.makeDateStringWith("HH:mm"),
                         "index": CreateManager.sharedInstance.currentBeginIndex]


                data.append(date2 as AnyObject)

            }

            if let begin3 = CreateManager.sharedInstance.begin2 {

                CreateManager.sharedInstance.increaseCurrentBeginIndex()

                var date3: [String: Any] = [:]
                date3 = ["title": "dritter Termin".localized,
                         "type": PlanningCellType.dateAdded,
                         "date": begin3.makeDateStringWith("dd.MM.yy"),
                         "time": begin3.makeDateStringWith("HH:mm"),
                         "index": CreateManager.sharedInstance.currentBeginIndex]


                data.append(date3 as AnyObject)

            }


            data.append(secondHeader as AnyObject)
            data.append(duration as AnyObject)
            data.append(thirdHeader as AnyObject)
            data.append(handoutURL as AnyObject)
            data.append(handoutDescr as AnyObject)
            data.append(forthHeader as AnyObject)
            data.append(affiliate as AnyObject)
            */


        } else {
            data = CreateManager.sharedInstance.planningData
        }
        tableView.tableFooterView = UIView()
        tableView.dataSource = self
    }

    // MARK: Actions
    // =========================================================================================

    @IBAction func closeButton(_ sender: AnyObject) {
        //        CreateManager.sharedInstance.closeCreation()
        dismiss(animated: true, completion: { [weak self] _ in
            guard let self_ = self else {
                return
            }
            CreateManager.sharedInstance.planningData = self_.data
        })
    }

    @IBAction func clickNext(_ sender: AnyObject) {

        CreateManager.sharedInstance.planningData = data

        // check if date is expired
        /*
        if (!CreateManager.sharedInstance.isOnline) {
            self.createCoachingDataset()
        } else */

        if (CreateManager.sharedInstance.begin <= Date()) {
            self.present(UIAlertController("Bitte prüfen".localized, "Das Datum liegt in der Vergangenheit.".localized,
                    actions: UIAlertAction(.OK)), animated: true)
        } else {
            self.createCoaching()
        }

        /*
        if CreateManager.sharedInstance.isRepublishing == true {
            self.createCoaching()
        } else {
            uploadTitleImageToS3()
        }
        */


    }

    @IBAction func clickBack(_ sender: AnyObject) {
        //        dismissViewControllerAnimated(false, completion: {[weak self] _ in
        //            guard let self_ = self else {return}
        //            CreateManager.sharedInstance.planningData = self_.data
        //            })
        self.navigationController?.popViewController(animated: false, completion: { [weak self] _ in
            guard let self_ = self else {
                return
            }
            CreateManager.sharedInstance.planningData = self_.data
        })
    }

    func openPicker(_ isDate: Bool, isDuration: Bool) {
        view.addSubview(picker)
        picker.frame = CGRect(x: 0, y: view.frame.height, width: view.frame.width, height: 260)
        picker.show(isDate, isDuration: isDuration)
        UIView.animate(withDuration: 0.5, delay: 0, options: UIViewAnimationOptions(), animations: {
            [weak self] in
            self!.picker.frame = CGRect(x: 0, y: (self?.view.frame.height)! - 260, width: (self?.view.frame.width)!, height: 260)
        }, completion: nil)
    }

    func closePicker() {
        UIView.animate(withDuration: 0.5, delay: 0, options: UIViewAnimationOptions(), animations: {
            [weak self] in
            self!.picker.frame = CGRect(x: 0, y: (self?.view.frame.height)!, width: (self?.view.frame.width)!, height: 260)
        }, completion: { [weak self] _ in
            self?.picker.removeFromSuperview()
        })
    }

    func generateImageUrl(_ fileName: String, image: UIImage) -> URL {
        let fileURL = URL(fileURLWithPath: NSTemporaryDirectory() + fileName)
        let data = UIImageJPEGRepresentation(image, 0.6)
        try? data!.write(to: fileURL, options: [.atomic])

        return fileURL
    }

    func remoteImageWithUrl(_ fileName: String) {
        let fileURL = URL(fileURLWithPath: NSTemporaryDirectory() + fileName)
        do {
            try FileManager.default.removeItem(at: fileURL)
        } catch {
            OnDebugging.print(error)
        }
    }

    // MARK: Networking
    // =========================================================================================

//    
//    func videoUploading(videoUrl: NSURL, overlayView: MRProgressOverlayView) {
//        let date = NSDate().timeIntervalSince1970
//        let localFileName = "coaching_media_video-\(date)"
//        
//        let uploadRequest = AWSS3TransferManagerUploadRequest()
//        uploadRequest.body = videoUrl
//        uploadRequest.key = localFileName
//        uploadRequest.bucket = Constants.S3BucketName
//        uploadRequest.contentType = "movie/mov"
//        
//        uploadRequest?.uploadProgress = {(bytesSent:Int64, totalBytesSent:Int64, totalBytesExpectedToSend:Int64) in
//            
//            dispatch_sync(dispatch_get_main_queue(), { () -> Void in
//                // Prepare for progress view
//                //
//                if overlayView.mode != MRProgressOverlayViewMode.DeterminateCircular {
//                    overlayView.mode = MRProgressOverlayViewMode.DeterminateCircular
//                    overlayView.titleLabelText = "Übertrage Daten...".localized
//                }
//                let progress = ((Float(totalBytesSent) / Float(totalBytesExpectedToSend)) / 2) + 0.5
//                overlayView.setProgress(progress, animated: true)
//            })
//        }
//        
//        let transferManager = AWSS3TransferManager.defaultS3TransferManager()
//        
//        transferManager.upload(uploadRequest).continueWithBlock { (task) -> AnyObject! in
//            dispatch_async(dispatch_get_main_queue()) {
//                MRProgressOverlayView.dismissOverlayForView(self.view, animated: true)
//            }
//            
//            if let error = task.error {
//                OnDebugging.print("Upload failed with error: (\(error.localizedDescription))")
//            }
//            
//            if let exception = task.exception {
//                OnDebugging.print("Upload failed with exception (\(exception))")
//            }
//            
//            if task.result != nil {
//                let s3URL = "https://s3.amazonaws.com/\(Constants.S3BucketName)/\(uploadRequest.key!)"
//                CreateManager.sharedInstance.mediaS3Url = s3URL
//                OnDebugging.print("success upload to s3")
//                self.createCoaching()
//            }
//            return nil
//        }
//    }

/*
    func uploadTitleImageToS3() {
        // Get local image name
        //
        var localFileName: String?
        let date = Date().timeIntervalSince1970
        localFileName = "coaching_title_image-\(date).jpg"

        if localFileName == nil {
            return
        }

        let remoteName = localFileName!

        // Progress view
        //
        let overlayView = MRProgressOverlayView.showOverlayAdded(to: self.view, title: "Bitte warten....".localized, mode: MRProgressOverlayViewMode.indeterminate, animated: true)

        S3Manager.uploadFileToS3(localFileName, generatedImageUrl: generateImageUrl(remoteName, image: CreateManager.sharedInstance.img1), contentType: "image/jpeg", progress: { (totalBytesSent, totalBytesExpectedToSend) in
            // Prepare for progress view
            //
            if overlayView?.mode != MRProgressOverlayViewMode.determinateCircular {
                overlayView?.mode = MRProgressOverlayViewMode.determinateCircular
                overlayView?.titleLabelText = "Übertrage Daten...".localized
            }
            let progress = (Float(totalBytesSent) / Float(totalBytesExpectedToSend)) / 2
            overlayView?.setProgress(progress, animated: true)
        }) { (success, s3URL) in
            if success {
                // Remove locally stored file
                //
                self.remoteImageWithUrl(remoteName)

                if success {
                    CreateManager.sharedInstance.titleImageS3Url = remoteName
                    // Upload to s3 Video
                    //
                    if let videoUrl = CreateManager.sharedInstance.media.videoUrl {
                        self.mediaUploading(true, videoUrl: videoUrl, overlayView: overlayView!)
                    } else {
                        // Upload to s3 second image
                        //
                        self.mediaUploading(false, videoUrl: nil, overlayView: overlayView!)
                    }
                }
            }
        }
    }
    */

/*
    func mediaUploading(_ isVideo: Bool, videoUrl: URL?, overlayView: MRProgressOverlayView?) {


        if CreateManager.sharedInstance.media.image != nil || isVideo {

            var localFileName: String?
            if isVideo {
                let date = Date().timeIntervalSince1970
                localFileName = "coaching_media_video-\(date).mov"
            } else {
                let date = Date().timeIntervalSince1970
                localFileName = "coaching_media_image-\(date).jpg"
            }

            S3Manager.uploadFileToS3(localFileName, generatedImageUrl: isVideo ? videoUrl! : self.generateImageUrl(localFileName!, image: CreateManager.sharedInstance.media.image!), contentType: isVideo ? "movie/mov" : "image/jpeg", progress: { (totalBytesSent, totalBytesExpectedToSend) in
                // Prepare for progress view
                //
                if overlayView?.mode != MRProgressOverlayViewMode.determinateCircular {
                    overlayView?.mode = MRProgressOverlayViewMode.determinateCircular
                    overlayView?.titleLabelText = "Übertrage Daten...".localized
                }
                let progress = ((Float(totalBytesSent) / Float(totalBytesExpectedToSend)) / 2) + 0.5
                overlayView?.setProgress(progress, animated: true)
            }) { (success, s3URL) in

                DispatchQueue.main.async {
                    MRProgressOverlayView.dismissOverlay(for: self.view, animated: true)
                }
                // Remove locally stored file
                //
                self.remoteImageWithUrl(localFileName!)
                if success {
                    CreateManager.sharedInstance.mediaS3Url = localFileName!
                    CreateManager.sharedInstance.isVideo = isVideo
                    OnDebugging.print("success upload to s3")
                    self.createCoaching()
                }
            }
        } else {

            // in case no 2nd media available continue create process

            DispatchQueue.main.async {
                MRProgressOverlayView.dismissOverlay(for: self.view, animated: true)
            }

            self.createCoaching()
        }
    }
    */

    /*
        Create Event on Backend
    */
    func createCoaching() {

        let eventFolder = "event-" + UUID().uuidString

        // first set remote folder for changed content
        if (CreateManager.sharedInstance.img1.changed &&
                CreateManager.sharedInstance.img2.changed) {

            CreateManager.sharedInstance.img1.remoteFolder = eventFolder
            CreateManager.sharedInstance.img2.remoteFolder = eventFolder


        } else if (CreateManager.sharedInstance.img1.changed &&
                !CreateManager.sharedInstance.img2.changed) {

            CreateManager.sharedInstance.img1.remoteFolder = eventFolder
            CreateManager.sharedInstance.img2.remoteFolder =
                    CreateManager.sharedInstance.existingImg2Folder


        } else if (!CreateManager.sharedInstance.img1.changed &&
                CreateManager.sharedInstance.img2.changed) {

            CreateManager.sharedInstance.img1.remoteFolder =
                    CreateManager.sharedInstance.existingImg1Folder
            CreateManager.sharedInstance.img2.remoteFolder = eventFolder


        } else if (!CreateManager.sharedInstance.img1.changed &&
                !CreateManager.sharedInstance.img2.changed) {

            CreateManager.sharedInstance.img1.remoteFolder =
                    CreateManager.sharedInstance.existingImg1Folder
            CreateManager.sharedInstance.img2.remoteFolder =
                    CreateManager.sharedInstance.existingImg2Folder

        }


        // Process 1, img1 picture was changed

        if (CreateManager.sharedInstance.img1.changed) {

            let overlayView = MRProgressOverlayView.showOverlayAdded(to: self.view, title: "Bitte warten....".localized, mode: MRProgressOverlayViewMode.indeterminate, animated: true)

            CreateManager.sharedInstance.uploadImages(CreateManager.sharedInstance.img1, overlayView!) { (success) in

                if (success) {

                    // Process 1, img2 picture was changed

                    if (CreateManager.sharedInstance.img2.changed) {

                        CreateManager.sharedInstance.uploadImages(CreateManager.sharedInstance.img2, overlayView!) { (success2) in

                            MRProgressOverlayView.dismissOverlay(for: self.view, animated: true)

                            if (success2) {

                                self.createCoachingDataset()

                            } else {
                                self.present(UIAlertController("Upps!".localized, "Leider konnte nicht alle Medien übertragen werden. Bitte probiere es nochmals. Vielleicht war deine Internet-Verbindung unterbrochen.".localized,
                                        actions: UIAlertAction(.OK)), animated: true)
                            }

                        }

                    } else {

                        MRProgressOverlayView.dismissOverlay(for: self.view, animated: true)

                        self.createCoachingDataset()
                    }

                } else {

                    MRProgressOverlayView.dismissOverlay(for: self.view, animated: true)

                    self.present(UIAlertController("Upps!".localized, "Leider konnte nicht alle Medien übertragen werden. Bitte probiere es nochmals. Vielleicht war deine Internet-Verbindung unterbrochen.".localized,
                            actions: UIAlertAction(.OK)), animated: true)
                }
            }

        } else {

            // Process 2, img2 was changed
            if (CreateManager.sharedInstance.img2.changed) {

                let overlayView = MRProgressOverlayView.showOverlayAdded(to: self.view, title: "Bitte warten....".localized, mode: MRProgressOverlayViewMode.indeterminate, animated: true)

                CreateManager.sharedInstance.uploadImages(CreateManager.sharedInstance.img2, overlayView!) { (success2) in

                    MRProgressOverlayView.dismissOverlay(for: self.view, animated: true)

                    if (success2) {

                        self.createCoachingDataset()

                    } else {
                        self.present(UIAlertController("Upps!".localized, "Leider konnte nicht alle Medien übertragen werden. Bitte probiere es nochmals. Vielleicht war deine Internet-Verbindung unterbrochen.".localized,
                                actions: UIAlertAction(.OK)), animated: true)
                    }

                }

            } else {

                self.createCoachingDataset()
            }

        }

    }

    func createCoachingDataset() {

        SVProgressHUD.show()

        CreateManager.sharedInstance.createEvent { (success, eventId) in

            SVProgressHUD.dismiss()

            if let eventTrigger = CreateManager.sharedInstance.closeEvent {
                eventTrigger()
            }

            if (success && eventId > -1) {

                self.dismissViewController(true, completion: nil)
            } else {
                self.present(UIAlertController("Upps!".localized, "Leider konnte das Coaching aus technischen Gründen nicht erstellt werden. Bitte probiere es nochmals. Entweder liegt der Fehler auf unserer Seite oder deine Internet-Verbindung war unterbrochen.".localized,
                        actions: UIAlertAction(.OK)), animated: true)
            }
        }
    }
}

// MARK: UITableViewDelegate
// =========================================================================================


extension CoachingPlanningVC: UITableViewDelegate {


    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        tableView.deselectRow(at: indexPath, animated: true)

        let dictionary = data[indexPath.row]

        if (dictionary as! [String: Any])["type"] as! PlanningCellType
                   == PlanningCellType.handoutURL {

            let vc = Storyboards.ViewControllers.coachingHandoutURL
            show(vc)

        } else if (dictionary as! [String: Any])["type"] as! PlanningCellType
                          == PlanningCellType.handoutDescr {

            let vc = Storyboards.ViewControllers.coachingDescription
            vc.placeholder = "Gib hier bitte Deinen Text ein ...".localized
            vc.titleText = "ERLÄUTERUNGEN"
            vc.descrValue = CreateManager.sharedInstance.handoutDescr
            show(vc)
            vc.completion = { (txt) in

                CreateManager.sharedInstance.handoutDescr = txt

            }

        } else if (dictionary as! [String: Any])["type"] as! PlanningCellType
                          == PlanningCellType.affiliate {

            let vc = Storyboards.ViewControllers.coachingAffiliate
            show(vc)

        }
    }


    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let dictionary = data[indexPath.row]

        var d = (dictionary as! [String: Any])["type"] as! PlanningCellType

        if (d == PlanningCellType.header) {
            return 40
        }

        /* #swift3
        if dictionary["type"]   == PlanningCellType.header {
            return 55
        }
        */

        return 54
    }
}

// MARK: UITableViewDataSource
// =========================================================================================


extension CoachingPlanningVC: UITableViewDataSource {

    func addSeparator(_ cell: UITableViewCell) {

        let additionalSeparatorThickness = CGFloat(1)
        let additionalSeparator = UIView(frame: CGRect(x: 0, y: cell.frame.size.height - additionalSeparatorThickness, width: cell.frame.size.width, height: additionalSeparatorThickness))
        additionalSeparator.backgroundColor = UIColor(netHex: 0x95989A)
        cell.addSubview(additionalSeparator)

    }


    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let dictionary = data[indexPath.row]

        let deb = (dictionary as! [String: Any])["type"]

        debugPrint(deb)

        // header cells
        //
        if (dictionary as! [String: Any])["type"] as! PlanningCellType
                   == PlanningCellType.header {
            let cell = tableView.dequeueReusableCell(withIdentifier: HeaderCellId, for: indexPath) as! PlanningHeaderCell
            cell.setData(dictionary as! Dictionary<String, AnyObject>)
            addSeparator(cell)
            return cell
        }

        // handout cells
        //
        if (dictionary as! [String: Any])["type"] as! PlanningCellType
                   == PlanningCellType.handoutURL {
            let cell = tableView.dequeueReusableCell(withIdentifier: HandoutId, for: indexPath) as! UITableViewCell
            //cell.setData(dictionary as! Dictionary<String, AnyObject>)
            addSeparator(cell)
            return cell
        }

        if (dictionary as! [String: Any])["type"] as! PlanningCellType
                   == PlanningCellType.handoutDescr {
            let cell = tableView.dequeueReusableCell(withIdentifier: HandoutDescrId, for: indexPath) as! UITableViewCell
            //cell.setData(dictionary as! Dictionary<String, AnyObject>)
            addSeparator(cell)
            return cell
        }

        if (dictionary as! [String: Any])["type"] as! PlanningCellType
                   == PlanningCellType.affiliate {
            let cell = tableView.dequeueReusableCell(withIdentifier: AffiliateId, for: indexPath) as! UITableViewCell
            //cell.setData(dictionary as! Dictionary<String, AnyObject>)
            addSeparator(cell)
            return cell
        }

        // first time cell
        //
        if (dictionary as! [String: Any])["type"] as! PlanningCellType
                   == PlanningCellType.dateDefault {
            let cell = tableView.dequeueReusableCell(withIdentifier: DateCellId, for: indexPath) as! PlanningDateCell
            defaultDateCell = cell
            cell.delegate = self
            cell.setData(dictionary as! Dictionary<String, AnyObject>, indexPath: indexPath)
            addSeparator(cell)
            return cell
        }

        // cell which added
        //
        /*
        #beginchange
        if (dictionary as! [String: Any])["type"] as! PlanningCellType
                   == PlanningCellType.dateAdded {
            let cell = tableView.dequeueReusableCell(withIdentifier: DateCellId, for: indexPath) as! PlanningDateCell
            cell.delegate = self
            var d = dictionary as! Dictionary<String, AnyObject>;

            if (addedAddDate == 0) {
                d["title"] = "zweiter Termin".localized as AnyObject?

                if let dc = defaultDateCell {
                    dc.addButton.isHidden = false
                    dc.addImageView.isHidden = false
                }

            } else {
                d["title"] = "dritter Termin".localized as AnyObject?

                if let dc = defaultDateCell {
                    dc.addButton.isHidden = true
                    dc.addImageView.isHidden = true
                }
            }
            addedAddDate += 1

            cell.setData(d, isDefault: false, isDuration: false, indexPath: indexPath)
            addSeparator(cell)
            return cell
        }
        */

        // cell with duration
        //
        if (dictionary as! [String: Any])["type"] as! PlanningCellType
                   == PlanningCellType.duration {
            let cell = tableView.dequeueReusableCell(withIdentifier: DateCellId, for: indexPath) as! PlanningDateCell
            cell.delegate = self
            cell.setData(dictionary as! Dictionary<String, AnyObject>, isDefault: false, isDuration: true, indexPath: indexPath)
            addSeparator(cell)
            return cell
        }

        // age restriction cell
        //
        if (dictionary as! [String: Any])["type"] as! PlanningCellType == PlanningCellType.restriction {

            let cell = tableView.dequeueReusableCell(withIdentifier: RestrictionCellId, for: indexPath) as! PlanningAgeRestrictionCell
            cell.delegate = self
            cell.setData(dictionary as! Dictionary<String, AnyObject>)
            addSeparator(cell)
            return cell
        }

        return UITableViewCell()
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
}

// MARK: PlanningDateCellDelegate
// =========================================================================================


extension CoachingPlanningVC: PlanningDateCellDelegate {
    // add new cell with time
    //
    func addNewTime(_ sender: AnyObject) {
        CreateManager.sharedInstance.increaseCurrentBeginIndex()

        let date = Date().makeDateStringWith("dd.MM.yy")
        let time = Date().makeDateStringWith("HH:mm")
        let addedCell: [String: Any] = ["title": CreateManager.sharedInstance.currentBeginIndex == 2 ? "zweiter Termin".localized : "dritter Termin".localized,
                                        "type": PlanningCellType.dateAdded,
                                        "date": date,
                                        "time": time,
                                        "index": CreateManager.sharedInstance.currentBeginIndex]


        // reset addAddDAte counter (used to correct date add labels)
        addedAddDate = 0;
        data.insert(addedCell as AnyObject, at: insertIndex)
        tableView.reloadData()
        /*

        tableView.beginUpdates()
        data.insert(addedCell, atIndex: insertIndex)
        tableView.insertRowsAtIndexPaths([NSIndexPath(forRow: insertIndex, inSection: 0)], withRowAnimation: UITableViewRowAnimation.Fade)
        tableView.endUpdates()
        */
        insertIndex += 1
    }

    // remove cell which will add
    //
    func removeTime(_ sender: AnyObject) {
        CreateManager.sharedInstance.decreaseCurrentBeginIndex()

        let removeCell = sender as! PlanningDateCell
        let removeIndex = tableView.indexPath(for: removeCell)!
        var choosenItem = data[removeIndex.row] as! [String: Any]
        if choosenItem["title"] as! String == "zweiter Termin".localized {
            CreateManager.sharedInstance.begin1 = nil
        }

        if choosenItem["title"] as! String == "dritter Termin".localized {
            CreateManager.sharedInstance.begin2 = nil
        }

        // reset addAddDAte counter (used to correct date add labels)
        addedAddDate = 0;
        data.remove(at: removeIndex.row)
        tableView.reloadData()
        /*

        tableView.beginUpdates()
        data.removeAtIndex(removeIndex.row)
        tableView.deleteRowsAtIndexPaths([removeIndex], withRowAnimation: UITableViewRowAnimation.Fade)
        tableView.endUpdates()
        tableView.reloadData()
        */
        insertIndex -= 1

        // reset captions


    }
    //item: Dictionary<String, AnyObject>
    func showDatePicker(_ isDuration: Bool, indexPath: IndexPath) {
        selectedIndexPath = indexPath
        openPicker(true, isDuration: isDuration)
    }
}

extension CoachingPlanningVC: PlanningPickerViewDelegate {
    func donePicker(_ date: Date) {
        var choosenItem: [String: Any] = [:]
        choosenItem = data[selectedIndexPath.row] as! [String: Any]

        let celltype = (data[selectedIndexPath.row] as! [String: Any])["type"] as! PlanningCellType

        if celltype == PlanningCellType.dateDefault ||
                   celltype == PlanningCellType.dateAdded {
            choosenItem["date"] = date.makeDateStringWith("dd.MM.yy")
            choosenItem["time"] = date.makeDateStringWith("HH:mm")
            data[selectedIndexPath.row] = choosenItem as AnyObject
            if choosenItem["title"] as! String == "Termin wählen".localized {
                CreateManager.sharedInstance.begin = date
            }

            if choosenItem["title"] as! String == "zweiter Termin".localized {
                CreateManager.sharedInstance.begin1 = date
            }

            if choosenItem["title"] as! String == "dritter Termin".localized {
                CreateManager.sharedInstance.begin2 = date
            }

        } else if celltype == PlanningCellType.duration {
            choosenItem["date"] = date.makeDateStringWith("H") + " h"
            choosenItem["time"] = date.makeDateStringWith("mm") + " min"
            let time = date.makeDateStringWith("H") + ":" + date.makeDateStringWith("mm")
            CreateManager.sharedInstance.duration = time
            data[selectedIndexPath.row] = choosenItem as AnyObject
        }
        addedAddDate = 0
        tableView.reloadData()
        closePicker()
    }

    func donePickerFromRestriction(_ ageRestriction: String) {

        let celltype = (data[data.count - 1] as! [String: Any])["type"] as! PlanningCellType

        if celltype == PlanningCellType.restriction {
            var choosenItem = data[data.count - 1] as! [String: Any]
            choosenItem["restriction"] = ageRestriction
            if ageRestriction == "keine".localized {
                CreateManager.sharedInstance.ageRestriction = 0
            } else if ageRestriction == "ab 14 Jahren".localized {
                CreateManager.sharedInstance.ageRestriction = 3
            } else if ageRestriction == "ab 16 Jahren".localized {
                CreateManager.sharedInstance.ageRestriction = 1
            } else if ageRestriction == "ab 18 Jahre".localized {
                CreateManager.sharedInstance.ageRestriction = 2
            }

            //CreateManager.sharedInstance.ageRestriction = Int(ageRestriction)!
            data[data.count - 1] = choosenItem as AnyObject
            addedAddDate = 0
            tableView.reloadData()
            closePicker()
        }
    }

    func cancelPicker() {
        closePicker()
    }
}

extension CoachingPlanningVC: PlanningAgeRestrictionCellDelegate {
    func showPicker() {
        openPicker(false, isDuration: false)
    }
}

