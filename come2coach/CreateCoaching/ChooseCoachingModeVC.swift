//
//  ChooseCoachingModeVC.swift
//  come2coach
//
//  Created by Alex Kupchak on 10/26/16.
//  Copyright © 2016 MaytecNet. All rights reserved.
//

import Foundation

class ChooseCoachingModeVC: UIViewController {
    
    // MARK: Life cycle
    // =========================================================================================
    
    override func viewDidLoad() {
     
    }
    
    // MARK: Actions
    // =========================================================================================
    
    @IBAction func clickQuestionMark(_ sender: AnyObject) {
        
    }
    
    @IBAction func clickOnPlace(_ sender: AnyObject) {
        CreateManager.sharedInstance.isOnline = false
        show(Storyboards.ViewControllers.coachingDetails)
    }

    @IBAction func clickOnline(_ sender: AnyObject) {
        CreateManager.sharedInstance.isOnline = true
        show(Storyboards.ViewControllers.coachingDetails)
    }
    
    @IBAction func close() {
        dismissViewController(true, completion: nil)
    }
}
