//
//  EventIconInfoView.swift
//  come2coach
//
//  Created by Timotheus Laubengaier on 26.04.17.
//  Copyright © 2017 MaytecNet. All rights reserved.
//

import Foundation
import UIKit

class EventIconInfoView: UIView {
  
  let leftImageView: UIImageView = {
    let view = UIImageView()
    view.contentMode = .scaleAspectFill
    view.image = #imageLiteral(resourceName: "icon_calendar")
    view.clipsToBounds = true
    return view
  }()
  
  let leftTitleLabel: UILabel = {
    let label = UILabel()
    label.text = "15.06.17"
    label.font = Styleguide.Font.light(size: 13)
    label.textColor = Styleguide.Color.text
    return label
  }()
  
  let centerImageView: UIImageView = {
    let view = UIImageView()
    view.contentMode = .scaleAspectFill
    view.image = #imageLiteral(resourceName: "icon_clock")
    view.clipsToBounds = true
    return view
  }()
  
  let centerTitleLabel: UILabel = {
    let label = UILabel()
    label.text = "20:00"
    label.font = Styleguide.Font.light(size: 13)
    label.textColor = Styleguide.Color.text
    return label
  }()
  
  let rightImageView: UIImageView = {
    let view = UIImageView()
    view.contentMode = .scaleAspectFill
    view.image = #imageLiteral(resourceName: "icon_sandclock")
    view.clipsToBounds = true
    return view
  }()
  
  let rightTitleLabel: UILabel = {
    let label = UILabel()
    label.text = "1:30 h"
    label.font = Styleguide.Font.light(size: 13)
    label.textColor = Styleguide.Color.text
    return label
  }()
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    
    // left
    addSubview(leftImageView)
    leftImageView.snp.makeConstraints { (make) in
      make.top.equalTo(self).offset(20)
      make.centerX.equalTo(self).offset(-100)
      make.width.equalTo(32)
      make.height.equalTo(32)
    }
    
    addSubview(leftTitleLabel)
    leftTitleLabel.snp.makeConstraints { (make) in
      make.top.equalTo(leftImageView.snp.bottom).offset(15)
      make.centerX.equalTo(leftImageView)
    }
    
    // center
    addSubview(centerImageView)
    centerImageView.snp.makeConstraints { (make) in
      make.top.equalTo(self).offset(20)
      make.centerX.equalTo(self)
      make.width.equalTo(32)
      make.height.equalTo(32)
    }
    
    addSubview(centerTitleLabel)
    centerTitleLabel.snp.makeConstraints { (make) in
      make.top.equalTo(centerImageView.snp.bottom).offset(15)
      make.centerX.equalTo(centerImageView)
    }
    
    // right
    addSubview(rightImageView)
    rightImageView.snp.makeConstraints { (make) in
      make.top.equalTo(self).offset(20)
      make.centerX.equalTo(self).offset(100)
      make.width.equalTo(32)
      make.height.equalTo(32)
    }
    
    addSubview(rightTitleLabel)
    rightTitleLabel.snp.makeConstraints { (make) in
      make.top.equalTo(rightImageView.snp.bottom).offset(15)
      make.centerX.equalTo(rightImageView)
    }
    
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
}
