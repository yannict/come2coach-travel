//
//  CountDownVC.swift
//  come2coach
//
//  Created by Marc Ortlieb on 04.08.16.
//  Copyright © 2016 Marc Ortlieb. All rights reserved.
//

import UIKit
// import Async
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}



class CountDownDateValidator {
    
    //    static func isValideStartDate(startDate: String) -> Bool {
    //        return now.seconds(to: startDate) > 0 ? true : false
    //    }
    static func isValideStartDate(_ startDate: Int) -> Bool {
        return startDate > 0 ? true : false
    }
}



class CountDownVC: UIViewController {
    
    @IBOutlet weak var circleView: CircleLoaderView!
    @IBOutlet weak var timerLabel: UILabel!
    
    // coachingPreview
    @IBOutlet weak var coachingPreviewContainer: UIView!
    @IBOutlet weak var previewImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    
    // MOCK Event - for debugging CircleLoaderView
    func mockEvent() -> Event {
        let event = Event()
        let timeStamp = EventTimeStamp()
        timeStamp.dateString = "2017-01-03 01:51:00.000000" // mock startDate as String
        event.begin = timeStamp
        return event
    }


    @IBAction func closeButton(_ sender: AnyObject) {

        self.dismiss(animated: true, completion: nil)

    }
    

    var event: Event = Event() {
        didSet {
            OnDebugging
                .print(".didSet: \(event)", title: "CountDownVC.secTillEventBegins")
        }
    }
    var isValidEvent: Bool {
        return
            event.begin.secTillEventStart > 0 ? true : false
    }
    
    @IBAction func popVCAction(_ sender: AnyObject) {
        UIApplication.shared.isIdleTimerDisabled = false
        countDownTimer.invalidate()
        show(Storyboards.ViewControllers.coachings)
    }
    
    
    
    
    
    fileprivate let totalSeconds   = CGFloat(10*60)
    fileprivate var secRemaining   = 0
    fileprivate var countDownTimer = Timer()
    fileprivate var isRunning = false
    
    fileprivate var progress: CGFloat {
        let elapsedSec = totalSeconds - CGFloat(secRemaining)
        return
            elapsedSec / totalSeconds
    }
    fileprivate var timerLabelText: String {
        let min = secRemaining / 60
        let sec = secRemaining >= 60 ? (secRemaining % 60) : secRemaining
        return
            String(format: "%02d:%02d", min, sec)
    }
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.isIdleTimerDisabled = true

        // event = mockEvent() // only for debugging CircleLoaderView
        
        // hide storyboard Placeholders
        coachingPreviewContainer.isHidden = true
       
        self.setupTitle(self.navigationItem.title)
        
        let originalFont = UIFont.Montserrat.ultraLight(67)
        let originalFontDescriptor = originalFont.fontDescriptor
        
        let fontDescriptorFeatureSettings = [
            [
                UIFontFeatureTypeIdentifierKey: kNumberSpacingType,
                UIFontFeatureSelectorIdentifierKey: kMonospacedNumbersSelector
            ]
        ]
        
        let fontDescriptorAttributes = [UIFontDescriptorFeatureSettingsAttribute: fontDescriptorFeatureSettings]
        let fontDescriptor = originalFontDescriptor.addingAttributes(fontDescriptorAttributes)
        let font = UIFont(descriptor: fontDescriptor, size: 0)
        
        timerLabel.font = font
        
        // set Labels



        // #swift3 debug this code, Asnyc was removed.
        var image: UIImage?

        //todo: replace with uiimage.setbyurl
        image = UIImage(data: try! Data(contentsOf: self.event.imageURL!))

        // #swift3 (2)
        // image = self.event.imageURL
          //      .flatMap{Data(contentsOfURL: $0)}
            //    .flatMap{UIImage(data: $0)}

        // image
        previewImageView.image = image

        // title
        titleLabel.text = event.title

        // subTitle
        var subTitleLabelText: NSAttributedString
        switch event.userType {
        case .coach:
            subtitleLabel.text = event.descriptionText
            subtitleLabel.textColor = Designs.Colors.timerGray
        case .participant:
            subTitleLabelText = makeAttributedSubtitleText(event.owner)
            subtitleLabel.attributedText = subTitleLabelText
        }
        coachingPreviewContainer.isHidden = false

        /*

        Async
            .userInitiated {
                image = self.event.imageURL
                    .flatMap{Data(contentsOfURL: $0)}
                    .flatMap{UIImage(data: $0)}
            }
            .main { [weak self] in
                guard let self_ = self else {return}
                
                // image
                self_.previewImageView.image = image
                
                // title
                self_.titleLabel.text = self_.event.title
                
                // subTitle
                var subTitleLabelText: NSAttributedString
                switch self_.event.userType {
                case .coach:
                    self_.subtitleLabel.text = self_.event.descriptionText
                    self_.subtitleLabel.textColor = Designs.Colors.timerGray
                case .participant:
                    subTitleLabelText = self_.makeAttributedSubtitleText(self_.event.owner)
                    self_.subtitleLabel.attributedText = subTitleLabelText
                }
                self_.coachingPreviewContainer.hidden = false
                
        }
        */
        
        
        if isValidEvent {
            if let secTillEventStart = event.begin.secTillEventStart {
                
                secRemaining = secTillEventStart > 0 ? secTillEventStart : 0
                
                OnDebugging.print("Init CountdownTimer")
                
                isRunning = true
                
                countDownTimer =
                    Timer.scheduledTimer(timeInterval: 1,
                                                           target: self,
                                                           selector: #selector(updateProgressView),
                                                           userInfo: nil,
                                                           repeats: true)
            }
        }
    }
    

    
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if secRemaining <= 0 {
            countDownTimer.invalidate()

            //todo: #lvdebu
            self.present(UIAlertController("DEBUG".localized, "THE EVENT IS ALREADY RUNNING - THIS IS A TEST MESSAGE. THIS VIEW WILL NOT APPEAR IN CASE BEGIN, BEGIN2 OR BEGIN3 IS VALID.".localized,
                    actions: UIAlertAction(.OK)), animated: true)


            self.dismiss(animated: false, completion: nil)
            /*
            // navigation
            switch event.userType {
            case .coach:
                    let livestreamCoachVC = Storyboards.ViewControllers.livestreamingCoach
                    livestreamCoachVC.event = event
                    showEventIsHappeningNowAlert(onNextButtonPressed: livestreamCoachVC)
            case .participant:
                    let livestreamParticipantVC = Storyboards.ViewControllers.livestreamingParticipant
                    livestreamParticipantVC.event = event
//                    show(livestreamParticipantVC, animated: true, modally: true)
                    showEventIsHappeningNowAlert(onNextButtonPressed: livestreamParticipantVC)
                }
            */
        }
    }
    
    // after View is instanciated from StoryBoard
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        circleView.setCircleRadius(circleView.bounds.width / 2)
    }
    
    
    // ApplicationFlow: -> countDownTimer -> LiveStream -> (x)-Button -> countDownTimer
    
    func showEventIsHappeningNowAlert(onNextButtonPressed liveStreamingVC: UIViewController) {
        
        let alertViewController = UIAlertController(title: "Coaching ist LIVE".localized, message: "", preferredStyle: .alert)
        alertViewController += UIAlertAction(title: "Weiter zur Coaching-Liste".localized, style: .cancel, handler: { _ in
            self.show(Storyboards.ViewControllers.coachings)
        })
        alertViewController += UIAlertAction(title: "Weiter zum Livestream".localized, style: .default, handler: { _ in
            self.show(liveStreamingVC, sender: true)
        })
        self.show(alertViewController)
    }
    
    
    
    
    
    
    
    @objc
    fileprivate func updateProgressView() {
        if let secTillEventStart = event.begin.secTillEventStart {
            secRemaining = secTillEventStart > 0 ? secTillEventStart : 0
            circleView.progress = progress
            timerLabel.text = timerLabelText
            
            if secRemaining <= 0 && isRunning {
                isRunning = false
                countDownTimer.invalidate()
                
                OnDebugging.print("Redirecting to livestream")


                // start streaming
                let ls = LiveStreamingHandler()
                ls.launch(event.id, controller: self)
                /*
                // navigation
                switch event.userType {
                    
                case .participant:
                    let livestreamParticipantVC = Storyboards.ViewControllers.livestreamingParticipant
                    livestreamParticipantVC.event = event
                    show(livestreamParticipantVC, animated: true, modally: true)
                    
                case .coach:
                    let livestreamCoachVC = Storyboards.ViewControllers.livestreamingCoach
                    livestreamCoachVC.event = event
                    show(livestreamCoachVC, animated: true, modally: true)
                }
                */
            }
        }
    }
 
    
    
    
    
    
    
    fileprivate func makeAttributedSubtitleText(_ coachName: String) -> NSAttributedString {
        
        let font = UIFont.Montserrat.regular(12)
        
        let baseAttributes = [
            NSForegroundColorAttributeName: Designs.Colors.coachingTimeGray,
            NSFontAttributeName: font
        ]
        let locationAttributedString = NSAttributedString(string: "Dein Coach", attributes: baseAttributes)
        
        
        let nameAttributes = [
            NSForegroundColorAttributeName: Designs.Colors.timerCoachBlue,
            NSFontAttributeName: font
        ]
        let timeAttributedString = NSAttributedString(string: " \(coachName)", attributes: nameAttributes)
        
        return
            locationAttributedString + timeAttributedString
    }
}











