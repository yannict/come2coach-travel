//
//  ProfileViewController.swift
//  come2coach
//
//  Created by Timotheus Laubengaier on 27.04.17.
//  Copyright © 2017 MaytecNet. All rights reserved.
//

import Foundation
import UIKit

class ProfileViewController: UIViewController {
  
  var user: User? {
    didSet {
      if let user = user {
        userNameLabel.text = user.name
        if let imagePath = user.imageStringUrl, let imageUrl = URL(string: imagePath) {
          userImageView.sd_setImage(with: imageUrl)
        }
      }
    }
  }
  
  let backgroundImageView: UIImageView = {
    let imageView = UIImageView()
    imageView.image = #imageLiteral(resourceName: "WavesBackground")
    imageView.contentMode = .scaleAspectFill
    return imageView
  }()
  
  let scrollView: UIScrollView = {
    let scrollView = UIScrollView()
    scrollView.contentSize = UIScreen.main.bounds.size
    scrollView.showsVerticalScrollIndicator = true
    scrollView.backgroundColor = .clear
    scrollView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
    scrollView.backgroundColor = .clear
    return scrollView
  }()
  
  let contentView: UIView = {
    let view = UIView()
    view.backgroundColor = .clear
    return view
  }()
  
  let userNameLabel: UILabel = {
    let label = UILabel()
    label.text = "Maximilian Mustermann"
    label.font = Styleguide.Font.light(size: 20)
    label.textColor = Styleguide.Color.text
    label.textAlignment = .center
    label.numberOfLines = 2
    return label
  }()
  
  let userImageView: UIImageView = {
    let imageView = UIImageView()
    imageView.backgroundColor = Styleguide.Color.lightGray
    imageView.contentMode = .scaleAspectFill
    imageView.layer.cornerRadius = 65
    imageView.clipsToBounds = true
    imageView.layer.borderWidth = 5
    imageView.layer.borderColor = UIColor.white.withAlphaComponent(0.5).cgColor
    return imageView
  }()
  
  let profileIconInfoView: ProfileIconInfoView = {
    let view = ProfileIconInfoView()
    view.backgroundColor = .white
    
    view.leftTitleLabel.text = "105 Follower"
    view.leftImageView.image = #imageLiteral(resourceName: "iconFollower")
    
    view.rightTitleLabel.text = "23 Scoutings"
    view.rightImageView.image = #imageLiteral(resourceName: "icon_clock")
    
    return view
  }()
  
  let descriptionView: EventDescriptionView = {
    let view = EventDescriptionView()
    view.backgroundColor = .white
    view.locationImageView.snp.updateConstraints({ (make) in
      make.height.equalTo(0)
    })
    return view
  }()
  
  let bookingRecommendationsView: EventBookingsView = {
    let view = EventBookingsView()
    return view
  }()
  
  let ratingButton = Styleguide.Button.rating(title: "4.5 | 5 Sternen")
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    navigationItem.title = "COACH PROFIL"
    view.backgroundColor = .white
    
    initUI()
    
    let closeBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "usr-back-btn"), style: .plain, target: self, action: #selector(closeButtonPressed))
    closeBarButtonItem.tintColor = Styleguide.Color.text
    navigationItem.setLeftBarButton(closeBarButtonItem, animated: false)
    
  }
  
  func closeButtonPressed() {
    navigationController?.popViewController()
  }
  
  func initUI() {
    view.addSubview(backgroundImageView)
    backgroundImageView.snp.makeConstraints { (make) in
      make.edges.equalTo(view).inset(UIEdgeInsetsMake(200, 0, 0, 0))
    }
    
    view.addSubview(scrollView)
    scrollView.snp.makeConstraints { (make) in
      make.top.equalTo(view).offset(64)
      make.leading.equalTo(view)
      make.trailing.equalTo(view)
      make.bottom.equalTo(view)
    }
    
    scrollView.addSubview(contentView)
    contentView.snp.makeConstraints { (make) in
      make.top.equalTo(scrollView)
      make.leading.equalTo(scrollView)
      make.trailing.equalTo(scrollView)
      make.bottom.equalTo(scrollView)
      make.width.equalTo(scrollView.snp.width).offset(0) // subtract padding here
    }
    
    contentView.addSubview(userNameLabel)
    userNameLabel.snp.makeConstraints { (make) in
      make.top.equalTo(contentView).offset(80)
      make.leading.equalTo(contentView).offset(60)
      make.trailing.equalTo(contentView).offset(-60)
    }
    
    contentView.addSubview(userImageView)
    userImageView.snp.makeConstraints { (make) in
      make.top.equalTo(userNameLabel.snp.bottom).offset(40)
      make.centerX.equalTo(contentView)
      make.width.equalTo(130)
      make.height.equalTo(130)
    }
    
    contentView.addSubview(profileIconInfoView)
    profileIconInfoView.snp.makeConstraints { (make) in
      make.top.equalTo(userImageView.snp.bottom).offset(50)
      make.leading.equalTo(contentView)
      make.trailing.equalTo(contentView)
      make.height.equalTo(106)
    }
    
    contentView.addSubview(ratingButton)
    ratingButton.snp.makeConstraints { (make) in
      make.top.equalTo(profileIconInfoView.snp.top).offset(-15)
      make.centerX.equalTo(contentView)
      make.width.equalTo(140)
      make.height.equalTo(30)
    }
    
    contentView.addSubview(descriptionView)
    descriptionView.snp.makeConstraints { (make) in
      make.top.equalTo(profileIconInfoView.snp.bottom)
      make.leading.equalTo(contentView)
      make.trailing.equalTo(contentView)
    }
    
    contentView.addSubview(bookingRecommendationsView)
    bookingRecommendationsView.snp.makeConstraints { (make) in
      make.top.equalTo(descriptionView.snp.bottom)
      make.leading.equalTo(contentView)
      make.trailing.equalTo(contentView)
      make.height.equalTo(282)
      make.bottom.equalTo(contentView)
    }
  }
  
}
