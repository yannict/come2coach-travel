//
//  IAPManager.swift
//  come2coach
//
//  Created by Igor Prysyazhnyuk on 3/15/17.
//  Copyright © 2017 MaytecNet. All rights reserved.
//

import StoreKit

class IAPManager: NSObject {
    static let instance = IAPManager()
    
    fileprivate var productId: String?
    
    typealias OnSuccess = (String) -> ()
    typealias OnError = (_ error: String) -> ()
    typealias OnCancel = () -> ()
    
    fileprivate var onSuccess: OnSuccess?
    fileprivate var onError: OnError?
    fileprivate var onCancel: OnCancel?
    fileprivate var productsCountToBuy = 1
    
    func initialize() {
        SKPaymentQueue.default().add(self)
    }
    
    func buyProduct(count: Int = 1, productId: String, onSuccess: @escaping OnSuccess, onError: @escaping OnError, onCancel: OnCancel? = nil) {
        self.onSuccess = onSuccess
        self.onError = onError
        self.onCancel = onCancel
        self.productId = productId
        productsCountToBuy = count
        
        guard SKPaymentQueue.canMakePayments() else {
            onError("Zahlung nicht möglich".localized)
            return
        }
        
        let productsRequest = SKProductsRequest(productIdentifiers: [productId])
        productsRequest.delegate = self
        productsRequest.start()
    }
}

extension IAPManager: SKProductsRequestDelegate {
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        guard let product = response.products.filter({ $0.productIdentifier == productId }).first else {
            onError?("Kauf nicht möglich".localized)
            return
        }
        let payment = SKMutablePayment(product: product)
        payment.quantity = productsCountToBuy
        SKPaymentQueue.default().add(payment)
    }
    
    func currencySymbolForProduct(product: SKProduct) -> String? {
        let locale = product.priceLocale
        return locale.currencySymbol
    }
}

extension IAPManager: SKPaymentTransactionObserver {
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        for transaction in transactions {
            var shouldFinishTransaction = true
            switch transaction.transactionState {
            case .purchasing:
                shouldFinishTransaction = false
            case .purchased:
                guard let transactionId = transaction.transactionIdentifier else { break }
                onSuccess?(transactionId)
            case .failed:
                if let error = transaction.error as? SKError {
                    switch error.code {
                    case .paymentCancelled: onCancel?()
                    default: onError?(error.localizedDescription)
                    }
                } else { onError?("Kauf fehlgeschlagen".localized) }
            default: break
            }
            if shouldFinishTransaction {
                SKPaymentQueue.default().finishTransaction(transaction)
            }
        }
    }
}
