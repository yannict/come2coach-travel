//
//  LoginManager.swift
//  come2coach
//
//  Created by Marc Ortlieb on 19.08.16.
//  Copyright © 2016 MaytecNet. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON


class LoginManagerOld {

    static let sharedInstance = LoginManagerOld()

    static var AutoLoginAllowed = true

    fileprivate init() {
    }

    // MARK: LOGIN: COME2COACH

    func loginCome2Coach(_ user: User, handler: @escaping (_ errors: [String], _ user: User?) -> Void) {

        let headers = [
                "Token": Constants.API.accessToken,
                "Content-Type": "application/json"
        ]
        var parameters = [
                "email": user.email,
                "password": user.password
        ]
        if let pushToken = EventManager.sharedInstance.pushNotificationDeviceToken {
            parameters["APNSPushToken"] = pushToken
        }
        //OnDebugging.print("login params: \(parameters)")

        Alamofire
                .request(HTTPRequest.rootUrl + "auth/login",
                        method: .post,
                        parameters: parameters, encoding: JSONEncoding.default, headers: headers)
                .validate()
                .debugRequest(HTTPRequest.rootUrl + "auth/login")
                .responseJSON { response in

                    if let statusCode = response.response?.statusCode {
                        switch statusCode {
                        case 401:
                            // fixed MAY-83, avoid auto-login in case of exception
                            LoginManagerOld.AutoLoginAllowed = false
                            handler(["Mail und/oder Passwort falsch"], user)
                            break
                        default: break
                        }
                    }

                    var alertErrors = [String]()

                    switch response.result {
                    case .failure(let error):

                        OnDebugging
                                .printError("HTTPRequest.login() response.result.Failure: \(error)")

                        // fixed MAY-83, avoid auto-login in case of exception
                        LoginManagerOld.AutoLoginAllowed = false

                        handler([error.localizedDescription], nil)
                    case .success:
                        guard let value = response.result.value else {
                            OnDebugging
                                    .printError("HTTPRequest.login() response.result.value == nil")
                            handler(["Ungültige Antwort vom Server"], nil)
                            return
                        }
                        let json = JSON(value)
                        OnDebugging
                                .print("HTTPRequest.login() - JSON(value)\(json)")


                        let success = json["success"].boolValue

                        if !success {


                            // fixed MAY-83, avoid auto-login in case of exception
                            LoginManagerOld.AutoLoginAllowed = false

                            // possible errors
                            let responseErrors = json["errors"]
                            let email = responseErrors["email"].arrayValue
                            let password = responseErrors["password"].arrayValue

                            // handle errors
                            if email.contains("min") {
                                alertErrors += "Deine E-Mail ist zu kurz"
                            }
                            if email.contains("required") {
                                alertErrors += "Gib deine E-Mail ein"
                            }
                            if password.contains("required") {
                                alertErrors += "Gib dein Passwort ein"
                            }
                            if password.contains("min") {
                                alertErrors += "Dein Passwort ist zu kurz"
                            }
                            if alertErrors.isEmpty {
                                alertErrors += "E-Mail und/oder Passwort falsch"
                            }
                            handler(alertErrors, nil)
                        } else {
                            let userJSON = json["user"]

                            let newUser = User()
                            newUser.JSONWebToken = json["token"].stringValue
                            newUser.email = user.email
                            newUser.password = user.password

                            newUser.isVerified = userJSON["verified"].intValue
                            newUser.lname = userJSON["lname"].stringValue
                            newUser.name = userJSON["name"].stringValue
                            newUser.provider = userJSON["provider"].stringValue
                            newUser.provider_id = userJSON["provider_id"].stringValue
                            newUser.lastLoginType = User.LoginType.come2coach.rawValue
                            newUser.id = userJSON["id"].intValue


                            /* json["user"]
                             "is_company":0,
                             "lang":"",
                             "rating_votes":0,
                             "active":0,
                             "image":"",
                             "updated_at":"2016-08-19 23:19:05",
                             "lname":"Kratochwil",
                             "last_online":{},
                             "rating_details":{},
                             "pp_email":"buchhaltung@come2coach.de",
                             "invoice_data":{},
                             "name":"Werner",
                             "verified":1470900667,
                             "rating_points":"0.0",
                             "id":61,
                             "gender":null,
                             "settings":{},
                             "email":"test@me.com",
                             "invoicing":0,
                             "mobile":"004369911527104",
                             "about":null,
                             "newsletter":0,
                             "competences":[],
                             "avatar":"",
                             "provider_id":null,
                             "provider":"internal",
                             "created_at":"2016-08-03 09:16:44",
                             "interests":[],
                             "qualifications":[],
                             "bday":null
                             */

                            handler([], newUser)
                        }

                    }


                }
    }


    func loginCome2Coach(mail: String, password: String, handler: @escaping (_ errors: [String], _ user: User?) -> Void) {

        let headers = [
                "Token": Constants.API.accessToken,
                "Content-Type": "application/json"
        ]
        var parameters = [
                "email": mail,
                "password": password
        ]

        if let pushToken = EventManager.sharedInstance.pushNotificationDeviceToken {
            parameters["APNSPushToken"] = pushToken
        }
        //OnDebugging.print("login params: \(parameters)")


        Alamofire
                .request(HTTPRequest.rootUrl + "auth/login",
                        method: .post,
                        parameters: parameters,
                        encoding: JSONEncoding.default,
                        headers: headers)
                .validate()
                .responseJSON { response in

                    OnDebugging.print("HTTPRequest.login() - statusCode \(response.response?.statusCode)")

                    if let statusCode = response.response?.statusCode {
                        switch statusCode {
                        case 401: handler(["Mail und/oder Passwort falsch"], nil)
                        default: break
                        }
                    }

                    var alertErrors = [String]()
                    var user = User()

                    switch response.result {
                    case .failure(let error):
                        OnDebugging
                                .printError("HTTPRequest.login() response.result.Failure: \(error)")
                        handler([error.localizedDescription], nil)
                    case .success:
                        guard let value = response.result.value else {
                            OnDebugging
                                    .printError("HTTPRequest.login() response.result.value == nil")
                            return
                        }
                        let json = JSON(value)

                        OnDebugging
                                .print("HTTPRequest.login() - JSON(value)\(json)")


                        let success = json["success"].boolValue

                        if !success {
                            // possible errors
                            let responseErrors = json["errors"]
                            let email = responseErrors["email"].arrayValue
                            let password = responseErrors["password"].arrayValue

                            // handle errors
                            if email.contains("min") {
                                alertErrors += "Deine E-Mail ist zu kurz".localized
                            }
                            if email.contains("required") {
                                alertErrors += "Gib deine E-Mail ein".localized
                            }
                            if password.contains("required") {
                                alertErrors += "Gib dein Passwort ein".localized
                            }
                            if password.contains("min") {
                                alertErrors += "Dein Passwort ist zu kurz".localized
                            }
                            if alertErrors.isEmpty {
                                alertErrors += "Mail und/oder Passwort falsch".localized
                            }
                            handler(alertErrors, nil)
                        } else {
                            //                        if let jwt = json["token"].string {
                            //                            JSONWebToken.sharedInstance.value = jwt
                            //                        } else {
                            //                            OnDebugging
                            //                                .printError("HTTPRequest.login(): Could not decode JSONWebToken from JSON")
                            //                        }


                            user = User()
                            user.JSONWebToken = json["token"].stringValue
                            let userJSON = json["user"]
                            user.email = userJSON["email"].stringValue
                            user.password = password
                            user.isVerified = userJSON["verified"].intValue
                            user.lname = userJSON["lname"].stringValue
                            user.name = userJSON["name"].stringValue
                            user.provider = userJSON["provider"].stringValue
                            user.provider_id = userJSON["provider_id"].stringValue
                            user.lastLoginType = User.LoginType.come2coach.rawValue
                            user.isVerified = userJSON["verified"].intValue
                            user.id = userJSON["id"].intValue
                            user.push_token = json["push_token"].stringValue


                            /*
                            #swift3
                            user = User().initWith {
                                $0.JSONWebToken = json["token"].stringValue
                                let userJSON = json["user"]
                                $0.email = userJSON["email"].stringValue
                                $0.password = password

                                $0.isVerified = userJSON["verified"].boolValue
                                $0.lname = userJSON["lname"].stringValue
                                $0.name = userJSON["name"].stringValue
                                $0.provider = userJSON["provider"].stringValue
                                $0.provider_id = userJSON["provider_id"].stringValue

                                $0.lastLoginType = User.LoginType.come2coach.rawValue
                            }
                            */


                            //user.save()

                            /* json["user"]
                             "is_company":0,
                             "lang":"",
                             "rating_votes":0,
                             "active":0,
                             "image":"",
                             "updated_at":"2016-08-19 23:19:05",
                             "lname":"Kratochwil",
                             "last_online":{},
                             "rating_details":{},
                             "pp_email":"buchhaltung@come2coach.de",
                             "invoice_data":{},
                             "name":"Werner",
                             "verified":1470900667,
                             "rating_points":"0.0",
                             "id":61,
                             "gender":null,
                             "settings":{},
                             "email":"test@me.com",
                             "invoicing":0,
                             "mobile":"004369911527104",
                             "about":null,
                             "newsletter":0,
                             "competences":[],
                             "avatar":"",
                             "provider_id":null,
                             "provider":"internal",
                             "created_at":"2016-08-03 09:16:44",
                             "interests":[],
                             "qualifications":[],
                             "bday":null
                             */

                            handler([], user)
                        }

                    }


                }
    }


    // MARK: LOGIN: FACEBOOK

    func loginFB(_ FBToken: String, handler: @escaping (_ errors: [String], _ user: User?) -> Void) {

        let headers = [
                "Token": Constants.API.accessToken,
                "Content-Type": "application/json"
        ]
        OnDebugging.print("FBToken: \(FBToken)")

        var parameters = [
                "token": FBToken,
        ]

        if let pushToken = EventManager.sharedInstance.pushNotificationDeviceToken {
            parameters["APNSPushToken"] = pushToken
        }

        //OnDebugging.print("login params: \(parameters)")

        //todo: move to httprequest


        Alamofire
                .request(
                        HTTPRequest.rootUrl + "auth/login/facebook",
                        method: .post,
                        parameters: parameters,
                        encoding: JSONEncoding.default,
                        headers: headers)
                .validate()
                .responseJSON { response in


                    if let statusCode = response.response?.statusCode {
                        switch statusCode {
                        case 401: handler(["Mail und/oder Passwort falsch"], nil)
                        default: break
                        }
                    }



                    var errors = [String]()


                    switch response.result {
                    case .failure(let error):
                        OnDebugging
                                .printError("HTTPRequest: response.result == failure error:\(error.localizedDescription)")

                        handler([error.localizedDescription], nil)

                    case .success:
                        OnDebugging.print("HTTPRequest: Success")
                        guard let value = response.result.value else {
                            OnDebugging
                                    .printError("value = response.result.value",
                                    title: "HTTPRequest.loginFB()")
                            return
                        }
                        let json = JSON(value); OnDebugging.print(json)


                        let success = json["success"].boolValue

                        if !success {
                            // possible errors
                            let responseErrors = json["errors"]
                            let email = responseErrors["email"].arrayValue
                            let password = responseErrors["password"].arrayValue

                            // handle errors
                            if email.contains("min") {
                                errors += ["Dein Passwort ist zu kurz".localized]
                            }
                            if email.contains("required") {
                                errors += ["Gib deine Mail ein".localized]
                            }
                            if password.contains("required") {
                                errors += ["Gib dein Passwort ein".localized]
                            }

                            if let accessDenied = json["error"].string {
                                errors += accessDenied

                                OnDebugging
                                        .print("API: Access denied - error: \(accessDenied)")
                            }
                            handler(errors, nil)
                        } else {
                            let userJSON = json["user"]
                            let newUser = User()
                            newUser.JSONWebToken = json["token"].stringValue
                            newUser.facebookToken = FBToken

                            newUser.isVerified = userJSON["verified"].intValue
                            newUser.lname = userJSON["lname"].stringValue
                            newUser.name = userJSON["name"].stringValue
                            newUser.provider = userJSON["provider"].stringValue
                            newUser.provider_id = userJSON["provider_id"].stringValue
                            newUser.lastLoginType = User.LoginType.facebook.rawValue

                            handler(errors, newUser)
                        }
                    }


                }
    }


    func register(_ user: User, handler: @escaping (_ errors: [String]) -> Void) {

        let headers = [
                "Token": Constants.API.accessToken,
                "Content-Type": "application/json"
        ]

        let parameters: [String: AnyObject] = [
                "name": user.name as AnyObject,
                "lname": user.lname as AnyObject,
                "email": user.email as AnyObject,
                "provider": user.provider as AnyObject,
                "provider_id": user.provider_id as AnyObject,
                "provider_token": user.provider_token as AnyObject,
                "password": user.password as AnyObject,
                "newsletter": user.subscribeNewsletter as AnyObject,
                "image": user.imageStringUrl! as AnyObject
        ]

        Alamofire
                .request(
                        HTTPRequest.rootUrl + "auth/register",
                        method: .post,
                        parameters: parameters,
                        encoding: JSONEncoding.default,
                        headers: headers)
                .validate()
                .responseJSON { response in

                    OnDebugging.print("serverResponseStatusCode: \(response.response?.statusCode)")


                    switch response.result {
                    case .failure(let error):
                        handler([error.localizedDescription])
                    case .success:
                        guard let value = response.result.value else {
                            OnDebugging.print("response.result.value == nil")
                            return
                        }
                        let json = JSON(value); OnDebugging.print(json)

                        // Wired API Response
                        // If registration is successfull server response is null!
                        // if not sucessfull -> only Errors (& success value not delivered)
                        let success = json["sucess"].boolValue // API typo ?

                        if !success {

                            // possible errors
                            let responseErrors = json["errors"]
                            let name = responseErrors["name"].arrayValue
                            let email = responseErrors["email"].arrayValue
                            let password = responseErrors["password"].arrayValue

                            var errors = [String]()

                            // handle errors
                            if email.contains("unique") {
                                errors += ["Diese E-Mail ist bereits registriert"]
                            }
                            if email.contains("email") {
                                errors += ["Diese E-Mail ist unzulässig"]
                            }
                            if name.contains("min") {
                                errors += ["Wähle einen längeren Namen"]
                            }
                            if name.contains("alpha") {
                                errors += ["Nur Buchstaben sind im Namen erlaubt"]
                            }
                            // check emtyness

                            if name.contains("required") {
                                errors += ["Gib deinen Namen ein"]
                            }
                            if email.contains("required") {
                                errors += ["Gib deine Mail an"]
                            }
                            if password.contains("required") {
                                errors += ["Setze ein Passwort"]
                            }

                            if password.contains("min") {
                                errors += ["Benutze ein längeres Passwort"]
                            }
                            handler(errors)
                        }
                    }
                }
    }

    func resetPassword(_ email: String, handler: @escaping (_ errors: [String]?) -> Void) {
        // Production
        //
        let url = URL(string: HTTPRequest.rootUrl + "auth/reset")!
        let headers = [
                "Token": Constants.API.accessToken,
                "Content-Type": "application/json"
        ]
        let parameters = [
                "email": email
        ]

        Alamofire
                .request(url,
                        method: .post,
                        parameters: parameters,
                        encoding: JSONEncoding.default,
                        headers: headers)
                .validate()
                .responseJSON { response in
                    switch response.result {
                    case .failure(let error):
                        OnDebugging.print("Error - \(error.localizedDescription)")
                        handler([error.localizedDescription])
                    case .success:
                        guard let value = response.result.value else {
                            OnDebugging.print("response.result.value == nil")
                            return
                        }

                        let json = JSON(value)
                        let responseDictionary = json.dictionaryValue

                        // posible error
                        if let errorString = responseDictionary["message"]?.stringValue {
                            OnDebugging.print("Error - \(errorString)")
                            handler(["Error - \(errorString)"])
                            return
                        }

                        if let responseErrors = responseDictionary["errors"]?.dictionaryValue {
                            // posible error
                            OnDebugging.print("Error - \(responseErrors)")
                            handler(["Error - \(responseErrors)"])
                        } else {
                            handler(nil)
                        }
                    }
                }
    }

    func newPassword(_ token: String, _ password: String, handler: @escaping (_ success : Bool) -> Void) {

        let url = URL(string: HTTPRequest.rootUrl + "auth/password/" + token)!

        let headers = [
                "Token": Constants.API.accessToken,
                "Content-Type": "application/json"
        ]

        let parameters = [
                "password": password
        ]

        Alamofire
                .request(url,
                        method: .post,
                        parameters: parameters,
                        encoding: JSONEncoding.default,
                        headers: headers)
                .validate()
                .debugRequest(url.absoluteString)
                .responseJSON { response in


                    switch response.result {
                    case .failure(let error):
                        // OnDebugging.print("Error - \(error.localizedDescription)")
                        handler(false)
                    case .success:

                        guard let value = response.result.value else {
                            OnDebugging.print("response.result.value == nil")
                            return
                        }

                        handler(true)

                        return

                    }
                }


    }
}








