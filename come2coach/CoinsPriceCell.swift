//
//  CoinsPriceCell.swift
//  come2coach
//
//  Created by Igor Prysyazhnyuk on 3/20/17.
//  Copyright © 2017 MaytecNet. All rights reserved.
//

import UIKit

class CoinsPriceCell: UITableViewCell {
    @IBOutlet weak var selectionImageView: UIImageView!
    @IBOutlet weak var coinsCountLabel: UILabel!
    @IBOutlet weak var coinsPriceLabel: UILabel!
    
    func setData(coinsProduct: CoinsProduct, selected: Bool) {
        coinsCountLabel.text = "\(coinsProduct.coinsCount.stringFormattedWithSeparator) \("Coins".localized)"
        coinsPriceLabel.text = "\(coinsProduct.price) €"
        selectionImageView.isHighlighted = selected
    }
}
