//
//  CircleLoader.swift
//  come2coach
//
//  Created by Marc Ortlieb on 04.08.16.
//  Copyright © 2016 Marc Ortlieb. All rights reserved.
//

import Foundation


class CircleLoaderView: UIView {

    fileprivate var backgroundCircle: CircularLoaderView!
    fileprivate var progressCircle: CircularLoaderView!

    var progress: CGFloat = 0 {
        didSet {
            progressCircle.progress = progress
        }
    }
    func updateProgress(to progress: CGFloat) {
        progressCircle.progress = progress
    }

    /// call this method to adjust circleRadius in viewDidLayoutSubviews() of parentVC that owns CircleLoaderView

    func setCircleRadius(_ radius: CGFloat, circleLineWidth: CGFloat = 10) {
        //backgroundCircle.lineWidth = circleLineWidth
        //backgroundCircle.circleRadius = radius - backgroundCircle.lineWidth / 2

        progressCircle.lineWidth = circleLineWidth
        progressCircle.circleRadius = radius - progressCircle.lineWidth / 2
    }

    init() {
        super.init(frame: CGRect.zero)
        configure()
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configure()
    }

    func configure() {

        /*
        backgroundCircle = CircularLoaderCoach(frame: bounds)
        backgroundCircle.progress = 1
        backgroundCircle.circlePathLayer.strokeColor = UIColor.white.cgColor
        //backgroundCircle.backgroundColor = UIColor.black
        //backgroundCircle.alpha = 0.85
        addSubview(backgroundCircle)
        //sendSubview(toBack: backgroundCircle)
        */

        progressCircle = CircularLoaderView(frame: bounds)
        // first progress begins at 12 o' Clock
        progressCircle.transform = CGAffineTransform(rotationAngle: CGFloat(-M_PI_2))
        addSubview(progressCircle)
        sendSubview(toBack: progressCircle)
    }

}

extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")

        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }

    convenience init(netHex: Int) {
        self.init(red: (netHex >> 16) & 0xff, green: (netHex >> 8) & 0xff, blue: netHex & 0xff)
    }
}


private class CircularLoaderView: UIView {

    let circlePathLayer = CAShapeLayer()
    var circleRadius: CGFloat = 0
    var lineWidth: CGFloat = 10

    var progress: CGFloat {
        get {
            return circlePathLayer.strokeEnd
        }
        set {
            circlePathLayer.strokeEnd = 1

            if (newValue > 1) {
                circlePathLayer.strokeStart = 1
            } else if (newValue < 0) {
                circlePathLayer.strokeStart = 0
            } else {
                circlePathLayer.strokeStart = newValue
            }
        }
    }

    init() {
        super.init(frame: CGRect.zero)
        configure()
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configure()
    }

    func configure() {
        progress = 0
        circleRadius = 150
        circlePathLayer.frame = bounds
        circlePathLayer.lineWidth = lineWidth
        circlePathLayer.fillColor = UIColor(netHex: 0x252525).withAlphaComponent(0.8).cgColor // UIColor(red: 0.0/0.0, green: 0.0/0.0, blue: 0.0/0.0, alpha: 0.85).cgColor // UIColor.clear.cgColor
        circlePathLayer.strokeColor = UIColor.white.withAlphaComponent(0.3).cgColor // UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 0.5).cgColor // UIColor(red: 137.0/255.0, green: 212.0/255.0, blue: 253.0/255.0, alpha: 1.00).cgColor

        layer.addSublayer(circlePathLayer)
        backgroundColor = .clear
        autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }

    func circleFrame() -> CGRect {
        var circleFrame = CGRect(x: 0, y: 0, width: 2 * circleRadius, height: 2 * circleRadius)
        circleFrame.origin.x = circlePathLayer.bounds.midX - circleFrame.midX
        circleFrame.origin.y = circlePathLayer.bounds.midY - circleFrame.midY
        return circleFrame
    }

    func circlePath() -> UIBezierPath {
        return UIBezierPath(ovalIn: circleFrame())
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        circlePathLayer.frame = bounds
        circlePathLayer.path = circlePath().cgPath
    }

}
