//
//  LoginPreviewVC.swift
//  come2coach
//
//  Created by Marc Ortlieb on 01.08.16.
//  Copyright © 2016 Marc Ortlieb. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import RealmSwift


// Facebook

//import FBSDKCoreKit
//import FBSDKLoginKit
import FacebookCore
import FacebookLogin


class LoginPreviewVC: UIViewController {


    @IBAction
    func loginWithFacebook(_ sender: AnyObject) {


        User.loginManager.completion = { () in


            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let window = appDelegate.window

            window?.rootViewController = UINavigationController(Storyboards.ViewControllers.mainTabBar)

        }

        User.loginManager.error = { (txt) in


            self.present(UIAlertController("Es ist ein Fehler aufgetreten".localized, txt, actions: UIAlertAction(.OK)), animated: true)

        }

        User.loginManager.loginFacebook(self)

        /*
         let loginManager = LoginManager()

        loginManager.logIn([ .publicProfile ], viewController: self) { loginResult in
            switch loginResult {
            case .failed(let error):
                print(error)
            case .cancelled:
                print("User cancelled login.")
            case .success(let grantedPermissions, let declinedPermissions, let accessToken):
                print("Logged in! ")
                debugPrint(accessToken)
                break
            }
        }

*/

        /*
        if ((FBSDKAccessToken.current()) != nil) {
            //returnUserData()
            self.loginFacebookUser2Server()
        } else {

            let fbLoginManager = FBSDKLoginManager()
            fbLoginManager.loginBehavior = FBSDKLoginBehavior.systemAccount

            fbLoginManager.logIn(withReadPermissions: ["email"], from: self, handler: { (result, error) -> Void in

                if (error != nil) {

                    // #swift 3
                    //_self.unwind(for: _self) {
                    let alertViewController =
                            UIAlertController(title: "Keine Berechtigung für Facebook".localized, message: "Bitte versuche es später noch einmal. Fachebook verweigerte soeben den Zugriff.".localized, preferredStyle: .alert)
                    alertViewController += UIAlertAction(.OK)
                    self.present(alertViewController, animated: true)
                    //}
                    OnDebugging.print("Keine Berechtigung für Facebook:\(error)")
                } else {
                    let fbloginresult: FBSDKLoginManagerLoginResult = result!

                    if (fbloginresult.isCancelled) {
                        // #swift3
                        //_self.unwind(for: _self) {
                        let alertViewController =
                                UIAlertController(title: "Keine Berechtigung für Facebook".localized, message: "Bitte bei Facebook einloggen und die Berechtigungen bestätigen.".localized, preferredStyle: .alert)
                        alertViewController += UIAlertAction(.OK)
                        self.present(alertViewController, animated: true)
                        //}
                    } else if (fbloginresult.grantedPermissions.contains("email")) {

                        let currAccessToken = FBSDKAccessToken.current();
                        if (currAccessToken != nil) {
                            //self.returnUserData()
                            self.loginFacebookUser2Server()
                        }

                        //fbLoginManager.logOut()
                    } else {
                        // #swift3
                        //_self.unwind(for: _self) {
                        let alertViewController =
                                UIAlertController(title: "Keine Berechtigung für Facebook".localized, message: "Bitte die Erlaubnis geben, die EMail-Adresse zu lesen.".localized, preferredStyle: .alert)
                        alertViewController += UIAlertAction(.OK)
                        self.present(alertViewController, animated: true)
                        // }

                    }
                }
            })
        }

        */


    }

    @IBAction func facebookLogoutAction(_ sender: AnyObject) {
        // FBSDKLoginManager().logOut() #facebook
    }


    @IBAction func showLoginVC(_ sender: AnyObject) {
        User.loginManager.AutoLoginAllowed = true
        show(Storyboards.ViewControllers.login)
    }

    @IBAction func showRegistrationVC(_ sender: AnyObject) {
        show(Storyboards.ViewControllers.registration)
    }

    @IBOutlet weak var btLoginFacebook: UIButton!
    @IBOutlet weak var btLoginMail: UIButton!
    @IBOutlet weak var btRegister: UIButton!

    // MARK: ViewDidLoad

    override func viewDidLoad() {
        super.viewDidLoad()

        //btLoginFacebook.delgate = self
        // if login data already there, load data and proceed
    }


    func loginHandler(_ errors: [String], user: User?) {

        if errors.isContaining {
            OnDebugging
                    .printError("loginCome2Coach - errors \(errors)", title: "InitialStartVC.viewDidLoad()")

        }

        guard let user = user else {
            self.show(Storyboards.ViewControllers.loginPreview)
            return
        }
        user.save()

        let coachingsVC = Storyboards.ViewControllers.mainTabBar
        self.show(coachingsVC)

    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        /*   #inetmanager
        guard InternetConnectionManager.sharedInstance.isConnected == true else
        {
            // wenn keine Internet-Verbindung zu den Coachings weiterleiten
            self.show(Storyboards.ViewControllers.coachings)
            return
        }
        */

        hideBackButton()

        // Change later (for logout state)
        //
        if User.current?.lastLoginType == "" {
            return
        }

        User.loginManager.completion = { () in


            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let window = appDelegate.window

            window?.rootViewController = UINavigationController(Storyboards.ViewControllers.mainTabBar)

        }

        User.loginManager.error = { (txt) in


            self.present(UIAlertController("Bitte korrigieren:".localized, txt, actions: UIAlertAction(.OK)), animated: true)

        }

        User.loginManager.showLogin = { () in

            self.show(Storyboards.ViewControllers.login)

        }

        User.loginManager.autoLogin(User.current)



        /*

        #loginchange

        if (User.current != nil && LogiManager.AutoLoginAllowed) {


            if (LoginManager.AutoLoginAllowed) {

                switch User.current!.lastLoginType {
                case User.LoginType.come2coach.rawValue:
                    LoginManager.sharedInstance.loginCome2Coach(User.current!, handler: loginHandler)

                case User.LoginType.facebook.rawValue:
                    LoginManager.sharedInstance.loginFB(User.current!.facebookToken, handler: loginHandler)

                default:
                    LoginManager.AutoLoginAllowed = true
                    self.show(Storyboards.ViewControllers.login)
                    OnDebugging
                            .printError("switch user.lastLoginType -> case is default/ user.lastLoginType == empty or nil",
                            title: "EventManager.processEventsSequence()")
                }
            } else
            {
                LoginManager.AutoLoginAllowed = true
                self.show(Storyboards.ViewControllers.login)
            }
            */


//            let topViewController = ViewControllerManager.sharedInstance.topViewController()
//            topViewController

        // #flow

        // EventManager.sharedInstance.processEventsSequence(on: self)

        /*   self.show(Storyboards.ViewControllers.initialStart)
           {
               EventManager.sharedInstance.processEventsSequence(on: self)
        } */

    }


    func loginFacebookUser2Server() {


        /*
        #facebook
        // INFO: requires currentAccessToken to be NOT nil
        FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
                    /*if (error == nil){
                     OnDebugging.print(result)
                     result.valueForKey("email") as! String
                     result.valueForKey("id") as! String
                     result.valueForKey("name") as! String
                     result.valueForKey("first_name") as! String
                     result.valueForKey("last_name") as! String

                     }*/
                    if error != nil {
                        // #swift3
                        // self.unwind(for: self) {
                        let alertViewController =
                                UIAlertController(title: "Fehler beim Facebook Login".localized, message: "Bitte probiere es später nochmals.".localized, preferredStyle: .alert)
                        alertViewController += UIAlertAction(.OK)
                        self.present(alertViewController, animated: true)
                        // }
                        OnDebugging.print("Fehler bei FB Login:\(error)")
                    }

                })
        OnDebugging
                .print("Sending FBToken to server", title: "")


        let currAccessToken = FBSDKAccessToken.current()
        let token = currAccessToken!.tokenString

        LoginManagerOld.sharedInstance.loginFB(token!) { (errors, user) in

            if errors.isContaining {
                // #swift3
                //self.unwind(to: self) {
                let alertViewController =
                        UIAlertController(title: "Fehler beim Facebook Login".localized, message: "Bitte probiere es später nochmals.".localized, preferredStyle: .alert)
                alertViewController += UIAlertAction(.OK)
                self.present(alertViewController, animated: true)
                //}

                OnDebugging.print("Fehler bei FB Login:\(errors)")
            }
            // onSuccess
            else if errors.isEmpty {
                user?.save()
                EventManager.sharedInstance.processEventsSequence(on: self)
            }
        }
        */



        //WARNING ONLY USE FOR DEBUGGING
        //OnDebugging.print ("FBT:"+facebookToken)
    }


}



