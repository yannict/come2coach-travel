//
//  NSAttributedString+ex.swift
//  come2coach
//
//  Created by Marc Ortlieb on 17.08.16.
//  Copyright © 2016 MaytecNet. All rights reserved.
//


import Foundation


/** 
 concatenate attributed strings
 usecase: let helloworld = NSAttributedString(string:"Hello ") + NSAttributedString(string:"World")
 
 http://stackoverflow.com/questions/18518222/how-can-i-concatenate-nsattributedstrings
*/

func + (left: NSAttributedString, right: NSAttributedString) -> NSAttributedString {
    let result = NSMutableAttributedString()
    result.append(left)
    result.append(right)
    return result
}


// MARK: make Attributed String
// http://stackoverflow.com/questions/24666515/how-do-i-make-an-attributed-string-using-swift
