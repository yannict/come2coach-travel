//
//  BookingPopup.swift
//  come2coach
//
//  Created by Victor on 29.11.16.
//  Copyright © 2016 MaytecNet. All rights reserved.
//

import UIKit

protocol BookingPopupDelegate {
    func joinEvent(_ event: Event, participants: Int)
    func showBuyCoinsVC()
    var userCoinsCount: Int { get }
}

class BookingPopup: UIView {

    @IBOutlet weak var popoverView : UIView!
    @IBOutlet weak var participantsButton : UIButton!
    @IBOutlet weak var pickerView : UIView!
    @IBOutlet weak var countPickerView : UIPickerView!
    @IBOutlet weak var bookButton: CoinsCountButton!
    
    var delegate : BookingPopupDelegate?
    var participants = 1
    
    var event : Event?
    
    private var isCoinsEnough: Bool {
        guard let event = event, let delegate = delegate else { return false }
        return delegate.userCoinsCount >= Int(event.price) * participants
    }
    
    func showPopover() {
        self.layoutSubviews()

        /*

        let roundedPath = UIBezierPath.init(roundedRect: popoverView.frame, cornerRadius: 4)
        
        path.append(roundedPath)
        path.usesEvenOddFillRule = true
        */


        let path = UIBezierPath.init(rect: self.bounds)
        let fillLayer = CAShapeLayer.init()
        fillLayer.path = path.cgPath
        fillLayer.fillRule = kCAFillRuleEvenOdd
        fillLayer.fillColor = UIColor.black.cgColor
        fillLayer.opacity = 0.5
        self.layer.insertSublayer(fillLayer, at: 0)
        let participant = 1 /*event!.minParticipants */
        let title = participant == 1 ? String(participant) + " " + "Person".localized : String(participant) + " " + "Personen".localized
        participantsButton.setTitle(title, for: UIControlState())
        participants = 1
        updateBookButton()
        registerLocalNotifications()
    }
    
    private func registerLocalNotifications() {
        NotificationCenter.register(observer: self, localNotificationType: .updateBookingView, selector: #selector(updateBookButton))
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc fileprivate func updateBookButton() {
        guard let delegate = delegate else { return }
        if isCoinsEnough {
            bookButton.isCoinsEnoughTitle = "Buchung bestätigen".localized
            bookButton.setBlueBackground()
        } else {
            bookButton.isCoinsEnoughTitle = "Coins kaufen".localized
            bookButton.setRedBackground()
        }
        bookButton.coins = delegate.userCoinsCount
    }
    
    @IBAction func selectParticipants() {
        pickerView.isHidden = false
        countPickerView.reloadAllComponents()
        countPickerView.selectRow(0, /*participants! - event!.minParticipants*/ inComponent: 0, animated: false)
    }

    @IBAction func selectCount() {
        participants = countPickerView.selectedRow(inComponent: 0) + 1 /*+ event!.minParticipants*/
        let title = participants == 1 ? String(participants) + " " + "Person".localized : String(participants) + " " + "Personen".localized
        participantsButton.setTitle(title, for: UIControlState())
        pickerView.isHidden = true
        updateBookButton()
    }
    
    @IBAction func cancel() {
        pickerView.isHidden = true
    }
    
    @IBAction func saveEvent() {
        self.removeFromSuperview()
        if let _ = delegate {
            delegate?.joinEvent(event!, participants: participants)
        }
    }
    @IBAction func BookingButton(_ sender: AnyObject) {
        if isCoinsEnough {
            self.removeFromSuperview()
            if let _ = delegate {
                delegate?.joinEvent(event!, participants: participants)
            }
        } else { delegate?.showBuyCoinsVC() }
    }
    
    @IBAction func hidePopover() {
        self.removeFromSuperview()
    }
    
}

extension BookingPopup : UIPickerViewDataSource, UIPickerViewDelegate {
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return event!.maxParticipants - event!.minParticipants + 1
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let participant = row+1 // + event!.minParticipants
        return participant == 1 ? String(participant) + " " + "Person".localized : String(participant) + " " + "Personen".localized
    }
    
}
