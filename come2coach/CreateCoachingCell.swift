//
//  CreateCoachingCell.swift
//  come2coach
//
//  Created by Victor on 09.11.16.
//  Copyright © 2016 MaytecNet. All rights reserved.
//

import Foundation

protocol CreateCoachingCellDelegate {
    func didInitiateNewCoachingCreating()
}

class CreateCoachingCell: UICollectionViewCell {
    
    var delegate : CreateCoachingCellDelegate?
    
    @IBAction func createNewCoaching() {
        if let _ = delegate {
            delegate!.didInitiateNewCoachingCreating()
        }
    }
    
}
