//
//  LocalNotificationsExtension.swift
//
//
//  Created by Igor Prysyazhnyuk on 12/14/16.
//  Copyright © 2017 Steelkiwi. All rights reserved.
//

import Foundation

enum LocalNotificationType: String {
    case coinsBought = "coinsBought"
    case updateBookingView = "updateBookingView"
    
    var notificationName: Notification.Name {
        return NSNotification.Name(self.rawValue)
    }
    
    static let coinsCountParam = "coinsCount"
}

extension NotificationCenter {
    
    static func register(observer: Any, localNotificationType: LocalNotificationType, selector: Selector) {
        NotificationCenter.default.addObserver(observer,
                                               selector: selector,
                                               name: localNotificationType.notificationName,
                                               object: nil)
    }
    
    static func postNotification(localNotificationType: LocalNotificationType) {
        NotificationCenter.default.post(name: localNotificationType.notificationName, object: nil)
    }
    
    static func postCoinsBoughtNotification(coinsCount: Int) {
        NotificationCenter.default.post(name: LocalNotificationType.coinsBought.notificationName,
                                        object: nil,
                                        userInfo: [LocalNotificationType.coinsCountParam: coinsCount])
    }
}

extension Notification {
    
    var coinsCount: Int? {
        return userInfo?[LocalNotificationType.coinsCountParam] as? Int
    }
}
