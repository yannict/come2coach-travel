//
//  ParticipantEmailCell.swift
//  come2coach
//
//  Created by Igor Prysyazhnyuk on 3/23/17.
//  Copyright © 2017 MaytecNet. All rights reserved.
//

import UIKit

protocol ParticipantEmailCellDelegate: class {
    func emailChanged(email: String, index: Int)
}

class ParticipantEmailCell: UITableViewCell {
    @IBOutlet weak var emailAddressTextField: PaddedTextField!
    @IBOutlet weak var validationIndicator: UIImageView!
    
    private weak var delegate: ParticipantEmailCellDelegate?
    private var index = 0
    
    func setData(email: String?, index: Int, delegate: ParticipantEmailCellDelegate?) {
        emailAddressTextField.text = email
        self.index = index
        self.delegate = delegate
        emailAddressTextField.delegate = self
        validate()
    }
    
    private func validate() {
        if emailAddressTextField.text?.validateEmail() ?? false {
            validationIndicator.image = #imageLiteral(resourceName: "concurrent_true")
        } else { validationIndicator.image = #imageLiteral(resourceName: "concurrent_false") }
    }
    
    @IBAction func emailValueChanged(_ sender: Any) {
        validate()
        guard let text = emailAddressTextField.text else { return }
        delegate?.emailChanged(email: text, index: index)
    }
}

extension ParticipantEmailCell: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        contentView.endEditing(true)
        return true
    }
}
