//
//  UIViewController.swift
//  come2coach
//
//  Created by Marc Ortlieb on 01.08.16.
//  Copyright © 2016 Marc Ortlieb. All rights reserved.
//

import Foundation

public enum NavigationDirection {
    case forwards, backwards
}

public extension UIViewController {
    
    /*
     automatically generates StoryboardIdentifier for every ViewController
     you must set the ViewControllers Identifier in the storyBoard to the ViewControllers class name to match the storyboardID, used for ManagedViewControllers
     **/
    
    class var storyboardID: String {
        return String(describing: self)
    }
    
    
    /// one and only Navigation-forward method
    
    public func show(_ viewController: UIViewController, animated: Bool = false, modally: Bool = false, modalTransitionStyle: UIModalTransitionStyle = .crossDissolve, afterViewDidAppear completion: (() -> Void)? = nil) {
        
        if viewController is UIAlertController {
            present(viewController, animated: true, completion:completion)
            return
        }
        if modally {
            viewController.modalTransitionStyle = modalTransitionStyle
            present(viewController, animated: animated, completion: completion)
        } else {
            if let navigationController = navigationController {
                navigationController.push(viewController, animated: animated, completion:completion)
            }  else {
                present(viewController, animated: animated, completion: completion)
            }
        }
    }
    
    /// one and only navigation-back method
    
    func unwind(modaly: Bool = false, to destinationVC: UIViewController? = nil, animated: Bool = false, completion: (()->Void)? = nil) {
        
        if modaly || navigationController == nil {
            dismiss(animated: animated, completion: completion)
        }
        if let navigationController = navigationController {
            if let destinationVC = destinationVC {
                navigationController.pop(to: destinationVC, animated: animated, completion: completion)
            } else {
                navigationController.popToRoot(animated, completion: completion)
            }
        }
    }
    
    func hideBackButton(_ isHidden: Bool = true, animated: Bool = false) {
        navigationItem.setHidesBackButton(isHidden, animated: animated)
    }
    
    public func setupTitle(_ title_in: String?)
    {
        if let title = title_in
        {
            let titleLabel = UILabel()
            
            let attributes: NSDictionary = [
                NSFontAttributeName:Designs.FontVars.navigationBarTitleFont,
                NSForegroundColorAttributeName:Designs.FontVars.navigationBarTitleColor,
                NSKernAttributeName:Designs.FontVars.navigationBarTitleKern
            ]
            
            let attributedTitle = NSAttributedString(string: title, attributes: attributes as? [String : AnyObject])
            
            titleLabel.attributedText = attributedTitle
            titleLabel.sizeToFit()
            self.navigationItem.titleView = titleLabel
        }
    }

    
    
    /// depricated! -> use show()
    public func navigate(to viewController: UIViewController, animated: Bool = true) {
        navigationController?.push(viewController, animated: animated)
    }
    /*
    /// depricated! -> use show()
    public func present(_ viewController: UIViewController, animated: Bool = true, completion: (() -> Void)? = nil) {
        self.present(viewController, animated: animated, completion: completion)
    }
    */

    /// depricated! -> use unwind()
    public func dismissViewController(_ animated: Bool = true, completion: (() -> Void)? = nil ) {
        dismiss(animated: animated, completion: completion)
    }
    
}
