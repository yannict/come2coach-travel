//
//  CoachView.swift
//  come2coach
//
//  Created by Timotheus Laubengaier on 26.04.17.
//  Copyright © 2017 MaytecNet. All rights reserved.
//

import Foundation
import UIKit

class EventCoachView: UIView {
  
  let coachImageBackgroundView: UIView = {
    let view = UIView()
    view.backgroundColor = Styleguide.Color.tint.withAlphaComponent(0.3)
    view.clipsToBounds = true
    view.layer.cornerRadius = 40
    return view
  }()
  
  let coachImageButton: UIButton = {
    let view = UIButton()
    view.imageView?.contentMode = .scaleAspectFill
    view.backgroundColor = Styleguide.Color.lightGray
    view.clipsToBounds = true
    view.layer.cornerRadius = 35
    return view
  }()
  
  let nameLabel: UILabel = {
    let label = UILabel()
    label.text = "Akira Wong"
    label.font = Styleguide.Font.medium(size: 13)
    label.textColor = Styleguide.Color.text
    return label
  }()
  
  let ratingButton: UIButton = Styleguide.Button.rating(title: "4.5 | 5 Sternen")
  
  let seperatorView: UIView = {
    let view = UIView()
    view.backgroundColor = Styleguide.Color.tint.withAlphaComponent(0.3)
    return view
  }()
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    
    addSubview(coachImageBackgroundView)
    coachImageBackgroundView.snp.makeConstraints { (make) in
      make.leading.equalTo(self).offset(35)
      make.centerY.equalTo(self)
      make.width.equalTo(80)
      make.height.equalTo(80)
    }
    
    coachImageBackgroundView.addSubview(coachImageButton)
    coachImageButton.snp.makeConstraints { (make) in
      make.centerX.equalTo(coachImageBackgroundView)
      make.centerY.equalTo(coachImageBackgroundView)
      make.width.equalTo(70)
      make.height.equalTo(70)
    }
    
    addSubview(nameLabel)
    nameLabel.snp.makeConstraints { (make) in
      make.leading.equalTo(coachImageButton.snp.trailing).offset(52)
      make.top.equalTo(self).offset(29)
    }
    
    addSubview(ratingButton)
    ratingButton.snp.makeConstraints { (make) in
      make.top.equalTo(nameLabel.snp.bottom).offset(10)
      make.leading.equalTo(nameLabel.snp.leading)
      make.width.equalTo(140)
      make.height.equalTo(30)
    }
    
    addSubview(seperatorView)
    seperatorView.snp.makeConstraints { (make) in
      make.leading.equalTo(self)
      make.bottom.equalTo(self)
      make.trailing.equalTo(self)
      make.height.equalTo(1)
    }
    
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
}
