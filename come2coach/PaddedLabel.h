//
//  PaddedLabel.h
//  come2coach
//
//  Created by Werner Kratochwil Full on 04.08.16.
//  Copyright © 2016 MaytecNet. All rights reserved.
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE

@interface PaddedLabel : UILabel
@property (nonatomic, assign) IBInspectable CGFloat leftEdge;
@property (nonatomic, assign) IBInspectable CGFloat rightEdge;
@property (nonatomic, assign) IBInspectable CGFloat topEdge;
@property (nonatomic, assign) IBInspectable CGFloat bottomEdge;

@property (nonatomic, assign) UIEdgeInsets edgeInsets;

@end
