//
//  ViewControllerManager.swift
//  come2coach
//
//  Created by Marc Ortlieb on 01.08.16.
//  Copyright © 2016 Marc Ortlieb. All rights reserved.
//

import Foundation





struct Storyboards {
    
    
    // MARK: ViewControllers
    
    struct ViewControllers {
        
        /// list of all VCs from Storyboards, used for ManagedViewControllers
        
        static let all: [UIViewController] = [
            // Login & Registration
            Storyboards.ViewControllers.initialStart,
            Storyboards.ViewControllers.loginPreview,
            Storyboards.ViewControllers.login,
            Storyboards.ViewControllers.registration,
            Storyboards.ViewControllers.profilePhoto,
            Storyboards.ViewControllers.registrationConfirm,
//            Storyboards.ViewControllers.resetPassword,
            // Event Overview
            Storyboards.ViewControllers.eventOverview,
            // Coachings
            Storyboards.ViewControllers.coachings,
            Storyboards.ViewControllers.detailsWeb,
            // Livestreaming
            Storyboards.ViewControllers.countDown ,
            Storyboards.ViewControllers.coachPublish ,
            Storyboards.ViewControllers.participantSubsribe ,
            Storyboards.ViewControllers.livestreamingCoach ,
            Storyboards.ViewControllers.livestreamingParticipant,
            // Create Coaching
            Storyboards.ViewControllers.createCoachingNavCtrl,
            Storyboards.ViewControllers.chooseCoachingMode,
            Storyboards.ViewControllers.coachingDetails,
            Storyboards.ViewControllers.coachingPrice,
            Storyboards.ViewControllers.coachingDescription,
            Storyboards.ViewControllers.coachingPlanning,
            Storyboards.ViewControllers.resetPassword,
            Storyboards.ViewControllers.newPassword
        ]
        
        
        
        // MARK: 1.) Login & Registration
        
        fileprivate static let loginStoryBoard = UIStoryboard(name: Constants.Storyboards.login)
        
        static var initialStart: InitialStartVC {
            return loginStoryBoard.instanciateVC()
        }
        static var loginPreview: LoginPreviewVC {
            return loginStoryBoard.instanciateVC()
        }
        static var login: LoginVC {
            return loginStoryBoard.instanciateVC()
        }
        static var registration: RegistrationVC {
            return loginStoryBoard.instanciateVC()
        }
        static var profilePhoto: AddProfilePhotoVC {
            return loginStoryBoard.instanciateVC()
        }

        static var registrationConfirm : ConfirmNotificationVC {
            return loginStoryBoard.instanciateVC()
        }

        static var resetPassword: ResetPasswordVC {
            return loginStoryBoard.instanciateVC()
        }
        static var newPassword: NewPasswordVC {
            return loginStoryBoard.instanciateVC()
        }
        
        
        // MARK: 2.) Coachings
        
        fileprivate static let coachingsStoryBoard = UIStoryboard(name: Constants.Storyboards.coachings)

        static var coachings: CoachingsVC {
            return coachingsStoryBoard.instanciateVC()
        }
        
        static var eventOverview: EventOverviewVC {
            return coachingsStoryBoard.instanciateVC()
        }
        
        static var eventAdmin: EventAdminVC {
            return coachingsStoryBoard.instanciateVC()
        }

        static var favoriteEvents: FavoriteEventsVC {
            return coachingsStoryBoard.instanciateVC()
        }
        
        static var eventDetails: EventDetailsVC {
            return coachingsStoryBoard.instanciateVC()
        }

        static var coachDetails: CoachDetailsVC {
            return coachingsStoryBoard.instanciateVC()
        }

        static var coachRatings : CoachRating {
            return coachingsStoryBoard.instanciateVC()
        }

        static var verification1 : VerificationVC1 {
            return coachingsStoryBoard.instanciateVC()
        }

        static var verification2 : VerificationVC2 {
            return coachingsStoryBoard.instanciateVC()
        }


        static var emailVC: EmailViewController {
            return coachingsStoryBoard.instanciateVC()
        }
        
        static var detailsWeb: DetailsWebVC {
            return coachingsStoryBoard.instanciateVC()
        }
        
        // MARK: 3.) Livestreaming
        
        fileprivate static let livestreamingStoryBoard = UIStoryboard(name: Constants.Storyboards.livestreaming)
        
        static var countDown: CountDownVC {
            return livestreamingStoryBoard.instanciateVC()
        }

        static var coachPublish: CoachPublishVC {
            return livestreamingStoryBoard.instanciateVC()
        }

        static var participantSubsribe: ParticipantSubscribeVC {
            return livestreamingStoryBoard.instanciateVC()
        }
        
        static var livestreamingCoach: LivestreamingCoachVC {
            return livestreamingStoryBoard.instanciateVC()
        }
        static var livestreamingParticipant: LivestreamingParticipantVC {
            return livestreamingStoryBoard.instanciateVC()
        }
        
        // MARK: 4.) Create Coaching
        
        fileprivate static let createCoachingStoryboard = UIStoryboard(name: Constants.Storyboards.createCoaching)

        static var createCoachingNavCtrl : CreateCoachingVC {
            return createCoachingStoryboard.instanciateVC()
        }

        static var chooseCoachingMode: ChooseCoachingModeVC {
            return createCoachingStoryboard.instanciateVC()
        }
        
        static var coachingDetails: CoachingDetailsVC {
            return createCoachingStoryboard.instanciateVC()
        }
        
        static var coachingPrice: CoachingPriceVC {
            return createCoachingStoryboard.instanciateVC()
        }
        
        static var coachingDescription: CoachingDescriptionVC {
            return createCoachingStoryboard.instanciateVC()
        }
        
        static var coachingPlanning: CoachingPlanningVC {
            return createCoachingStoryboard.instanciateVC()
        }

        static var coachingHandoutURL : CoachingHandoutVC {
            return createCoachingStoryboard.instanciateVC()
        }

        static var coachingAffiliate : CoachingAffiliateVC {
            return createCoachingStoryboard.instanciateVC()
        }
        
        // MARK: Profile
        
        fileprivate static let profileStoryboard = UIStoryboard(name: Constants.Storyboards.profile)
        
        static var profile: ProfileVC {
            return profileStoryboard.instanciateVC()
        }
        
        static var profileNotification: ProfileNotificationVC {
            return profileStoryboard.instanciateVC()
        }

        static var ProfileDescriptionVC : ProfileDescriptionVC {
            return profileStoryboard.instanciateVC()
        }
        
        // MARK: 5.) Main TabBar
        static var newProfilePassword: NewProfilePasswordVC {
            return profileStoryboard.instanciateVC()
        }
        
        static var mainTabBar: MainTabBarController {
            return coachingsStoryBoard.instanciateVC()
        }
        
        static var verifyCoach: VerifyCoachVC {
            return profileStoryboard.instanciateVC()
        }
        
    }
}
