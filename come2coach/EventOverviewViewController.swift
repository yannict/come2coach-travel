//
//  EventOverviewViewController.swift
//  come2coach
//
//  Created by Timotheus Laubengaier on 24.04.17.
//  Copyright © 2017 MaytecNet. All rights reserved.
//

// Scrolling Fix
// http://stackoverflow.com/questions/13228600/uicollectionview-align-logic-missing-in-horizontal-paging-scrollview/27242179#27242179

import Foundation
import UIKit
import SnapKit
import SVProgressHUD

class EventOverviewViewController: UIViewController {
  
  var live: Bool = true
  
  let scrollView: UIScrollView = {
    let scrollView = UIScrollView()
    scrollView.contentSize = UIScreen.main.bounds.size
    scrollView.showsVerticalScrollIndicator = true
    scrollView.backgroundColor = .clear
    scrollView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
    scrollView.backgroundColor = Styleguide.Color.tint
    return scrollView
  }()
  
  let contentView: UIView = {
    let view = UIView()
    view.backgroundColor = .clear
    return view
  }()
  
  let headerView: EventOverviewHeaderView = {
    let view = EventOverviewHeaderView()
    view.titleLabel.text = "Scoutings".uppercased()
    view.options = ["Videostream", "Livestream", "Map"]
    return view
  }()
  
  let HeaderViewCellIdentifier = "HeaderCellView"
  
  var upcomingEvents: [Event] = []
  var upcomingPage = 0
  let UpcomingEventsCellIdentifier = "CoachingEventsUpcomingCell"
  let upcomingCollectionView: UICollectionView = {
    let flowLayout = UICollectionViewFlowLayout()
    flowLayout.scrollDirection = .horizontal
    flowLayout.itemSize = CGSize(width: 310, height: 262)
    flowLayout.minimumLineSpacing = 20
    flowLayout.minimumInteritemSpacing = 10
    flowLayout.sectionInset = UIEdgeInsetsMake(50, 15, 10, 15)
    let collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: flowLayout)
    // collectionView.isPagingEnabled = true
    // collectionView.contentInset = UIEdgeInsetsMake(10, 50, 10, 50)
    collectionView.backgroundColor = .clear
    collectionView.showsHorizontalScrollIndicator = false
    return collectionView
  }()
  
  var allEvents: [Event] = []
  var allPage = 0
  let AllEventsCellIdentifier = "AllEventsUpcomingCell"
  var allEventsCollectionView: UICollectionView = {
    let flowLayout = UICollectionViewFlowLayout()
    flowLayout.scrollDirection = .vertical
    
    // # sollte proportional skalieren
    let ratio = 165.0 / 185.0
    let itemWidth = UIScreen.main.bounds.width / 2.0 - 30
    
    flowLayout.itemSize = CGSize(width: itemWidth, height: 185)
    flowLayout.minimumLineSpacing = 15
    flowLayout.minimumInteritemSpacing = 15
    flowLayout.sectionInset = UIEdgeInsets(top: 60, left: 15, bottom: 15, right: 15)
    
    let collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: flowLayout)
    collectionView.backgroundColor = .white
    collectionView.isScrollEnabled = false
    //collectionView.contentInset = UIEdgeInsetsMake(10, 10, 10, 10)
    return collectionView
  }()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    view.backgroundColor = Styleguide.Color.tint
    navigationController?.setNavigationBarHidden(true, animated: false)
    
    view.addSubview(scrollView)
    scrollView.snp.makeConstraints { (make) in
      make.top.equalTo(view).offset(100)
      make.leading.equalTo(view)
      make.trailing.equalTo(view)
      make.bottom.equalTo(view)
    }
    
    scrollView.addSubview(contentView)
    contentView.snp.makeConstraints { (make) in
      make.top.equalTo(scrollView)
      make.leading.equalTo(scrollView)
      make.trailing.equalTo(scrollView)
      make.bottom.equalTo(scrollView)
      make.width.equalTo(scrollView.snp.width).offset(0) // subtract padding here
    }
    
    headerView.segmentedView.selectedSegmentIndex = 1
    headerView.backgroundColor = Styleguide.Color.tint
    headerView.delegate = self
    view.addSubview(headerView)
    headerView.snp.makeConstraints { (make) in
      make.top.equalTo(view).offset(40)
      make.leading.equalTo(view)
      make.trailing.equalTo(view)
      make.height.equalTo(80)
    }
    
    // #1 Upcoming Events Collection
    upcomingCollectionView.register(EventCell.self, forCellWithReuseIdentifier: UpcomingEventsCellIdentifier)
    upcomingCollectionView.dataSource = self
    upcomingCollectionView.delegate = self
    contentView.addSubview(upcomingCollectionView)
    upcomingCollectionView.snp.makeConstraints { (make) in
      make.top.equalTo(contentView).offset(20)
      make.leading.equalTo(contentView)
      make.trailing.equalTo(contentView)
      make.height.equalTo(302)
    }
    
    // dirty but quick
    let upcomingLabel = UILabel()
    upcomingLabel.text = "In Kürze"
    upcomingLabel.font = UIFont.systemFont(ofSize: 20, weight: UIFontWeightLight)
    upcomingLabel.textColor = Styleguide.Color.text
    upcomingLabel.textAlignment = .center
    upcomingCollectionView.addSubview(upcomingLabel)
    upcomingLabel.snp.makeConstraints { (make) in
      make.top.equalTo(upcomingCollectionView.snp.top)
      make.centerX.equalTo(upcomingCollectionView)
    }
    
    // #2 All Events Collection
    allEventsCollectionView.register(EventCell.self, forCellWithReuseIdentifier: AllEventsCellIdentifier)
    allEventsCollectionView.dataSource = self
    allEventsCollectionView.delegate = self
    contentView.addSubview(allEventsCollectionView)
    allEventsCollectionView.snp.makeConstraints { (make) in
      make.top.equalTo(upcomingCollectionView.snp.bottom).offset(20)
      make.leading.equalTo(contentView)
      make.trailing.equalTo(contentView)
      make.bottom.equalTo(contentView)
      make.height.equalTo(460)
    }
    
    // dirty but quick should be implemtend in header view
    let allEventsLabel = UILabel()
    allEventsLabel.text = "Weitere Coachings"
    allEventsLabel.font = UIFont.systemFont(ofSize: 20, weight: UIFontWeightLight)
    allEventsLabel.textColor = Styleguide.Color.text
    allEventsCollectionView.addSubview(allEventsLabel)
    allEventsLabel.snp.makeConstraints { (make) in
      make.top.equalTo(allEventsCollectionView.snp.top).offset(15)
      make.leading.equalTo(allEventsCollectionView).offset(20)
      make.trailing.equalTo(allEventsCollectionView).offset(-20)
    }
    
    view.layoutIfNeeded()
    
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    
    if headerView.segmentedView.selectedSegmentIndex == 0 {
      headerView.segmentedView.selectedSegmentIndex = 1
    }
    
    getAllTodayEvents()
    getAllUpcomingEvents()
  }
  
  func getAllTodayEvents() {
    
    let testEvent1 = Event()
    testEvent1.id = 100
    testEvent1.title = "Hidden Beach in Okinawa"
    testEvent1.tmpImageUrl = "https://s-media-cache-ak0.pinimg.com/originals/10/de/6c/10de6cda4a9ab7a573292111d129092e.jpg"
    testEvent1.tmpUserImageUrl = "https://cdn.pastemagazine.com/www/articles/blmain1.jpg"
    
    let testEvent2 = Event()
    testEvent2.id = 101
    testEvent2.title = "Secret Corner in Cuba"
    testEvent2.tmpImageUrl = "http://static2.businessinsider.com/image/549c2996ecad04ea450ad1af-1190-625/21-photos-that-will-make-you-want-to-visit-cuba.jpg"
    testEvent2.tmpUserImageUrl = "http://ldcss.s3-website-us-east-1.amazonaws.com/static/landing/images/img/iStock_000019778147_Large.jpg"
    
    let testUser2 = User()
    testUser2.name = "Judith Nehmet"
    testUser2.imageStringUrl = "http://ldcss.s3-website-us-east-1.amazonaws.com/static/landing/images/img/iStock_000019778147_Large.jpg"
    testEvent2.tmpUser = testUser2
    
    let testEvent3 = Event()
    testEvent3.id = 102
    testEvent3.title = "Dubai Beach"
    testEvent3.tmpImageUrl = "http://cdn2.timeoutdubai.com/thumb/gallery-article/outlets/20795/2016_1_nikkibeach_base.jpg"
    testEvent3.tmpUserImageUrl = "https://cdn.pastemagazine.com/www/articles/blmain1.jpg"
    
    let testUser3 = User()
    testUser3.name = "Ben Stiller"
    testUser3.imageStringUrl = "https://cdn.pastemagazine.com/www/articles/blmain1.jpg"
    testEvent1.tmpUser = testUser3
    testEvent3.tmpUser = testUser3
    
    allEvents = [testEvent1, testEvent2, testEvent3]
    self.allEventsCollectionView.reloadData()
    
//    HTTPRequest.fetchTodayEvents(0, perPage: (allPage + 1) * HTTPRequest.coachingsPerPage, live : live ? 1 : 0) { (events, page, count) in
//      self.allEvents = events
//      self.allEventsCollectionView.reloadData()
//    }
    
  }
  
  func getAllUpcomingEvents() {
    
    let testEvent1 = Event()
    testEvent1.id = 103
    testEvent1.title = "Tokyo"
    testEvent1.tmpImageUrl = "https://cache-graphicslib.viator.com/graphicslib/thumbs674x446/2142/SITours/tokyo-tower-tea-ceremony-and-sumida-river-cruise-day-tour-in-tokyo-115671.jpg"
    testEvent1.tmpUserImageUrl = "http://cdn.neonsky.com/4db89e34d3312/images/Portraits_14-1.jpg"
    
    let testUser = User()
    testUser.name = "Cho Wang"
    testUser.imageStringUrl = "http://cdn.neonsky.com/4db89e34d3312/images/Portraits_14-1.jpg"
    testEvent1.tmpUser = testUser
    
    let testEvent2 = Event()
    testEvent2.id = 104
    testEvent2.title = "Singapore Explorer"
    testEvent2.tmpImageUrl = "https://www.singaporeair.com/saar5/images/navigation/plan-travel/packages/singapore-stopover-holiday.jpg"
    testEvent2.tmpUserImageUrl = "http://www.businesstimes.com.sg/sites/default/files/styles/large_popup/public/image/2017/01/05/13.jpg?itok=tRhVpXMf"
    
    let testUser2 = User()
    testUser2.name = "Christian Bauer"
    testUser2.imageStringUrl = "http://www.businesstimes.com.sg/sites/default/files/styles/large_popup/public/image/2017/01/05/13.jpg?itok=tRhVpXMf"
    testEvent2.tmpUser = testUser2
    
    let testEvent3 = Event()
    testEvent3.id = 105
    testEvent3.title = "Costa Rica"
    testEvent3.tmpImageUrl = "http://images.kuoni.co.uk/73/dubai-33488291-1476718880-ImageGalleryLightboxLarge.jpg"
    testEvent3.tmpUserImageUrl = "https://cdn.pastemagazine.com/www/articles/blmain1.jpg"
    
    let testUser3 = User()
    testUser3.name = "Ben Stiller"
    testUser3.imageStringUrl = "https://cdn.pastemagazine.com/www/articles/blmain1.jpg"
    testEvent3.tmpUser = testUser3
    
    upcomingEvents = [testEvent1, testEvent2, testEvent3]
    self.upcomingCollectionView.reloadData()
    
//    HTTPRequest.fetchUpcomingEvents(0, perPage: (upcomingPage + 1) * HTTPRequest.coachingsPerPage, live: live ? 1 : 0) { (events, page, count) in
//      self.upcomingEvents = events
//      self.upcomingCollectionView.reloadData()
//      // count * 185.0
//    }
    
  }
  

  
}

extension EventOverviewViewController: UICollectionViewDataSource {
  
  func numberOfSections(in collectionView: UICollectionView) -> Int {
    return 1
  }
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    if collectionView == upcomingCollectionView {
      return upcomingEvents.count > 0 ? upcomingEvents.count : 3
    } else if collectionView == allEventsCollectionView {
      return allEvents.count > 0 ? allEvents.count : 3
    }
    return 0
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    
    if collectionView == upcomingCollectionView {
      let cell = collectionView.dequeueReusableCell(withReuseIdentifier: UpcomingEventsCellIdentifier, for: indexPath) as! EventCell
      
      // set properties
      if indexPath.row < upcomingEvents.count {
        let event = upcomingEvents[indexPath.row]
        cell.event = event
      }
      
      cell.delegate = self
      
      return cell
    } else {
      let cell = collectionView.dequeueReusableCell(withReuseIdentifier: AllEventsCellIdentifier, for: indexPath) as! EventCell
      
      cell.isSmallCard = true
      
      
      if indexPath.row < allEvents.count {
        let event = allEvents[indexPath.row]
        cell.event = event
      }
      
      cell.delegate = self
      
      cell.layer.borderWidth = 1
      cell.layer.borderColor = Styleguide.Color.tint.cgColor
//      cell.layer.shadowColor = Styleguide.Color.tint.cgColor
//      cell.layer.shadowRadius = 6
//      cell.layer.shadowOffset = CGSize(width: 3, height: 6)
      return cell
    }
    
  }
  
  
  
//  func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
//    
//    var reusableview: UICollectionReusableView!
//    
//    if (kind == UICollectionElementKindSectionHeader) {
//      reusableview = collectionView.dequeueReusableCell(withReuseIdentifier: HeaderViewCellIdentifier, for: indexPath)
//    }
//    
//    return reusableview;
//  }
  
}

extension EventOverviewViewController: UICollectionViewDelegate {
  
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    if collectionView == upcomingCollectionView {
      if indexPath.row < upcomingEvents.count {
        let event = upcomingEvents[indexPath.row]
        let vc = TravelMapDetailViewController()
        let nvc = UINavigationController(vc)
        vc.event = event
        present(nvc, animated: true, completion: nil)
      }
    } else if collectionView == allEventsCollectionView {
      if indexPath.row < allEvents.count {
        let event = allEvents[indexPath.row]
        let vc = TravelMapDetailViewController()
        let nvc = UINavigationController(vc)
        vc.event = event
        present(nvc, animated: true, completion: nil)
      }
    }
  }
  
}

extension EventOverviewViewController: EventOverviewHeaderViewDelegate {
  
  func segmentViewPressed() {
    if headerView.segmentedView.selectedSegmentIndex == 0 {
      
      let vc = TravelMapViewController()
      navigationController?.push(vc, animated: false, completion: nil)
      
    } else if headerView.segmentedView.selectedSegmentIndex == 1 {
      // livestream
    } else if headerView.segmentedView.selectedSegmentIndex == 2 {
      // videostream
    }
  }
  
}

extension EventOverviewViewController: EventCellDelegate {
  
  func likeButtonPressed(cell: EventCell) {
    
    print("liked")
    if let indexPath = upcomingCollectionView.indexPath(for: cell) {
      let event = upcomingEvents[indexPath.row]
      
      HTTPRequest.likeEvent(with: event.id, handler: { (success) in
        print("like sucessful")
      })
      
      if event.isLiked {
        
      }
      
    }
    
    
  }
  
}
