//
//  Array+Extensions.swift
//  Interstellar
//
//  Created by Marc Orlieb on 31.05.16.
//  Copyright © 2016 Marc Orlieb. All rights reserved.
//

import Foundation

/// Shorthand: append newElement to existing Array, mutating array
/// var box = [1,2]
/// box += 3 // box == [1,2,3]

func += <T>(left: inout Array<T>, right: T) {
    left.append(right)
}

/// Shorthand: sum Array & single Element
/// let box = [1,2] + 3 // box == [1,2,3]

func + <T>(left: Array<T>, right: T) -> Array<T> {
    var a: Array<T> = []
    a += left
    a.append(right)
    return a
}


extension Array {
    
    // See Swiftz: https://github.com/typelift/Swiftz/blob/master/Swiftz/ArrayExt.swift#L214
    /// Safely indexes into an array by converting out of bounds errors to nils.
    
    public func safeIndex(_ i : Int) -> Element? {
        if i < self.count && i >= 0 {
            return self[i]
        } else {
            return nil
        }
    }
    
        
    var isContaining: Bool {
        return !isEmpty
    }
}


