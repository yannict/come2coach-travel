//
// Created by Bernd Freier on 15.02.17.
// Copyright (c) 2017 MaytecNet. All rights reserved.
//

import Foundation

class CoachingAffiliateVC : UIViewController {
    
    @IBOutlet weak var URLtext: PaddedTextField!
    
    @IBOutlet weak var URLdescr: UITextView!
    @IBOutlet weak var bottomConstrain: NSLayoutConstraint!

    override func viewDidLoad() {

        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardNotification(notification:)), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)

        URLtext.returnKeyType = UIReturnKeyType.default
        URLtext.text = CreateManager.sharedInstance.affiliateURL
        URLdescr.text = CreateManager.sharedInstance.affiliateDescr

    }

    func saveAndClose() {
        if let t = URLtext.text {
            CreateManager.sharedInstance.affiliateURL = t
        }
        if let t2 = URLdescr.text {
            CreateManager.sharedInstance.affiliateDescr = t2
        }

        self.navigationController?.popViewController(animated: false, completion: { [weak self] _ in
        })
    }


    @IBAction func saveButtonPressed(_ sender: Any) {

        saveAndClose()
    }
    
    @IBAction func backButtonPressed(_ sender: Any) {

        saveAndClose()
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    func keyboardNotification(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            let endFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            let duration: TimeInterval = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurveRawNSN = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIViewAnimationOptions.curveEaseInOut.rawValue
            let animationCurve: UIViewAnimationOptions = UIViewAnimationOptions(rawValue: animationCurveRaw)
            if (endFrame?.origin.y)! >= UIScreen.main.bounds.size.height {
                bottomConstrain?.constant = 0.0
            } else {
                bottomConstrain?.constant = endFrame?.size.height ?? 0.0
            }
            UIView.animate(withDuration: duration,
                    delay: TimeInterval(0),
                    options: animationCurve,
                    animations: { self.view.layoutIfNeeded() },
                    completion: nil)
        }
    }


}
