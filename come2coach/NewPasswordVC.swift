//
//  NewPasswordVC.swift
//  come2coach
//
//  Created by Alex Kupchak on 11/7/16.
//  Copyright © 2016 MaytecNet. All rights reserved.
//

import Foundation
import SVProgressHUD

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func <<T:Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l < r
    case (nil, _?):
        return true
    default:
        return false
    }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func ><T:Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l > r
    default:
        return rhs < lhs
    }
}


class NewPasswordVC: UIViewController {

    @IBOutlet weak var passwordTextField: PaddedTextField!
    @IBOutlet weak var repeatPasswordTextField: PaddedTextField!
    @IBOutlet weak var backBlackView: UIView!

    @IBOutlet weak var bottomNewPasswordConstraint: NSLayoutConstraint!

    var token: String?

    override func viewDidLoad() {
        setupKeyboardObservers()

        let backgroundTap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        backBlackView += backgroundTap
    }

    // MARK: Actions

    @IBAction func clickConfirm(_ sender: AnyObject) {
        if passwordTextField.text?.characters.count > 0 && repeatPasswordTextField.text?.characters.count > 0 && passwordTextField.text == repeatPasswordTextField.text {

            if let t = User.token {
                if let p = passwordTextField.text {
                    SVProgressHUD.show()
                    LoginController.newPassword(t, p) { (success) in
                        SVProgressHUD.dismiss()

                        if (success) {
                            self.dismiss(animated: true, completion: {
                                //[weak self] _ in
                                UIApplication.shared.delegate?.window!!.rootViewController = UINavigationController(Storyboards.ViewControllers.login)
                            })
                        } else {

                            User.checkConnection(self)

                        }

                    }


                }
            }
        } else {
            self.present(UIAlertController("Passwörter stimmen nicht überein".localized, "Bitte überprüfe deine Eingabe. Die Passwörter sind nicht identisch.".localized,
                    actions: UIAlertAction(.OK)), animated: true)
        }
    }

    // MARK: NSNotificationCenter

    func keyboardWillShow(_ notification: Notification) {
        guard let info = notification.userInfo else {
            return
        }

        let keyboardFrame: CGRect = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let slideDuration = info[UIKeyboardAnimationDurationUserInfoKey] as! Double
        let curve = info[UIKeyboardAnimationCurveUserInfoKey] as! UInt
        let options = UIViewAnimationOptions(rawValue: curve)

        bottomNewPasswordConstraint.constant = keyboardFrame.height + 4
        view.layoutIfNeeded()

        UIView
                .animate(withDuration: slideDuration, delay: 0, options: options, animations: { [weak self] in
            self?.view.layoutIfNeeded()
        }, completion: nil)
    }

    func keyboardWillHide(_ notification: Notification) {
        guard let info = notification.userInfo else {
            OnDebugging
                    .printError("notification.userInfo == nil", title: "LoginVC.keyboardWillHide()")
            return
        }
        let slideDuration = info[UIKeyboardAnimationDurationUserInfoKey] as! Double
        let curve = info[UIKeyboardAnimationCurveUserInfoKey] as! UInt
        let options = UIViewAnimationOptions(rawValue: curve)

        bottomNewPasswordConstraint.constant = 4

        UIView
                .animate(withDuration: slideDuration, delay: 0, options: options, animations: { [weak self] in
            self?.view.layoutIfNeeded()
        }, completion: nil)
    }

    // MARK: Others

    func dismissKeyboard() {
        passwordTextField.resignFirstResponder()
        repeatPasswordTextField.resignFirstResponder()
    }

    fileprivate func setupKeyboardObservers() {
        NotificationCenter.default
                .addObserver(self, selector: #selector(keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default
                .addObserver(self, selector: #selector(keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
}
