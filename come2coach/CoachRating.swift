//
// Created by Bernd Freier on 21.02.17.
// Copyright (c) 2017 MaytecNet. All rights reserved.
//

import Foundation
import SVProgressHUD

class CoachRating : UIViewController {

    var event: Event!

    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var eventLabel2: UILabel!
    @IBOutlet weak var eventImage: UIImageView!
    @IBOutlet weak var eventLabel1: UILabel!
    @IBOutlet weak var slider: UISlider!

    let step: Float = 0.5

    @IBAction func sliderValueChanged(_ sender: Any) {

        let roundedValue = round(slider.value / step) * step
        slider.value = roundedValue

        ratingLabel.text = String(format: "%.2f", slider.value) + " | 5 Sterne".localized
    }

    @IBAction func sliderChanged(_ sender: Any) {


    }

    override func viewDidLoad() {
        super.viewDidLoad()

        let path = UIBezierPath(roundedRect:eventImage.bounds,
                byRoundingCorners:[.topRight, .topLeft],
                cornerRadii: CGSize(width: 4, height:  4))

        let maskLayer = CAShapeLayer()

        maskLayer.path = path.cgPath
        eventImage.layer.mask = maskLayer

        ratingLabel.text  = String(event.ownerRate) + " | 5 Sterne".localized

        if event.img1PathExtension != "" {
            eventImage.sd_setImage(with: event.imageURL as URL!)
        }

        eventLabel1.text = event.title
        eventLabel2.attributedText = stateStringForEvent(event)


    }

    @IBAction func btnBackPressed(_ sender: Any) {

        navigationController?.popViewController(animated: true)

    }

    @IBAction func rateEventPressed(_ sender: Any) {

        SVProgressHUD.show()

        HTTPRequest.rateEvent(with: event.id, rate: Int(slider.value)) { (success) in
            SVProgressHUD.dismiss()

            self.navigationController?.popViewController(animated: true)

        }
    }


    fileprivate func stateStringForEvent(_ event: Event) -> NSAttributedString {

        let currentEvent = LiveStreamingHandler.getEventStatus(event)

        if let time = currentEvent.currentDate {

            let timeAttributes: NSDictionary = [NSForegroundColorAttributeName: UIColor.black]
            let timeString = NSMutableAttributedString(string: time.shortTimeString + " Uhr | ".localized, attributes: timeAttributes as? [String: AnyObject])

            //todo: hier auf live wechseln
            let stateAttributes: NSDictionary = [NSForegroundColorAttributeName: Designs.Colors.coachingTypeBlue]
            //let stateString = NSMutableAttributedString(string: event.isWebinar == true ? "Livestream" : "Videostream")
            let stateString = NSAttributedString(string: event.live == 1 ? "Livestream".localized : "Videostream".localized, attributes: stateAttributes as? [String: AnyObject])

            timeString.append(stateString)

            return timeString

        } else {

            let stateAttributes: NSDictionary = [NSForegroundColorAttributeName: Designs.Colors.coachingTypeBlue]
            return NSMutableAttributedString(string: event.live == 1 ? "Livestream".localized : "Videostream".localized)

        }

    }


}
