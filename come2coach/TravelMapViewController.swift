//
//  TravelMapViewController.swift
//  come2coach
//
//  Created by Timotheus Laubengaier on 25.04.17.
//  Copyright © 2017 MaytecNet. All rights reserved.
//

import Foundation
import UIKit

class TravelMapViewController: UIViewController {
  
  var isShowingScout: Bool = false
  
  // FIXME: Replace ImageView with GMaps
  let mapImageView: UIImageView = {
    let view = UIImageView()
    view.contentMode = .scaleAspectFill
    view.image = #imageLiteral(resourceName: "costaricamap")
    return view
  }()
  
  let headerView: EventOverviewHeaderView = {
    let view = EventOverviewHeaderView()
    return view
  }()
  
  let location1: UIButton = {
    let button = UIButton(type: .custom)
    button.setImage(#imageLiteral(resourceName: "locations-40"), for: .normal)
    button.imageView?.contentMode = .scaleAspectFill
    return button
  }()
  
  let location2: UIButton = {
    let button = UIButton(type: .custom)
    button.setImage(#imageLiteral(resourceName: "locations-40"), for: .normal)
    button.imageView?.contentMode = .scaleAspectFill
    return button
  }()
  
  let scoutDetailView: TravelLocationScoutView = {
    let view = TravelLocationScoutView()
    view.backgroundColor = Styleguide.Color.lightGray
    return view
  }()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    view.backgroundColor = .white
    
    view.addSubview(mapImageView)
    mapImageView.snp.makeConstraints { (make) in
      make.edges.equalTo(view)
    }
    
    headerView.titleLabel.text = "SCOUTINGS"
    headerView.segmentedView.selectedSegmentIndex = 0
    headerView.delegate = self
    view.addSubview(headerView)
    headerView.snp.makeConstraints { (make) in
      make.top.equalTo(view).offset(40)
      make.leading.equalTo(view)
      make.trailing.equalTo(view)
      make.height.equalTo(80)
    }
    
    view.addSubview(location1)
    location1.addTarget(self, action: #selector(locationPressed), for: .touchUpInside)
    location1.snp.makeConstraints { (make) in
      make.centerX.equalTo(view).offset(40)
      make.centerY.equalTo(view).offset(20)
      make.width.equalTo(60)
      make.height.equalTo(60)
    }
    
    view.addSubview(location2)
    location2.addTarget(self, action: #selector(locationPressed), for: .touchUpInside)
    location2.snp.makeConstraints { (make) in
      make.centerX.equalTo(view).offset(-50)
      make.centerY.equalTo(view).offset(-40)
      make.width.equalTo(60)
      make.height.equalTo(60)
    }
    
    scoutDetailView.delegate = self
    view.addSubview(scoutDetailView)
    scoutDetailView.snp.makeConstraints { (make) in
      make.bottom.equalTo(view).offset(55)
      make.leading.equalTo(view)
      make.trailing.equalTo(view)
      make.height.equalTo(104)
    }
    
    self.view.layoutIfNeeded()
    
    let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapRecognized))
    self.view.addGestureRecognizer(tapRecognizer)
    
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    self.navigationController?.setNavigationBarHidden(true, animated: true)
  }
  
  func showScout() {
    
    UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.6, initialSpringVelocity: 0.25, options: UIViewAnimationOptions.curveEaseIn, animations: {
      self.scoutDetailView.snp.updateConstraints({ (make) in
        make.bottom.equalTo(self.view).offset(-49)
      })
      self.view.layoutIfNeeded()
    }, completion: nil)
    
    isShowingScout = true
    
  }
  
  func hideScout() {
    
    UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.6, initialSpringVelocity: 0.25, options: UIViewAnimationOptions.curveEaseOut, animations: {
      self.scoutDetailView.snp.updateConstraints({ (make) in
        make.bottom.equalTo(self.view).offset(55)
      })
      self.view.layoutIfNeeded()
    }, completion: nil)
    
    isShowingScout = false
    
  }
  
  func tapRecognized() {
    hideScout()
  }
  
  func locationPressed() {
    showScout()
  }
  
  func showLocation() {
    
    let testEvent2 = Event()
    testEvent2.id = 101
    testEvent2.title = "Secret Corner in Cuba"
    testEvent2.tmpImageUrl = "http://static2.businessinsider.com/image/549c2996ecad04ea450ad1af-1190-625/21-photos-that-will-make-you-want-to-visit-cuba.jpg"
    testEvent2.tmpUserImageUrl = "http://cdn.neonsky.com/4db89e34d3312/images/Portraits_14-1.jpg"
    
    let testUser = User()
    testUser.name = "Cho Wang"
    testUser.imageStringUrl = "http://cdn.neonsky.com/4db89e34d3312/images/Portraits_14-1.jpg"
    testEvent2.tmpUser = testUser
    
    let vc = TravelMapDetailViewController()
    vc.event = testEvent2
    let nvc = UINavigationController(vc)
    present(nvc, animated: true, completion: nil)
    
  }
 
  
}

extension TravelMapViewController: EventOverviewHeaderViewDelegate {
  
  func segmentViewPressed() {
    if headerView.segmentedView.selectedSegmentIndex == 0 {
      
    } else if headerView.segmentedView.selectedSegmentIndex == 1 {
      navigationController?.popViewController(animated: false)
    } else if headerView.segmentedView.selectedSegmentIndex == 2 {
      navigationController?.popViewController(animated: false)
    }
  }
  
}

extension TravelMapViewController: TravelLocationScoutViewDelegate {
  func overlayPressed() {
    showLocation()
  }
}
