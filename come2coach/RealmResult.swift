//
//  Result.swift
//  come2coach
//
//  Created by Marc Ortlieb on 23.08.16.
//  Copyright © 2016 MaytecNet. All rights reserved.
//

import Foundation


enum RealmResult<T> {
    
    case success(T)
    case Error(Error)
    
    
    func onSuccess(_ f: (T)->Void) -> RealmResult<T> {
        switch self {
        case .success(let value): f(value)
        case .Error(_): break
        }
        return self
    }
    func onError(_ f: (_ error: Error)->Void) -> RealmResult<T> {
        switch self {
        case .success(_): break
        case .Error(let error): f(error)
        }
        return self
    }
    
    
    func map<U>(_ f: (T) -> U) -> RealmResult<U> {
        switch self {
        case let .success(v): return .success(f(v))
        case let .Error(error): return .Error(error)
        }
    }
    func fmap<U>(_ f: (T) -> RealmResult<U>) -> RealmResult<U> {
        switch self {
        case let .success(v): return f(v)
        case let .Error(error): return .Error(error)
        }
    }
    
    init(success value: T) {
        self = RealmResult.success(value)
    }
    init(error: Error) {
        self = RealmResult.Error(error)
    }
    var value: T? {
        switch self {
        case let .success(v): return v
        case .Error: return nil
        }
    }
    var error: Error? {
        switch self {
        case .success: return nil
        case .Error(let e): return e
        }
    }
}
