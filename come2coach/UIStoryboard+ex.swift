//
//  UIStoryboard+init.swift
//  come2coach
//
//  Created by Marc Ortlieb on 01.08.16.
//  Copyright © 2016 Marc Ortlieb. All rights reserved.
//

import Foundation


public extension UIStoryboard {
    
    public convenience init(name: String) {
        self.init(name: name, bundle: nil)
    }
    
    // get ViewController from Storyboard with default storyboardID
    
    public func controller<T: UIViewController>() -> T {
        return instantiateViewController(withIdentifier: T.storyboardID) as! T
    }
    
    
    /// Storyboards.main.controller() - struct Storyboards { static let main = UIStoryboard("Main") }
    ///
    
    public func instanciateVC<T: UIViewController>() -> T {
        return instantiateViewController(withIdentifier: T.storyboardID) as! T
    }

    public func instanciateVCByName<T: UIViewController>() -> T {
        return instantiateViewController(withIdentifier: T.name) as! T
    }
}

