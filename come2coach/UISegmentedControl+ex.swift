//
//  UISegmentedControl+ex.swift
//  come2coach
//
//  Created by Marc Ortlieb on 25.08.16.
//  Copyright © 2016 MaytecNet. All rights reserved.
//

import UIKit


//class segmentedControlDemoVC: UIViewController {
//    
//    enum segmentedControlTitles: String {
//        case title1
//        case title2
//        case title3 = "customTitle"
//    }
//}
//
//extension UISegmentedControl {
//
//    func segments(segments: [segmentedControlDemoVC.segmentedControlTitles]) {
//        removeAllSegments()
//        for (index, title) in segments.enumerate() {
//            insertSegmentWithTitle(title.rawValue, atIndex: index, animated: false)
//        }
//    }
//    func segments(segments: [segmentedControlDemoVC.segmentedControlTitles], selected title: segmentedControlDemoVC.segmentedControlTitles) {
//        removeAllSegments()
//        for (index, title) in segments.enumerate() {
//            insertSegmentWithTitle(title.rawValue, atIndex: index, animated: false)
//        }
//        selectSegment(title)
//    }
//
//    /// if there is a segment with this title in segmentedControl returns true else false
//
//    func selectSegment(title: segmentedControlDemoVC.segmentedControlTitles) -> Bool {
//        for index in (0..<self.numberOfSegments) {
//            if let titleForSegment = titleForSegmentAtIndex(index) {
//                if titleForSegment == title.rawValue {
//                    selectedSegmentIndex = index
//                    return true
//                }
//            }
//        }
//        return false
//    }
//}
